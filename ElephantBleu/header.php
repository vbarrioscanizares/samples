<?php 

try{
$template_url  = get_template_directory_uri();



session_start();
//echo  $template_url.'/lib/simple-php-captcha-master/simple-php-captcha.php';
require_once 'lib/simple-php-captcha-master/simple-php-captcha.php';



$_SESSION['captcha'] = simple_php_captcha();
//echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA" />';


//echo $template_url.'/lib/PHPMailer/PHPMailerAutoload.php';
        require_once 'lib/PHPMailer/PHPMailerAutoload.php'; 
//        require_once $template_url.'/lib/botdetect.php'; 
//        $GLOBALS['captcha'] = new Captcha();
        
        /*
        require_once 'lib/funcaptcha/funcaptcha.php'; 
        $GLOBALS['funcaptcha'] = new FUNCAPTCHA();
        $private_key = '172D2E4A-09EA-93EF-0111-1BE1D496FF87';
         * 
         */
        
}catch(Exception $e){
    echo $e->getMessage();
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php elegant_titles(); ?></title>
<?php elegant_description(); ?>
<?php elegant_keywords(); ?>
<?php elegant_canonical(); ?>



    <!-- Bootstrap Core CSS -->
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href="<?php echo get_template_directory_uri(); ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo get_template_directory_uri(); ?>/css/animate.css" rel="stylesheet" />
        <!--<link href="<?php echo get_template_directory_uri(); ?>/css/botdetect.css" rel="stylesheet" />-->
    <!-- Squad theme CSS -->
    <!--<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">-->
    <link href="<?php echo get_template_directory_uri(); ?>/color/default.css" rel="stylesheet">
            
            
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie6style.css" />
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/DD_belatedPNG_0.0.8a-min.js"></script>
	<script type="text/javascript">DD_belatedPNG.fix('img#logo, span.menu_arrow, a#search img, #searchform, .featured-img span.overlay, #featured .video-slide, #featured a.join-today, div#controllers a, a.readmore span, #breadcrumbs-left, #breadcrumbs-right, #breadcrumbs-content, .span.overlay2, span.overlay, a.zoom-icon, a.more-icon');</script>
<![endif]-->
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie7style.css" />
<![endif]-->
<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie8style.css" />
<![endif]-->        

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom" style="background-color: #fdfdfd !important;">
	<!-- Preloader -->
<!--	<div id="preloader">
	  <div id="load">
          </div>
	</div>-->
 
        
    <header>    
    <section id="header" class="header">    
        <div class="container-fluid clearfix">
            
            
            
            
            <div class="row" style='padding: 30px 0 30px 0;'>
                
                <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 col-xs-offset-0">
                    <a href='/'><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" class="img-responsive" alt="Logo"></a>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 " style='padding-top: 15px;' >
                    <h1 style='/*margin-top: 30px;*/'>Bienvenue au <span style='letter-spacing: -5px;'>C</span>PE l'&Eacute;l&eacute;phant Bleu</h1>
                    <p>2715 sherbrooke est, Montr&eacute;al, Qu&eacute;bec, H2K 1G7.</p>
                    <p>T&eacute;l&eacute;phone : (514) 526-6463, T&eacute;l&eacute;copieur : (514) 526-3414</p>
              
                   
                </div>
                 <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12" style='/*padding-top: 20px;*/'> 
                     <a style='font-size: 22px;margin-top: 15px;'  target='_blank' href="http://elephantbleu.ca/WebCPE.html" class="btn btn-primary" role="button">Se connecter</a> 
                     <br/>
                     <a style='font-size: 18px;margin-top: 15px;' type="button" href="/?page_id=87" class="btn btn-default" >Contactez-nous</a>
                 </div>
       
              </div>
            
             <div class="row">
                <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 col-xs-offset-0 ">
                     
                    
                            <nav class="navbar navbar-default"  style="padding:0;margin-bottom: 30px;">
                            <!--<div class="container-fluid">-->
                              <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                  <span class="sr-only">Toggle navigation</span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#"></a>
                              </div>
                              <div id="navbar" class="navbar-collapse collapse" style="padding: 0px;">
                                <ul class="nav navbar-nav" >
                                    <li style='background-color: #FE8080;'><a class='presentation'    onmouseenter="banner_animation('.banner_presentation')" href="/?page_id=29">Pr&eacute;sentation</a></li>
                                      <li  style='background-color: #D2E9FE;' ><a class='educatrices'  onmouseenter="banner_animation('.banner_educatrices')" href="/?page_id=31">Groupes et &Eacute;ducatrices</a></li>
                                      <li  style='background-color: #FEFEB6;' ><a class='sorties'   onmouseenter="banner_animation('.banner_sorties')" href="/?page_id=33">Activit&eacute;s et sorties</a></li>
                                      <li  style='background-color: #DBB7FE;'><a  class='cuisine'  onmouseenter="banner_animation('.banner_cuisine')" href="/?page_id=35">La cuisine</a></li>
                                      <li  style='background-color: #B8EDB8;' ><a class='soutien'  onmouseenter="banner_animation('.banner_soutien')" href="/?page_id=37">Personnel de soutien</a></li>
                                      <li  style='background-color: #FEA6A6;'><a  class='orthophonie'  onmouseenter="banner_animation('.bannerorthophonie')" href="/?page_id=491">Orthophonie</a></li>
                                      <li  style='background-color: #99CCFF;'><a  class='liste' onmouseenter="banner_animation('.banner_liste')" href="/?page_id=40">Liste d'attente</a></li>
                                      <li  style='background-color: #F1AEDD;'><a  class='photos'  onmouseenter="banner_animation('.banner_gallery')" href="/?page_id=42">Galerie de photos</a></li>
<!--                                  <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                      <li><a href="#">Action</a></li>
                                      <li><a href="#">Another action</a></li>
                                      <li><a href="#">Something else here</a></li>
                                      <li class="divider"></li>
                                      <li class="dropdown-header">Nav header</li>
                                      <li><a href="#">Separated link</a></li>
                                      <li><a href="#">One more separated link</a></li>
                                    </ul>
                                  </li>-->
                                </ul>
<!--                                <ul class="nav navbar-nav navbar-right">
                                  <li><a href="../navbar/">Default</a></li>
                                  <li><a href="../navbar-static-top/">Static top</a></li>
                                  <li class="active"><a href="./">Fixed top <span class="sr-only">(current)</span></a></li>
                                </ul>-->
                              </div><!--/.nav-collapse -->
                            <!--</div>-->
                          </nav>
                     </div>
                    
              </div>
            
            
            
            
            
           

         
        </div>
        <!-- /.container -->
   
       </section> 
        <!-- /.section header -->
        
        </header>

 <div class="container-fluid" style='font-size: 16px;font-weight: bold;'>
     <div class=" col-md-10 col-lg-10  col-sm-12  col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-0 col-xs-offset-0"> 
        <?php if ($_POST['from'] && $_POST['captcha_code'] == $_POST['captcha_value']){ echo '<div class="alert alert-success" role="alert">Votre courriel a &eacute;t&eacute; envoy&eacute; avec succ&egrave;s!</div>';} ?>
        <?php if ($_POST['from'] && $_POST['captcha_code'] != $_POST['captcha_value']){ echo '<div class="alert alert-danger" role="alert">Le courriel n\'a pas &eacute;t&eacute; envoy&eacute;! V&eacute;rifiez le captcha s v.p!</div>';} ?>
    </div>
</div>     