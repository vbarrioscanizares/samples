<?php get_header(); ?>
        
        
        
        <section id="home_page" class="home_page" style='top: -30px;position: relative;'>    
        <div class="container-fluid">
            
             <div class="row banner">
                
                <div class="col-md-10 col-md-offset-1 col-lg-9 col-lg-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                    
                   <div class="text-center"> 
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner.jpg" class="img-responsive banner" alt="Home" usemap="#planetmap">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_presentation.jpg" class="img-responsive banner_presentation" alt="Presentation" usemap="#planetmap">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_educatrices.jpg" class="img-responsive banner_educatrices" alt="Educatrices" usemap="#planetmap">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_sorties.jpg" class="img-responsive banner_sorties" alt="Sorties" usemap="#planetmap">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_cuisine.jpg" class="img-responsive banner_cuisine" alt="Cuisine" usemap="#planetmap">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_soutien.jpg" class="img-responsive banner_soutien" alt="Soutien" usemap="#planetmap">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/bannerorthophonie.jpg" class="img-responsive bannerorthophonie" alt="Orthophonie" usemap="#planetmap">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_liste.jpg" class="img-responsive banner_liste" alt="Liste" usemap="#planetmap">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_gallery.jpg" class="img-responsive banner_gallery" alt="Galerie"usemap="#planetmap" >
                        
                        
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_bus.jpg" class="img-responsive banner_bus" alt="Galerie"usemap="#planetmap" >
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_garderie.jpg" class="img-responsive banner_garderie" alt="Galerie"usemap="#planetmap" >
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_garderie_soutien.jpg" class="img-responsive banner_garderie_soutien" alt="Galerie"usemap="#planetmap" >
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_garderie_orthophonie.jpg" class="img-responsive banner_garderie_orthophonie" alt="Galerie"usemap="#planetmap" >
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_cuisine_over.jpg" class="img-responsive banner_cuisine_over" alt="Galerie"usemap="#planetmap" >
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_piscine.jpg" class="img-responsive banner_piscine" alt="Galerie"usemap="#planetmap" >
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_module.jpg" class="img-responsive banner_module" alt="Galerie"usemap="#planetmap" >
                        <img src="<?php echo get_template_directory_uri(); ?>/images/bannerorthophonie_over.jpg" class="img-responsive bannerorthophonie_over" alt="Galerie"usemap="#planetmap" >
                        <img src="<?php echo get_template_directory_uri(); ?>/images/banner_liste_attente_over.jpg" class="img-responsive banner_liste_attente_o" alt="Galerie"usemap="#planetmap" >
                        
                        
                          
                        
                   
                   </div> 
                   
                </div>
                 
                </div>
            
                 <div class="row">
                
                <div class="col-md-10 col-md-offset-1 col-lg-9 col-lg-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                 
                 <div id='design_animations' style='width: 100%;z-index: 2;position: absolute;height: 60;bottom: 45px;'>
                     <a   href="/?page_id=33"><img class='btn btn-link' data-toggle="tooltip" data-placement="top" title="Activit&eacute;s et sorties"  onmouseover="banner_animation('.banner_bus')" onmouseenter="banner_animation('.banner_bus')" style='position: absolute;left: 8%;width: 11%;bottom: 10px;' src="<?php echo get_template_directory_uri(); ?>/images/invisible.png" class=" img-responsive banner_bus_tr" alt="Home" usemap="#planetmap"></a>
                            
                            <a  href="/?page_id=29"><img data-toggle="tooltip" data-placement="top" title="Pr&eacute;sentation" onmouseover="banner_animation('.banner_garderie')" onmouseenter="banner_animation('.banner_garderie')" style='position: absolute;left: 24%;width: 18%;bottom: 10px;' src="<?php echo get_template_directory_uri(); ?>/images/invisible.png" class="btn btn-link  img-responsive banner_garderie_tr" alt="Home" usemap="#planetmap"></a>
                            <a href="/?page_id=37"><img  data-toggle="tooltip" data-placement="top" title="Personnel de soutien"  onmouseover="banner_animation('.banner_garderie_soutien')" onmouseenter="banner_animation('.banner_garderie_soutien')" style='position: absolute;left: 26%;width: 5%;bottom: 10px;' src="<?php echo get_template_directory_uri(); ?>/images/invisible.png" class="btn btn-link img-responsive banner_garderie_tr" alt="Home" usemap="#planetmap"></a>
                            <a href="/?page_id=491"><img data-toggle="tooltip" data-placement="top" title="Orthophonie"  onmouseover="banner_animation('.banner_garderie_orthophonie')" onmouseenter="banner_animation('.banner_garderie_orthophonie')" style='position: absolute;left: 35%;width: 5%;bottom: 10px;' src="<?php echo get_template_directory_uri(); ?>/images/invisible.png" class="btn btn-link img-responsive banner_garderie_tr" alt="Home" usemap="#planetmap"></a>
                            
                            <a  href="/?page_id=35"><img  data-toggle="tooltip" data-placement="top" title="La cuisine" onmouseover="banner_animation('.banner_cuisine_over')" onmouseenter="banner_animation('.banner_cuisine_over')" style='position: absolute;left: 42%;width: 11%;bottom: 10px;' src="<?php echo get_template_directory_uri(); ?>/images/invisible.png" class="btn btn-link  img-responsive banner_cuisine_over_tr" alt="Home" usemap="#planetmap"></a>
                            <a href="/?page_id=33"><img data-toggle="tooltip" data-placement="top" title="Activit&eacute;s et sorties"   onmouseover="banner_animation('.banner_piscine')" onmouseenter="banner_animation('.banner_piscine')" style='position: absolute;left: 54%;width: 13%;bottom: 0px;' src="<?php echo get_template_directory_uri(); ?>/images/invisible.png" class="btn btn-link img-responsive banner_piscine_tr" alt="Home" usemap="#planetmap"></a>
                            <a  href="/?page_id=33"><img data-toggle="tooltip" data-placement="top" title="Activit&eacute;s et sorties" onmouseover="banner_animation('.banner_module')" onmouseenter="banner_animation('.banner_module')" style='position: absolute;left: 67%;width: 16%;bottom: 0px;' src="<?php echo get_template_directory_uri(); ?>/images/invisible.png" class="btn btn-link img-responsive banner_module_tr" alt="Home" usemap="#planetmap"></a>
                            
                            
                            <a  href="/?page_id=40"><img data-toggle="tooltip" data-placement="top" title="Liste d'attente" onmouseover="banner_animation('.banner_liste_attente_o')" onmouseenter="banner_animation('.banner_liste_attente_o')" style='/*background-color:yellow*/;position: absolute;left: 48%;width: 6%;bottom: 0px;' src="<?php echo get_template_directory_uri(); ?>/images/invisible.png" class="btn btn-link img-responsive banner_liste_attente_over" alt="Home" usemap="#planetmap"></a>
                        </div>  
                     
                    </div> 
                   
                </div>   
             

            
         
        </div>
        <!-- /.container -->
   
       </section> 
        <!-- /.section home_page -->
        
        
<?php get_footer(); ?>