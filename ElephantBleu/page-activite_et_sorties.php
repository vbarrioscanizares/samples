<?php get_header(); ?>


<div class="container-fluid">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 
<div class="page_tire  col-md-10 col-lg-10  col-sm-12  col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-0 col-xs-offset-0" style="border-bottom:1px solid #ccc;">    
        
        <h2 class="pull-left"   style="margin: 0px;bottom: 0px;position: absolute;"><?php the_title(); ?></h2>
        <?php if (has_post_thumbnail( ) ): ?>
            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(  ), 'single-post-thumbnail' ); ?>
            <img style="height:100px;" src="<?=$image[0]?>" class="img-responsive pull-right hidden-xs" alt="">
        <?php endif; ?>
               
 </div>   
 <?php endwhile; endif; ?>


<?php $args = array(
	'posts_per_page'   => 100,
	'offset'           => 0,
	'category'         => '',
	'category_name'    => 'page_activities_sorties',
	'orderby'          => 'post_date',
	'order'            => 'ASC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'post',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'post_status'      => 'publish',
	'suppress_filters' => true 
);
$posts_array = get_posts( $args );


?>

   
      
            
             <div class="row clearfix">
                 <div class="col-md-10 col-lg-10  col-sm-12  col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-0 col-xs-offset-0" >
                     <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                     <?php the_content(); ?>
                     <?php endwhile; endif; ?>
                 </div>
             </div> 
            <br/><br/>
            
             <div class="row clearfix">
                 <div class="col-md-1 col-lg-1  col-sm-0  col-xs-0"></div>
               
                       <?
                            $position = 0;
                            foreach($posts_array as $post){
                                $position++;
                                setup_postdata( $post );
                                
                                $box_title          =  get_post_meta( get_the_ID(), 'box_title', true );
                                $photo1            =  get_post_meta( get_the_ID(), 'photo1', true );
                                $photo2            =  get_post_meta( get_the_ID(), 'photo2', true );

                                ?>
                                 <div class="col-md-5 col-md-offset-0 col-lg-5 col-lg-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                                    <div class="thumbnail">
                                        
                                        <div class="caption">
                                          <h4><?=$box_title?></h4>
                                          <p></p>
                                            <p>
                                                
                                                
                                                <?php if(!empty($photo1)){?>
                                                    <button type="button" onclick="update_photo_src('<?=$photo1;?>');return true;" class="btn btn-link" data-toggle="modal" data-target="#modal_image">
                                         
                                                            <img src="<?=$photo1;?>" alt="1" width='275' class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                
                                                 <?php if(!empty($photo2)){?>
                                                    <button type="button" onclick="update_photo_src('<?=$photo2;?>');return true;" class="btn btn-link" data-toggle="modal" data-target="#modal_image">
                                                           
                                                            <img src="<?=$photo2;?>" alt="2" width='275' class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                
                                            
                                               
                                                
                                              
                                          
                                            <!--<p style='text-align:right;'><a href='/?page_id=42'>Plus</a></p>-->
                                        </div>
                                      </div>
                   
                                 </div>    
                                
                                <?
                                if ($position % 2 == 0){
                                    echo '</div><div class="row clearfix"> <div class="col-md-1 col-lg-1  col-sm-0  col-xs-0" ></div>';
                                }
                                //echo '<h2>'.the_title().'</h2>';
                            };

                        ?>
              </div>
        </div>
        <!-- /.container -->
   

                 
                    
<?php get_footer(); ?>
