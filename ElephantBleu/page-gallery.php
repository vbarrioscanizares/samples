<?php get_header(); ?>

<section id="gallery_page" class="gallery_page">    
            
         <div class="container-fluid" >
             

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="page_tire  col-md-10 col-lg-10  col-sm-12  col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-0 col-xs-offset-0" style="border-bottom:1px solid #ccc;">    

                    <h2 class="pull-left"   style="margin: 0px;bottom: 0px;position: absolute;"><?php the_title(); ?></h2>
                    <?php if (has_post_thumbnail( ) ): ?>
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(  ), 'single-post-thumbnail' ); ?>
                        <img style="height:100px;" src="<?=$image[0]?>" class="img-responsive pull-right hidden-xs" alt="">
                    <?php endif; ?>

             </div>   
                 
              <div class="row clearfix" >
                <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 col-xs-offset-0 ">
                        <?php the_content(); ?>
                </div> 
             </div>    
                 
             <?php endwhile; endif; ?>


                
            
            
		
	</div> <!-- end .container -->    
            
            

<?php 

$args = array(
	'type'                     => 'post',
	'child_of'                 => '',
	'parent'                   => 8,
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 1,
	'hierarchical'             => 1,
	'exclude'                  => '',
	'include'                  => '',
	'number'                   => '',
	'taxonomy'                 => 'category',
	'pad_counts'               => false 

); 

?>
<div class="container-fluid" >
<?php $categories = get_categories( $args ); ?> 

<?php 



foreach($categories as $gallery){

  $args = array(
	'posts_per_page'   => 30,
	'offset'           => 0,
	'category'         => '',
	'category_name'    => $gallery->cat_name,
	'orderby'          => 'post_date',
	'order'            => 'ASC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'post',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'post_status'      => 'publish',
	'suppress_filters' => true 
);
$posts_array = get_posts( $args );
?>

         
            
             <div class="row clearfix">
                 <div class="col-md-10 col-lg-10  col-sm-12  col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-1 col-xs-offset-0" >
                     <p></p>
                    <h2> <?= $gallery->description ?> </h2>
                 </div>
             </div> 

            
             <div class="row clearfix col-md-10 col-lg-10  col-sm-12  col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-1 col-xs-offset-0 ">
                 <!--<div class="col-md-1 col-lg-1  col-sm-0  col-xs-0"></div>-->
               
                       <?
                            $position = 0;
                            foreach($posts_array as $post){
                                
                                $position++;
                                setup_postdata( $post );
                                
                      ?>
                                 <div class="col-middle col-md-4 col-md-offset-0 col-lg-3 col-lg-offset-0 col-sm-6 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                                    <div class="thumbnail">
                                        
                                        <?php if (has_post_thumbnail(get_the_ID() ) ): ?>
                                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' ); ?>
                                                <?php if(!empty($image[0])){?>
                                                    <a type="button" onclick="update_photo_src('<?=$image[0]?>');return true;" class="btn btn-link img-responsive " data-toggle="modal" data-target="#modal_image">
                                                            <img  style='/*max-width:250px;width:100%;*/' class='img-responsive normal img-rounded' src="<?=$image[0]?>" alt="<?=the_title();?>">
                                                    </a>
                                                <?php }; ?>
                                        
                                        <?php endif;?>
                                        <div class="caption">
                                             <p style='word-wrap: normal;font-size:1.8em;'><?php the_title(); ?></p>
                                        </div>

                                      </div>
                                 </div>    
                                 <?
                                  if ($position % 5 == 0){
                                    //echo '</div><div class="row clearfix"> <div class="col-md-1 col-lg-1  col-sm-0  col-xs-0"></div>';
                                }
                             };  ?>
              </div>

                 
        <?php }; //END FOREACH GALLERIES ?>      
            
            
            
        </div>
        <!-- /.container -->
   
</section>             
            
<?php get_footer(); ?>
