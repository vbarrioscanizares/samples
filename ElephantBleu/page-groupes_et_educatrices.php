<?php get_header(); ?>




<section id="educatrices_page" class="educatrices_page">    
     <div class="container-fluid" >
             
            
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class=" page_tire col-md-10 col-lg-10  col-sm-12  col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-0 col-xs-offset-0" style="border-bottom:1px solid #ccc;">    

                    <h2 class="pull-left"   style="margin: 0px;bottom: 0px;position: absolute;"><?php the_title(); ?></h2>
                    <?php if (has_post_thumbnail( ) ): ?>
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(  ), 'single-post-thumbnail' ); ?>
                        <img style="height:100px;" src="<?=$image[0]?>" class="img-responsive pull-right hidden-xs" alt="">
                    <?php endif; ?>

             </div>   
                 
              <div class="row clearfix" >
                <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 col-xs-offset-0 ">
                        <?php the_content(); ?>
                </div> 
             </div>    
                 
             <?php endwhile; endif; ?>
    

                
            
            
		
	</div> <!-- end .container -->
    
    
    
        <div class="container-fluid">
            
          
            <div role="tabpanel" class="col-md-10 col-lg-10  col-sm-12  col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-0 col-xs-offset-0" >

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" style='margin-bottom: 40px;'>
              <li role="presentation" class="active"><a href="#elephant" aria-controls="elephant" role="tab" data-toggle="tab">Installation &Eacute;l&eacute;phant Bleu</a></li>
              <li role="presentation"><a href="#elephanteau" aria-controls="elephanteau" role="tab" data-toggle="tab">Installation &Eacute;l&eacute;phanteau</a></li>
              <li role="presentation"><a href="#cpe" aria-controls="cpe" role="tab" data-toggle="tab">Remplacentes permanentes du CPE</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="elephant">
                  
                  

                    <?php $args = array(
                            'posts_per_page'   => 30,
                            'offset'           => 0,
                            'category'         => '',
                            'category_name'    => 'page_groupe_educatrices_a',
                            'orderby'          => 'meta_value_num',
                            'order'            => 'ASC',
                            'include'          => '',
                            'exclude'          => '',
                            'meta_key'         => 'order',
                            'meta_value'       => '',
                            'post_type'        => 'post',
                            'post_mime_type'   => '',
                            'post_parent'      => '',
                            'post_status'      => 'publish',
                            'suppress_filters' => true 
                    );
                    $posts_array = get_posts( $args );


        ?>
            
            
         <div class="row clearfix">
                 <div class="col-md-12 col-lg-12  col-sm-12  col-xs-12 col-md-offset-0 col-lg-offset-0 col-sm-offset-0 col-xs-offset-0" >
                     <h4>Les &Eacute;ducatrices : Installation &Eacute;l&eacute;phant Bleu</h4>
                 </div>
             </div>  
                  
              
         <div class="row clearfix">
                 <!--<div class="col-md-1 col-lg-1  col-sm-0  col-xs-0"></div>-->
               
                       <?
                            $position = 0;
                            foreach($posts_array as $post){
                                $position++;
                                setup_postdata( $post );
                                
                                
                                $id =  get_the_ID();
                                $name               =  get_post_meta( get_the_ID(), 'contact_name', true );
                                $title              =  get_post_meta( get_the_ID(), 'contact_title', true );
                                $email              =  get_post_meta( get_the_ID(), 'contact_email', true );
                                $img_on             =  get_post_meta( get_the_ID(), 'image_default', true );
                                $img_rollover       =  get_post_meta( get_the_ID(), 'image_rollover', true );
                                $text               = get_the_content();
                                $anecdote           =  get_post_meta( get_the_ID(), 'anecdote', true );
                                
                                $reduced_text       = implode(' ', array_slice(explode(' ', $text), 0, 35));
                                $reduced_anecdote   = implode(' ', array_slice(explode(' ', $anecdote), 0, 35));
                                
                                $box_title          =  get_post_meta( get_the_ID(), 'box_title', true );
                                $box_description    =  get_post_meta( get_the_ID(), 'box_description', true );
                                $box_img            =  get_post_meta( get_the_ID(), 'box_img', true );
                                
                                $photo1            =  get_post_meta( get_the_ID(), 'photo1', true );
                                $photo2            =  get_post_meta( get_the_ID(), 'photo2', true );
                                $photo3            =  get_post_meta( get_the_ID(), 'photo3', true );
                                $photo4            =  get_post_meta( get_the_ID(), 'photo4', true );
                                
                                
                                 if (empty($title)){
                                    echo '<div class="col-md-6 col-md-offset-0 col-lg-4 col-lg-offset-0 col-sm-6 col-sm-offset-0 col-xs-12 col-xs-offset-0"></div>';
                                      if (false/*$position % 3 == 0*/){
                                    echo '</div><div class="row clearfix"> ';
                                }
                                    
                                    continue;
                                }
                                ?>
                                 
                 
<div class="modal fade" id="modal_text_<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?=$name?></h4>
      </div>
      <div class="modal-body">
           <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-md-12 col-lg-12  col-sm-12  col-xs-12" >
                       <p class='text'><?=nl2br($text);?></p>
                       <p></p>
                       <p class='strong' style='font-weight: bold;'>Mots d'enfant ou anecdote pr&eacute;f&eacute;r&eacute;:</p>
                       <p class='anecdote'><?=nl2br($anecdote);?></p>
                    </div>
                 </div>
           </div>     
          
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>     
                 
                                  <div class="col-middle col-md-4 col-md-offset-0 col-lg-4 col-lg-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">

                                     <div class="panel panel-default">
                                        <div class="panel-heading">
                                          <p class="panel-title"><strong><?=$box_title;?></strong></p>
                                          <p class="panel-title" style='max-width:70%;'><span><?=$box_description;?></span></p>
                                           <?php if(!empty($box_img)){?>
                                                <img style='position: absolute;right: 15px;top: 0;' class='img-responsive img-thumbnail' src="<?=$box_img?>" alt="<?=$box_title?>">
                                            <?php }; ?>
                                        
                                        </div>
                                         
                                        <div class="panel-body center-block rolloverthumbnail" style='font-size: 12px;overflow: hidden;'>
                                            <img class='img-responsive normal center-block' src="<?=$img_on?>" alt="<?=$title?>">
                                            
                                             <div class='rollovercaption' style='top:14px'>
                                                <img class='img-responsive normal center-block centered center' src="<?=$img_rollover?>" alt="<?=$title?>">
                                            </div>
                                            
                                            <p></p>
                                            <h4 style='margin-bottom: 5px;' class="center-block"><?=$name?></h4>
                                            <h5 class="center-block"><?=$title;?></h5>

                                             <?php
                                               
                                                if ($reduced_text != $text){
                                                      ?>    
                                            <p class="center-block text"><?=  $reduced_text?>
                                                      
                                                       <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_text_<?=$id?>">Lire la suite</button>  
                                             </p>         
                                                      <?php 
                                                }else{
                                                    echo '<p class="center-block text">'.$reduced_text.'</p>';
                                                }
                                            ?>
                                            
                                            
                                            <p></p>
                                            
                                            <p class='strong' style='font-weight: bold;'>Mots d'enfant ou anecdote pr&eacute;f&eacute;r&eacute;:</p>
                                            <?php
                                               
                                                if ($reduced_anecdote != $anecdote){
                                                      ?>    
                                            <p class="center-block anecdote"><?=  $reduced_anecdote?>
                                                      
                                                       <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_text_<?=$id?>">Lire la suite</button>  
                                            </p>          
                                                      <?php 
                                                }else{
                                                    echo '<p class="center-block anecdote">'.$reduced_anecdote.'</p>';
                                                }
                                            ?>
                                            <p></p>
                                             
                                            <?php if(!empty($photo1)){?>
                                            <p class="center-block">Photo du local</p>
                                             <?php }; ?>
                                         
                                                <div class="col-md-6 col-md-offset-0 col-lg-6 col-lg-offset-0 col-sm-6 col-sm-offset-0 col-xs-6 col-xs-offset-0 ">
                                                
                                                <?php if(!empty($photo1)){?>
                                                    <button style="max-width:75%;max-height:75%;" type="button" onclick="update_photo_src('<?=$photo1?>');return true;" class="btn btn-link" data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo1;?>" alt="1" class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                
                                                 </div>   
                                                    
                                                <div class="col-md-6 col-md-offset-0 col-lg-6 col-lg-offset-0 col-sm-6 col-sm-offset-0 col-xs-6 col-xs-offset-0">
                                                <?php if(!empty($photo2)){?>
                                                    <button style="max-width:75%;max-height:75%;" type="button" onclick="update_photo_src('<?=$photo2?>');return true;" class="btn btn-link" data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo2;?>" alt="1"  class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                </div>
                                                <!--
                                                <?php if(!empty($photo3)){?>
                                                    <button type="button" onclick="update_photo_src('<?=$photo3?>');return true;" class="btn  btn-link"  data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo3;?>" alt="1" width='150' class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                
                                                <?php if(!empty($photo4)){?>
                                                    <button type="button" onclick="update_photo_src('<?=$photo4?>');return true;" class="btn  btn-link"  data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo4;?>" alt="1" width='150' class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                -->
                                                
                                               
                                            
                                            
                                            <p></p>
                                            
                                            <p class="center-block">
                                            <?php if(!empty($email)){?>
                                                    <button style='margin-top:20px;' type="button" onclick="keep_email_to('<?=$email?>');return true;" class="btn btn-default" data-toggle="modal" data-target="#modal_email_form">Envoyer un courriel</button>
                                              <?php }; ?>
                                            </p>        
                                                    
                                  
                                            
                                        </div>
                                      </div>
                                     
                   
                                 </div>    
                                
                                <?
                                if ($position % 3 == 0){
                                    echo '</div><div class="row clearfix">';
                                }
                                //echo '<h4>'.the_title().'</h4>';
                            };

                        ?>
              </div>   
            
            
            
            
            
            
            
            
            
            
            
            
            <br/><br/>        
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
              </div>
                
                
                
                
              <div role="tabpanel" class="tab-pane" id="elephanteau">
                  
                  
                  
            
            
            
            

                    <?php $args = array(
                            'posts_per_page'   => 30,
                            'offset'           => 0,
                            'category'         => '',
                            'category_name'    => 'page_groupe_educatrices_b',
                            'orderby'          => 'meta_value_num',
                            'order'            => 'ASC',
                            'include'          => '',
                            'exclude'          => '',
                            'meta_key'         => 'order',
                            'meta_value'       => '',
                            'post_type'        => 'post',
                            'post_mime_type'   => '',
                            'post_parent'      => '',
                            'post_status'      => 'publish',
                            'suppress_filters' => true 
                    );
                    $posts_array = get_posts( $args );


        ?>
            
            
         <div class="row clearfix">
                 <div class="col-md-12 col-lg-12  col-sm-12  col-xs-12 col-md-offset-0 col-lg-offset-0 col-sm-offset-0 col-xs-offset-0" >
                     <h4>Les &Eacute;ducatrices : Installation &Eacute;l&eacute;phanteau</h4>
                 </div>
             </div>    
            
           <div class="row clearfix">
                 <!--<div class="col-md-1 col-lg-1  col-sm-0  col-xs-0"></div>-->
               
                       <?
                            $position = 0;
                            foreach($posts_array as $post){
                                $position++;
                                setup_postdata( $post );
                                
                                
                                $id =  get_the_ID();
                                $name               =  get_post_meta( get_the_ID(), 'contact_name', true );
                                $title              =  get_post_meta( get_the_ID(), 'contact_title', true );
                                $email              =  get_post_meta( get_the_ID(), 'contact_email', true );
                                $img_on             =  get_post_meta( get_the_ID(), 'image_default', true );
                                $img_rollover       =  get_post_meta( get_the_ID(), 'image_rollover', true );
                                $text               = get_the_content();
                                $anecdote           =  get_post_meta( get_the_ID(), 'anecdote', true );
                                
                                $reduced_text       = implode(' ', array_slice(explode(' ', $text), 0, 35));
                                $reduced_anecdote   = implode(' ', array_slice(explode(' ', $anecdote), 0, 35));
                                
                                $box_title          =  get_post_meta( get_the_ID(), 'box_title', true );
                                $box_description    =  get_post_meta( get_the_ID(), 'box_description', true );
                                $box_img            =  get_post_meta( get_the_ID(), 'box_img', true );
                                
                                $photo1            =  get_post_meta( get_the_ID(), 'photo1', true );
                                $photo2            =  get_post_meta( get_the_ID(), 'photo2', true );
                                $photo3            =  get_post_meta( get_the_ID(), 'photo3', true );
                                $photo4            =  get_post_meta( get_the_ID(), 'photo4', true );
                                
                                
                                 if (empty($title)){
                                    echo '<div class="col-md-4 col-md-offset-0 col-lg-4 col-lg-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0"></div>';
                                      if ($position % 3 == 0){
                                    echo '</div><div class="row clearfix"> ';
                                }
                                    
                                    continue;
                                }
                                ?>
                                 
                 
<div class="modal fade" id="modal_text_<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?=$name?></h4>
      </div>
      <div class="modal-body">
           <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-md-12 col-lg-12  col-sm-12  col-xs-12" >
                       <p class='text'><?=nl2br($text);?></p>
                       <p></p>
                       <p class='strong' style='font-weight: bold;'>Mots d'enfant ou anecdote pr&eacute;f&eacute;r&eacute;:</p>
                       <p class='anecdote'><?=nl2br($anecdote);?></p>
                    </div>
                 </div>
           </div>     
          
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>     
                 
                                  <div class="col-middle col-md-4 col-md-offset-0 col-lg-4 col-lg-offset-0 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">

                                     <div class="panel panel-default">
                                        <div class="panel-heading">
                                          <p class="panel-title"><strong><?=$box_title;?></strong></p>
                                          <p class="panel-title" style='max-width:70%;'><?=$box_description;?></p>
                                           <?php if(!empty($box_img)){?>
                                                <img style='position: absolute;right: 15px;top: 0;' class='img-responsive img-thumbnail' src="<?=$box_img?>" alt="<?=$box_title?>">
                                            <?php }; ?>
                                        
                                        </div>
                                         
                                        <div class="panel-body center-block rolloverthumbnail" style='font-size: 12px;overflow: hidden;'>
                                            <img class='img-responsive normal center-block' src="<?=$img_on?>" alt="<?=$title?>">
                                            
                                             <div class='rollovercaption' style='top:14px'>
                                                <img class='img-responsive normal center-block centered center' src="<?=$img_rollover?>" alt="<?=$title?>">
                                            </div>
                                            
                                            <p></p>
                                            <h4 style='margin-bottom: 5px;' class="center-block"><?=$name?></h4>
                                            <h5 class="center-block"><?=$title;?></h5>

                                             <?php
                                               
                                                if ($reduced_text != $text){
                                                      ?>    
                                            <p class="center-block text"><?=  $reduced_text?>
                                                      
                                                       <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_text_<?=$id?>">Lire la suite</button>  
                                             </p>        
                                                      <?php 
                                                }else{
                                                    echo '<p class="center-block text">'.$reduced_text.'</p>';
                                                }
                                            ?>
                                            
                                            
                                            <p></p>
                                            
                                            <p class='strong' style='font-weight: bold;'>Mots d'enfant ou anecdote pr&eacute;f&eacute;r&eacute;:</p>
                                            <?php
                                               
                                                if ($reduced_anecdote != $anecdote){
                                                      ?>    
                                            <p class="center-block anecdote"><?=  $reduced_anecdote?>
                                                      
                                                       <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_text_<?=$id?>">Lire la suite</button>  
                                            </p>                
                                                      <?php 
                                                }else{
                                                    echo '<p class="center-block anecdote">'.$reduced_anecdote.'</p>';
                                                }
                                            ?>
                                            <p></p>
                                             
                                            <?php if(!empty($photo1)){?>
                                            <p class="center-block">Photo du local</p>
                                             <?php }; ?>
                                         
                                                <div class="col-md-6 col-md-offset-0 col-lg-6 col-lg-offset-0 col-sm-6 col-sm-offset-0 col-xs-6 col-xs-offset-0 ">
                                                
                                                <?php if(!empty($photo1)){?>
                                                    <button style="max-width:75%;max-height:75%;" type="button" onclick="update_photo_src('<?=$photo1?>');return true;" class="btn btn-link" data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo1;?>" alt="1" class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                
                                                 </div>   
                                                    
                                                <div class="col-md-6 col-md-offset-0 col-lg-6 col-lg-offset-0 col-sm-6 col-sm-offset-0 col-xs-6 col-xs-offset-0">
                                                <?php if(!empty($photo2)){?>
                                                    <button style="max-width:75%;max-height:75%;" type="button" onclick="update_photo_src('<?=$photo2?>');return true;" class="btn btn-link" data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo2;?>" alt="1"  class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                </div>
                                                <!--
                                                <?php if(!empty($photo3)){?>
                                                    <button type="button" onclick="update_photo_src('<?=$photo3?>');return true;" class="btn  btn-link"  data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo3;?>" alt="1" width='150' class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                
                                                <?php if(!empty($photo4)){?>
                                                    <button type="button" onclick="update_photo_src('<?=$photo4?>');return true;" class="btn  btn-link"  data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo4;?>" alt="1" width='150' class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                -->
                                                
                                               
                                            
                                            
                                            <p></p>
                                            
                                            <p class="center-block">
                                            <?php if(!empty($email)){?>
                                                    <button style='margin-top:20px;' type="button" onclick="keep_email_to('<?=$email?>');return true;" class="btn btn-default" data-toggle="modal" data-target="#modal_email_form">Envoyer un courriel</button>
                                              <?php }; ?>
                                            </p>        
                                                    
                                  
                                            
                                        </div>
                                      </div>
                                     
                   
                                 </div>    
                                
                                <?
                                if ($position % 3 == 0){
                                    echo '</div><div class="row clearfix">';
                                }
                                //echo '<h4>'.the_title().'</h4>';
                            };

                        ?>
              </div>   
            
            
            
            
            
            
            
            
            
            
            <br/><br/>
            
            
            
                  
                  
                  
                  
                  
                  
                  
                  
              </div>
                
                
                
                
                
                
                
                
                
                
              <div role="tabpanel" class="tab-pane" id="cpe">
                  
                  
                  
                  
        
            

                    <?php $args = array(
                            'posts_per_page'   => 30,
                            'offset'           => 0,
                            'category'         => '',
                            'category_name'    => 'page_groupe_educatrices_c',
                            'orderby'          => 'meta_value_num',
                            'order'            => 'ASC',
                            'include'          => '',
                            'exclude'          => '',
                            'meta_key'         => 'order',
                            'meta_value'       => '',
                            'post_type'        => 'post',
                            'post_mime_type'   => '',
                            'post_parent'      => '',
                            'post_status'      => 'publish',
                            'suppress_filters' => true 
                    );
                    $posts_array = get_posts( $args );


        ?>
            
            
         <div class="row clearfix">
                 <div class="col-md-12 col-lg-12  col-sm-12  col-xs-12 col-md-offset-0 col-lg-offset-0 col-sm-offset-0 col-xs-offset-0" >
                     <h4>Les &Eacute;ducatrices :  Remplacentes permanentes du CPE</h4>
                 </div>
             </div>    
            
         <div class="row clearfix">
                 <!--<div class="col-md-1 col-lg-1  col-sm-0  col-xs-0"></div>-->
               
                       <?
                            $position = 0;
                            foreach($posts_array as $post){
                                $position++;
                                setup_postdata( $post );
                                
                                
                                $id =  get_the_ID();
                                $name               =  get_post_meta( get_the_ID(), 'contact_name', true );
                                $title              =  get_post_meta( get_the_ID(), 'contact_title', true );
                                $email              =  get_post_meta( get_the_ID(), 'contact_email', true );
                                $img_on             =  get_post_meta( get_the_ID(), 'image_default', true );
                                $img_rollover       =  get_post_meta( get_the_ID(), 'image_rollover', true );
                                $text               = get_the_content();
                                $anecdote           =  get_post_meta( get_the_ID(), 'anecdote', true );
                                
                                $reduced_text       = implode(' ', array_slice(explode(' ', $text), 0, 35));
                                $reduced_anecdote   = implode(' ', array_slice(explode(' ', $anecdote), 0, 35));
                                
                                $box_title          =  get_post_meta( get_the_ID(), 'box_title', true );
                                $box_description    =  get_post_meta( get_the_ID(), 'box_description', true );
                                $box_img            =  get_post_meta( get_the_ID(), 'box_img', true );
                                
                                $photo1            =  get_post_meta( get_the_ID(), 'photo1', true );
                                $photo2            =  get_post_meta( get_the_ID(), 'photo2', true );
                                $photo3            =  get_post_meta( get_the_ID(), 'photo3', true );
                                $photo4            =  get_post_meta( get_the_ID(), 'photo4', true );
                                
                                
                                 if (empty($title)){
                                    echo '<div class="col-md-6 col-md-offset-0 col-lg-4 col-lg-offset-0 col-sm-6 col-sm-offset-0 col-xs-12 col-xs-offset-0"></div>';
                                      if (false/*$position % 3 == 0*/){
                                    echo '</div><div class="row clearfix"> ';
                                }
                                    
                                    continue;
                                }
                                ?>
                                 
                 
<div class="modal fade" id="modal_text_<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?=$name?></h4>
      </div>
      <div class="modal-body">
           <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-md-12 col-lg-12  col-sm-12  col-xs-12" >
                       <p class='text'><?=nl2br($text);?></p>
                       <p></p>
                       <p class='strong' style='font-weight: bold;'>Mots d'enfant ou anecdote pr&eacute;f&eacute;r&eacute;:</p>
                       <p class='anecdote'><?=nl2br($anecdote);?></p>
                    </div>
                 </div>
           </div>     
          
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>     
                 
                                  <div class="col-middle col-md-6 col-md-offset-0 col-lg-4 col-lg-offset-0 col-sm-6 col-sm-offset-0 col-xs-12 col-xs-offset-0">

                                     <div class="panel panel-default">
                                        <div class="panel-heading">
                                          <p class="panel-title"><strong><?=$box_title;?></strong></p>
                                          <p class="panel-title" style='max-width:70%;'><?=$box_description;?></p>
                                           <?php if(!empty($box_img)){?>
                                                <img style='position: absolute;right: 15px;top: 0;' class='img-responsive img-thumbnail' src="<?=$box_img?>" alt="<?=$box_title?>">
                                            <?php }; ?>
                                        
                                        </div>
                                         
                                        <div class="panel-body center-block rolloverthumbnail" style='font-size: 12px;overflow: hidden;'>
                                            <img class='img-responsive normal center-block' src="<?=$img_on?>" alt="<?=$title?>">
                                            
                                             <div class='rollovercaption' style='top:14px'>
                                                <img class='img-responsive normal center-block centered center' src="<?=$img_rollover?>" alt="<?=$title?>">
                                            </div>
                                            
                                            <p></p>
                                            <h4 style='margin-bottom: 5px;' class="center-block"><?=$name?></h4>
                                            <h5 class="center-block"><?=$title;?></h5>

                                             <?php
                                               
                                                if ($reduced_text != $text){
                                                      ?>    
                                            <p class="center-block text"><?=  $reduced_text?>
                                                      
                                                       <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_text_<?=$id?>">Lire la suite</button>  
                                            </p>          
                                                      <?php 
                                                }else{
                                                    echo '<p class="center-block text">'.$reduced_text.'</p>';
                                                }
                                            ?>
                                            
                                            
                                            <p></p>
                                            
                                            <p class='strong' style='font-weight: bold;'>Mots d'enfant ou anecdote pr&eacute;f&eacute;r&eacute;:</p>
                                            <?php
                                               
                                                if ($reduced_anecdote != $anecdote){
                                                      ?>    
                                            <p class="center-block anecdote"><?=  $reduced_anecdote?>
                                                      
                                                      <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_text_<?=$id?>">Lire la suite</button>  
                                            </p>            
                                                      <?php 
                                                }else{
                                                    echo '<p class="center-block anecdote">'.$reduced_anecdote.'</p>';
                                                }
                                            ?>
                                            <p></p>
                                             
                                            <?php if(!empty($photo1)){?>
                                            <p class="center-block">Photo du local</p>
                                             <?php }; ?>
                                         
                                                <div class="col-md-6 col-md-offset-0 col-lg-6 col-lg-offset-0 col-sm-6 col-sm-offset-0 col-xs-6 col-xs-offset-0 ">
                                                
                                                <?php if(!empty($photo1)){?>
                                                    <button style="max-width:75%;max-height:75%;" type="button" onclick="update_photo_src('<?=$photo1?>');return true;" class="btn btn-link" data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo1;?>" alt="1" class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                
                                                 </div>   
                                                    
                                                <div class="col-md-6 col-md-offset-0 col-lg-6 col-lg-offset-0 col-sm-6 col-sm-offset-0 col-xs-6 col-xs-offset-0">
                                                <?php if(!empty($photo2)){?>
                                                    <button style="max-width:75%;max-height:75%;" type="button" onclick="update_photo_src('<?=$photo2?>');return true;" class="btn btn-link" data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo2;?>" alt="1"  class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                </div>
                                                <!--
                                                <?php if(!empty($photo3)){?>
                                                    <button type="button" onclick="update_photo_src('<?=$photo3?>');return true;" class="btn  btn-link"  data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo3;?>" alt="1" width='150' class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                
                                                <?php if(!empty($photo4)){?>
                                                    <button type="button" onclick="update_photo_src('<?=$photo4?>');return true;" class="btn  btn-link"  data-toggle="modal" data-target="#modal_image">
                                                            <img src="<?=$photo4;?>" alt="1" width='150' class="img-thumbnail img-responsive ">
                                                    </button>
                                                <?php }; ?>
                                                -->
                                                
                                               
                                            
                                            
                                            <p></p>
                                            
                                            <p class="center-block">
                                            <?php if(!empty($email)){?>
                                                    <button style='margin-top:20px;' type="button" onclick="keep_email_to('<?=$email?>');return true;" class="btn btn-default" data-toggle="modal" data-target="#modal_email_form">Envoyer un courriel</button>
                                              <?php }; ?>
                                            </p>        
                                                    
                                  
                                            
                                        </div>
                                      </div>
                                     
                   
                                 </div>    
                                
                                <?
                                if (false/*$position % 3 == 0*/){
                                    echo '</div><div class="row clearfix">';
                                }
                                //echo '<h4>'.the_title().'</h4>';
                            };

                        ?>
              </div>   
            
            
            
            
            
            
            
                  
                  
                  
                  
                  
                  
                  
                  
              </div>
            </div>

          </div> <!-- TABS --> 
            
              
     
            
            
            
            
             
        </div>
        <!-- /.container -->
   
</section> 
                 
                    
<?php get_footer(); ?>
