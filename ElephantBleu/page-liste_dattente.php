<?php get_header(); ?>


    
             
             
             
             <div class="container-fluid" style=''>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="page_tire  col-md-10 col-lg-10  col-sm-12  col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-0 col-xs-offset-0" style="border-bottom:1px solid #ccc;">    

                    <h2 class="pull-left"   style="margin: 0px;bottom: 0px;position: absolute;"><?php the_title(); ?></h2>
                    <?php if (has_post_thumbnail( ) ): ?>
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(  ), 'single-post-thumbnail' ); ?>
                        <img style="height:100px;" src="<?=$image[0]?>" class="img-responsive pull-right hidden-xs" alt="">
                    <?php endif; ?>

             </div>   
                 
              <div class="row clearfix" >
                <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 col-xs-offset-0 ">
                        <?php the_content(); ?>
                </div> 
             </div>    
                 
             <?php endwhile; endif; ?>
                
            
            
		
	</div> <!-- end .container -->



<?php get_footer(); ?>