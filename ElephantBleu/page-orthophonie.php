<?php get_header(); ?>


<?php $args = array(
	'posts_per_page'   => 30,
	'offset'           => 0,
	'category'         => '',
	'category_name'    => 'page_orthoponie',
	'orderby'          => '',
	'order'            => 'ASC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'post',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'post_status'      => 'publish',
	'suppress_filters' => true 
);
$posts_array = get_posts( $args );


?>

<section id="orthophonie_page" class="orthophonie_page">   
    
             
             <div class="container-fluid" style=''>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <div class="page_tire  col-md-10 col-lg-10  col-sm-12  col-xs-12 col-md-offset-1 col-lg-offset-1 col-sm-offset-0 col-xs-offset-0" style="border-bottom:1px solid #ccc;">    

                    <h2 class="pull-left"   style="margin: 0px;bottom: 0px;position: absolute;"><?php the_title(); ?></h2>
                    <?php if (has_post_thumbnail( ) ): ?>
                        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(  ), 'single-post-thumbnail' ); ?>
                        <img style="height:100px;" src="<?=$image[0]?>" class="img-responsive pull-right hidden-xs" alt="">
                    <?php endif; ?>

             </div>   
                 
              <div class="row clearfix" >
                <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0 col-xs-offset-0 ">
                        <?php the_content(); ?>
                </div> 
             </div>    
                 
             <?php endwhile; endif; ?>
            </div>

                
            
            
		
    
    
        <div class="container-fluid">
            
            
             <div class="row clearfix ">
                 <div class="col-md-1 col-lg-1  col-sm-0  col-xs-0"></div>
               
                       <?
                            $position = 0;
                            foreach($posts_array as $post){
                                $position++;
                                setup_postdata( $post );
                                
                                $name           =  get_post_meta( get_the_ID(), 'contact_name', true );
                                $title          =  get_post_meta( get_the_ID(), 'contact_title', true );
                                $email          =  get_post_meta( get_the_ID(), 'contact_email', true );
                                $img_on         =  get_post_meta( get_the_ID(), 'image_default', true );
                                $img_rollover   =  get_post_meta( get_the_ID(), 'image_rollover', true );
                                $text           = get_the_content();
                                
                                
                                $id =  get_the_ID();
                                $reduced_text       = implode(' ', array_slice(explode(' ', $text), 0, 35));
                                
                                
                                ?>
                 
 <div class="modal fade" id="modal_text_<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?=$name?></h4>
      </div>
      <div class="modal-body">
           <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-md-12 col-lg-12  col-sm-12  col-xs-12" >
                       <p class='text'><?=nl2br($text);?></p>
                     </div>
                 </div>
           </div>     
          
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>                        
                 
                 
                 
                 
                                 <div class="col-middle col-md-5 col-md-offset-0 col-lg-4 col-lg-offset-0 col-sm-5 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                                    <div class="thumbnail rolloverthumbnail">
                                        <img class='img-responsive normal' src="<?=$img_on?>" alt="<?=$title?>">
                                        <!--<img class='img-responsive hover' src="<?=$img_rollover?>" alt="<?=$title?>">-->
                                        
                                        <div class='rollovercaption'>
                                            <img class='img-responsive normal center-block centered center' src="<?=$img_rollover?>" alt="<?=$title?>">
                                        </div>
                                        
                                        
                                        <div class="caption">
                                          <h4 style='margin-bottom: 5px;'><?=$name?></h4>
                                          <h5><?=$title;?></h5>
                                          <!--<p><?=$text?></p>-->
                                          
                                          
                                           <?php
                                               
                                                if ($reduced_text != $text){
                                                      ?>    
                                            <p class="center-block text"><?=  $reduced_text?>
                                                      
                                                       <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_text_<?=$id?>">Lire la suite</button>  
                                             </p>         
                                                      <?php 
                                                }else{
                                                    echo '<p class="center-block text">'.$reduced_text.'</p>';
                                                }
                                            ?>
                                          
                                          
                                          <p>
                                              <?php if(!empty($email)){?>
                                                    <button type="button" onclick="keep_email_to('<?=$email?>');return true;" class="btn btn-default" data-toggle="modal" data-target="#modal_email_form">Envoyer un courriel</button>
                                              <?php }; ?>
                                          </p>
                                          
                                        </div>
                                      </div>
                   
                                 </div>    
                                
                                <?
                                if ($position % 3 == 0){
                                    echo '</div><div class="row clearfix"> <div class="col-md-1 col-lg-1  col-sm-0  col-xs-0"></div>';
                                }
                                //echo '<h2>'.the_title().'</h2>';
                            };

                        ?>
              </div>
        </div>
        <!-- /.container -->
   
</section> 
                 
                    
<?php get_footer(); ?>
