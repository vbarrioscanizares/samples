unit Corpus;

interface
uses
  Windows, Forms, SysUtils, ComCtrls, Gauges, 
  {, Token}, Dialogs, Utils;
type
  TCorpus = class
    private
      fEOT : string;
      fEOP : string;
      fSeparators : TDelimiters;
      fHandle : TextFile;
      fTokensFileName : TFileName;
      fTokensFile : TextFile;
      fSortedFileName : TFileName;
      fSortedFile : TextFile;
      fTypesFileName : TFileName;
      fTypesFile : TextFile;
      fFileSize : LongWord;
      fWholeName : TFileName;
      procedure UpdateGauge(Gauge : TGauge; CharCount : Longword);
      function GetName: string;
    public
      constructor Create(OrigFile : TFileName;var DelimFile : TextFile);
      property Handle : TextFile read fHandle;
      property WholeName : TFilename read fWholeName;
      property Name : string read GetName;
      procedure CreateTknFile(var Gauge : TGauge;  out TokenCount : LongWord);
      procedure CreateTypFile(var Gauge : TGauge;  out TypeCount : LongWord);      
      procedure SortIdxFile(Column : byte);
//      function SentencesWithWord(WordArray : TStringArray):TRelPosArray;
//      procedure CreateDB(var Gauge : TGauge; idText : word; out TokenCount : LongWord);
//      procedure ShowIdxFile(RichEdit : TRichEdit);
      destructor Free;
  end;
implementation

uses CorpusMemo, Results, Main;


{ TDocument }

constructor TCorpus.Create(OrigFile : TFileName;var DelimFile : TextFile);
var
  WHandle : THandle;
  Separator : char;
begin
  inherited Create;
  reset(DelimFile);
  readln(DelimFile, fEOT); //Leyendo de fich el delimitador de texto
  readln(DelimFile, fEOP); //Leyendo de fich el delimitador de parrafo
  fSeparators := fSeparators + [' '];
  while not seekeof(Delimfile) do            //Los delimiatadores
  begin                                      //de token
    readln(DelimFile, Separator);            // estan en las
    fSeparators := fSeparators + [Separator]; //otras lineas del fich. de delimitadores
  end;
  fWholeName := OrigFile; //Guarda la ruta completa del fich
  WHandle := CreateFile(PChar(fWholeName), GENERIC_READ, FILE_SHARE_READ, nil,
                       OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  fFileSize := GetFileSize(WHandle, nil); //Para poder saber el tama�o del fich y asi actualizar el Gauge
  AssignFile(fHandle, OrigFile); //Tomando un handle del fich original
  fTokensFileName := Appdir + 'Idx\' + Name + '.tkn';
  fSortedFileName := Appdir + 'Idx\' + Name + '.srt';
  fTypesFileName := Appdir + 'Idx\' + Name + '.typ';
  AssignFile(fTokensFile, fTokensFileName); // Y otro para el fich resultado de tokens
  AssignFile(fSortedFile, fSortedFileName); // Y otro para el fich resultado de tokens ordenado alfabeticamente
  AssignFile(fTypesFile, fTypesFileName); // Y otro para el fich resultado de tipos
end;

destructor TCorpus.Free;
begin
  inherited Free;
end;

function TCorpus.GetName: string;// Funcion auxiliar para obtener solo el nombre del fich
var
  i : byte;
begin
  i := length(fWholeName);
  repeat
    dec(i);
  until fWholeName[i] = '\';
  Result := copy(fWholeName, i+1, length(fWholeName) - i + 1);
end;

procedure TCorpus.CreateTknFile(var Gauge : TGauge; //Funcion para crear el fich de indices
                                out TokenCount : LongWord);
var
  TempToken : TToken;
  j, CharCount : Longword;
  i : Byte;
  Line : string;
  TokenArray : TStringArray;
  ParagraphIndex, SentenceIndex : LongWord;
  DummyStr : string;

begin
  TokenCount := 0;
  CharCount := 0;
  ParagraphIndex := 0;
  SentenceIndex := 1;
  Reset(fHandle);
  Rewrite(fTokensFile);
  while not SeekEof(fHandle) do
    begin
      readln(fHandle, Line);     // Leer una linea del fichero
      Inc(CharCount, length(Line));
      if copy(Line, 1, length(fEOP)) = fEOP // Si me encuentro el caracter de fin de Parrafo
        then
          begin
            delete(Line, 1, length(fEOP));
            inc(ParagraphIndex);
            SentenceIndex := 1;
          end
        else
          inc(SentenceIndex);
      Line := AnsiUpperCase(Line);
      TokenArray := SplitString(Line, fSeparators); //Parsear los tokens de la oracion
      if Length(TokenArray) > 0 then
        for j := 0 to high(TokenArray) do
          begin
            inc(TokenCount);
            TempToken.WordType := TokenArray[j];
            TempToken.AbsPos := TokenCount;
            TempToken.RelPos.Text := 1; // Hay que utilizar fEOT
            TempToken.RelPos.Paragraph := ParagraphIndex;
            TempToken.RelPos.Sentence := SentenceIndex;
            TempToken.RelPos.Pos := j+1;

            DummyStr := TempToken.WordType;
            for i := 1 to 50 - length(TempToken.WordType) do
              DummyStr := DummyStr + ' ';
            write(fTokensFile, DummyStr);

            write(fTokensFile, ' ');

            DummyStr := '';
            for i := 1 to 10 - length(IntToStr(TempToken.AbsPos)) do
              DummyStr := DummyStr + '0';
            DummyStr := DummyStr + IntToStr(TempToken.AbsPos);
            write(fTokensFile, DummyStr);

            write(fTokensFile, ' ');

            DummyStr := '';
            for i := 1 to 4 - length(IntToStr(TempToken.RelPos.Text)) do
              DummyStr := DummyStr + '0';
            DummyStr := DummyStr + IntToStr(TempToken.RelPos.Text);
            write(fTokensFile, DummyStr);

            write(fTokensFile, ' ');

            DummyStr := '';
            for i := 1 to 4 - length(IntToStr(TempToken.RelPos.Paragraph)) do
              DummyStr := DummyStr + '0';
            DummyStr := DummyStr + IntToStr(TempToken.RelPos.Paragraph);
            write(fTokensFile, DummyStr);

            write(fTokensFile, ' ');

            DummyStr := '';
            for i := 1 to 4 - length(IntToStr(TempToken.RelPos.Sentence)) do
              DummyStr := DummyStr + '0';
            DummyStr := DummyStr + IntToStr(TempToken.RelPos.Sentence);
            write(fTokensFile, DummyStr);

            write(fTokensFile, ' ');

            DummyStr := '';
            for i := 1 to 4 - length(IntToStr(TempToken.RelPos.Pos)) do
              DummyStr := DummyStr + '0';
            DummyStr := DummyStr + IntToStr(TempToken.RelPos.Pos);
            writeln(fTokensFile, DummyStr);

            {TempType.WordType := TempToken.WordType;
            TempType.Lexical := True;//?
            Pos := TokenFound(TempType.WordType, fTypesFile, TempType.TokenCount);
            if Pos = -1
              then
                TempType.TokenCount := 1
              else
                inc(TempType.TokenCount);
            Reset(fTypesFile);
            if Pos = -1
              then
                Seek(fTypesFile, FileSize(fTypesFile))
              else
                Seek(fTypesFile, Pos - 1);
            Write(fTypesFile, TempType);
            CloseFile(fTypesFile);}
          end;
      UpdateGauge(Gauge, CharCount);
      Application.ProcessMessages;
    end;
  UpdateGauge(Gauge, fFileSize);
  CloseFile(fHandle);
  CloseFile(fTokensFile);
  //Showmessage('OK');
end;

procedure TCorpus.UpdateGauge(Gauge : TGauge;CharCount: Longword);
begin
  Gauge.Progress := (CharCount*100) div fFileSize + 1;
end;

{procedure TCorpus.ShowIdxFile(RichEdit: TRichEdit);
var
  TempType : TType;
  Line : string;
  i : LongWord;
begin
  reset(fTypesFile);
  RichEdit.Lines.Clear;
  i := 0;
  while not eof(fTypesFile) do
    begin
      read(fTypesFile, TempType);
      Line := TempType.WordType + ' ' + IntToStr(TempType.TokenCount);
      RichEdit.Lines.Add(Line);
      if i mod 10 = 0
        then
          RichEdit.Update;
      Application.ProcessMessages;
      inc(i);
    end;
end;}


{procedure TCorpus.CreateDB(var Gauge : TGauge; idText : word; out TokenCount : LongWord);
var
  NewParagraph : boolean;
  TempToken : TToken;
//  TempSentence : TSentence;
//  TempParagraph : TParagraph;
  j, CharCount : LongWord;
  Line : string;
  TokenArray : TStringArray;
  ParagraphIndex, SentenceIndex : LongWord;
begin
  DBModule.TTexts.Open;
  DBModule.TParagraphs.Open;
  DBModule.TSentences.Open;
  DBModule.TTypes.Open;
  DBModule.TTokens.Open;
  DBModule.TTexts.InsertRecord([idText, Name]);
  TokenCount := 0;
  CharCount := 0;
  reset(fHandle);
  ParagraphIndex := 1;
  SentenceIndex := 1;
  DBModule.TParagraphs.InsertRecord([idText, ParagraphIndex]);
  while not SeekEof(fHandle) do
    begin
      readln(fHandle, Line);     // Leer una linea del fichero
      Inc(CharCount, length(Line));
      UpdateGauge(Gauge, CharCount);
      if Line[length(Line)] = fEOP
        then // Si la oracion es la ultima del parrafo
          begin
            Delete(Line, length(Line), 1);  //Quitar el caracter especial EOP de Line
            NewParagraph := True;
          end
        else
          NewParagraph := False;
      DBModule.TSentences.InsertRecord([idText, ParagraphIndex, SentenceIndex]);
      Line := AnsiUpperCase(Line);
      TokenArray := SplitString(Line, fSeparators); //Parsear los tokens de la oracion
      TokenCount := TokenCount + length(TokenArray);
      if Length(TokenArray) > 0 then
        for j := 0 to high(TokenArray) do
          begin
            TempToken := TToken.Create(TokenArray[j], j+1);
            TempToken.SaveToDB(DBModule.QSelect, DBModule.TTypes, DBModule.TTokens, 1, ParagraphIndex, SentenceIndex);
            TempToken.Destroy;
          end;
      if NewParagraph
        then
          begin
            inc(ParagraphIndex);
            SentenceIndex := 1; // Inicializar SentenceIndex
            DBModule.TParagraphs.InsertRecord([idText, ParagraphIndex]);
          end
        else
          inc(SentenceIndex);
      Application.ProcessMessages;
    end;
  DBModule.TTexts.Close;
  DBModule.TParagraphs.Close;
  DBModule.TSentences.Close;
  DBModule.TTypes.Close;
  DBModule.TTokens.Close;
end;}

{function TCorpus.SentencesWithWord(WordArray : TStringArray): TRelPosArray;
var
  Token : TToken;
begin
  SetLength(Result, 0);
  Reset(fTokensFile);
  while not Eof(fTokensFile) do
    begin
      read(fTokensFile, Token);
      if Token.WordType = WordArray[] then
        begin
          SetLength(Result, Length(Result)+1);
          Result[high(Result)] := Token.RelPos;
        end;
    end;
  ShowMessage('OK');
end;}

procedure TCorpus.SortIdxFile(Column: byte);
var
  MyString : string;
  ShortPath : string;
begin
  MyString := 'sort '+ ExtractShortPathName(fTokensFileName);
  Rewrite(fSortedFile);
  CloseFile(fSortedFile);
  ShortPath := ExtractShortPathName(fSortedFileName);
  ExecConsoleApp(MyString, Shortpath);
{  for i := 0 to high(Results) do
   FResults.RichEdit.Lines.Add(Results[i]);}
end;

procedure TCorpus.CreateTypFile(var Gauge: TGauge;
  out TypeCount: LongWord);
var
  ForWrite : string;
  OneChar : char;
  OldString, NewString : String50;
  TokenCount, i : Longword;
begin
  TokenCount := 0;
  Reset(fSortedFile);
  Rewrite(fTypesFile);
  OldString := '';
  while not Eof(fSortedFile) do
    begin
      NewString := '';
      OneChar := 'z';
      // Se lee una cadena del fich de tokens ordenado
      while (OneChar <> ' ') and (OneChar <> '') do
        begin
          Read(fSortedFile, OneChar);
          if OneChar <> ' '
            then
              NewString := NewString + OneChar
            else
              readln(fSortedFile);
        end;
      if Oldstring = ''          //Si es el primer tipo(token)
        then
          begin
            ForWrite := NewString;
            for i := 0 to 50 - length(NewString) do
              ForWrite := ForWrite + ' ';
            write(fTypesFile, ForWrite);
            write(fTypesFile, ' ');
            write(fTypesFile, '1');
            TokenCount := 1;
            TypeCount := 1;
            OldString := NewString;
          end
        else                          //Si no, si
          if (NewString <> OldString) //Se trata de un nuevo tipo
            then
              begin
                inc(TypeCount);
                write(fTypesFile, ' ');
                ForWrite := '';
                for i := 0 to 10 - Length(IntToStr(TokenCount)) do
                  ForWrite := ForWrite + '0';
                ForWrite := ForWrite + IntToStr(TokenCount);
                writeln(fTypesFile, ForWrite); // escribiendo el tokencount anterior y cambiando de linea
                TokenCount := 1; //reinicializo el tokenCount para el nuevo tipo
                ForWrite := NewString;
                for i := 0 to 50 - length(NewString) do
                  ForWrite := ForWrite + ' ';
                write(fTypesFile, ForWrite);
                write(fTypesFile, ' ');
                write(fTypesFile, 1);// Indica si el tipo es Lexical o gramatical
                OldString := NewString;
              end
            else               //Si no, es otro token del mismo tipo
              inc(TokenCount);
    end;{while not eof}
end;

end.
