unit Document;

interface
uses
  TxtFile, TknFile, TknSrtFile, TypFile, Classes;

const
  StateCount = 8;
type
  TDocument = class
    private
      fTxtFile : TTxtFile;
      fTknFile : TTknFile;
      fTknSrtFile : TTknSrtFile;
      fTypFile : TTypFile;
      fState : TBits;
    public
      constructor Create;
      property TxtFile : TTxtFile read fTxtFile write fTxtFile;
      property TknFile : TTknFile read fTknFile write fTknFile;
      property TknSrtFile : TTknSrtFile read fTknSrtFile write fTknSrtFile;
      property TypFile : TTypFile read fTypFile write fTypFile;
      property State : TBits read fState write fState;
      destructor Free;
    end;
implementation

{ TDocument }

constructor TDocument.Create;
begin
  Inherited Create;
  fState := TBits.Create;
  fState.Size := StateCount;
end;

destructor TDocument.Free;
begin
  fState.Free;
  fTxtFile.Free;
  fTknFile.Free;
  fTknSrtFile.Free;
  fTypFile.Free;
end;

end.
