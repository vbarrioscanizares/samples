unit Normalize;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, Document, Utils, Dialogs,Progress, WinSkinData,
  WinSkinStore,ComCtrls,FileQuery;

type
  TNormalizeDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    GroupBox1: TGroupBox;
    Contractions: TCheckBox;
    Spelling: TCheckBox;
    Abbreviations: TCheckBox;
    Symbols: TCheckBox;
    Lemmatize: TCheckBox;
    Numbers: TCheckBox;
    Degram: TCheckBox;
    SpellingOptions: TRadioGroup;
    WordFamily: TCheckBox;
    GroupBox2: TGroupBox;
    DegramOptions: TRadioGroup;
    MarkWith: TEdit;
    procedure OKBtnClick(Sender: TObject);
    procedure SpellingClick(Sender: TObject);
    procedure DegramClick(Sender: TObject);
    procedure DegramOptionsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NormalizeDlg: TNormalizeDlg;
  OutFileName : string;
implementation
uses Main, CorpusMemo, Results, UProject;
{$R *.dfm}

procedure TNormalizeDlg.OKBtnClick(Sender: TObject);
var
  DelimFileName, BatFileName, ExeFileName, InFileName,
                                              DataFileName, Line : string;
  BatFile, DataFile, DataFile2 : TextFile;
   j : Byte;
  StrArray : TStringArray;
  FirstCB : Boolean;
  TmpFiles : TStringList;
  node : TTreeNode;
  ind,i : Integer;

begin
  InFileName := TDocument(DocList[Ind_DOCUMENTLIST]).TxtFile.NameAndPath;
  CorpusMemo.OutFileName := Copy(InFileName, 1, Length(InFileName) - 4) + '[nrm].txt';
  if FileExists(CorpusMemo.OutFileName)then
   begin
    if MessageDlg('A previous Normalized File for this corpus exists on disk. �Do you want to rebuild it anyway?', mtConfirmation, [mbyes, mbNo], 0) = mrNo
      then Exit;
   end;

  Close;
  setlength(StrArray,0);

  FLog.RichEdit.Lines.Add('');
  FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Starting Tokenization');
  FLog.RichEdit.Refresh;

  Application.CreateForm(TFProgress, FProgress);
  FProgress.Show;
  FProgress.Job.Caption := 'Checking Contractions';
  FProgress.Gauge.MaxValue := 13;
  FProgress.Gauge.Progress := 0;
  FProgress.Refresh;

  TmpFiles := TStringList.Create;
  Screen.Cursor := crHourGlass;
  FirstCB := True;
  ExeFileName := ExtractShortPathName(Appdir +
                                        'Plugins\needs\Repl.exe ');
  DelimFileName := ExtractShortPathName(Appdir +
                                         'Data\Delims\delim02.txt ');
  i := 1;
  BatFileName := ExtractShortPathName(Appdir) + 'Plugins\needs\Replace.bat';
  AssignFile(BatFile, BatFileName);

  if (Contractions.Checked) then
    begin
     FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Starting to rewrite contractions into full forms');
     FLog.RichEdit.Refresh;
      if FirstCB
        then
          begin
            InFileName := ExtractShortPathName(TDocument(
            DocList[Ind_DOCUMENTLIST]).TxtFile.NameAndPath) + ' ';
            FirstCB := False;
          end//FirstCB
        else
          begin
            InFileName := ExtractShortPathName(Appdir + 'Files\Temporal\File' +
                                         IntToStr(i) + '.tmp ');
            TmpFiles.Add(InFileName);
            Inc(i);
          end;
      OutFileName := ExtractShortPathName(Appdir) + 'Files\Temporal\File' +
                                           IntToStr(i) + '.tmp ';
      DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\Contractions.txt ';
      Rewrite(BatFile);
      Write(BatFile, ExeFileName);
      Write(BatFile, InFileName);
      Write(BatFile, OutFileName);
      Write(BatFile, DataFileName);
      Write(BatFile, DelimFileName);
      CloseFile(BatFile);
      ExecConsoleApp(BatFileName);
      FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Contractions rewritten in full');
      FLog.RichEdit.Refresh;
    end;//if (Contractions.Checked) then

   FProgress.Gauge.AddProgress(1);
   FProgress.Job.Caption := 'Checking Abbreviations';
   FProgress.Refresh;

    //////////////////////////////////////////
  if (Abbreviations.Checked) then
    begin
      FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Starting to write abbreviations in full');
      FLog.RichEdit.Refresh;
      if FirstCB
        then
          begin
            InFileName := ExtractShortPathName(TDocument(
            DocList[Ind_DOCUMENTLIST]).TxtFile.NameAndPath) + ' ';
            FirstCB := False;
          end//FirstCB
        else
          begin
            InFileName := ExtractShortPathName(Appdir + 'Files\Temporal\File' +
                                         IntToStr(i) + '.tmp ');
            TmpFiles.Add(InFileName);
            Inc(i);
          end;
      OutFileName := ExtractShortPathName(Appdir) + 'Files\Temporal\File' +
                                           IntToStr(i) + '.tmp ';
      DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\Abbreviations.txt ';
      Rewrite(BatFile);
      Write(BatFile, ExeFileName);
      Write(BatFile, InFileName);
      Write(BatFile, OutFileName);
      Write(BatFile, DataFileName);
      Write(BatFile, DelimFileName);
      CloseFile(BatFile);
      ExecConsoleApp(BatFileName);
      FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Abbreviations represented in full');
      FLog.RichEdit.Refresh;
    end;//if (Contractions.Checked) then

   FProgress.Gauge.AddProgress(1);
   FProgress.Job.Caption := 'Checking Spelling Uniformity';
   FProgress.Refresh;

    //////////////////////////////////////////
  if (Spelling.Checked) then
    begin
     FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Starting to make spelling uniform');
     FLog.RichEdit.Refresh;
      if FirstCB
        then
          begin
            InFileName := ExtractShortPathName(TDocument(
            DocList[Ind_DOCUMENTLIST]).TxtFile.NameAndPath) + ' ';
            FirstCB := False;
          end//FirstCB
        else
          begin
            InFileName := ExtractShortPathName(Appdir + 'Files\Temporal\File' +
                                         IntToStr(i) + '.tmp ');
            TmpFiles.Add(InFileName);
            Inc(i);
          end;
      OutFileName := ExtractShortPathName(Appdir) + 'Files\Temporal\File' +
                                           IntToStr(i) + '.tmp ';
      if SpellingOptions.ItemIndex = 0
        then
          DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\BSpelling.txt'
        else
          DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\ASpelling.txt ';
      Rewrite(BatFile);
      Write(BatFile, ExeFileName);
      Write(BatFile, InFileName);
      Write(BatFile, OutFileName);
      Write(BatFile, DataFileName);
      Write(BatFile, DelimFileName);
      CloseFile(BatFile);
      ExecConsoleApp(BatFileName);
      FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Spelling made uniform');
      FLog.RichEdit.Refresh;
    end;//if (Contractions.Checked) then

   FProgress.Gauge.AddProgress(1);
   FProgress.Job.Caption := 'Checking Symbols';
   FProgress.Refresh;

    //////////////////////////////////////////
  if (Symbols.Checked) then
    begin
     FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Starting to represent symbols as words');
     FLog.RichEdit.Refresh;
      if FirstCB
        then
          begin
            InFileName := ExtractShortPathName(TDocument(
            DocList[Ind_DOCUMENTLIST]).TxtFile.NameAndPath) + ' ';
            FirstCB := False;
          end//FirstCB
        else
          begin
            InFileName := ExtractShortPathName(Appdir + 'Files\Temporal\File' +
                                         IntToStr(i) + '.tmp ');
            TmpFiles.Add(InFileName);
            Inc(i);
          end;
      OutFileName := ExtractShortPathName(Appdir) + 'Files\Temporal\File' +
                                           IntToStr(i) + '.tmp ';
      DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\Symbols.txt ';
      Rewrite(BatFile);
      Write(BatFile, ExeFileName);
      Write(BatFile, InFileName);
      Write(BatFile, OutFileName);
      Write(BatFile, DataFileName);
      Write(BatFile, DelimFileName);
      CloseFile(BatFile);
      ExecConsoleApp(BatFileName);
      FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Symbols represented as words');
      FLog.RichEdit.Refresh;
    end;//if (Contractions.Checked) then

   FProgress.Gauge.AddProgress(1);
   FProgress.Job.Caption := 'Checking Numbers';
   FProgress.Refresh;

    //////////////////////////////////////////
  if (Numbers.Checked) then
    begin
     FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Starting to represent figures as words');
     FLog.RichEdit.Refresh;
      if FirstCB
        then
          begin
            InFileName := ExtractShortPathName(TDocument(
            DocList[Ind_DOCUMENTLIST]).TxtFile.NameAndPath) + ' ';
            FirstCB := False;
          end//FirstCB
        else
          begin
            InFileName := ExtractShortPathName(Appdir + 'Files\Temporal\File' +
                                         IntToStr(i) + '.tmp ');
            TmpFiles.Add(InFileName);
            Inc(i);
          end;
      OutFileName := ExtractShortPathName(Appdir) + 'Files\Temporal\File' +
                                           IntToStr(i) + '.tmp ';
      DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\Numbers.txt ';
      Rewrite(BatFile);
      Write(BatFile, ExeFileName);
      Write(BatFile, InFileName);
      Write(BatFile, OutFileName);
      Write(BatFile, DataFileName);
      Write(BatFile, DelimFileName);
      CloseFile(BatFile);
      ExecConsoleApp(BatFileName);
      FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Figures represented as words');
      FLog.RichEdit.Refresh;
    end;//if (Contractions.Checked) then

   FProgress.Gauge.AddProgress(1);
   FProgress.Job.Caption := 'Checking Lemmatization File';
   FProgress.Refresh;

    //////////////////////////////////////////
  if (Lemmatize.Checked) then
    begin
     FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Starting Lemmatization');
     FLog.RichEdit.Refresh;
      if FirstCB
        then
          begin
            InFileName := ExtractShortPathName(TDocument(
            DocList[Ind_DOCUMENTLIST]).TxtFile.NameAndPath) + ' ';
            FirstCB := False;
          end//FirstCB
        else
          begin
            InFileName := ExtractShortPathName(Appdir + 'Files\Temporal\File' +
                                         IntToStr(i) + '.tmp ');
            TmpFiles.Add(InFileName);
            Inc(i);
          end;
      OutFileName := ExtractShortPathName(Appdir) + 'Files\Temporal\File' +
                                           IntToStr(i) + '.tmp ';
      for j := 1 to 6 do  //Son 6 ficheros de lematizacion
        begin
          FProgress.Gauge.AddProgress(1);
          //FProgress.Job.Caption := 'Checking Word Family';
          FProgress.Refresh;

          DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\Lem00' + IntToStr(j) + '.txt ';
          Rewrite(BatFile);
          Write(BatFile, ExeFileName);
          Write(BatFile, InFileName);
          Write(BatFile, OutFileName);
          Write(BatFile, DataFileName);
          Write(BatFile, DelimFileName);
          CloseFile(BatFile);
          ExecConsoleApp(BatFileName);
          FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Lemmatize Reduced by file #'+inttostr(j));
          FLog.RichEdit.Refresh;
          if j < 6 then
            begin
              Inc(i);
              InFileName := OutFileName;
              TmpFiles.Add(InFileName);
              OutFileName := ExtractShortPathName(Appdir) + 'Files\Temporal\File' +
                                               IntToStr(i) + '.tmp ';
            end;
        end;
      FProgress.Job.Caption := 'Checking Word Families';
      FProgress.Refresh;
    end//if (Contractions.Checked) then
    else
     begin
      FProgress.Gauge.AddProgress(6);
      FProgress.Job.Caption := 'Checking Word Families';
      FProgress.Refresh;
     end;


    //////////////////////////////////////////
  if (WordFamily.Checked) then
    begin
     FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Starting Word Family replacement');
     FLog.RichEdit.Refresh;
      if FirstCB
        then
          begin
            InFileName := ExtractShortPathName(TDocument(
            DocList[Ind_DOCUMENTLIST]).TxtFile.NameAndPath) + ' ';
            FirstCB := False;
          end//FirstCB
        else
          begin
            InFileName := ExtractShortPathName(Appdir + 'Files\Temporal\File' +
                                           IntToStr(i) + '.tmp ');
            TmpFiles.Add(InFileName);
            Inc(i);
          end;
      OutFileName := ExtractShortPathName(Appdir) + 'Files\Temporal\File' +
                                           IntToStr(i) + '.tmp ';
      DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\WordFamily.txt ';
      Rewrite(BatFile);
      Write(BatFile, ExeFileName);
      Write(BatFile, InFileName);
      Write(BatFile, OutFileName);
      Write(BatFile, DataFileName);
      Write(BatFile, DelimFileName);
      CloseFile(BatFile);
      ExecConsoleApp(BatFileName);
      FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'WordFamily Reduced');
      FLog.RichEdit.Refresh;
    end;//if (Contractions.Checked) then

   FProgress.Gauge.AddProgress(1);
   FProgress.Job.Caption := 'Checking Gramatics Words';
   FProgress.Refresh;

    //////////////////////////////////////////
  if (Degram.Checked) then
    begin
     FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Starting Degrammatization');
     FLog.RichEdit.Refresh;
      if FirstCB
        then
          begin
            InFileName := ExtractShortPathName(TDocument(
            DocList[Ind_DOCUMENTLIST]).TxtFile.NameAndPath) + ' ';
          end//FirstCB
        else
          begin
            InFileName := ExtractShortPathName(Appdir + 'Files\Temporal\File' +
                                         IntToStr(i) + '.tmp ');
            TmpFiles.Add(InFileName);
            Inc(i);
          end;
      OutFileName := ExtractShortPathName(Appdir) + 'Files\Temporal\File' +
                                           IntToStr(i) + '.tmp ';
      if DegramOptions.ItemIndex = 0
        then
          DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\DegramDel.txt '
        else
          begin
            DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\DegramDel.txt';
            AssignFile(DataFile, DataFileName);
            DataFileName := ExtractShortPathName(Appdir) + 'Data\Lists\DegramMark.txt';
            AssignFile(DataFile2, DataFileName);
            Reset(DataFile);
            Rewrite(DataFile2);
            while not EOF(DataFile) do
              begin
                Readln(DataFile, Line);
                StrArray := SplitString(Line, ['|']);
                Writeln(DataFile2, StrArray[0] + '|' + MarkWith.Text
                                                     + StrArray[0]);
              end;
            CloseFile(DataFile);
            CloseFile(DataFile2);
            DataFileName := DataFileName + ' ';
          end;
      Rewrite(BatFile);
      Write(BatFile, ExeFileName);
      Write(BatFile, InFileName);
      Write(BatFile, OutFileName);
      Write(BatFile, DataFileName);
      Write(BatFile, DelimFileName);
      CloseFile(BatFile);
      ExecConsoleApp(BatFileName);
      FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Corpus has been degrammatized');
      FLog.RichEdit.Refresh;
    end;//if (Contractions.Checked) then

   FProgress.Gauge.AddProgress(1);
   FProgress.Job.Caption := 'Deleting Temporary Files';
   FProgress.Refresh;
   
  //Si el .nrm existe que lo borre
  DeleteFile(Copy(FCorpusMemo.ActiveDocument.TxtFile.NameAndPath, 1, Length(FCorpusMemo.ActiveDocument.TxtFile.NameAndPath) - 4) + '[nrm.txt]');
  //Para renombrar el ultimo fich de salida a .nrm
  RenameFile(OutFileName,
             Copy(FCorpusMemo.ActiveDocument.TxtFile.NameAndPath, 1, Length(FCorpusMemo.ActiveDocument.TxtFile.NameAndPath) - 4) + '[nrm].txt');
  OutFileName := Copy(FCorpusMemo.ActiveDocument.TxtFile.NameAndPath, 1, Length(FCorpusMemo.ActiveDocument.TxtFile.NameAndPath) - 4) + '[nrm].txt';
  FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Loading Normalized Corpus.');
  FLog.RichEdit.Refresh;
  //FCorpusMemo.FileOpenCorpusExecute(Sender);
  DeleteFile(BatFileName);
  FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Deleting Temporary Files');
  FLog.RichEdit.Refresh;
  if TmpFiles.Count > 0 then//Hay fichs. temporales
    for j := 0 to TmpFiles.Count - 1 do
      DeleteFile(TmpFiles[j]);
  TmpFiles.Free;
  Screen.Cursor := crDefault;
  FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Tokenization performed successfully');
  FLog.RichEdit.Refresh;

  FProgress.Close;
  FProgress.Free;

  if not FileExistsOnProject(CorpusMemo.OutFileName) then
  begin
   //Trabajo con los nodos del Arbol
   node := FProject.TreeView1.Items[ind_NODO];
   node := FProject.TreeView1.Items.AddChild(node,ExtractFileName(CorpusMemo.OutFileName));
   node.ImageIndex := 2;
   node.SelectedIndex := 2;
   ind := node.AbsoluteIndex;

   nodes_data.insert(ind,TNodesData.Create);
   TNodesdata(nodes_data[ind]).data.indNod := ind;
   TNodesdata(nodes_data[ind]).data.indDoc := 0;
   TNodesdata(nodes_data[ind]).data.PathandName := CorpusMemo.OutFileName;
   TNodesdata(nodes_data[ind]).data.IIndex := 2;
   TNodesdata(nodes_data[ind]).data.SIndex := 2;


   for i:=ind+1 to nodes_data.count-1 do
    TNodesData(nodes_data[i]).data.IndNod := i;

  node.Selected := True;
  end;
end;

procedure TNormalizeDlg.SpellingClick(Sender: TObject);
begin
  SpellingOptions.Enabled := Spelling.Checked;
end;

procedure TNormalizeDlg.DegramClick(Sender: TObject);
begin
  DegramOptions.Enabled := Degram.Checked;
end;

procedure TNormalizeDlg.DegramOptionsClick(Sender: TObject);
begin
  MarkWith.Enabled := DegramOptions.ItemIndex = 1;
end;

end.
