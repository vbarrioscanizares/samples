unit Statistics;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, MultiGrid, Menus, ActnList, ImgList, Utils, FileQuery,
  Document, FreqVSPercent, xxx;

type
  TFStatistics = class(TForm)
    ActionList: TActionList;
    SelectTable: TAction;
    TablesSave: TAction;
    SaveDialog: TSaveDialog;
    TablesOpen: TAction;
    OpenDialog: TOpenDialog;
    TablesClear: TAction;
    MultiGrid: TMultiGrid;
    PopupMenuStatistics: TPopupMenu;
    SortinAscendingorder1: TMenuItem;
    SortinDescendingorder1: TMenuItem;
    Maximize1: TMenuItem;
    Minimize1: TMenuItem;
    N1: TMenuItem;
    ArrangeAll1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure MultiGridGetCellColor(Sender: TObject; ARow, Acol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure MultiGridGetCellAlignment(Sender: TObject; ARow,
      ACol: Integer; var HorAlignment: TAlignment;
      var VerAlignment: TVerticalAlignment);
    procedure TablesTextsExecute(Sender: TObject);
    procedure TablesSaveExecute(Sender: TObject);
    procedure TablesOpenExecute(Sender: TObject);
    procedure TablesClearExecute(Sender: TObject);
    procedure SelectTableExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SortinAscendingorder1Click(Sender: TObject);
    procedure SortinDescendingorder1Click(Sender: TObject);
    procedure Maximize1Click(Sender: TObject);
    procedure Minimize1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FStatistics: TFStatistics;
  GridState : TGridState;
implementation

uses Main, ComCtrls, CorpusMemo, Results, SelectTable, Progress, UProject;

var
  Query : TFileQuery;

{$R *.dfm}
procedure TFStatistics.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caMinimize;
end;

procedure TFStatistics.FormCreate(Sender: TObject);
begin
  MultiGrid.Font.Pitch := fpFixed;//Para que las letras tengan un ancho estandar
  GridState := gsUndefined;
end;

procedure TFStatistics.MultiGridGetCellColor(Sender: TObject; ARow,
  Acol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ARow = 0) then
    begin
      ABrush.Color := clBtnFace;
      AFont.Style := [fsBold];
      AFont.Color := clBlack;
    end;
  case GridState of
    gsUndefined : ;
    gsProfile : ;
    gsTexts : ;
    gsKeywords : ;
    gsTypes : ;
  end;//case
end;

procedure TFStatistics.MultiGridGetCellAlignment(Sender: TObject; ARow,
  ACol: Integer; var HorAlignment: TAlignment;
  var VerAlignment: TVerticalAlignment);
begin
  if Multigrid.RowCount > 1 then
    MultiGrid.FixedRows := 1;
  case GridState of
    gsUndefined, gsProfile : HorAlignment := taCenter;
    gsTexts : if ACol = 0 then
                HorAlignment := taCenter;
    gsKeywords : if ACol = 0 then
                HorAlignment := taLeftJustify;
    gsLogLikelihood: if ACol = 0 then
                HorAlignment := taLeftJustify;    
    gsTypes :
      if (ACol = 0)
        then
          HorAlignment := taLeftJustify
        else
          if (ARow = 0) then
            HorAlignment := taCenter;
  end;//case
end;

procedure TFStatistics.TablesTextsExecute(Sender: TObject);
begin
  Query := TFileQuery.Create(TDocument(DocList[Ind_DOCUMENTLIST]), MultiGrid);
  Query.TextsTable;
  Query.Destroy;
end;

procedure TFStatistics.TablesSaveExecute(Sender: TObject);
begin
  if SaveDialog.Execute then
    MultiGrid.SaveToFile(SaveDialog.FileName);
end;

procedure TFStatistics.TablesOpenExecute(Sender: TObject);
{var
  Extension : string[4]; }
begin
  OpenDialog.InitialDir := project_path;
  if OpenDialog.Execute then
    begin
      {Extension := Copy(OpenDialog.FileName, Length(OpenDialog.FileName) - 4, 4);
      if Extension = '*.tb0'
        then
          GridState := gsProfile
        else if Extension = '*.tb1' then
          GridState := gsTypes
        else if Extension = '*.tb2' then
          GridState := gsTexts
        else if Extension = '*.tb3' then
          GridState := gsKeywords
        else   }
          GridState := gsUndefined;
    MultiGrid.LoadFromFile(OpenDialog.FileName);
  end;
end;

procedure TFStatistics.TablesClearExecute(Sender: TObject);
Var
 i:integer;
begin
  with FStatistics.MultiGrid do
  begin
  Hint := '';
  for i:= 0 to RowCount-1 do
    Rows[i].Clear;
  RowCount := 30;
  ColCount := 30;  
  end;
end;

procedure TFStatistics.SelectTableExecute(Sender: TObject);
var
  Temp : boolean;
  TableName : String;
begin
 try
  Application.CreateForm(TFSelectTable, FSelectTable);
  if (FSelectTable.ShowModal = mrOk) then
    begin
      TableName := project_path + Copy(FCorpusMemo.ActiveDocument.TxtFile.Name, 1,
           Length(FCorpusMemo.ActiveDocument.TxtFile.Name) - 4) + '[tb' + inttoStr(FSelectTable.RadioGroup.ItemIndex)+'].txt';
      if (TDocument(DocList[Ind_DOCUMENTLIST]).TypFile = nil)
         and (FSelectTable.RadioGroup.ItemIndex > 0) then
        FCorpusMemo.FileCreateTypFileExecute(nil);
      if CanContinue then
        begin
          Query := TFileQuery.Create(TDocument(DocList
                  [Ind_DOCUMENTLIST]), FStatistics.MultiGrid);
          Application.CreateForm(TFProgress, FProgress);
          FProgress.Show;
          case FSelectTable.RadioGroup.ItemIndex of
            0: begin
                 FProgress.Job.Caption := 'Character Profiling:';
                 Query.Profile;
               end;
            1: begin
                 FProgress.Job.Caption := 'Creating Word Form Profile:';
                 Query.TypesTable;
                 FLog.TableCalculate.Enabled := True;
               end;
            2: begin
                 FProgress.Job.Caption := 'Creating Text Lexical Profile:';
                 Query.TextsTable;
               end;
            3: begin
                 FProgress.Job.Caption := 'Creating Text Keyword Profile:';
                 rdFreqVSPercent.Enabled;
                 rdFreqVSPercent.ShowModal;
                 Screen.Cursor := crHourGlass;
                 Temp := rdFreqVSPercent.rgFreqVSPercent.ItemIndex = 1;
                 if (rdFreqVSPercent.rbfifty.Checked)
                   then  Query.FindKeywords(Temp)
                   else  Query.FindKeywordsAboveMean();
                 Screen.Cursor := crDefault;
               end;
            4: begin
                 FProgress.Job.Caption := 'Representativness Scale:';
                 Screen.Cursor := crHourGlass;
                 Query.Representativness;
                 Screen.Cursor := crDefault;
               end;
            5: begin
                 Screen.Cursor := crHourGlass;
                 Query.LogLikelihoodTable;
                 Screen.Cursor := crDefault;
               end;
            6: begin
                 FProgress.Job.Caption := 'Creating Text Lexical Classification Profile:';
                 Query.Class_of_Types_and_Tokens_in_Texts(6);
               end;
            7: begin
                 FProgress.Job.Caption := 'Creating Text Lexical Classification Profile:';
                 Query.Calculate_Principal_Class_for_Types(7);
               end;
            8: begin
                 FProgress.Job.Caption := 'Creating Emblemathic Classification Profile:';
                 Query.Emblematics_Table(8);
               end;
            9: begin
                 FProgress.Job.Caption := 'Creating Schematic Classification in Texts:';
                 Query.Make_Schematic_Text(9);
               end;
            10: begin
                 FProgress.Job.Caption := 'Creating Schematic Classification in Texts:';
                 Query.Make_Schematic_Corpus(10);
               end;
            11: begin
                 FProgress.Job.Caption := 'Creating Text-Text Comparison:';
                 Query.Text_Text_Comparison(11);
               end;
          end;
          Query.Destroy;
          FProgress.Close;
          FProgress.Free;
      end;
    end;
  FSelectTable.Free;
  except
  else
   begin
    FProgress.Close;
    FProgress.Free;
    raise EDateTimeError.Create('Be sure that corpus is well formed and delimiters set is correctly ');
   end;
  end;
end;

procedure TFStatistics.FormShow(Sender: TObject);
begin
  Left := 265;
  Top := 178;
  Width := 751;
  Height := 175;
end;

procedure TFStatistics.SortinAscendingorder1Click(Sender: TObject);
var
 FileHandle : TextFile;
 Line, encabezado,text : String;
 StrArray : TStringArray;
 col,i,d : Word;
 number : boolean;
 SortList: TStringList;
begin
 //Inicializo
 SortList := TStringList.Create;
 SortList.Sorted := True;
 SortList.Duplicates := dupIgnore;

 col := MultiGrid.Col;
 SetCurrentDir(Appdir+'Files\Temporal\');
 MultiGrid.SaveToFile('temp.txt');
 for i:= 0 to MultiGrid.RowCount-1 do
  MultiGrid.Rows[i].Clear;

 AssignFile(FileHandle,'temp.txt');
 Reset(FileHandle);
 readln(FileHandle,Line);
 encabezado := Line;
 readln(FileHandle,Line);
 StrArray := SplitString(Line, [#9]);
 number := True;
 text := StrArray[col];
 for i:=1 to length(text) do
  if (ord(Text[i]) < 48) or (ord(Text[i]) >57) then number := false;

 Reset(FileHandle);
 Readln(FileHandle);

 //Inserto las lineas al fichero ordenado
 while not eof(FileHandle) do
  begin
   readln(FileHandle,Line);
   StrArray := SplitString(Line, [#9]);
   if StrArray[0] = '0' then continue;
   text := StrArray[col];

   if number then
    while length(text) < 20 do
      text := '0'+text;

   SortList.Add(text+#9+Line);
  end;
 CloseFile(FileHandle);

 //Edito el fichero
 Rewrite(FileHandle);
 writeln(FileHandle,encabezado);
 for i:= 0 to SortList.Count-1 do
  begin
   StrArray :=  SplitString(SortList.Strings[i], [#9]);
   Line := StrArray[1];
   for d:= 2 to length(StrArray)-1 do
    Line:= Line + #9 + StrArray[d];
   writeln(FileHandle,Line);
  end;
 CloseFile(FileHandle);

 //Cargo el fichero a la tabla
 MultiGrid.LoadFromFile('temp.txt');

 //Elimino los archivos utilizados
 deletefile('temp.txt');
 setlength(StrArray,0);

end;

procedure TFStatistics.SortinDescendingorder1Click(Sender: TObject);
var
 FileHandle : TextFile;
 Line, encabezado,text : String;
 StrArray : TStringArray;
 col,i,d : Word;
 number : boolean;
 SortList: TStringList;
begin
 //Inicializo
 SortList := TStringList.Create;
 SortList.Sorted := True;
 SortList.Duplicates := dupIgnore;

 col := MultiGrid.Col;
 SetCurrentDir(Appdir+'Files\Temporal\');
 MultiGrid.SaveToFile('temp.txt');
 for i:= 0 to MultiGrid.RowCount-1 do
  MultiGrid.Rows[i].Clear;

 AssignFile(FileHandle,'temp.txt');
 Reset(FileHandle);
 readln(FileHandle,Line);
 encabezado := Line;
 readln(FileHandle,Line);
 StrArray := SplitString(Line, [#9]);
 number := True;
 text := StrArray[col];
 for i:=1 to length(text) do
  if (ord(Text[i]) < 48) or (ord(Text[i]) >57) then number := false;

 Reset(FileHandle);
 Readln(FileHandle);

 //Inserto las lineas al fichero ordenado
 while not eof(FileHandle) do
  begin
   readln(FileHandle,Line);
   StrArray := SplitString(Line, [#9]);
   if StrArray[0] = '0' then continue;
   text := StrArray[col];

   if number then
    while length(text) < 20 do
      text := '0'+text;

   SortList.Add(text+#9+Line);
  end;
 CloseFile(FileHandle);

 //Edito el fichero
 Rewrite(FileHandle);
 writeln(FileHandle,encabezado);
 for i:=SortList.Count-1 downto 0 do
  begin
   StrArray :=  SplitString(SortList.Strings[i], [#9]);
   Line := StrArray[1];
   for d:= 2 to length(StrArray)-1 do
    Line:= Line + #9 + StrArray[d];
   writeln(FileHandle,Line);
  end;
 CloseFile(FileHandle);

 //Cargo el fichero a la tabla
 MultiGrid.LoadFromFile('temp.txt');

 //Elimino los archivos utilizados
 deletefile('temp.txt');
 setlength(StrArray,0);

end;

procedure TFStatistics.Maximize1Click(Sender: TObject);
begin
 FStatistics.WindowState := wsMaximized;
end;

procedure TFStatistics.Minimize1Click(Sender: TObject);
begin
 FStatistics.WindowState := wsMinimized;
end;

procedure TFStatistics.FormActivate(Sender: TObject);
begin
//if FStatistics.MultiGrid.Hint = '' then FStatistics.PopupMenu := nil;
end;

procedure TFStatistics.FormDeactivate(Sender: TObject);
begin
 //FStatistics.PopupMenu := PopupMenuStatistics;
end;

end.

