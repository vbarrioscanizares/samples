unit TknFile;

interface
uses Windows, sysutils, Classes, Graphics, Forms, Controls, Menus,
  StdCtrls, Dialogs, Buttons, Messages, ExtCtrls, ComCtrls, StdActns,
  ActnList, ToolWin, Utils, TxtFile, Gauges,results, UProject, ConvUtils;
type
    Ttexto = Record
    sentences        : longint;
    paragraphs       : longint;
    cantidaddetokens : longint;
    end;
  TTknFile = class
    private
      fHandle: TextFile;
      fResultsHandle : TextFile;
      fBOT : string;
      fBOP : string;
      fBOS : string;
      fTknDelims : TDelimiters;
      fNameAndPath: TFileName;
      fTxtFileSize : Longword;
      fTextCount : Longword;
      fParagraphCount : Longword;
      fSentenceCount : Longword;
      function GetName: string;
      procedure CreateTknFile(TxtFile : TTxtFile);
      procedure ReadDelims(DelimFile : TFileName);
    public

      constructor Create(TxtFile : TTxtFile;
                                  DelimFile : TFileName);overload;
      constructor Create(TknFileName, DelimFile : TFileName);overload;
      property Bot: String Read fBot;
      property Bop: String Read fBop;
      property Bos: String Read fBos;
      property TknDelims: TDelimiters Read fTknDelims;
      property Handle : TextFile read fHandle;
      property ResultsHandle : TextFile read fResultsHandle;
      property NameAndPath : TFileName read fNameAndPath;
      property Name : string read GetName;
      property Size : Longword read fTxtFileSize;
      property TextCount : Longword read fTextCount;
      property ParagraphCount : Longword read fParagraphCount;
      property SentenceCount : Longword read fSentenceCount;
      procedure Free;
  end;

Var {Variables que necesito en TipesFile.pas}
  Sentencespertext : array of Ttexto;
  PDelims          : TDelimiters;
implementation

{ TTxtFile }
uses
  Main, Progress;
constructor TTknFile.Create(TxtFile : TTxtFile; DelimFile : TFileName);
var
  WHandle : THandle;
begin
  inherited Create;
  // Lectura de los delimitadores
  ReadDelims(DelimFile);
  fNameAndPath := project_path + Copy(TxtFile.Name, 1, Length(
                  TxtFile.Name)-4) + '[tkn].txt';
  AssignFile(fHandle, fNameAndPath); //Tomando un handle para el fich de tokens
  AssignFile(fResultsHandle, project_path + Copy(TxtFile.Name, 1, Length(
                  TxtFile.Name)-4) + '[res].txt');
  WHandle := CreateFile(PChar(TxtFile.NameAndPath), GENERIC_READ, FILE_SHARE_READ, nil,
                       OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  fTxtFileSize := GetFileSize(WHandle, nil); //Para poder saber el tama�o del fich texto original y asi actualizar el Gauge
  CloseHandle(WHandle);
  //Creacion del fich de tokens a partir del original
  fTextCount := 0;
  fParagraphCount := 0;
  fSentenceCount := 0;
  CreateTknFile(TxtFile);
end;

constructor TTknFile.Create(TknFileName, DelimFile : TFileName);
var
  WHandle : THandle;
begin
  inherited Create;
  ReadDelims(DelimFile);
  fNameAndPath := TknFileName;
  AssignFile(fHandle, fNameAndPath);
//  WHandle := CreateFile(PChar(TxtFile.NameAndPath), GENERIC_READ, FILE_SHARE_READ, nil,
  //                    OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  fTxtFileSize := GetFileSize(WHandle, nil); //Para poder saber el tama�o del fich texto original y asi actualizar el Gauge
  CloseHandle(WHandle);
end;

procedure TTknFile.CreateTknFile(TxtFile : TTxtFile);
var
  Position, SentenceIndex, SentencePerText, ParagraphIndex, CharCount,
  TokenCount : LongWord;
  Line, DummyStr : string;
  TokenArray : TStringArray;
  j,d : Longword;

  {Variables  para procesar los elementos de la tabla de Tokens}
  sentences        : longint;
  paragraphs       : longint;
  cantidaddetokens : longint;
  coocurrentindex          : integer;
  PercentofTokeninSentence : real;
  PercentofTextSentences    : real;
  PercentageofTokeninSentence : integer;
  PercentageofTextSentences : integer;
  strTokenCount             : String;

  {Variables  para procesar los elementos de la tabla de Tipos}

begin
  FProgress.Job.Caption := 'Tokenizing File:';
  TokenCount := 0;
  CharCount := 0;
  Position := 0;
  ParagraphIndex := 0;
  SentenceIndex := 0;
  SentencePerText := 0;
  Reset(TxtFile.Handle);
  Rewrite(fHandle);
  Rewrite(fResultsHandle);

  paragraphs       := 0;
  sentences        := 0;
  cantidaddetokens :=0;
  setlength(Sentencespertext,0);

  PDelims := fTknDelims;

  while not SeekEof(TxtFile.Handle) do
    begin
      ReadLn(TxtFile.Handle, Line); // Leer una linea del fichero orig.
      if Pos(fBOT, Line) = 1 // Si es un nuevo texto
        then
          begin
            if length(Sentencespertext) <> 0
              then
                begin
                  Sentencespertext[length(Sentencespertext) - 1].sentences := sentences;
                  Sentencespertext[length(Sentencespertext) - 1].paragraphs := paragraphs;
                  Sentencespertext[length(Sentencespertext) - 1].cantidaddetokens := cantidaddetokens;
                end;
            setlength(Sentencespertext,length(Sentencespertext) + 1);
            cantidaddetokens := 0;
            sentences  := 0;
            paragraphs := 0;
          end
        else
          begin
           Line := AnsiUpperCase(Line);
           TokenArray := SplitString(Line, fTknDelims); //Parsear los tokens de la oracion
           if Pos(fBOP, Line) = 1 // Si es un nuevo parrafo
            then  inc(paragraphs);
           cantidaddetokens := cantidaddetokens + length(Tokenarray);
           inc(sentences);
          end;
    end;
  If length(Sentencespertext)=0
   then
    begin
     //MessageDlg('This Corpus is not correctly formated',   mtError, [mbAbort], 0);
     CloseFile(TxtFile.Handle);
     CloseFile(fResultsHandle);
     CloseFile(fHandle);

     FLog.RichEdit.Lines.Add(TimeToStr(Time)+'|'+#9+'Operation interrumpted!!! Corpus incorrectly formated. Try again with other Corpus');
     FLog.RichEdit.Refresh;

     FProgress.Close;
     FProgress.Free;
     RaiseConversionError('This File: '+FPRoject.TReeView1.Hint+' need be correctly formated. First you must build a Corpus.');
    end;
  Sentencespertext[length(Sentencespertext) - 1].sentences := sentences;
  Sentencespertext[length(Sentencespertext) - 1].paragraphs := paragraphs;
  Sentencespertext[length(Sentencespertext) - 1].cantidaddetokens := cantidaddetokens;
  sentences := 0;
  CloseFile(TxtFile.Handle);
  Reset(TxtFile.Handle);


  while not SeekEof(TxtFile.Handle) do
    begin
      ReadLn(TxtFile.Handle, Line); // Leer una linea del fichero orig.
      Inc(CharCount, Length(Line)); // Para poder establecer un criterio de cuanto he procesado.
      if Pos(fBOT, Line) = 1 // Si es un nuevo texto
        then
          begin
            if fTextCount > 0 then
              begin
                Write(fResultsHandle, fTextCount);       // Estadisticas
                Write(fResultsHandle, #9);               // para el fich.
                Write(fResultsHandle, ParagraphIndex);   // de resultados
                Write(fResultsHandle, #9);               // correspondiente
                Writeln(fResultsHandle, SentencePerText);// al de Tokens.
              end;
            inc(fTextCount);
            ParagraphIndex := 0;
            SentenceIndex := 0;
            SentencePerText := 0;
            Position := 0;
          end
        else
          if Pos(fBOP, Line) = 1 // Si es un nuevo parrafo
            then
              begin
                inc(ParagraphIndex);
                inc(fParagraphCount);
                SentenceIndex := 0;
                Position := 0;
              end
            else
              begin // Si es una linea normal, procesarla
                inc(SentenceIndex);
                inc(SentencePerText);
                inc(fSentenceCount);
                Position := 0;
                Line := AnsiUpperCase(Line);
                TokenArray := SplitString(Line, fTknDelims); //Parsear los tokens de la oracion
                if Length(TokenArray) > 0 then
                  for j := 0 to high(TokenArray) do
                    if TokenArray[j] <> 'S'
                      then
                        begin
                          inc(TokenCount);
                          inc(Position);

                          PercentofTokeninSentence    := Position / length(tokenarray); // Calculo el porciento del token en la oracion
                          PercentageofTokeninSentence := round(PercentofTokeninSentence);
                          PercentofTextSentences      := SentenceIndex / SentencesPerText[fTextCount -1].sentences;  //Calculo la relacion  de la oracion respectiva en cuanto al resto de las oraciones del texto
                          PercentageofTextSentences   := Round(PercentofTextSentences);
                          if length(TokenArray) > (SentencesPerText[fTextCount -1].cantidaddetokens / SentencesPerText[fTextCount -1].sentences ) //Si la cantidad de tokens de la oracion es mayor que el promedio de cantidad de tokens por oracion
                            then coocurrentindex := 1
                            else coocurrentindex := 0;

                         strTokenCount :=   IntToStr(TokenCount);
                         if length(strTokenCount) < 9 then
                           for d := 1 to 9 -  length(strTokenCount) do
                              strTokenCount := '0' + strTokenCount;
                         if length(TokenArray[j]) < 50
                            then
                               for d:= 1 to 50 - length(Tokenarray[j]) do
                                  Tokenarray[j] := Tokenarray[j] + ' ' ; // Completo cada palabra con espacios hasta 50 caracteres

                          DummyStr := TokenArray[j]  + #9 + StrTokenCount + #9 + IntToStr(fTextCount) + #9 +
                                       IntToStr(SentenceIndex) + #9 +   IntToStr(Position) + #9 +
                                       IntToStr(PercentageofTextSentences) + #9 +
                                       IntToStr(PercentageofTokeninSentence) + #9 +
                                       IntToStr(coocurrentindex)
                                        {+    #9 +
                                      IntToStr(SentenceCount)};
                          Writeln(fHandle, DummyStr);
                        end;
                  SetLength(TokenArray, 0);
              end;// Procesar la linea
      UpdateGauge(CharCount, fTxtfileSize);
      Application.ProcessMessages;
    end;
  UpdateGauge(fTxtFileSize, fTxtFileSize);
  Write(fResultsHandle, fTextCount);       // Estadisticas del ultimo texto
  Write(fResultsHandle, #9);               // para el fich.
  Write(fResultsHandle, ParagraphIndex);   // de resultados
  Write(fResultsHandle, #9);               // correspondiente
  Writeln(fResultsHandle, SentencePerText);// al de Tokens.
  CloseFile(fResultsHandle);
  CloseFile(TxtFile.Handle);
  CloseFile(fHandle);
end;

procedure TTknFile.Free;
begin
  inherited Free;
end;

function TTknFile.GetName: string;
begin
  Result := ExtractFileName(fNameAndPath);
end;

procedure TTknFile.ReadDelims(DelimFile : TFileName);
var
  Delimiter : char;
  DelimHandle : TextFile;
begin
  AssignFile(DelimHandle, DelimFile);
  Reset(DelimHandle);
  readln(DelimHandle, fBOT); //Leyendo de fich el delimitador de texto
  readln(DelimHandle, fBOP); //Leyendo de fich el delimitador de parrafo
  fTknDelims := fTknDelims + [' '];
  while not SeekEof(DelimHandle) do            //Los delimiatadores
  begin                                      //de token
    readln(DelimHandle, Delimiter);            // estan en las
    fTknDelims := fTknDelims + [Delimiter]; //otras lineas del fich. de delimitadores
  end;
  CloseFile(DelimHandle);
end;

end.

