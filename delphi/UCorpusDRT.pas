unit UCorpusDRT;

interface
uses UCharTree, utils,forms,sysutils,ComCtrls,Classes,UInformativeWindow;

const
Temporal_File          = 'Temp\Tempfile.txt';
Tokens_File            = 'Temp\tokensfile.txt';
SortTokens_File        = 'Temp\tokensSRTfile.txt';
Tips_File              = 'Temp\tipsfile.txt';
Sort_Tips_File         = 'Temp\tipsSRTfile.txt';
Key_File               = 'Temp\keyfile.dat';
Sorter_File            = 'Temp\sorter.bat';
restauration_File      = 'Temp\restauration.txt';
data_restauration_File = 'Temp\data_restauration.txt';

type
TKeyRecord = record
  word     : String[100];
  Translate: String[100];
 end;

TcorpusDRT = class
 private
  Appdir, Text_Delimiter, Paragraph_Delimiter : String;
  GramaticsTree, SinonimsTree, ContractionsTree, WordFamilyTree : TCharTree;
  inputfile, outputfile                         : TextFile;
  KeyFileLines : LongWord;
  contractions,sinonims,gramatics,family,freqs : boolean;
  indLine : LongWord;
 public
  property App_Path: String read Appdir;
  property Pzpif: boolean read freqs;
  constructor Create(input, output,textdelimiter,paragraphdelimiter: String;contb,sinb,grab,famb,freqb : boolean);
  constructor RestaurationMode();

  destructor  Eliminate();

  function Contractions_Translate(word: String):String;
  function Sin_Translate(word: String): String;
  function Grama_Translate(word : String): String;
  function WordFamily_Translate(word:String):String;
  function Zpif_Translate(word:String):String;
  function GetCleanString(word1:String):String;

  procedure Zpif(input: String;min,max:LongWord);
  procedure ReduceDimensionality();
  procedure MakeTokensSrtFile();
  procedure MakeTipsFile();
  procedure MakeSortTipsFile();
  procedure MakeKeyFile(lower,higer:LongWord);
end;

implementation

{ TcorpusDRT }

function TcorpusDRT.Contractions_Translate(word: String): String;
begin
  result := ContractionsTree.Get_Translate(word);
end;

constructor TcorpusDRT.Create(input, output,textdelimiter,paragraphdelimiter: String;contb,sinb,grab,famb,freqb : boolean);
Var
 filehandle : TextFile;
begin
 InformativeWindow.Memo.Lines.Add('Creating Data Structures at '+TimeToStr(Time));
 if GetPlatform = 'Winnt'
    then
     AppDir := ExtractFilePath(Application.ExeName)
    else
     AppDir := ExtractShortPathName(ExtractFilePath(Application.ExeName));

 AssignFile(inputfile,input);
 AssignFile(outputfile,output);

 GramaticsTree    := TcharTree.Create;
 SinonimsTree     := TCharTree.Create;
 ContractionsTree := TcharTree.Create;
 WordFamilyTree   := TCharTree.Create;

 GramaticsTree.Load_From_file(Appdir+'Data\Grammemes.txt');
 SinonimsTree.Load_From_file(Appdir+'Data\Synonyms.txt');
 ContractionsTree.Load_From_file(Appdir+'Data\Contractions.txt');
 WordFamilyTree.Load_From_file(Appdir+'Data\WordFamily.txt');

 Text_Delimiter      := textdelimiter;
 Paragraph_Delimiter := paragraphdelimiter;

 freqs        := freqb;
 family       := famb;
 sinonims     := sinb;
 gramatics    := grab;
 contractions := contb;
 indLine      := 0;

 //trabajo con los ficheros de restauracion
 AssignFile(filehandle,Appdir+data_restauration_File);
 Rewrite(filehandle);
 writeln(filehandle,input);
 writeln(filehandle,output);
 writeln(filehandle,text_delimiter);
 writeln(filehandle,paragraph_delimiter);
 writeln(filehandle,booltostr(freqs));
 writeln(filehandle,booltostr(family));
 writeln(filehandle,booltostr(sinonims));
 writeln(filehandle,booltostr(gramatics));
 writeln(filehandle,booltostr(contractions));
 CloseFile(filehandle);

 AssignFile(filehandle,Appdir+restauration_File);
 Rewrite(filehandle);
 writeln(filehandle,'0');
 CloseFile(filehandle);
end;

destructor TcorpusDRT.Eliminate;
begin
 InformativeWindow.Memo.Lines.Add('Destroying Data Structures at '+TimeToStr(Time));
 CloseFile(inputfile);
 CloseFile(outputFile);

 GramaticsTree.Eliminate;
 SinonimsTree.Eliminate;
 ContractionsTree.Eliminate;

 DeleteFile(Appdir+Temporal_File);
 DeleteFile(Appdir+Tokens_File);
 DeleteFile(Appdir+Tips_File);
 DeleteFile(Appdir+Sort_Tips_File);
 DeleteFile(Appdir+Key_File);
 DeleteFile(Appdir+SortTokens_File);
 DeleteFile(Appdir+restauration_File);
 DeleteFile(Appdir+data_restauration_File);
end;

function TcorpusDRT.GetCleanString(word1: String): String;
Var
 i,ordinal: Word;
begin
 result := '';
 for i:=1 to length(word1) do
  begin
   ordinal := ord(word1[i]);
   if (ordinal >=65) and (ordinal <= 90) then result := result + word1[i];
  end;
end;

function TcorpusDRT.Grama_Translate(word: String): String;
begin
  result := GramaticsTree.Get_Translate(word);
end;

procedure TcorpusDRT.MAkeKeyFile(lower,higer:LongWord);
Var
FileHAndle : TextFile;
KeyRecord : TKeyRecord;
KeyFileHandle : File of TKeyRecord;
Line : String;
StrArray : TStringArray;
posicion : Integer;
begin
 InformativeWindow.Memo.Lines.Add('Making Key File at '+TimeToStr(Time));
  //Creo el Archivo de claves para Codificar
  AssignFile(FileHandle,Appdir+Sort_Tips_File);
  Reset(FileHandle);
  AssignFile(KeyFileHandle,Appdir+Key_File);
  Rewrite(KeyFileHandle);
  setlength(strarray,0);
  KeyFileLines := 0;
  while not eof(FileHandle) do
   begin
    readln(FileHandle,Line);
    Line := uppercase(Line);
    StrArray := splitString(Line,[#9]);
    inc(KeyFileLines);
    posicion := pos('.',StrArray[0]);
    KeyRecord.word      := copy(StrArray[0],1,posicion-1);
    if (LongWord(strtoint(Strarray[1])) >= lower) and (LongWord(strtoint(Strarray[1])) <= higer)
     then  KeyRecord.Translate := copy(StrArray[0],posicion+1,length(StrArray[0])-posicion)
     else  KeyRecord.Translate := '';
    write(KeyFileHandle,KeyRecord);
   end;

  CloseFile(FileHandle);
  CloseFile(KeyFileHandle);
end;

procedure TcorpusDRT.MAkeSortTipsFile;
Var
 FileHAndle : TextFile;
 Line : String;
begin
 InformativeWindow.Memo.Lines.Add('Sorting Tips File at '+TimeToStr(Time));
 //Ordeno el fichero de tipos segun las frecuencias
 AssignFile(FileHandle,Appdir+Sort_Tips_File);
 Rewrite(FileHandle);
 CloseFile(FileHandle);

 AssignFile(FileHandle,Appdir+Sorter_File);
 Rewrite(FileHandle);
 Line := 'sort ' +
         ExtractShortPathName(Appdir+Tips_File) + ' /o ' +
         ExtractShortPathName(Appdir+Sort_Tips_File);
  Writeln(FileHandle, Line);
  CloseFile(FileHandle);
  ExecConsoleApp(ExtractShortPathName(Appdir+Sorter_File));
  DeleteFile(Appdir+Sorter_File);
end;

procedure TcorpusDRT.MakeTipsFile;
Var
 Line,Temp,Aux : String;
 TknFileHandle, FileHandle : TextFile;
 totalfreq : LongWord;
begin

 InformativeWindow.Memo.Lines.Add('Making Tips File at '+TimeToStr(Time));
 //Formo fichero de tipos y concateno todos los ficheros en uno general de tipos

 AssignFile(TknFileHandle,Appdir+Tips_File);
 Rewrite(TknFileHandle);
 AssignFile(FileHandle,Appdir+SortTokens_File);
 Reset(FileHandle);
 readln(FileHandle,Aux);
 Line := Aux;
 totalfreq:= 0;
 while not eof(FileHandle) do
  begin
   if Line = Aux
    then inc(totalfreq)
     else
      begin
        temp := inttostr(totalfreq);
        Aux:=GetCleanString(uppercase(Aux))+'.'+Aux;
        writeln(TknFileHandle,Aux+#9+temp);
        totalfreq := 1;
        Aux := Line;
       end;
     readln(FileHandle,Line);
    end;
   if aux <> '' then
    begin
     temp := inttostr(totalfreq);
     Aux:=GetCleanString(uppercase(Aux))+'.'+Aux;
     writeln(TknFileHandle,Aux+#9+temp);
    end;
  CloseFile(FileHandle);
  CloseFile(TknFileHandle);
end;

procedure TcorpusDRT.MakeTokensSrtFile;
Var
 FileHandle : TextFile;
 Line : String;
begin
 InformativeWindow.Memo.Lines.Add('Sorting Tokens File at '+TimeToStr(Time));
 //Ordeno los fihceros de Tokens producidos anteriormente
 AssignFile(FileHandle,Appdir+SortTokens_File);
 Rewrite(FileHandle);
 CloseFile(FileHandle);

 AssignFile(FileHandle,Appdir+Sorter_File);
 Rewrite(FileHandle);
 Line := 'sort ' +
         ExtractShortPathName(Appdir+Tokens_File) + ' /o ' +
         ExtractShortPathName(Appdir+SortTokens_File);
  Writeln(FileHandle, Line);
  CloseFile(FileHandle);
  ExecConsoleApp(ExtractShortPathName(Appdir+Sorter_File));
  DeleteFile(Appdir+Sorter_File);
end;

procedure TcorpusDRT.ReduceDimensionality();
Var
 Line,aux,temp,FinalLine : String;
 bool : boolean;
 i : LongWord;
 ordinal : byte;
 TokensFileHandle,TemporalFileHandle : TextFile;

begin
 InformativeWindow.Memo.Lines.Add('Beginnig Dimensionality Reduction at '+TimeToStr(Time));
 //Datos necesarios
 AssignFile(TokensFileHandle,Appdir+Tokens_File);
 AssignFile(TemporalFileHandle,Appdir+Temporal_File);

 Rewrite(TokensFileHandle);
 Rewrite(TemporalFileHandle);  
 Reset(inputfile);

 while not eof(inputfile) do
  begin
   readln(inputfile,Line);
   Line := uppercase(Line);
   //Aseguro que las lineas que contienen un delimitador de texto o parrafo queden iguales
   if (Text_Delimiter <> '') and (pos(Text_Delimiter,Line) <> 0)
    then
    begin
     writeln(TemporalFileHandle,Line);
     Continue;
    end;

   if (Paragraph_Delimiter <> '') and (pos(Paragraph_Delimiter,Line) <> 0) then
    begin
     writeln(TemporalFileHandle,Line);
     Continue;
    end;

   //elimino los caracteres indeseados
   Aux := '';
   bool := false;
   for i:=1 to length(Line) do
    begin
     ordinal := ord(Line[i]);
     case  ordinal of
      32,39,45               : Aux := Aux + Line[i];
      97..122,65..90,209,241 : begin
                                bool := true;
                                Aux := Aux + Line[i];
                               end;
     end;
    end;

  //Aseguro que las lineas vacias no se tomen en cuenta
    if not bool
     then
      begin
       writeln(TemporalFileHandle,Aux);
       Continue;
      end
     else Line := Aux;

   //Proceso cada palabra por separado
     i := pos(' ',Line);
     FinalLine := '';
     while i <> 0 do
       begin
         Aux := uppercase(copy(Line,1,i-1));
         if (Aux<> '') and (Aux <> '-') and (Aux <> '''')
          then
           begin

             temp:= GramaticsTree.Get_Translate(aux);
             if (gramatics) and (temp <> Not_represented) and (temp <> aux)
              then
               begin
                delete(Line,1,i);
                Line := temp + ' ' + Line;
                i := pos(' ',Line);
                Continue;
               end;

              temp := ContractionsTree.Get_Translate(aux);
              if (contractions) and (temp <> Not_represented) and (temp <> aux)
               then
                begin
                 delete(Line,1,i);
                 Line := temp + ' ' + Line;
                 i := pos(' ',Line);
                 Continue;
                end;

              temp:= SinonimsTree.Get_Translate(aux);
              if (sinonims) and (temp <> Not_represented) and (temp <> aux)
               then
                begin
                 delete(Line,1,i);
                 Line := temp + ' ' + Line;
                 i := pos(' ',Line);
                 Continue;
                end;

              temp:= WordFamilyTree.Get_Translate(aux);
              if (family) and (temp <> Not_represented) and (temp <> aux)
               then
                begin
                 delete(Line,1,i);
                 Line := temp + ' ' + Line;
                 i := pos(' ',Line);
                 Continue;
                end;

              if Aux <> '' then //En el caso en que la palabra sea gramatical, se sustituira por ''
               begin
                Writeln(TokensFileHandle,Aux);
                flush(TokensFileHandle);
                FinalLine := FinalLine+ Aux + ' ';
                //Write(TemporalFileHandle,Aux+' ');
               end;
           end;
         delete(Line,1,i);
         i := pos(' ',Line);
         if (i = 0) and (length(line) <> 0) then i := length(Line)+1;
       end;
     writeln(TemporalFileHandle,FinalLine);
     flush(TemporalFileHandle);
  end;
 CloseFile(TokensFileHandle);
 CloseFile(TemporalFileHandle);
 InformativeWindow.Memo.Lines.Add('Finished Dimensionaity Reduction at '+TimeToStr(Time));
end;

constructor TcorpusDRT.RestaurationMode;
Var
 Line: String;
 filehandle : TextFile;
begin
 if GetPlatform = 'Winnt'
  then
   AppDir := ExtractFilePath(Application.ExeName)
  else
   AppDir := ExtractShortPathName(ExtractFilePath(Application.ExeName));

 AssignFile(Filehandle,Appdir+data_restauration_File);
 Reset(FileHAndle);
 readln(Filehandle,Line);
 AssignFile(inputfile,Line);
 readln(Filehandle,Line);
 AssignFile(outputfile,Line);
 readln(Filehandle,Line);
 Text_Delimiter      := Line;
 readln(Filehandle,Line);
 Paragraph_Delimiter := Line;
 readln(Filehandle,Line);
 freqs        := StrToBool(Line);
 readln(Filehandle,Line);
 family       := StrToBool(Line);
 readln(Filehandle,Line);
 sinonims     := StrToBool(Line);
 readln(Filehandle,Line);
 gramatics    := StrToBool(Line);
 readln(Filehandle,Line);
 contractions := StrToBool(Line);
 CloseFile(FileHandle);

 AssignFile(Filehandle,Appdir+restauration_File);
 Reset(FileHAndle);
 readln(Filehandle,Line);
 indLine := StrtoInt(Line);
 CloseFile(FileHandle);

 GramaticsTree    := TcharTree.Create;
 SinonimsTree     := TCharTree.Create;
 ContractionsTree := TcharTree.Create;
 WordFamilyTree   := TCharTree.Create;

 GramaticsTree.Load_From_file(Appdir+'Data\Grammemes.txt');
 SinonimsTree.Load_From_file(Appdir+'Data\Synonyms.txt');
 ContractionsTree.Load_From_file(Appdir+'Data\Contractions.txt');
 WordFamilyTree.Load_From_file(Appdir+'Data\WordFamily.txt');
end;

function TcorpusDRT.Sin_Translate(word: String): String;
begin
 result := SinonimsTree.Get_Translate(word);
end;

function TcorpusDRT.WordFamily_Translate(word: String): String;
begin

end;

procedure TcorpusDRT.Zpif(input: String; min, max: LongWord);
Var
 Line,aux,FinalLine,temp : String;
 bool : boolean;
 i,ind : LongWord;
 ordinal : byte;
 inputFileHandle,restaurationfilehandle : TextFile;
 
begin
 InformativeWindow.Memo.Lines.Add('Beginnig Frequencys analysis at '+TimeToStr(Time));
 MakeTokensSrtFile;
 MakeTipsFile;
 MAkeSortTipsFile;
 MAkeKeyFile(min, max);
 InformativeWindow.Memo.Lines.Add('Making Frequencys Analisys at '+TimeToStr(Time));
 InformativeWindow.Memo.Lines.Add('');
  //Datos necesarios
 AssignFile(inputFileHAndle,input);
 Reset(inputfileHandle);

 AssignFile(restaurationfilehandle,Appdir+restauration_File);

 If indLine <> 0
  then  Append(outputfile)
  else  Rewrite(outputfile);

 Reset(inputfileHandle);
 ind := 0;
 while not eof(inputfileHandle) do
  begin
   readln(inputfileHandle,Line);
   Line := uppercase(Line);
   inc (ind);
   if ind <= indLine then Continue;
   InformativeWindow.Memo.Lines.Delete(InformativeWindow.Memo.Lines.Count-1);
   InformativeWindow.Memo.Lines.Add('Analising Line number: '+InttoStr(ind));
   //Aseguro que las lineas que contienen un delimitador de texto o parrafo queden iguales
   if (Text_Delimiter <> '') and (pos(Text_Delimiter,Line) <> 0)
    then
    begin
     Rewrite(restaurationfilehandle);
     writeln(restaurationfilehandle,inttostr(ind));
     flush(restaurationfilehandle);
     writeln(outputFile,Line);
     flush(outputFile);
     Continue;
    end;

   if (Paragraph_Delimiter <> '') and (pos(Paragraph_Delimiter,Line) <> 0) then
    begin
     Rewrite(restaurationfilehandle);
     writeln(restaurationfilehandle,inttostr(ind));
     flush(restaurationfilehandle);
     writeln(outputFile,Line);
     flush(outputFile);
     Continue;
    end;

   //elimino los caracteres indeseados
   Aux := '';
   bool := false;
   for i:=1 to length(Line) do
    begin
     ordinal := ord(Line[i]);
     case  ordinal of
      32,39,45               : Aux := Aux + Line[i];
      97..122,65..90,209,241 : begin
                                bool := true;
                                Aux := Aux + Line[i];
                               end;
     end;
    end;

  //Aseguro que las lineas vacias no se tomen en cuenta
    if not bool
     then
      begin
       Rewrite(restaurationfilehandle);
       writeln(restaurationfilehandle,inttostr(ind));
       flush(restaurationfilehandle);
       writeln(outputFile,Aux);
       flush(outputFile);
       Continue;
      end
     else Line := Aux;

   //Proceso cada palabra por separado
     i := pos(' ',Line);
     FinalLine := '';
     while i <> 0 do
       begin
         Aux := GetCleanString(uppercase(copy(Line,1,i-1)));
         if (Aux<> '') and (Aux <> '-') and (Aux <> '''')
          then
           begin
            temp := Zpif_Translate(Aux);
            if temp <> '' then  FinalLine := FinalLine + temp + ' ';
           end;
         delete(Line,1,i);
         i := pos(' ',Line);
         if (i = 0) and (length(line) <> 0) then i := length(Line)+1;
       end;
     Rewrite(restaurationfilehandle);
     writeln(restaurationfilehandle,inttostr(ind));
     flush(restaurationfilehandle);
     writeln(outputFile,FinalLine);
     flush(outputFile);
  end;
 CloseFile(inputFileHandle);
 CloseFile(restaurationfilehandle);
 InformativeWindow.Memo.Lines.Add('Finished Frequencys analysis at '+TimeToStr(Time));
end;

function TcorpusDRT.Zpif_Translate(word: String): String;
Var
KeyFileHandle : File of TKeyRecord ;
KeyRecord : TKeyRecord;
posicion,max,min : LongWord;
did : boolean;
begin
 result := '';
 AssignFile(KeyFileHandle,Appdir+Key_File);
 Reset(KeyFileHandle);
 did:= False;
 min := 0;
 max := KeyFileLines;
 posicion := max div 2;
 while (not did) do
  begin
   //posicion:= (max - min) div 2 +1;
   seek(KeyFileHandle,posicion);
   read(KeyFileHAndle,KeyRecord);
   If KeyRecord.word = word then
     begin
      did := True;
      result := KeyRecord.Translate;
     end
     else
     If KeyRecord.word > word
       then
        begin
         if max = posicion then
          begin
           did:= True;
          end
          else
           begin
            max := posicion; //Si es la del registro es mayor que la palabra
            posicion := min + ((max - min) div 2);
           end;
        end
       else
        begin
         if min = posicion then
          begin
           did:=true;
          end
         else
          begin
           min := posicion;
           posicion := min + ((max - min) div 2);
          end;
        end;
  end;

 if result = '' then
  begin
   seek(KeyFileHandle,min-1);
   read(KeyFileHAndle,KeyRecord);
   If KeyRecord.word = word
    then result := KeyRecord.Translate
    else
     begin
      seek(KeyFileHandle,min);
      read(KeyFileHAndle,KeyRecord);
      If KeyRecord.word = word
       then result := KeyRecord.Translate
       else
        begin
         seek(KeyFileHandle,max);
         read(KeyFileHAndle,KeyRecord);
         If KeyRecord.word = word
          then result := KeyRecord.Translate
          else
           begin
            seek(KeyFileHandle,max+1);
            read(KeyFileHAndle,KeyRecord);
            If KeyRecord.word = word
             then result := KeyRecord.Translate;
           end;
        end;
     end;
   end;
 CloseFile(KeyFileHandle);
end;

end.
