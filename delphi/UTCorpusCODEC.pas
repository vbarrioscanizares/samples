unit UTCorpusCODEC;
interface

uses utils,forms,sysutils,ComCtrls,Classes,UFormInformativeWindow, idGlobal;

const
 //Max_TknFiles_Lines_Number = 1000000;
 Max_Digits_Freq_Count = 10;
 max_activity_count = 10;
 recuperation_const_file_name      = 'recuperation.txt';
 data_recuperation_const_file_name = 'data_recuperation.txt';
type
 TKeyRecord = record
  word     : String[100];
  Translate: String[10];
 end;

 TActivity = -1..max_activity_count;

TCorpusCodec = Class
private
 input_file  : TextFile;
 output_file : TextFile;
 recuperation_file : TextFile;

 key_Encode_file_Path     : String;
 key_Decode_file_Path     : String;
 freq_file_path           : String;
 Appdir                   : String;
 translate_String         : String;
 text_delimiter           : String;
 paragraph_delimiter      : String;

 indicate_last_activity       : TActivity;
 indicate_last_line_processed : LongWord;

public

 Constructor SimpleCreate();
 Constructor Create(input,output,keyEncode,KeyDecode,text,paragraph,freq: String);
 Constructor Analize_Recuperation_File();
 Destructor Destroy_Analize_File();
 Destructor Destroyer;

 procedure Encode();
 procedure Decode();
 procedure GetKeyFile();
 procedure GenerateNewTranslateString();

 procedure MakeTokensFiles();
 procedure SortTokensFiles();
 procedure MakeTipsFiles();
 procedure SortTipsFiles();
 procedure AssignTranslateStrings();
 procedure SortCodecKeyTempFile();
 procedure SortDecodeKeyTempFile();
 procedure MakeEncodeKeyFile();
 procedure MAkeDecodeKeyFile();

 function GetCleanString(word1:String):String;
 function Translate(word:String):String;
 function DTranslate(word:String):String;
end;

implementation

{ TCorpusCodec }

constructor TCorpusCodec.Analize_Recuperation_File;
Var
 Line:string;
begin
 if GetPlatform = 'Winnt'
    then
     AppDir := ExtractFilePath(Application.ExeName)
    else
     AppDir := ExtractShortPathName(ExtractFilePath(Application.ExeName));

 AssignFile(recuperation_file,Appdir+recuperation_const_file_name);

   //Initializing
   Reset(recuperation_file);
   if eof(recuperation_file) then
    begin
     indicate_last_activity       := -1;
     indicate_last_line_processed := 0;
    end
    else
    begin
     readln(recuperation_file,Line);
     indicate_last_activity := strToInt(Line);
     readln(recuperation_file,Line);
     indicate_last_line_processed := strToInt(Line);
     CloseFile(recuperation_file);
    end;


   AssignFile(recuperation_file,Appdir+data_recuperation_const_file_name);
   Reset(recuperation_file);
   readln(recuperation_file,Line);
   AssignFile(input_file,Line);
   readln(recuperation_file,Line);
   AssignFile(output_file,Line);
   readln(recuperation_file,Line);
   key_Encode_file_Path   := Line;
   readln(recuperation_file,Line);
   key_Decode_file_Path   := Line;
   readln(recuperation_file,Line);
   text_delimiter := Line;
   readln(recuperation_file,Line);
   paragraph_delimiter := Line;
   translate_String       := 'a';
   CloseFile(recuperation_file);

   AssignFile(recuperation_file,Appdir+recuperation_const_file_name);
   Reset(recuperation_file);
   if indicate_last_activity < 10
    then Encode
    else Decode;
end;

constructor TCorpusCodec.Create(input,output,keyEncode,KeyDecode,text,paragraph,freq: String);
begin
 if GetPlatform = 'Winnt'
    then
     AppDir := ExtractFilePath(Application.ExeName)
    else
     AppDir := ExtractShortPathName(ExtractFilePath(Application.ExeName));

 AssignFile(output_file,output);
 AssignFile(input_file,input);

 //LeftTime :=  (FileSize(input_file)div 1024)*20;
 translate_String       := 'a';
 text_delimiter         := text;
 paragraph_delimiter    := paragraph;
 key_Encode_file_Path   := keyEncode;
 key_Decode_file_Path   := KeyDecode;
 freq_file_path         := freq;

 AssignFile(recuperation_file,Appdir+data_recuperation_const_file_name);
 Rewrite(recuperation_file);
 writeln(recuperation_file,input);
 writeln(recuperation_file,output);
 writeln(recuperation_file,keyEncode);
 writeln(recuperation_file,keyDecode);
 writeln(recuperation_file,text);
 writeln(recuperation_file,paragraph);
 CloseFile(recuperation_file);

 AssignFile(recuperation_file,Appdir+recuperation_const_file_name);
 Rewrite(recuperation_file);
 writeln(recuperation_file,'0');
 writeln(recuperation_file,'0');
 CloseFile(recuperation_file);

 indicate_last_activity := -1;
 indicate_last_line_processed := 0;
end;

procedure TCorpusCodec.Decode;
Var
 Line,aux,FinalLine : String;
 i,ind : LongWord;
 ordinal : byte;
 KeyFileHandle : File of TKeyRecord ;
 KeyRecord : TKeyRecord;

begin
 InformativeWindow.Show;
 InformativeWindow.Memo.Lines.Add('Begin Decoding Process at '+TimeToStr(Time));
 InformativeWindow.Memo.Lines.Add('');
 //Datos necesarios
 AssignFile(KeyFileHandle,key_Decode_file_Path);
 Reset(KeyFileHandle);
 seek(KeyFileHandle,FileSize(KeyFileHandle)-1);
 read(KeyFileHAndle,KeyRecord);
 KeyFileLines :=strtoint(KeyRecord.word);
 CloseFile(KeyFileHandle);
 ind := 0;
 Reset(input_file);
 if indicate_last_activity = -1
  then Rewrite(output_file)
  else Append(output_file);

 while not eof(input_file) do
  begin
   readln(input_file,Line);
   Line := uppercase(Line);
   inc(ind);
   if ind <= indicate_last_line_processed then Continue;

   //Aseguro que las lineas que contienen un delimitador de texto o parrafo queden iguales
   if (Text_Delimiter <> '') and (pos(Text_Delimiter,Line) <> 0)
    then
    begin
     writeln(output_file,Line);
     flush(output_file);
     Continue;
    end;

   if (Paragraph_Delimiter <> '') and (pos(Paragraph_Delimiter,Line) <> 0) then
    begin
     writeln(output_file,Line);
     flush(output_file);
     Continue;
    end;

  Aux       := '';
  FinalLine := '';
  for i:=1 to length(Line) do
   begin
    ordinal := ord(Line[i]);
    case  ordinal of
     65..90: Aux := Aux + Line[i];
     else
      begin
       if Aux <> '' then
        begin
         FinalLine:= FinalLine+DTranslate(Aux);
         Aux := '';
        end;
       FinalLine := FinalLine + Line[i];
      end;
    end;
   end;
  if Aux <> '' then FinalLine:= FinalLine+DTranslate(Aux);

  Writeln(output_file,FinalLine);
  flush(output_file);
  InformativeWindow.Memo.Lines.Delete(InformativeWindow.Memo.Lines.Count-1);
  InformativeWindow.Memo.Lines.Add('Line '+ InttoStr(ind)+' Decoded at '+TimeToStr(Time));
  Rewrite(recuperation_file);
  writeln(recuperation_file,'10');
  flush(recuperation_file);
  writeln(recuperation_file,inttoStr(ind));
  flush(recuperation_file);
  CloseFile(recuperation_file);
 end;
 CloseFile(input_file);
 CloseFile(output_file);
 InformativeWindow.Memo.Lines.Add('End Decode Process at '+TimeToStr(Time));
end;

destructor TCorpusCodec.Destroyer;
begin
 deleteFile(Appdir+recuperation_const_file_name);
 deleteFile(Appdir+data_recuperation_const_file_name);
end;

destructor TCorpusCodec.Destroy_Analize_File;
begin
 deleteFile(Appdir+recuperation_const_file_name);
 deleteFile(Appdir+data_recuperation_const_file_name);
end;

function TCorpusCodec.DTranslate(word: String): String;
Var
KeyFileHandle : File of TKeyRecord ;
KeyRecord : TKeyRecord;
posicion,max,min : LongWord;
did : boolean;
begin
 result := '';
 AssignFile(KeyFileHandle,key_Decode_file_Path);
 Reset(KeyFileHandle);
 did:= False;
 max := KeyFileLines;
 min := 0;
 posicion := max div 2;
 while (not did) do
  begin
   //posicion:= (max - min) div 2 +1;
   seek(KeyFileHandle,posicion);
   read(KeyFileHAndle,KeyRecord);
   If KeyRecord.Translate = word then
     begin
      did := True;
      result := KeyRecord.word;
     end
     else
     If KeyRecord.Translate > word
       then
        begin
         if max = posicion then
          begin
           did:= True;
          end
          else
           begin
            max := posicion; //Si es la del registro es mayor que la palabra
            posicion := min + ((max - min) div 2);
           end;
        end
       else
        begin
         if min = posicion then
          begin
           did:=true;
          end
         else
          begin
           min := posicion;
           posicion := min + ((max - min) div 2);
          end;
        end;
  end;

 if result = '' then
  begin
   seek(KeyFileHandle,min-1);
   read(KeyFileHAndle,KeyRecord);
   If KeyRecord.Translate = word
    then result := KeyRecord.word
    else
     begin
      seek(KeyFileHandle,min);
      read(KeyFileHAndle,KeyRecord);
      If KeyRecord.Translate = word
       then result := KeyRecord.word
       else
        begin
         seek(KeyFileHandle,max);
         read(KeyFileHAndle,KeyRecord);
         If KeyRecord.Translate = word
          then result := KeyRecord.word
          else
           begin
            seek(KeyFileHandle,max+1);
            read(KeyFileHAndle,KeyRecord);
            If KeyRecord.Translate = word
             then result := KeyRecord.word;
           end;
        end;
     end;
   end;
 CloseFile(KeyFileHandle);
end;

procedure TCorpusCodec.Encode();
Var
 Line,aux,FinalLine : String;
 bool : boolean;
 i,ind : LongWord;
 ordinal : byte;
 KeyFileHandle : File of TKeyRecord ;
 KeyRecord : TKeyRecord;

begin
 InformativeWindow.Show;
 GetKeyFile();

 Rewrite(recuperation_file);
 writeln(recuperation_file,'9');
 writeln(recuperation_file,'0');
 CloseFile(recuperation_file);

 InformativeWindow.Memo.Lines.Add('Begin Encoding Process at '+TimeToStr(Time));
 InformativeWindow.Memo.Lines.Add('');
 //Datos necesarios
 AssignFile(KeyFileHandle,key_Encode_file_Path);
 Reset(KeyFileHandle);
 seek(KeyFileHandle,FileSize(KeyFileHandle)-1);
 read(KeyFileHAndle,KeyRecord);
 KeyFileLines :=strtoint(KeyRecord.word);
 CloseFile(KeyFileHandle);

 ind := 0;

 Reset(input_file);
 if indicate_last_activity = -1
  then  Rewrite(output_file)
  else  Append(output_file);

 while not eof(input_file) do
  begin
   readln(input_file,Line);
   inc(ind);
   if ind <= indicate_last_line_processed then Continue; //Garantizo que se continue a partir de la ultima linea calculada
   Line := uppercase(Line);
   //Aseguro que las lineas que contienen un delimitador de texto o parrafo queden iguales
   if (Text_Delimiter <> '') and (pos(Text_Delimiter,Line) <> 0)
    then
    begin
     writeln(output_file,Line);
     flush(output_file);
     Continue;
    end;

   if (Paragraph_Delimiter <> '') and (pos(Paragraph_Delimiter,Line) <> 0) then
    begin
     writeln(output_file,Line);
     flush(output_file);
     Continue;
    end;

   //elimino los caracteres indeseados
   Aux := '';
   bool := false;
   for i:=1 to length(Line) do
    begin
     ordinal := ord(Line[i]);
     case  ordinal of
      32,39,45               : Aux := Aux + Line[i];
      97..122,65..90,209,241 : begin
                                bool := true;
                                Aux := Aux + Line[i];
                               end;
     end;
    end;

  //Aseguro que las lineas vacias no se tomen en cuenta
    if not bool
     then
      begin
       writeln(output_file,Line);
       flush(output_file);
       Continue;
      end
     else Line := Aux;

    bool := False;
    FinalLine := '';
   //Guardo los tokens en un fichero especifico
     i := pos(' ',Line);
     while i <> 0 do
       begin
         Aux := GetCleanString(copy(Line,1,i-1));
         if (Aux<> '') and (Aux <> '-') and (Aux <> '''')
          then
           begin
             if not bool
              then
               begin
                bool:= true;
                FinalLine := Translate(Aux);
               end
              else  FinalLine := FinalLine + ' ' + Translate(Aux);
           end;
         delete(Line,1,i);
         i := pos(' ',Line);
       end;
     if (length(Line) > 0) and (Line<> '') and (Line <> '-') and (Line <> '''')
      then
       begin
        Line := GetCleanString(Line);
        if not bool
         then  FinalLine := Translate(Line)
         else  FinalLine := FinalLine + ' ' + Translate(Line);
       end;
   InformativeWindow.Memo.Lines.Delete(InformativeWindow.Memo.Lines.Count-1);
   InformativeWindow.Memo.Lines.Add('Line '+ InttoStr(ind)+' Encoded at '+TimeToStr(Time));
   Rewrite(recuperation_file);
   writeln(recuperation_file,'9');
   flush(recuperation_file);
   writeln(recuperation_file,inttoStr(ind));
   flush(recuperation_file);
   CloseFile(recuperation_file);
   Writeln(output_file,FinalLine);
   flush(output_file);
  end;
 CloseFile(input_file);
 CloseFile(output_file);
 InformativeWindow.Memo.Lines.Add('Finishing Encoding Process at '+TimeToStr(Time));
end;

procedure TCorpusCodec.GenerateNewTranslateString;
var
ind: Word;
did : Boolean;
ordinal : byte;
begin
  did := false;
  ind := length(translate_String);
  while not did do
   begin
    ordinal := ord(translate_String[ind]);
    if ordinal < 122
     then
      begin
        translate_String[ind] := Chr(ordinal +1);
        did := true;
      end
     else
      begin
       translate_String[ind] := 'a';
       dec(ind);
      end;
    if ind = 0
     then
      begin
       translate_String := 'a'+translate_String;
       did := True;
      end;
   end;
end;

procedure TCorpusCodec.GetKeyFile();
begin
 InformativeWindow.Memo.Lines.Add('Beginning Key Files Creation at '+TimeToStr(Time));

 if (indicate_last_activity < 1) then
 begin
  Rewrite(recuperation_file);
  writeln(recuperation_file,'0');
  writeln(recuperation_file,'0');
  flush(recuperation_file);
  CloseFile(recuperation_file);
  InformativeWindow.Memo.Lines.Add('Forming Tokens File at '+TimeToStr(Time));
  MakeTokensFiles;
 end;

 if (indicate_last_activity < 2) then
 begin
  Rewrite(recuperation_file);
  writeln(recuperation_file,'1');
  writeln(recuperation_file,'0');
  flush(recuperation_file);
  CloseFile(recuperation_file);
  InformativeWindow.Memo.Lines.Add('Sorting Tokens File at '+TimeToStr(Time));
  SortTokensFiles;
 end;

 if (indicate_last_activity < 3) then
 begin
  Rewrite(recuperation_file);
  writeln(recuperation_file,'2');
  writeln(recuperation_file,'0');
  flush(recuperation_file);
  CloseFile(recuperation_file);
  InformativeWindow.Memo.Lines.Add('Forming Unique Types File at '+TimeToStr(Time));
  MakeTipsFiles;
 end;

 if (indicate_last_activity < 4) then
 begin
  Rewrite(recuperation_file);
  writeln(recuperation_file,'3');
  writeln(recuperation_file,'0');
  flush(recuperation_file);
  CloseFile(recuperation_file);
  SortTipsFiles;
 end;

 if (indicate_last_activity < 5) then
 begin
  Rewrite(recuperation_file);
  writeln(recuperation_file,'4');
  writeln(recuperation_file,'0');
  flush(recuperation_file);
  CloseFile(recuperation_file);
  InformativeWindow.Memo.Lines.Add('Setting coding string for each type. Beginning at  '+TimeToStr(Time));
  AssignTranslateStrings;
 end;

 if (indicate_last_activity < 6) then
 begin
  Rewrite(recuperation_file);
  writeln(recuperation_file,'5');
  writeln(recuperation_file,'0');
  flush(recuperation_file);
  CloseFile(recuperation_file);
  InformativeWindow.Memo.Lines.Add('Creating Temporary Codification Key File');
  SortCodecKeyTempFile;
 end;

 if (indicate_last_activity < 7) then
 begin
  Rewrite(recuperation_file);
  writeln(recuperation_file,'6');
  writeln(recuperation_file,'0');
  flush(recuperation_file);
  CloseFile(recuperation_file);
  InformativeWindow.Memo.Lines.Add('Creating Temporary Decodification Key File at '+TimeToStr(Time));
  SortDecodeKeyTempFile;
 end;

 if (indicate_last_activity < 8) then
 begin
  Rewrite(recuperation_file);
  writeln(recuperation_file,'7');
  writeln(recuperation_file,'0');
  flush(recuperation_file);
  CloseFile(recuperation_file);
  InformativeWindow.Memo.Lines.Add('Creating Decodification Key File at '+TimeToStr(Time));
  MAkeDecodeKeyFile;
 end;

 if (indicate_last_activity < 9) then
 begin
  Rewrite(recuperation_file);
  writeln(recuperation_file,'8');
  writeln(recuperation_file,'0');
  flush(recuperation_file);
  CloseFile(recuperation_file);
  InformativeWindow.Memo.Lines.Add('Creating Codification Key File at '+TimeToStr(Time));
  MakeEncodeKeyFile;
 end;

 InformativeWindow.Memo.Lines.Add('Finishing Key Files Creation at '+TimeToStr(Time));
end;


constructor TCorpusCodec.SimpleCreate;
begin

end;

function TCorpusCodec.Translate(word: String): String;
Var
KeyFileHandle : File of TKeyRecord ;
KeyRecord : TKeyRecord;
posicion,max,min : LongWord;
did : boolean;
comp : Integer;
myword : String;
begin
 result := '';
 AssignFile(KeyFileHandle,key_Encode_file_Path);
 Reset(KeyFileHandle);
 did:= False;
 min := 0;
 max := KeyFileLines;
 posicion := max div 2;
 while (not did) and (min <= max) do
  begin
   //posicion:= (max - min) div 2 +1;
   seek(KeyFileHandle,posicion-1);
   read(KeyFileHAndle,KeyRecord);

   myword:= GetCleanString(uppercase(KeyRecord.word));
   comp := CompareStr(myword,word);


   If comp=0 then
     begin
      did := True;
      result := KeyRecord.Translate;
     end
     else
     If comp > 0
       then
        begin
         if max = 0 then did:= True;
         max := posicion-1; //Si es la del registro es mayor que la palabra
         posicion := min + ((max - min) div 2);
        end
       else
        begin
         min := posicion+1;
         posicion := min + ((max - min) div 2);
        end;
  end;

 CloseFile(KeyFileHandle);
end;

{ TKeymaker }

procedure TCorpusCODEC.AssignTranslateStrings;
Var
 FileHandle, KeyHandle, TknFileHandle : TextFile;
 Line : String;
 StrArray : TStringArray;
 
begin
//Asigno las cadenas de traduccion
  AssignFile(FileHandle,Appdir+'Temp\tipsFreqfile.txt');
  AssignFile(KeyHandle,Appdir+'Temp\keysfileFirstTranslateWords.txt');
  AssignFile(TknFileHandle,Appdir+'Temp\keysfileFirstOriginalWords.txt');

  Reset(FileHandle);
  Rewrite(KeyHandle);
  Rewrite(TknFileHandle);

  setlength(strarray,0);
  while not eof(FileHandle) do
   begin
    readln(FileHandle,Line);
    StrArray := splitString(Line,[#9]);
    writeln(TknFileHandle,GetCleanString(uppercase(Strarray[1]))+#9+translate_string);
    writeln(KeyHandle, translate_string +#9+ Strarray[1]);
    GenerateNewTranslateString;
   end;
  CloseFile(FileHandle);
  CloseFile(KeyHandle);
  CloseFile(TknFileHandle);
  //DeleteFile(Appdir+'Temp\tipsFreqfile.txt');
end;

procedure TCorpusCODEC.MAkeDecodeKeyFile;
Var
FileHAndle : TextFile;
KeyRecord : TKeyRecord;
KeyFileHandle : File of TKeyRecord;
Line : String;
StrArray : TStringArray;

begin
  //Creo el fichero de claves para decodificar
  AssignFile(FileHandle,Appdir+'Temp\Keysfile.txt');
  Reset(FileHandle);
  AssignFile(KeyFileHandle,key_Decode_file_Path);
  Rewrite(KeyFileHandle);
  setlength(strarray,0);
  KeyFileLines := 0;
  while not eof(FileHandle) do
   begin
    readln(FileHandle,Line);
    Line := uppercase(Line);
    StrArray := splitString(Line,[#9]);
    inc(KeyFileLines);
    KeyRecord.word      := StrArray[1];
    KeyRecord.Translate := StrArray[0];
    write(KeyFileHandle,KeyRecord);
   end;

  KeyRecord.word := InttoStr(KeyFileLines);
  KeyRecord.Translate := '';
  write(KeyFileHandle,KeyRecord);
  CloseFile(FileHandle);
  CloseFile(KeyFileHandle);
end;

procedure TCorpusCODEC.MakeEncodeKeyFile;
Var
FileHAndle : TextFile;
KeyRecord : TKeyRecord;
KeyFileHandle : File of TKeyRecord;
Line : String;
StrArray : TStringArray;
begin
//Creo el Archivo de claves para Codificar
  AssignFile(FileHandle,Appdir+'Temp\KeysSrtfile.txt');
  Reset(FileHandle);
  AssignFile(KeyFileHandle,key_Encode_file_Path);
  Rewrite(KeyFileHandle);
  setlength(strarray,0);
  KeyFileLines := 0;
  while not eof(FileHandle) do
   begin
    readln(FileHandle,Line);
    Line := uppercase(Line);
    StrArray := splitString(Line,[#9]);
    inc(KeyFileLines);
    KeyRecord.word      := StrArray[0];
    KeyRecord.Translate := StrArray[1];
    write(KeyFileHandle,KeyRecord);
   end;

  KeyRecord.word := InttoStr(KeyFileLines);
  KeyRecord.Translate := '';
  write(KeyFileHandle,KeyRecord);
  CloseFile(FileHandle);
  CloseFile(KeyFileHandle);
  //DeleteFile(Appdir+'Temp\KeysSrtfile.txt');
end;

procedure TCorpusCODEC.MakeTipsFiles;
Var
 Line,Temp,Aux : String;
 TknFileHandle, FileHandle : TextFile;
 totalfreq : LongWord;
begin
//Formo fichero de tipos y concateno todos los ficheros en uno general de tipos

 AssignFile(TknFileHandle,Appdir+'Temp\tipsfile.txt');
 Rewrite(TknFileHandle);
 AssignFile(FileHandle,Appdir+'Temp\tknsrtfile.txt');
 Reset(FileHandle);
 readln(FileHandle,Aux);
 Line := Aux;
 totalfreq:= 0;
 while not eof(FileHandle) do
  begin
   if Line = Aux
    then inc(totalfreq)
     else
      begin
        temp := inttostr(totalfreq);
        while length(temp) < Max_Digits_Freq_Count do
         temp := '0'+temp;
        writeln(TknFileHandle,temp+#9+Aux);
        totalfreq := 1;
        Aux := Line;
       end;
     readln(FileHandle,Line);
    end;
   if aux <> '' then
    begin
     temp := inttostr(totalfreq);
     while length(temp) < Max_Digits_Freq_Count do
      temp := '0'+temp;
     writeln(TknFileHandle,temp+#9+Aux);
    end;
   CloseFile(FileHandle);
   //DeleteFile(Appdir+'Temp\tknsrtfile.txt');
  CloseFile(TknFileHandle);
end;

procedure TCorpusCODEC.MakeTokensFiles;
Var
TknFileHandle : TextFile;
bool : boolean;
Aux, Line : String;
i : Word;
ordinal : byte;
begin
//Formo fichero de Tokens
 AssignFile(TknFileHandle,Appdir+'Temp\tknfile.txt');
 Rewrite(TknFileHandle);
 Reset(input_file);
 while not EOF(input_file) do
  begin
   readln(input_file,Line);

   //Aseguro que las lineas que contienen un delimitador de texto o parrafo queden iguales
   if (Text_Delimiter <> '') and (pos(Text_Delimiter,Line) <> 0)
    then Continue;

   if (Paragraph_Delimiter <> '') and (pos(Paragraph_Delimiter,Line) <> 0)
    then Continue;

   //elimino los caracteres indeseados
   Aux := '';
   bool := false;
   for i:=1 to length(Line) do
    begin
     ordinal := ord(Line[i]);
     case  ordinal of
      32,39,45                : Aux := Aux + Line[i];
      97..122,65..90,209,241  : begin
                                 bool := true;
                                 Aux := Aux + Line[i];
                                end;
     end;
    end;

  //Aseguro que las lineas vacias no se tomen en cuenta
    if not bool
     then Continue
     else Line := Aux;

   //Guardo los tokens en un fichero especifico
     i := pos(' ',Line);
     while i <> 0 do
       begin
         Aux := copy(Line,1,i-1);
         if (Aux<> '') and (Aux <> '-') and (Aux <> '''')
          then
           begin
            Writeln(TknFileHandle,uppercase(Aux));
           end;
         delete(Line,1,i);
         i := pos(' ',Line);
       end;
     if (length(Line) > 0) and (Line<> '') and (Line <> '-') and (Line <> '''')
      then
       begin
        writeln(TknFileHandle,uppercase(Line));
       end;
  end;
 CloseFile(TknFileHandle);
 CloseFile(input_file);
end;

procedure TCorpusCODEC.SortCodecKeyTempFile;
Var
 FileHAndle : TextFile;
 Line : String;
begin
//Ordeno el fichero para construir la clave de codificacion
 AssignFile(FileHandle,Appdir+'Temp\KeysSrtfile.txt');
 Rewrite(FileHandle);
 CloseFile(FileHandle);

 AssignFile(FileHandle,Appdir+'Temp\sorter.bat');
 Rewrite(FileHandle);
 Line := 'sort ' +
         ExtractShortPathName(Appdir+'Temp\keysfileFirstOriginalWords.txt') + ' /o ' +
         ExtractShortPathName(Appdir+'Temp\KeysSrtfile.txt');
  Writeln(FileHandle, Line);
  CloseFile(FileHandle);
  ExecConsoleApp(ExtractShortPathName(Appdir+'Temp\sorter.bat'));
  //DeleteFile(Appdir+'Temp\keysfileFirstOriginalWords.txt');
  DeleteFile(Appdir+'Temp\sorter.bat');
end;

procedure TCorpusCODEC.SortDecodeKeyTempFile;
Var
 FileHandle : TextFile;
 Line : String;
begin
 //Ordeno el fichero para construir la clave de decodificacion
 AssignFile(FileHandle,Appdir+'Temp\Keysfile.txt');
 Rewrite(FileHandle);
 CloseFile(FileHandle);

 AssignFile(FileHandle,Appdir+'Temp\sorter.bat');
 Rewrite(FileHandle);
 Line := 'sort ' +
         ExtractShortPathName(Appdir+'Temp\keysfileFirstTranslateWords.txt') + ' /o ' +
         ExtractShortPathName(Appdir+'Temp\Keysfile.txt');
  Writeln(FileHandle, Line);
  CloseFile(FileHandle);
  ExecConsoleApp(ExtractShortPathName(Appdir+'Temp\sorter.bat'));
  //DeleteFile(Appdir+'Temp\keysfileFirstTranslateWords.txt');
  DeleteFile(Appdir+'Temp\sorter.bat');
end;

procedure TCorpusCODEC.SortTipsFiles;
Var
 FileHAndle : TextFile;
 Line : String;
begin
 //Ordeno el fichero de tipos segun las frecuencias
 AssignFile(FileHandle,Appdir+'Temp\tipsFreqfile.txt');
 Rewrite(FileHandle);
 CloseFile(FileHandle);

 AssignFile(FileHandle,Appdir+'Temp\sorter.bat');
 Rewrite(FileHandle);
 Line := 'sort /R ' +
         ExtractShortPathName(Appdir+'Temp\tipsfile.txt') + ' /o ' +
         ExtractShortPathName(Appdir+'Temp\tipsFreqfile.txt');
  Writeln(FileHandle, Line);
  CloseFile(FileHandle);
  ExecConsoleApp(ExtractShortPathName(Appdir+'Temp\sorter.bat'));
  //DeleteFile(Appdir+'Temp\tipsfile.txt');
  DeleteFile(Appdir+'Temp\sorter.bat');
  if freq_file_path <> '' then copyfileto(Appdir+'Temp\tipsFreqfile.txt',freq_file_path);
end;

procedure TCorpusCODEC.SortTokensFiles;
Var
 FileHandle : TextFile;
 Line : String;
begin
 //Ordeno los fihceros de Tokens producidos anteriormente
 AssignFile(FileHandle,Appdir+'Temp\tknsrtfile.txt');
 Rewrite(FileHandle);
 CloseFile(FileHandle);

 AssignFile(FileHandle,Appdir+'Temp\sorter.bat');
 Rewrite(FileHandle);
 Line := 'sort ' +
         ExtractShortPathName(Appdir+'Temp\tknfile.txt') + ' /o ' +
         ExtractShortPathName(Appdir+'Temp\tknsrtfile.txt');
  Writeln(FileHandle, Line);
  CloseFile(FileHandle);
  ExecConsoleApp(ExtractShortPathName(Appdir+'Temp\sorter.bat'));
  //DeleteFile(Appdir+'Temp\tknfile.txt');
  DeleteFile(Appdir+'Temp\sorter.bat');
end;

function TCorpusCodec.GetCleanString(word1: String): String;
var
i:Word;
ordinal : byte;
begin
 result := '';
 for i:= 1 to length(word1) do
  begin
    ordinal := ord(word1[i]);
    if (ordinal >= 65) and (ordinal <= 90) then result:= result + word1[i];
  end;
end;

end.

