<?php

header('Access-Control-Allow-Origin: *');
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: text/html'); 

try{

$m = new MongoClient();
$db = $m->powerquebec;

$collection_new_data 				= $db->data;
$collection_analyzed_data 				= $db->data_analyzed_asis;
$collection_data_analyzed_year		= $db->data_analyzed_year;
$collection_data_analyzed_month		= $db->data_analyzed_month;
$collection_data_analyzed_day		= $db->data_analyzed_day;
$collection_data_analyzed_hour		= $db->data_analyzed_hour;
$collection_data_analyzed_minute	= $db->data_analyzed_minute;

$cursor         = $collection_new_data->find()->sort( array( 'datetime' => -1 ) );

$count = 0;
$hash  = '';


/*RETRIEVING NEW DATA INFO*/

echo "<p>Starting retrieving data from local mongoDB</p>";

$keys_to_analyze 	= array();
$type_keys			= array();//USED FOR CHOICE THE COLLECTION WHERE TO SAVE THE LECTURE

foreach ($cursor as $data) {
unset($data['_id']);
$datetime 			= $data["datetime"];
$time_year_id		= date("Y", intval($datetime));
$time_month_id		= date("Y-m", intval($datetime));
$time_day_id		= date("Y-m-d", intval($datetime));
$time_hour_id		= date("Y-m-d H", intval($datetime));
$time_minute_id		= date("Y-m-d H:i", intval($datetime));

$hash[$time_year_id][] 		= $data; 
$hash[$time_month_id][] 	= $data;
$hash[$time_day_id][] 		= $data;
$hash[$time_hour_id][] 		= $data;
$hash[$time_minute_id][] 	= $data;


if (!isset($keys_to_analyze[$time_year_id])) 	{$keys_to_analyze[$time_year_id] = 1;	$type_keys[$time_year_id] = "year";}
if (!isset($keys_to_analyze[$time_month_id])) 	{$keys_to_analyze[$time_month_id] = 1;	$type_keys[$time_month_id] = "month";}
if (!isset($keys_to_analyze[$time_day_id])) 	{$keys_to_analyze[$time_day_id] = 1;	$type_keys[$time_day_id] = "day";}
if (!isset($keys_to_analyze[$time_hour_id])) 	{$keys_to_analyze[$time_hour_id] = 1;	$type_keys[$time_hour_id] = "hour";}
if (!isset($keys_to_analyze[$time_minute_id])) 	{$keys_to_analyze[$time_minute_id] = 1;	$type_keys[$time_minute_id] = "minute";}

//INSERTING THE DATA IN THE BACKUP COLLECTION

$collection_analyzed_data->insert($data);
//REMOVING THE DATA ALRREADY ANALYZED
$collection_new_data->remove(array('datetime' => $datetime ));

		
}


//echo "<p>".json_encode($hash)."</p>";die();

$hash_boucle = $hash;
//ADD CURRENT EXISTING DATA IF EXISTING IN DB & DELETE IT IN THE CORRESPONDING COLLECTION
foreach($hash_boucle as $time_id=>$compacted){
		$type_time_id = $type_keys[$time_id];
		switch($type_time_id){
			case "year":
						$cursor = $collection_data_analyzed_year->find(array('time_id' => $time_id ));
						foreach($cursor as $item){unset($item['_id']);array_unshift($hash[$time_id], $item);}
						$collection_data_analyzed_year->remove(array('time_id' => $time_id ));
						break;
			case "month":
						$cursor = $collection_data_analyzed_month->find(array('time_id' => $time_id ));
						foreach($cursor as $item){unset($item['_id']);array_unshift($hash[$time_id], $item);}
						$collection_data_analyzed_month->remove(array('time_id' => $time_id ));
						break;			
			case "day":
						$cursor = $collection_data_analyzed_day->find(array('time_id' => $time_id ));
						foreach($cursor as $item){unset($item['_id']);array_unshift($hash[$time_id], $item);}
						$collection_data_analyzed_day->remove(array('time_id' => $time_id ));
						break;
			case "hour":
						$cursor = $collection_data_analyzed_hour->find(array('time_id' => $time_id ));
						foreach($cursor as $item){unset($item['_id']);array_unshift($hash[$time_id], $item);}
						$collection_data_analyzed_hour->remove(array('time_id' => $time_id ));
						break;
			case "minute":
						$cursor = $collection_data_analyzed_minute->find(array('time_id' => $time_id ));
						foreach($cursor as $item){unset($item['_id']);array_unshift($hash[$time_id], $item);}
						$collection_data_analyzed_minute->remove(array('time_id' => $time_id ));
						break;			
			
		};
}
	



$total_analyzed = count($keys_to_analyze);

echo "<p>Total keys to analyze: $total_analyzed item(s).</p>";
if ($total_analyzed == 0) {die();}


/*ANALYZING DATA DUPLICATIONS IN VARIABLE*/

echo "<p>Starting the compact process!</p>";


foreach($keys_to_analyze as $time_id=>$time_value){
	echo "<p>- Analyzing key '$time_id'</p>";


$total_items_to_compact 	= count($hash[$time_id]);
echo "<p>-- Total items to compact for $time_id: $total_items_to_compact</p>";
//echo'<pre>'.var_export($hash[$time_id]).'</pre>';die();

while (count($hash[$time_id]) > 1){
	
	$compacted 	= $hash[$time_id][0];
	$data 		= $hash[$time_id][count($hash[$time_id])-1];
	
	foreach($compacted as $key=>$val){	
		if ($key!='datetime_clear' && $key!='datetime' && $key!='_id' && $key!='time_id' && $key!='time_id_type'){
			if (isset($data[$key]) && !empty($data[$key])){
				if ($data[$key]['Min'] < $compacted[$key]['Min'])	$compacted[$key]['Min']		= $data[$key]['Min'];
				if ($data[$key]['Max'] > $compacted[$key]['Max'])	$compacted[$key]['Max']		= $data[$key]['Max'];
				$compacted[$key]['Last']	 	= $data[$key]['Last'];
				$compacted[$key]['Nbr']			= $data[$key]['Nbr'];
				$compacted[$key]['Avg']			= ($compacted[$key]['Avg']	+  $data[$key]['Avg'])/2;
				//$compacted[$key]['First']		= $data[$key]['First'];		
			}
		}else{//TO DO NOTHING THIS ONES ARE NOT VALUES
		}
	}//foreach

	//unset($hash[$time_id][1]);
	array_splice($hash[$time_id], -1, 1);
	$hash[$time_id][0] = $compacted;
}//while

//CORRECT THE AVERAGE
$hash[$time_id][0]["time_id"]  		= $time_id;
$hash[$time_id][0]["time_id_type"]  = $type_keys[$time_id];
$hash[$time_id]						= $hash[$time_id][0];

//echo'<pre>'.var_export($hash[$time_id]).'</pre>';die();
}//foreach


//echo json_encode($hash[$time_id]);die();
echo "<p>Finish the compacting process</p>";







echo "<p>Starting DB insertion to respective collection... & delecting from data collection</p>";
foreach($hash as $time_id=>$compacted){
		$type_time_id = $type_keys[$time_id];
		
		//var_dump($compacted);
		
		
		switch($type_time_id){
			case "year":
						$collection_data_analyzed_year->insert($compacted);
						break;
			case "month":
						$collection_data_analyzed_month->insert($compacted);
						break;			
			case "day":
						$collection_data_analyzed_day->insert($compacted);
						break;
			case "hour":
						$collection_data_analyzed_hour->insert($compacted);
						break;
			case "minute":
						$collection_data_analyzed_minute->insert($compacted);
						break;			
			
		};

}



}catch(Exception $e){
	echo "<p>".$e->message()."</p>";die();
}




