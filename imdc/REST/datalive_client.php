<?php

header('Access-Control-Allow-Origin: *');
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 2020 05:00:00 GMT');
header('Content-type: application/json'); 


try{

$m = new MongoClient();
$db = $m->powerquebec;
$collection = $db->datalive;
$collectionconfig = $db->config;

$cursor         = $collection->find();
$cursor_config  = $collectionconfig->find();

/*DATA TO RETURN*/
$json = array();


/* CLIENT DATA -- MUST BE OBTAINED FROM THE MYSQLDB OF IMDC*/
$json["detail_client"] = array();
$json["detail_client"]["logo"] = "http://192.168.2.25/lab/img/metroplus.jpg";
$json["detail_client"]["nom"] = "Metro Bourgeault";
$json["detail_client"]["address"] = array();
$json["detail_client"]["address"][0] = array();
$json["detail_client"]["address"][0]["street"] = "160 Rue Saint-Gabriel";
$json["detail_client"]["address"][0]["city"] = "Saint-Gabriel";
$json["detail_client"]["address"][0]["prov"] = "Qc";
$json["detail_client"]["address"][0]["pcode"] = "J0K 2N0";

$json["detail_client"]["contact"][0]["fonction"] = "Resp";
$json["detail_client"]["contact"][0]["nom"] = "Serge Bourgeault";
$json["detail_client"]["contact"][0]["phone"] = "450-835-4794";

$json["detail_client"]["contact"][1]["fonction"] = "Resp H-Q";
$json["detail_client"]["contact"][1]["nom"] = "Caroline Nadon";
$json["detail_client"]["contact"][1]["phone"] = "1-800-463-9900 ext 8245";


foreach ($cursor_config as $data) {
/*WM40 DATA*/
$json["E1"]["name"] 						= "Oven";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar1"]["name"]
$json["E1"]["powerConsumptionReal"] 		= "37";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar1"][""]?
$json["E1"]["powerConsumptionAllowed"] 		= "37";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar1"]["kw_signal"]/255*100?
$json["E1"]["powerConsumptionUnit"] 		= "Kw";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar1"][""]?
$json["E1"]["powerConsumptionRealPercent"] 	= round($json["E1"]["powerConsumptionReal"]/$json["E1"]["powerConsumptionAllowed"]*100,2);  
$json["E1"]["temperatureCelcius"] 			= "25";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar1"]["temperature"]		
$json["E1"]["active"] 						= "0";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar1"]["active"]		

$json["E2"]["name"] 						= "Entry";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar2"]["name"]
$json["E2"]["powerConsumptionReal"] 		= "25.9";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar2"][""]?
$json["E2"]["powerConsumptionAllowed"] 		= "37";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar2"]["kw_signal"]/255*100?
$json["E2"]["powerConsumptionUnit"] 		= "Kw";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar2"][""]?
$json["E2"]["powerConsumptionRealPercent"] 	= round($json["E2"]["powerConsumptionReal"]/$json["E2"]["powerConsumptionAllowed"]*100,2);  
$json["E2"]["temperatureCelcius"] 			= "25";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar2"]["temperature"]		
$json["E2"]["active"] 						= "1";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar1"]["active"]

$json["E3"]["name"] 						= "Warehouse";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar3"]["name"]
$json["E3"]["powerConsumptionReal"] 		= "7.4";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar3"][""]?
$json["E3"]["powerConsumptionAllowed"] 		= "28";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar3"]["kw_signal"]/255*100?
$json["E3"]["powerConsumptionUnit"] 		= "Kw";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar3"][""]?
$json["E3"]["powerConsumptionRealPercent"] 	= round($json["E3"]["powerConsumptionReal"]/$json["E3"]["powerConsumptionAllowed"]*100,2);  
$json["E3"]["temperatureCelcius"] 			= "25";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar3"]["temperature"]	
$json["E3"]["active"] 						= "0";  	//IDEALLY IT WILL BE TAKEN FROM $data["ar1"]["active"]

$json["TOTAL"]["name"] 							= "Total";  	
$json["TOTAL"]["powerConsumptionReal"] 			= round($json["E1"]["powerConsumptionReal"] + $json["E2"]["powerConsumptionReal"]  +$json["E3"]["powerConsumptionReal"],2) ;  	
$json["TOTAL"]["powerConsumptionAllowed"] 		= round($json["E1"]["powerConsumptionAllowed"]+$json["E2"]["powerConsumptionAllowed"]+$json["E3"]["powerConsumptionAllowed"],2);  	
$json["TOTAL"]["powerConsumptionUnit"] 			= "Kw";  	
$json["TOTAL"]["powerConsumptionRealPercent"] 	= round($json["TOTAL"]["powerConsumptionReal"]/$json["TOTAL"]["powerConsumptionAllowed"]*100,2);  
}






foreach ($cursor as $data) {
/*READING DATA*/
$json['clock'] = 1; //IDEALLY SOME PARAMETER IN THE DATA 
$json["datetime"] = date("d.m.Y H:i:s", $data["datetime_clear"]);
$json["date"] = date("Y-m-d", $data["datetime_clear"]);
$json["time"] = date("H:i:s", $data["datetime_clear"]);

$json["time_hour"] = date("h", $data["datetime_clear"]);
$json["time_minute"] = date("i", $data["datetime_clear"]);
$json["time_second"] = date("s", $data["datetime_clear"]);



$json["real_power"]			= array(
		"value"=>rand(0,37),//round($data["W E"]["Avg"]/1000,2),
		"unit"=>"Kw",
		"notation"=>"P",
		
);


$json["power_consumption"]			= array(
		"value"=>rand(0,60),//"...", //WAITING FOR DATA
		"unit"=>"Kwh",
		"notation"=>"P",
		
);


$json["apparent_power"]			= array(
		"value"=>round($data["VA E"]["Avg"]/1000,2),
		"unit"=>"KVA",
		"notation"=>"S",
		
);


$json["power_factor"]			= array(
		"value"=>round($data["VA E"]["Avg"],2),
		"unit"=>"PF",
		"notation"=>"PF",
		
);



$json["utilisation_factor"]			= array(
		"value"=>(empty($json["real_power"]["value"]) || empty($json["apparent_power"]["value"]) )?0:$json["real_power"]["value"]/$json["apparent_power"]["value"]*100,
		"unit"=>"FU",
		"notation"=>"FU",
		
);

$json["tension"]			= array(
		"value"=>round($data["V L-L E"]["Avg"],2),
		"unit"=>"V",
		"notation"=>"V",
		
);


$json["frequency"]			= array(
		"value"=>round($data["Hz"]["Avg"],2),
		"unit"=>"Hz",
		"notation"=>"Hz",
		
);


$json["total_harmonic_distorsion"]			= array(
		"value"=>"s3",// Ideally it will calculated using champs 'THD TOT VL - 12' +  'THD TOT VL - 23' + 'THD TOT VL - 31' if > 5% red
		"unit"=>"%THD",
		"notation"=>"THD",
		
);


$json["total_current_demand_distorsion"]			= array(
		"value"=>"s5",// Ideally it will calculated using champs 'THD TOT AL1' +  'THD TOT AL2' + 'THD TOT AL3' if > 10% red
		"unit"=>"%TDD",
		"notation"=>"TDD",
		
);


$json["k_factor"]			= array(
		"value"=>round($data["K-Factor L1"]["Avg"]+$data["K-Factor L2"]["Avg"]+$data["K-Factor L3"]["Avg"],2),
		"unit"=>"K_FA",
		"notation"=>"K-FA",
		
);


    
}


}catch(Exception $e){
	echo $e->message();die();
}

	echo json_encode($json);




