<?php

header('Access-Control-Allow-Origin: *');
header('Cache-Control: no-cache, must-revalidate');
header('Expires: Mon, 26 Jul 2020 05:00:00 GMT');
header('Content-type: application/json'); 


try{
/*	SAMPLE DATA	*/

$variable 		= @$_GET['var'];
$frequence 		= @$_GET['freq'];	/*FREQUENCEyear,month,day,hour,minute*/
$entry 			= @$_GET['entry'];	/*ENTRYe1,e2,e3,total*/
if (isset($_GET['from']) && !empty($_GET['from']))	$time_from      = strtotime(@$_GET['from']);
if (isset($_GET['to']) && !empty($_GET['to']))		$time_to	    = strtotime(@$_GET['to']);//+60*60*24;


$LIMIT			= 60; /*COLLECTION LIMIT FOR ITEMS*/

//var_dump(array($time_from,$time_to));die();

/****VARIABLE
 power_consumption,
 power_apparent,
 power_factor,
 utilisation_factor,
 tension,
 frequency,
 thd,
 tcdd,
 k-factor
*/


//CREATION THE COLLECTION DATA

$m = new MongoClient();
$db = $m->powerquebec;

switch($frequence){
	case "year":
		$collection		= $db->data_analyzed_year;
		break;
	case "month":
		$collection		= $db->data_analyzed_month;
		break;		
	case "day":
		$collection		= $db->data_analyzed_day;
		break;	
	case "hour":
		$collection		= $db->data_analyzed_hour;
		break;	
	case "minute":
		$collection		= $db->data_analyzed_minute;
		break;			
}		

//var_dump($collection);die();
if(!isset($collection)) {echo"Empty collection" ;die();}


//SELECTING DATA FROM DB

if(isset($time_from ) && !empty($time_from) && isset($time_to ) && !empty($time_to)){
	//SEARCH DATA IN A TIME RANGE
	$cursor         = $collection->find( array('datetime' => array( '$gte' => $time_from, '$lte' => $time_to )) )->limit( $LIMIT )->sort( array( 'datetime' => 1 ) );
}elseif(isset($time_from ) && !empty($time_from)){
	//SEARCH DATA FROM A DATE
	$cursor         = $collection->find( array('datetime' => array( '$gte' => $time_from)))->limit( $LIMIT )->sort( array( 'datetime' => 1 ) );
}elseif(isset($time_to ) && !empty($time_to)){
	//SEARCH DATA UNTIL A DATE
	$cursor         = $collection->find( array('datetime' => array( '$lte' => $time_to )) )->limit( $LIMIT )->sort( array( 'datetime' => 1 ) );
}else{
	//SEARCH DATA WITHOUT A DATE SPECIFICATION
	$cursor         = $collection->find()->limit( $LIMIT )->sort( array( 'datetime' => 1 ) );
};


//START PREPARING THE ARRAY TO RESULT
$ranges 	= array();
$averages	= array();
$categories	= array();

foreach($cursor as $item){

$avg 		= '';
$min 		= '';
$max 		= '';
$datetime  	= intval($item['datetime']);
$datetime 	= date("l Y-m-d H:i:s",$datetime);//str_pad($datetime, 13,0); 


switch($frequence){
	case "year":
		$categories[] 	= date("Y",$item['datetime']); 
		break;
	case "month":
		$categories[] 	= date("Y-m",$item['datetime']);
		break;		
	case "day":
		$categories[] 	= date("Y-m-d",$item['datetime']);
		break;	
	case "hour":
		$categories[] 	= date("Y-m-d H",$item['datetime']);
		break;	
	case "minute":
		$categories[] 	= date("Y-m-d H:i",$item['datetime']);
		break;			
}


$suffix		= '';
$messure	= '';

switch($variable){
	case "power_consumption":
		//TO COME WHEN DATA AVAILABLE IN COLLECTION: DIFFERENT CALCUL FOR EACH ENTRY TYPE
		$flag = "W E";
		
		$avg = round($item['W E']['Avg'] / 1000,2);
		$max = round($item['W E']['Max'] / 1000,2);
		$min = round($item['W E']['Min'] / 1000,2);
		
		$suffix		= 'Kw';
		$messure	= 'Power Consumption';
		
		break;
	case "power_apparent":
		//TO COME WHEN DATA AVAILABLE IN COLLECTION: DIFFERENT CALCUL FOR EACH ENTRY TYPE
		$flag = "VA E";
		
		$avg = round($item['VA E']['Avg'] /1000,2);
		$max = round($item['VA E']['Max'] /1000,2);
		$min = round($item['VA E']['Min'] /1000,2);
		
		$suffix		= 'KVA';
		$messure	= 'Apparent Power';
		
		break;
	case "power_factor":
		//TO COME WHEN DATA AVAILABLE IN COLLECTION: DIFFERENT CALCUL FOR EACH ENTRY TYPE
		$flag = "PF E";
		
		$avg = round(@$item['PF E']['Avg'],2);
		$max = round(@$item['PF E']['Max'],2);
		$min = round(@$item['PF E']['Min'],2);
		
		$suffix		= 'PF';
		$messure	= 'Power Factor';
		
		break;
	case "utilisation_factor":
		//TO COME WHEN DATA AVAILABLE IN COLLECTION: DIFFERENT CALCUL FOR EACH ENTRY TYPE
		$flag = "UF";
		
		$avg = round($item['W E']['Avg'] / $item['VA E']['Avg'] / 1000 * 100,2); //%power consumed from apparent power
		$max = round($item['W E']['Max'] / $item['VA E']['Max'] / 1000 * 100,2); //%power consumed from apparent power
		$min = round($item['W E']['Min'] / $item['VA E']['Min'] / 1000 * 100,2); //%power consumed from apparent power
		
		$suffix		= '%';
		$messure	= 'Utilisation Factor';
		
		break;
	case "tension":
		//TO COME WHEN DATA AVAILABLE IN COLLECTION: DIFFERENT CALCUL FOR EACH ENTRY TYPE
		$flag = "V L-L E";
		
		$avg = round($item['V L-L E']['Avg'],2);
		$max = round($item['V L-L E']['Max'],2);
		$min = round($item['V L-L E']['Min'],2);
		
		$suffix		= 'V';
		$messure	= 'Tension';
		
		break;
	case "frequency":
		//TO COME WHEN DATA AVAILABLE IN COLLECTION: DIFFERENT CALCUL FOR EACH ENTRY TYPE
		$flag = "Hz";
		
		$avg = round($item['Hz']['Avg'],2);
		$max = round($item['Hz']['Max'],2);
		$min = round($item['Hz']['Min'],2);
		
		$suffix		= 'Hz';
		$messure	= 'Frequency';
		
		break;	
	case "thd":
		//TO COME WHEN DATA AVAILABLE IN COLLECTION: DIFFERENT CALCUL FOR EACH ENTRY TYPE
		$flag = "";//TO INCLUDE
		
		$avg = round($item[''][''],2);
		$max = round($item[''][''],2);
		$min = round($item[''][''],2);
		
		$suffix		= 'THD';
		$messure	= 'Total Harmonic Distorsion';
		
		break;
	case "tcdd":
		//TO COME WHEN DATA AVAILABLE IN COLLECTION: DIFFERENT CALCUL FOR EACH ENTRY TYPE
		$flag = "";//TO INCLUDE 'TDD TOT AL1', 'TDD TOT AL2' 
		
		$avg = round($item[''][''],2);
		$max = round($item[''][''],2);
		$min = round($item[''][''],2);
		
		$suffix		= 'TDD';
		$messure	= 'Total Current Demand Distorsion';
		break;
	case "k-factor":
		$flag = "K-FACTOR L1";
		$suffix		= 'K-FA';
		$messure	= 'K-FACTOR';
		
		switch($entry){
			case "e1":
				$avg = round($item['K-FACTOR L1']['Avg'],2);
				$max = round($item['K-FACTOR L1']['Max'],2);
				$min = round($item['K-FACTOR L1']['Min'],2);
				break;
			case "e2":
				$avg = round($item['K-FACTOR L2']['Avg'],2);
				$max = round($item['K-FACTOR L2']['Max'],2);
				$min = round($item['K-FACTOR L2']['Min'],2);
				break;		
			case "e3":
				$avg = round($item['K-FACTOR L3']['Avg'],2);
				$max = round($item['K-FACTOR L3']['Max'],2);
				$min = round($item['K-FACTOR L3']['Min'],2);
				break;	
			case "total":
				$avg = round(($item['K-FACTOR L1']['Avg']+$item['K-FACTOR L2']['Avg']+$item['K-FACTOR L3']['Avg'])/3 ,2);
				$max = round(max($item['K-FACTOR L1']['Max'],$item['K-FACTOR L2']['Max'],$item['K-FACTOR L3']['Max']),2);
				$min = round(min($item['K-FACTOR L1']['Min'],$item['K-FACTOR L2']['Min'],$item['K-FACTOR L3']['Min']),2);
				break;		
		}
		break;	
		
}//switch	

$ranges[] 	= array($datetime,$min,$max);
$averages[]	= array($datetime,$avg);

	
	
}//foreach




/*RETURN THE REQUESTED ARRAY*/

echo json_encode(array("ranges"=>$ranges,"averages"=>$averages,"suffix"=>$suffix, "messure"=>$messure, "categories"=>$categories));


}catch(Exception $e){
	echo $e->getMessage();die();
}












































/*


JSON SAMPLE

var ranges = [
            [1246406400000, 14.3, 27.7],
            [1246492800000, 14.5, 27.8],
            [1246579200000, 15.5, 29.6],
            [1246665600000, 16.7, 30.7],
            [1246752000000, 16.5, 25.0],
            [1246838400000, 17.8, 25.7],
            [1246924800000, 13.5, 24.8],
            [1247011200000, 10.5, 21.4],
            [1247097600000, 9.2, 23.8],
            [1247184000000, 11.6, 21.8],
            [1247270400000, 10.7, 23.7],
            [1247356800000, 11.0, 23.3],
            [1247443200000, 11.6, 23.7],
            [1247529600000, 11.8, 20.7],
            [1247616000000, 12.6, 22.4],
            [1247702400000, 13.6, 19.6],
            [1247788800000, 11.4, 22.6],
            [1247875200000, 13.2, 25.0],
            [1247961600000, 14.2, 21.6],
            [1248048000000, 13.1, 17.1],
            [1248134400000, 12.2, 15.5],
            [1248220800000, 12.0, 20.8],
            [1248307200000, 12.0, 17.1],
            [1248393600000, 12.7, 18.3],
            [1248480000000, 12.4, 19.4],
            [1248566400000, 12.6, 19.9],
            [1248652800000, 11.9, 20.2],
            [1248739200000, 11.0, 19.3],
            [1248825600000, 10.8, 17.8],
            [1248912000000, 11.8, 18.5],
            [1248998400000, 10.8, 16.1]
        ],
        averages = [
            [1246406400000, 21.5],
            [1246492800000, 22.1],
            [1246579200000, 23],
            [1246665600000, 23.8],
            [1246752000000, 21.4],
            [1246838400000, 21.3],
            [1246924800000, 18.3],
            [1247011200000, 15.4],
            [1247097600000, 16.4],
            [1247184000000, 17.7],
            [1247270400000, 17.5],
            [1247356800000, 17.6],
            [1247443200000, 17.7],
            [1247529600000, 16.8],
            [1247616000000, 17.7],
            [1247702400000, 16.3],
            [1247788800000, 17.8],
            [1247875200000, 18.1],
            [1247961600000, 17.2],
            [1248048000000, 14.4],
            [1248134400000, 13.7],
            [1248220800000, 15.7],
            [1248307200000, 14.6],
            [1248393600000, 15.3],
            [1248480000000, 15.3],
            [1248566400000, 15.8],
            [1248652800000, 15.2],
            [1248739200000, 14.8],
            [1248825600000, 14.4],
            [1248912000000, 15],
            [1248998400000, 13.6]
        ];






*/
?>

