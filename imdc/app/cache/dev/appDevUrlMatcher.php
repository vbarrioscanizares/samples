<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/client')) {
            // app_bills_index
            if (0 === strpos($pathinfo, '/client_bills') && preg_match('#^/client_bills/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_bills_index')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::indexAction',));
            }

            if (0 === strpos($pathinfo, '/clients_bills_')) {
                // app_bills_edit
                if (0 === strpos($pathinfo, '/clients_bills_edit') && preg_match('#^/clients_bills_edit/(?P<client_id>[^/]++)/(?P<bill_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_bills_edit')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::editAction',));
                }

                // app_bills_delete
                if (0 === strpos($pathinfo, '/clients_bills_delete') && preg_match('#^/clients_bills_delete/(?P<client_id>[^/]++)/(?P<bill_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_bills_delete')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::deleteAction',));
                }

                if (0 === strpos($pathinfo, '/clients_bills_create')) {
                    // app_bills_createm
                    if (0 === strpos($pathinfo, '/clients_bills_create_m') && preg_match('#^/clients_bills_create_m/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_bills_createm')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::createmAction',));
                    }

                    // app_bills_create
                    if (preg_match('#^/clients_bills_create/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_bills_create')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::createAction',));
                    }

                }

                // app_bills_general
                if (0 === strpos($pathinfo, '/clients_bills_general') && preg_match('#^/clients_bills_general/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_bills_general')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::generalAction',));
                }

            }

        }

        // app_chart_index
        if (preg_match('#^/(?P<_locale>[^/]++)/chart(?:/(?P<var>[^/]++)(?:/(?P<entry>[^/]++)(?:/(?P<freq>[^/]++)(?:/(?P<ip_raspberry>[^/]++))?)?)?)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_chart_index')), array (  'var' => 'p',  'entry' => 'total',  'freq' => 'month',  'ip_raspberry' => '192.168.2.10',  'from' => '',  'to' => '',  '_controller' => 'AppBundle\\Controller\\ChartController::indexAction',));
        }

        if (0 === strpos($pathinfo, '/clients')) {
            // app_clients_index
            if ($pathinfo === '/clients') {
                return array (  '_controller' => 'AppBundle\\Controller\\ClientsController::indexAction',  '_route' => 'app_clients_index',);
            }

            if (0 === strpos($pathinfo, '/clients_')) {
                // app_clients_edit
                if (0 === strpos($pathinfo, '/clients_edit') && preg_match('#^/clients_edit/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_clients_edit')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::editAction',));
                }

                // app_clients_delete
                if (0 === strpos($pathinfo, '/clients_delete') && preg_match('#^/clients_delete/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_clients_delete')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::deleteAction',));
                }

                // app_clients_create
                if ($pathinfo === '/clients_create') {
                    return array (  '_controller' => 'AppBundle\\Controller\\ClientsController::createAction',  '_route' => 'app_clients_create',);
                }

                // app_clients_board
                if (0 === strpos($pathinfo, '/clients_board') && preg_match('#^/clients_board/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_clients_board')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::boardAction',));
                }

                // app_clients_chart
                if (0 === strpos($pathinfo, '/clients_chart') && preg_match('#^/clients_chart(?:/(?P<client_id>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_clients_chart')), array (  'client_id' => '',  'var' => 'power_consumption',  'entry' => 'total',  'freq' => 'day',  'ip_raspberry' => '',  'from' => '',  'to' => '',  '_controller' => 'AppBundle\\Controller\\ClientsController::chartAction',));
                }

                // app_clients_presets
                if (0 === strpos($pathinfo, '/clients_presets') && preg_match('#^/clients_presets/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_clients_presets')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::presetsAction',));
                }

            }

        }

        if (0 === strpos($pathinfo, '/active_power_presets')) {
            // app_clients_powerpresets
            if (preg_match('#^/active_power_presets/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_clients_powerpresets')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::powerpresetsAction',));
            }

            // app_clients_powerpresetsedit
            if (0 === strpos($pathinfo, '/active_power_presets_edit') && preg_match('#^/active_power_presets_edit/(?P<client_id>[^/]++)/(?P<bill_id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_clients_powerpresetsedit')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::powerpresetseditAction',));
            }

        }

        if (0 === strpos($pathinfo, '/clients_alarms')) {
            // app_clients_alarms
            if (preg_match('#^/clients_alarms/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_clients_alarms')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::alarmsAction',));
            }

            // app_clients_alarms_reset
            if (0 === strpos($pathinfo, '/clients_alarms_reset') && preg_match('#^/clients_alarms_reset/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_clients_alarms_reset')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::alarms_resetAction',));
            }

        }

        // app_login_index
        if (rtrim($pathinfo, '/') === '/login') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'app_login_index');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\LoginController::indexAction',  '_route' => 'app_login_index',);
        }

        // app_login_call
        if (rtrim($pathinfo, '/') === '/call') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'app_login_call');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\LoginController::callAction',  '_route' => 'app_login_call',);
        }

        // app_sample_index
        if (rtrim($pathinfo, '/') === '/sample') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'app_sample_index');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\SampleController::indexAction',  '_route' => 'app_sample_index',);
        }

        if (0 === strpos($pathinfo, '/users')) {
            // app_users_index
            if ($pathinfo === '/users') {
                return array (  '_controller' => 'AppBundle\\Controller\\UsersController::indexAction',  '_route' => 'app_users_index',);
            }

            if (0 === strpos($pathinfo, '/users_')) {
                // app_users_edit
                if (0 === strpos($pathinfo, '/users_edit') && preg_match('#^/users_edit/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_users_edit')), array (  '_controller' => 'AppBundle\\Controller\\UsersController::editAction',));
                }

                // app_users_delete
                if (0 === strpos($pathinfo, '/users_delete') && preg_match('#^/users_delete/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_users_delete')), array (  '_controller' => 'AppBundle\\Controller\\UsersController::deleteAction',));
                }

                // app_users_create
                if ($pathinfo === '/users_create') {
                    return array (  '_controller' => 'AppBundle\\Controller\\UsersController::createAction',  '_route' => 'app_users_create',);
                }

            }

        }

        // hello
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'hello')), array (  '_controller' => 'AppBundle:Hello:index',));
        }

        // _root
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_root');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\UsersController::indexAction',  '_route' => '_root',);
        }

        // sample
        if (preg_match('#^/(?P<_locale>en|fr|it)/sample/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'sample');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sample')), array (  '_controller' => 'AppBundle\\Controller\\SampleController::indexAction',));
        }

        // _login
        if (preg_match('#^/(?P<_locale>en|fr|it)/login/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_login');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => '_login')), array (  '_controller' => 'AppBundle\\Controller\\LoginController::indexAction',));
        }

        // _login_call
        if (preg_match('#^/(?P<_locale>en|fr|it)/call/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_login_call');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => '_login_call')), array (  '_controller' => 'AppBundle\\Controller\\LoginController::callAction',));
        }

        // _users
        if (preg_match('#^/(?P<_locale>en|fr|it)/users/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_users');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => '_users')), array (  '_controller' => 'AppBundle\\Controller\\UsersController::indexAction',));
        }

        // _users_edit
        if (preg_match('#^/(?P<_locale>en|fr|it)/users_edit/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_users_edit')), array (  '_controller' => 'AppBundle\\Controller\\UsersController::editAction',));
        }

        // _users_create
        if (preg_match('#^/(?P<_locale>en|fr|it)/users_create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_users_create')), array (  '_controller' => 'AppBundle\\Controller\\UsersController::createAction',));
        }

        // _users_delete
        if (preg_match('#^/(?P<_locale>en|fr|it)/users_delete/(?P<user_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_users_delete')), array (  '_controller' => 'AppBundle\\Controller\\UsersController::deleteAction',));
        }

        // _clients
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_clients');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::indexAction',));
        }

        // _chart
        if (preg_match('#^/(?P<_locale>en|fr|it)/chart(?:/(?P<var>[^/]++)(?:/(?P<entry>[^/]++)(?:/(?P<freq>[^/]++)(?:/(?P<ip_raspberry>[^/]++)(?:/(?P<from>[^/]++)(?:/(?P<to>[^/]++))?)?)?)?)?)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_chart')), array (  '_controller' => 'AppBundle\\Controller\\ChartController::indexAction',  'var' => 'p',  'entry' => 'total',  'freq' => 'month',  'ip_raspberry' => '192.168.2.25',  'from' => '',  'to' => '',));
        }

        // _clients_edit
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_edit/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_edit')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::editAction',));
        }

        // _clients_create
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_create')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::createAction',));
        }

        // _clients_delete
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_delete/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_delete')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::deleteAction',));
        }

        // _clients_board
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_board/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_board')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::boardAction',));
        }

        // _clients_chart
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_chart(?:/(?P<client_id>[^/]++)(?:/(?P<var>[^/]++)(?:/(?P<entry>[^/]++)(?:/(?P<freq>[^/]++)(?:/(?P<ip_raspberry>[^/]++)(?:/(?P<from>[^/]++)(?:/(?P<to>[^/]++))?)?)?)?)?)?)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_chart')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::chartAction',  'client_id' => '',  'var' => 'power_consumption',  'entry' => 'total',  'freq' => 'day',  'ip_raspberry' => '',  'from' => '',  'to' => '',));
        }

        // _clients_presets
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_presets/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_presets')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::presetsAction',));
        }

        // _clients_bills
        if (preg_match('#^/(?P<_locale>en|fr|it)/client_bills/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_bills')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::indexAction',));
        }

        // _clients_bills_edit
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_bills_edit/(?P<client_id>[^/]++)/(?P<bill_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_bills_edit')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::editAction',));
        }

        // _clients_bills_create
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_bills_create/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_bills_create')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::createAction',));
        }

        // _clients_bills_create_m
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_bills_create_m/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_bills_create_m')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::createmAction',));
        }

        // _clients_bills_delete
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_bills_delete/(?P<client_id>[^/]++)/(?P<bill_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_bills_delete')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::deleteAction',));
        }

        // _clients_bills_general
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_bills_general/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_bills_general')), array (  '_controller' => 'AppBundle\\Controller\\BillsController::generalAction',));
        }

        // _active_power_presets
        if (preg_match('#^/(?P<_locale>en|fr|it)/active_power_presets/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_active_power_presets')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::powerpresetsAction',));
        }

        // _active_power_presets_edit
        if (preg_match('#^/(?P<_locale>en|fr|it)/active_power_presets_edit/(?P<client_id>[^/]++)/(?P<bill_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_active_power_presets_edit')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::powerpresetseditAction',));
        }

        // _clients_alarms
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_alarms/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_alarms')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::alarmsAction',));
        }

        // _clients_alarms_reset
        if (preg_match('#^/(?P<_locale>en|fr|it)/clients_alarms_reset/(?P<client_id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_clients_alarms_reset')), array (  '_controller' => 'AppBundle\\Controller\\ClientsController::alarms_resetAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
