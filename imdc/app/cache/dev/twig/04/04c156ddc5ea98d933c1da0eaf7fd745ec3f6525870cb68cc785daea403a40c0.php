<?php

/* users_edit.html.twig */
class __TwigTemplate_a872fd9d1cbc381352fa1c275c212b9ae9814e5bae0da3d1d73329648b13bda7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "users_edit.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_63fb040c149a20730cc45bd93bc6282d35de36518b49839b7c03d5d958177ddc = $this->env->getExtension("native_profiler");
        $__internal_63fb040c149a20730cc45bd93bc6282d35de36518b49839b7c03d5d958177ddc->enter($__internal_63fb040c149a20730cc45bd93bc6282d35de36518b49839b7c03d5d958177ddc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users_edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_63fb040c149a20730cc45bd93bc6282d35de36518b49839b7c03d5d958177ddc->leave($__internal_63fb040c149a20730cc45bd93bc6282d35de36518b49839b7c03d5d958177ddc_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_6985e268f361885734e2df2403fb032bf0261fef617d6b1de13aa7ac30001be9 = $this->env->getExtension("native_profiler");
        $__internal_6985e268f361885734e2df2403fb032bf0261fef617d6b1de13aa7ac30001be9->enter($__internal_6985e268f361885734e2df2403fb032bf0261fef617d6b1de13aa7ac30001be9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
";
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb_reduced.html.twig", "users_edit.html.twig", 13)->display($context);
        // line 14
        echo "    <div class=\"col-md-12 main\">
          <h1 class=\"page-header\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Users"), "html", null, true);
        echo "</h1>
          
          <form  action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_users_edit", array("user_id" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "id", array()))), "html", null, true);
        echo "\" method=\"POST\">
            
            <div class=\"form-group\">
                <label for=\"fullname\">";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Logo"), "html", null, true);
        echo "</label><br/>
                <img src=\"http://www.gravatar.com/avatar/";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["hash"]) ? $context["hash"] : $this->getContext($context, "hash")), "html", null, true);
        echo "?s=48\"  alt=\"User Image\">  <br/>
                <a target=\"_blank\" href=\"https://gravatar.com\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Click here"), "html", null, true);
        echo "</a> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to modify your logo!"), "html", null, true);
        echo "
            </div>
            
            
            <div class=\"form-group\">
              <label for=\"fullname\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fullname"), "html", null, true);
        echo "</label>
              <input name=\"fullname\" value=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "fullname", array()), "html", null, true);
        echo "\" data-validation=\"required\" type=\"text\" class=\"form-control\" id=\"fullname\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fullname"), "html", null, true);
        echo "\"  data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">
            </div>
            
            <div class=\"form-group\">
              <label for=\"email\">";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
              <input name=\"email\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "\"  data-validation=\"email\" type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "\" data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not given a correct e-mail address"), "html", null, true);
        echo "\">
            </div>
              
              
            <div class=\"form-group\">
              <label for=\"password\">";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Password"), "html", null, true);
        echo "</label>
              <input name=\"password\" data-validation=\"required\" type=\"password\" class=\"form-control\" id=\"password\" placeholder=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Password"), "html", null, true);
        echo "\"  data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">
            </div>
  
              
              
              
            <div class=\"form-group\">
              <label for=\"address\">";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Address"), "html", null, true);
        echo "</label>
              <input value=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "address", array()), "html", null, true);
        echo "\" name=\"address\" type=\"text\" class=\"form-control\" id=\"address\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Address"), "html", null, true);
        echo "\">
            </div>
              
              
            <div class=\"form-group\">
              <label for=\"phone\">";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Phone"), "html", null, true);
        echo "</label>
              <input name=\"phone\" value=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "phone", array()), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"phone\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Phone"), "html", null, true);
        echo "\">
            </div>
            
            
            ";
        // line 57
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "role"), "method") == "ROLE_ADMIN")) {
            // line 58
            echo "            
            <div class=\"form-group\" style=\"width:50px;\">
              <label for=\"active\">";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Active"), "html", null, true);
            echo "</label>
              <input name=\"active\" ";
            // line 61
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "active", array()) == "1")) {
                echo "checked=\"checked\"";
            }
            echo " value=\"1\" type=\"checkbox\" class=\"form-control\" id=\"active\" >
            </div>
            
            <div class=\"form-group\">
              <label for=\"role\">";
            // line 65
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Role"), "html", null, true);
            echo "</label>
              <select name=\"role\" id=\"role\" class=\"form-control\" >
                  <option ";
            // line 67
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "role", array()) == "ROLE_ADMIN")) {
                echo "selected=\"selected\"";
            }
            echo " value=\"ROLE_ADMIN\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Admin"), "html", null, true);
            echo "</option>
                  <option ";
            // line 68
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "role", array()) == "ROLE_CLIENT")) {
                echo "selected=\"selected\"";
            }
            echo "value=\"ROLE_CLIENT\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Client"), "html", null, true);
            echo "</option>
                  <option ";
            // line 69
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "role", array()) == "ROLE_GUEST")) {
                echo "selected=\"selected\"";
            }
            echo "value=\"ROLE_GUEST\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guest"), "html", null, true);
            echo "</option>
                  
              </select>
              
            </div>

                
            <div class=\"form-group\" id=\"role_client\" ";
            // line 76
            if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "role", array()) != "ROLE_CLIENT")) {
                echo "style=\"display:none;\"";
            }
            echo ">
              <label for=\"clients\">";
            // line 77
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clients"), "html", null, true);
            echo "</label>
               <select multiple=\"multiple\" name=\"clients[]\" id=\"clients\" style=\"width:250px;\" >
                    ";
            // line 79
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["clients_list"]) ? $context["clients_list"] : $this->getContext($context, "clients_list")));
            foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
                // line 80
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "name", array()), "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo " 
                   
                </select>
                
              
            </div>
            
            ";
        }
        // line 88
        echo "       
            
            
            
           
            <input type=\"submit\" class=\"btn btn-primary btn-lg pull-right\" value=\"";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\"/>
          </form>
          
          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_6985e268f361885734e2df2403fb032bf0261fef617d6b1de13aa7ac30001be9->leave($__internal_6985e268f361885734e2df2403fb032bf0261fef617d6b1de13aa7ac30001be9_prof);

    }

    // line 108
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_6da72e92b70992ce2c08ca8e1855dc35ea942b6e49628fce1b52dc3fdbbd72ae = $this->env->getExtension("native_profiler");
        $__internal_6da72e92b70992ce2c08ca8e1855dc35ea942b6e49628fce1b52dc3fdbbd72ae->enter($__internal_6da72e92b70992ce2c08ca8e1855dc35ea942b6e49628fce1b52dc3fdbbd72ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 109
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/multipleselect/multiple-select.css"), "html", null, true);
        echo "\">    
";
        
        $__internal_6da72e92b70992ce2c08ca8e1855dc35ea942b6e49628fce1b52dc3fdbbd72ae->leave($__internal_6da72e92b70992ce2c08ca8e1855dc35ea942b6e49628fce1b52dc3fdbbd72ae_prof);

    }

    // line 116
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_52f868c64991ce2bc4fcb60a85d217180a717ce32d6496cd72e1eb3750c4dcd8 = $this->env->getExtension("native_profiler");
        $__internal_52f868c64991ce2bc4fcb60a85d217180a717ce32d6496cd72e1eb3750c4dcd8->enter($__internal_52f868c64991ce2bc4fcb60a85d217180a717ce32d6496cd72e1eb3750c4dcd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 117
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js\"></script>
<script src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/multipleselect/multiple-select.js"), "html", null, true);
        echo "\"></script> 
<script> \$.validate(); </script>
<script>
    \$('#role_client select').multipleSelect();
    
    ";
        // line 124
        if ( !twig_test_empty($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "clients", array()))) {
            // line 125
            echo "        \$(\"#role_client select\").multipleSelect(\"setSelects\", ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "clients", array()), "html", null, true);
            echo ");
    ";
        }
        // line 127
        echo "    
    //\$(\"select\").multipleSelect(\"getSelects\"); //values
    //\$(\"select\").multipleSelect(\"getSelects\", \"text\");//text

    \$('select#role').on(\"change\",function(){
        var option = \$(\"select#role  option:selected\").val();
        if (option == \"ROLE_CLIENT\"){
                    \$('#role_client').show();
        }else       \$('#role_client').hide();
        
    });
</script>

<script>
    
    
</script>

";
        
        $__internal_52f868c64991ce2bc4fcb60a85d217180a717ce32d6496cd72e1eb3750c4dcd8->leave($__internal_52f868c64991ce2bc4fcb60a85d217180a717ce32d6496cd72e1eb3750c4dcd8_prof);

    }

    public function getTemplateName()
    {
        return "users_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  326 => 127,  320 => 125,  318 => 124,  310 => 119,  305 => 117,  299 => 116,  290 => 110,  286 => 109,  280 => 108,  259 => 93,  252 => 88,  242 => 81,  231 => 80,  227 => 79,  222 => 77,  216 => 76,  202 => 69,  194 => 68,  186 => 67,  181 => 65,  172 => 61,  168 => 60,  164 => 58,  162 => 57,  153 => 53,  149 => 52,  139 => 47,  135 => 46,  123 => 39,  119 => 38,  107 => 33,  103 => 32,  92 => 28,  88 => 27,  78 => 22,  74 => 21,  70 => 20,  64 => 17,  59 => 15,  56 => 14,  54 => 13,  49 => 10,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/* {#        {% include 'sidebar_users.html.twig' %}#}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb_reduced.html.twig'%}*/
/*     <div class="col-md-12 main">*/
/*           <h1 class="page-header">{{"Users"|trans}}</h1>*/
/*           */
/*           <form  action="{{path('_users_edit',{ 'user_id': user.id})}}" method="POST">*/
/*             */
/*             <div class="form-group">*/
/*                 <label for="fullname">{{"Logo"|trans}}</label><br/>*/
/*                 <img src="http://www.gravatar.com/avatar/{{hash}}?s=48"  alt="User Image">  <br/>*/
/*                 <a target="_blank" href="https://gravatar.com">{{"Click here"|trans}}</a> {{"to modify your logo!"|trans}}*/
/*             </div>*/
/*             */
/*             */
/*             <div class="form-group">*/
/*               <label for="fullname">{{"Fullname"|trans}}</label>*/
/*               <input name="fullname" value="{{user.fullname}}" data-validation="required" type="text" class="form-control" id="fullname" placeholder="{{"Fullname"|trans}}"  data-validation-error-msg="{{"You have not answered all required fields"|trans}}">*/
/*             </div>*/
/*             */
/*             <div class="form-group">*/
/*               <label for="email">{{"Email"|trans}}</label>*/
/*               <input name="email" value="{{user.email}}"  data-validation="email" type="email" class="form-control" id="email" placeholder="{{"Email"|trans}}" data-validation-error-msg="{{"You have not given a correct e-mail address"|trans}}">*/
/*             </div>*/
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="password">{{"Password"|trans}}</label>*/
/*               <input name="password" data-validation="required" type="password" class="form-control" id="password" placeholder="{{"Password"|trans}}"  data-validation-error-msg="{{"You have not answered all required fields"|trans}}">*/
/*             </div>*/
/*   */
/*               */
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="address">{{"Address"|trans}}</label>*/
/*               <input value="{{user.address}}" name="address" type="text" class="form-control" id="address" placeholder="{{"Address"|trans}}">*/
/*             </div>*/
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="phone">{{"Phone"|trans}}</label>*/
/*               <input name="phone" value="{{user.phone}}" type="text" class="form-control" id="phone" placeholder="{{"Phone"|trans}}">*/
/*             </div>*/
/*             */
/*             */
/*             {%if app.session.get('role')=="ROLE_ADMIN" %}*/
/*             */
/*             <div class="form-group" style="width:50px;">*/
/*               <label for="active">{{"Active"|trans}}</label>*/
/*               <input name="active" {%if user.active=="1" %}checked="checked"{%endif%} value="1" type="checkbox" class="form-control" id="active" >*/
/*             </div>*/
/*             */
/*             <div class="form-group">*/
/*               <label for="role">{{"Role"|trans}}</label>*/
/*               <select name="role" id="role" class="form-control" >*/
/*                   <option {%if user.role=="ROLE_ADMIN" %}selected="selected"{%endif%} value="ROLE_ADMIN">{{"Admin"|trans}}</option>*/
/*                   <option {%if user.role=="ROLE_CLIENT" %}selected="selected"{%endif%}value="ROLE_CLIENT">{{"Client"|trans}}</option>*/
/*                   <option {%if user.role=="ROLE_GUEST" %}selected="selected"{%endif%}value="ROLE_GUEST">{{"Guest"|trans}}</option>*/
/*                   */
/*               </select>*/
/*               */
/*             </div>*/
/* */
/*                 */
/*             <div class="form-group" id="role_client" {%if user.role!="ROLE_CLIENT" %}style="display:none;"{%endif%}>*/
/*               <label for="clients">{{"Clients"|trans}}</label>*/
/*                <select multiple="multiple" name="clients[]" id="clients" style="width:250px;" >*/
/*                     {% for client in clients_list %}*/
/*                         <option value="{{client.id}}">{{client.name}}</option>*/
/*                     {% endfor %} */
/*                    */
/*                 </select>*/
/*                 */
/*               */
/*             </div>*/
/*             */
/*             {%endif%}       */
/*             */
/*             */
/*             */
/*            */
/*             <input type="submit" class="btn btn-primary btn-lg pull-right" value="{{"Submit"|trans}}"/>*/
/*           </form>*/
/*           */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/* <link rel="stylesheet" href="{{asset('libs/multipleselect/multiple-select.css')}}">    */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* {% block javascripts %}*/
/* {{parent()}}*/
/* <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>*/
/* <script src="{{asset('libs/multipleselect/multiple-select.js')}}"></script> */
/* <script> $.validate(); </script>*/
/* <script>*/
/*     $('#role_client select').multipleSelect();*/
/*     */
/*     {% if user.clients is not empty %}*/
/*         $("#role_client select").multipleSelect("setSelects", {{user.clients}});*/
/*     {% endif %}*/
/*     */
/*     //$("select").multipleSelect("getSelects"); //values*/
/*     //$("select").multipleSelect("getSelects", "text");//text*/
/* */
/*     $('select#role').on("change",function(){*/
/*         var option = $("select#role  option:selected").val();*/
/*         if (option == "ROLE_CLIENT"){*/
/*                     $('#role_client').show();*/
/*         }else       $('#role_client').hide();*/
/*         */
/*     });*/
/* </script>*/
/* */
/* <script>*/
/*     */
/*     */
/* </script>*/
/* */
/* {% endblock %}*/
/* */
/* */
/* */
