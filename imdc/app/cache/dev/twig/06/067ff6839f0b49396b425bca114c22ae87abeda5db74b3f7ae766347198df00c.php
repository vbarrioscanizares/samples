<?php

/* clients.html.twig */
class __TwigTemplate_bb4b9a9cea3b213880a671b4deff80278df04c89739fcddc760970e711e44a09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40449291f7c96e78a208ec052d368916b7cfdd30f91845222085befba7e14915 = $this->env->getExtension("native_profiler");
        $__internal_40449291f7c96e78a208ec052d368916b7cfdd30f91845222085befba7e14915->enter($__internal_40449291f7c96e78a208ec052d368916b7cfdd30f91845222085befba7e14915_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_40449291f7c96e78a208ec052d368916b7cfdd30f91845222085befba7e14915->leave($__internal_40449291f7c96e78a208ec052d368916b7cfdd30f91845222085befba7e14915_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_0d1be7c43b59809342fea657cf82a073030b68609560db52c23229c0ff2fa031 = $this->env->getExtension("native_profiler");
        $__internal_0d1be7c43b59809342fea657cf82a073030b68609560db52c23229c0ff2fa031->enter($__internal_0d1be7c43b59809342fea657cf82a073030b68609560db52c23229c0ff2fa031_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
";
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb_reduced.html.twig", "clients.html.twig", 13)->display($context);
        // line 14
        echo "    <div class=\"col-md-12  main\">
          <h1 class=\"page-header\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clients"), "html", null, true);
        echo "</h1>
          
          <div class=\"table-responsive\">
            <table class=\"table table-hover\">
               <thead>
                <tr>
                   <th style=\"width:7%;\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Id"), "html", null, true);
        echo "</th>
                   <th style=\"width:17%;\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Logo"), "html", null, true);
        echo "</th>
                   <th style=\"width:44%;\">";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "</th>
                   <th style=\"width:32%;\"></th>
                </tr>
               </thead>  
            <tbody>
                
                
            ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clients_list"]) ? $context["clients_list"] : $this->getContext($context, "clients_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 31
            echo "                    
                        <tr style=\"cursor:pointer;\"   onclick=\"document.location = '";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_board", array("client_id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
            echo "';\">

                        <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "id", array()), "html", null, true);
            echo "</td>
                        <td><img width=\"100\" src=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("upload/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "getLogoUrl", array(), "method"), "html", null, true);
            echo "\" alt=\"logo\"/></td>
                        <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "name", array()), "html", null, true);
            echo "</td>
                        <td>
                            
                            
                            ";
            // line 40
            if (((isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")) == "ROLE_ADMIN")) {
                echo "   
                                    <a href='";
                // line 41
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_edit", array("client_id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
                echo "'><!--edit--><span  data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Edit"), "html", null, true);
                echo "\" class=\"glyphicon glyphicon-pencil\"></span></a>       
                                    <a href=\"";
                // line 42
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_presets", array("client_id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
                echo "\"><!--presets--><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Presets"), "html", null, true);
                echo "\" class=\"glyphicon glyphicon-off\"></span></a>
                                    <a href=\"";
                // line 43
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills", array("client_id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
                echo "\"><!--factures--><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Bills"), "html", null, true);
                echo "\" class=\"glyphicon glyphicon-book\"></span></a>
                                    <a href=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_active_power_presets", array("client_id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
                echo "\"><!--active power preset--><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Power presets"), "html", null, true);
                echo "\" class=\"glyphicon glyphicon-certificate\"></span></a>
                                    <a href=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_delete", array("client_id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
                echo "\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Delete"), "html", null, true);
                echo "\" class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></a>
                            ";
            }
            // line 47
            echo "                            
                            
                            <a href=\"";
            // line 49
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_board", array("client_id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
            echo "\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Board"), "html", null, true);
            echo "\" class=\"glyphicon glyphicon-blackboard\"></span></a>
                            <a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
            echo "\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Chart"), "html", null, true);
            echo "\" class=\"glyphicon glyphicon-equalizer\"></span></a>    
                            <a href=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_alarms", array("client_id" => $this->getAttribute($context["client"], "id", array()))), "html", null, true);
            echo "\"><!--alarms--><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Alarms"), "html", null, true);
            echo "\" class=\"glyphicon glyphicon-bell\"></span></a>
                            
                            
                        </td>
                      </tr>
                    
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "    
              
            
            
            </tbody>  
            </table>
            ";
        // line 63
        if (((isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")) == "ROLE_ADMIN")) {
            // line 64
            echo "                <p><a class=\"btn btn-primary btn-lg pull-right\" href=\"";
            echo $this->env->getExtension('routing')->getPath("_clients_create");
            echo "\" role=\"button\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Add new client"), "html", null, true);
            echo "</a></p>
            ";
        }
        // line 66
        echo "            
          </div>
          
          
          
          
          
          
          
          
          
          
          
          

          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_0d1be7c43b59809342fea657cf82a073030b68609560db52c23229c0ff2fa031->leave($__internal_0d1be7c43b59809342fea657cf82a073030b68609560db52c23229c0ff2fa031_prof);

    }

    // line 93
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_862a3fb5311f1c6b47d4a155f48f0e241ee24a0f97374bee1c9f08a486c13cef = $this->env->getExtension("native_profiler");
        $__internal_862a3fb5311f1c6b47d4a155f48f0e241ee24a0f97374bee1c9f08a486c13cef->enter($__internal_862a3fb5311f1c6b47d4a155f48f0e241ee24a0f97374bee1c9f08a486c13cef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 94
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    
";
        
        $__internal_862a3fb5311f1c6b47d4a155f48f0e241ee24a0f97374bee1c9f08a486c13cef->leave($__internal_862a3fb5311f1c6b47d4a155f48f0e241ee24a0f97374bee1c9f08a486c13cef_prof);

    }

    public function getTemplateName()
    {
        return "clients.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 94,  229 => 93,  197 => 66,  189 => 64,  187 => 63,  179 => 57,  164 => 51,  158 => 50,  152 => 49,  148 => 47,  141 => 45,  135 => 44,  129 => 43,  123 => 42,  117 => 41,  113 => 40,  106 => 36,  101 => 35,  97 => 34,  92 => 32,  89 => 31,  85 => 30,  75 => 23,  71 => 22,  67 => 21,  58 => 15,  55 => 14,  53 => 13,  48 => 10,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/* {#        {% include 'sidebar_clients.html.twig' %}#}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb_reduced.html.twig'%}*/
/*     <div class="col-md-12  main">*/
/*           <h1 class="page-header">{{"Clients"|trans}}</h1>*/
/*           */
/*           <div class="table-responsive">*/
/*             <table class="table table-hover">*/
/*                <thead>*/
/*                 <tr>*/
/*                    <th style="width:7%;">{{"Id"|trans}}</th>*/
/*                    <th style="width:17%;">{{"Logo"|trans}}</th>*/
/*                    <th style="width:44%;">{{"Name"|trans}}</th>*/
/*                    <th style="width:32%;"></th>*/
/*                 </tr>*/
/*                </thead>  */
/*             <tbody>*/
/*                 */
/*                 */
/*             {% for client in clients_list %}*/
/*                     */
/*                         <tr style="cursor:pointer;"   onclick="document.location = '{{path("_clients_board", { 'client_id': client.id })}}';">*/
/* */
/*                         <td>{{client.id}}</td>*/
/*                         <td><img width="100" src="{{asset('upload/')}}{{client.getLogoUrl()}}" alt="logo"/></td>*/
/*                         <td>{{client.name}}</td>*/
/*                         <td>*/
/*                             */
/*                             */
/*                             {%if role == "ROLE_ADMIN"%}   */
/*                                     <a href='{{path("_clients_edit", { 'client_id': client.id })}}'><!--edit--><span  data-toggle="tooltip" data-placement="top" title="{{"Edit"|trans}}" class="glyphicon glyphicon-pencil"></span></a>       */
/*                                     <a href="{{path("_clients_presets", { 'client_id': client.id })}}"><!--presets--><span data-toggle="tooltip" data-placement="top" title="{{"Presets"|trans}}" class="glyphicon glyphicon-off"></span></a>*/
/*                                     <a href="{{path("_clients_bills", { 'client_id': client.id })}}"><!--factures--><span data-toggle="tooltip" data-placement="top" title="{{"Bills"|trans}}" class="glyphicon glyphicon-book"></span></a>*/
/*                                     <a href="{{path("_active_power_presets", { 'client_id': client.id })}}"><!--active power preset--><span data-toggle="tooltip" data-placement="top" title="{{"Total Power presets"|trans}}" class="glyphicon glyphicon-certificate"></span></a>*/
/*                                     <a href="{{path("_clients_delete", { 'client_id': client.id })}}"><span data-toggle="tooltip" data-placement="top" title="{{"Delete"|trans}}" class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>*/
/*                             {%endif%}*/
/*                             */
/*                             */
/*                             <a href="{{path("_clients_board", { 'client_id': client.id })}}"><span data-toggle="tooltip" data-placement="top" title="{{"Board"|trans}}" class="glyphicon glyphicon-blackboard"></span></a>*/
/*                             <a href="{{path("_clients_chart", { 'client_id': client.id })}}"><span data-toggle="tooltip" data-placement="top" title="{{"Chart"|trans}}" class="glyphicon glyphicon-equalizer"></span></a>    */
/*                             <a href="{{path("_clients_alarms", { 'client_id': client.id })}}"><!--alarms--><span data-toggle="tooltip" data-placement="top" title="{{"Alarms"|trans}}" class="glyphicon glyphicon-bell"></span></a>*/
/*                             */
/*                             */
/*                         </td>*/
/*                       </tr>*/
/*                     */
/*             {% endfor %}    */
/*               */
/*             */
/*             */
/*             </tbody>  */
/*             </table>*/
/*             {%if role == "ROLE_ADMIN"%}*/
/*                 <p><a class="btn btn-primary btn-lg pull-right" href="{{path("_clients_create")}}" role="button">{{"Add new client"|trans}}</a></p>*/
/*             {%endif%}*/
/*             */
/*           </div>*/
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/* */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/*     */
/* {% endblock %}*/
/* */
