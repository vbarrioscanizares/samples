<?php

/* sidebar_users.html.twig */
class __TwigTemplate_a37238c1cee6293478b171ac95618205d929145bad388187d5cbdee7856fbca6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9b985df9922918949dea4644c45f4c63045dccbcf64c140f56d861f8c8a648d3 = $this->env->getExtension("native_profiler");
        $__internal_9b985df9922918949dea4644c45f4c63045dccbcf64c140f56d861f8c8a648d3->enter($__internal_9b985df9922918949dea4644c45f4c63045dccbcf64c140f56d861f8c8a648d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "sidebar_users.html.twig"));

        // line 1
        $context["role"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "role"), "method");
        // line 2
        echo "
        <div class=\"col-sm-3 col-md-2 sidebar\">
          <ul class=\"nav nav-sidebar\">
            ";
        // line 5
        if (((isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")) == "ROLE_ADMIN")) {
            // line 6
            echo "                <li class=\"active\" ><a href=\"";
            echo $this->env->getExtension('routing')->getPath("_users");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Users"), "html", null, true);
            echo "</a></li>
            ";
        }
        // line 7
        echo "  
            
            ";
        // line 9
        if ((((isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")) == "ROLE_ADMIN") || ((isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")) == "ROLE_CLIENT"))) {
            // line 10
            echo "                <li style=\"/*display:none;*/\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("_clients");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clients"), "html", null, true);
            echo "</a></li>
            ";
        }
        // line 13
        echo "          </ul>
            
          ";
        // line 34
        echo "
            
         
         
        </div>
    
    
    
    ";
        
        $__internal_9b985df9922918949dea4644c45f4c63045dccbcf64c140f56d861f8c8a648d3->leave($__internal_9b985df9922918949dea4644c45f4c63045dccbcf64c140f56d861f8c8a648d3_prof);

    }

    public function getTemplateName()
    {
        return "sidebar_users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 34,  53 => 13,  45 => 10,  43 => 9,  39 => 7,  31 => 6,  29 => 5,  24 => 2,  22 => 1,);
    }
}
/* {% set role = app.session.get('role') %}*/
/* */
/*         <div class="col-sm-3 col-md-2 sidebar">*/
/*           <ul class="nav nav-sidebar">*/
/*             {% if role=="ROLE_ADMIN" %}*/
/*                 <li class="active" ><a href="{{path('_users')}}">{{"Users"|trans}}</a></li>*/
/*             {% endif %}  */
/*             */
/*             {% if role=="ROLE_ADMIN" or role=="ROLE_CLIENT" %}*/
/*                 <li style="/*display:none;*//* "><a href="{{path('_clients')}}">{{"Clients"|trans}}</a></li>*/
/*             {% endif %}*/
/* {#            <li ><a href="{{path('sample')}}">{{"Demo"|trans}}</a></li>#}*/
/*           </ul>*/
/*             */
/*           {#<div class="dropdown">*/
/*               <a id="dLabel" data-target="#" href="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">*/
/*                 {{"current_language"|trans}}*/
/*                 <span class="caret"></span>*/
/*               </a>*/
/* */
/*               <ul class="dropdown-menu" aria-labelledby="dLabel">*/
/*                 {%if app.request.locale == "en"%}*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), frParams) }}">French</a></li>*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), itParams) }}">Italien</a></li>*/
/*                {%elseif app.request.locale == "fr"%}*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), enParams) }}">English</a></li>*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), itParams) }}">Italien</a></li>*/
/*                {%else%}*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), enParams) }}">English</a></li>*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), frParams) }}">French</a></li>*/
/*                {%endif%}*/
/*               </ul>*/
/*             </div>  #}*/
/* */
/*             */
/*          */
/*          */
/*         </div>*/
/*     */
/*     */
/*     */
/*     */
