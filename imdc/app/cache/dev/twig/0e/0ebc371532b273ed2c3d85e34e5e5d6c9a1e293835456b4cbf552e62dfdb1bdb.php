<?php

/* clients_chart.html.twig */
class __TwigTemplate_3862633bc9784fcadf4ea1ad7a016c6af5ffaf7998a44a292d989f1bd8dcea10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_chart.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b61e0046c996067b3432f71c3f5ea1c0974b28ee23b68ee25efc4f570eda3014 = $this->env->getExtension("native_profiler");
        $__internal_b61e0046c996067b3432f71c3f5ea1c0974b28ee23b68ee25efc4f570eda3014->enter($__internal_b61e0046c996067b3432f71c3f5ea1c0974b28ee23b68ee25efc4f570eda3014_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_chart.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b61e0046c996067b3432f71c3f5ea1c0974b28ee23b68ee25efc4f570eda3014->leave($__internal_b61e0046c996067b3432f71c3f5ea1c0974b28ee23b68ee25efc4f570eda3014_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_7f56eacc72e19d300cbe39cf7a0650a8c9522a3577a286eb586b314f542e324e = $this->env->getExtension("native_profiler");
        $__internal_7f56eacc72e19d300cbe39cf7a0650a8c9522a3577a286eb586b314f542e324e->enter($__internal_7f56eacc72e19d300cbe39cf7a0650a8c9522a3577a286eb586b314f542e324e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "  IMDC Chart    ";
        
        $__internal_7f56eacc72e19d300cbe39cf7a0650a8c9522a3577a286eb586b314f542e324e->leave($__internal_7f56eacc72e19d300cbe39cf7a0650a8c9522a3577a286eb586b314f542e324e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        $__internal_3c4e9dc25d1771da54bb6807e5a807f2fcaed872de872d90b053d845ada67a81 = $this->env->getExtension("native_profiler");
        $__internal_3c4e9dc25d1771da54bb6807e5a807f2fcaed872de872d90b053d845ada67a81->enter($__internal_3c4e9dc25d1771da54bb6807e5a807f2fcaed872de872d90b053d845ada67a81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "

    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 11
        $this->loadTemplate("sidebar_clients_v2.html.twig", "clients_chart.html.twig", 11)->display($context);
        // line 12
        echo "    
    
     ";
        // line 14
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_chart.html.twig", 14)->display(array_merge($context, array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()))));
        echo "  
    <div class=\"col-md-9  main\">    
   
    
 <div  class=\"container-fluid\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                
             <form>
                 
              <input name=\"client_id\" id=\"client_id\" type=\"hidden\" val=\"";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")), "html", null, true);
        echo "\"/>   
                 
            <div class=\"form-group\">
                
                
              <label for=\"var\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Measure"), "html", null, true);
        echo "</label>
                 ";
        // line 39
        echo "              
              <select name=\"var\" id=\"variable\" class=\"form-control\" >
                  <option ";
        // line 41
        if (((isset($context["var"]) ? $context["var"] : $this->getContext($context, "var")) == "power_consumption")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"power_consumption\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power Consumption"), "html", null, true);
        echo "</option>
                  <option ";
        // line 42
        if (((isset($context["var"]) ? $context["var"] : $this->getContext($context, "var")) == "power_apparent")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"power_apparent\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apparent Power"), "html", null, true);
        echo "</option>
                  <option ";
        // line 43
        if (((isset($context["var"]) ? $context["var"] : $this->getContext($context, "var")) == "power_factor")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"power_factor\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power Factor"), "html", null, true);
        echo "</option>
                  <option ";
        // line 44
        if (((isset($context["var"]) ? $context["var"] : $this->getContext($context, "var")) == "utilisation_factor")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"utilisation_factor\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Utilisation Factor"), "html", null, true);
        echo "</option>
                  <option ";
        // line 45
        if (((isset($context["var"]) ? $context["var"] : $this->getContext($context, "var")) == "tension")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"tension\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tension"), "html", null, true);
        echo "</option>
                  <option ";
        // line 46
        if (((isset($context["var"]) ? $context["var"] : $this->getContext($context, "var")) == "frequency")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"frequency\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Frequency"), "html", null, true);
        echo "</option>
                  <option ";
        // line 47
        if (((isset($context["var"]) ? $context["var"] : $this->getContext($context, "var")) == "thd")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"thd\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Harmonic Distorsion"), "html", null, true);
        echo "</option>
                  <option ";
        // line 48
        if (((isset($context["var"]) ? $context["var"] : $this->getContext($context, "var")) == "tcdd")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"tcdd\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Current Demand Distorsion"), "html", null, true);
        echo "</option>
                  <option ";
        // line 49
        if (((isset($context["var"]) ? $context["var"] : $this->getContext($context, "var")) == "k-factor")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"k-factor\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("K-FACTOR"), "html", null, true);
        echo "</option>
                  
              </select>
            </div> 
                  
            
            <div class=\"form-group\">
              <label for=\"entry\">";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Phase"), "html", null, true);
        echo "</label>
              ";
        // line 58
        echo "              <select name=\"entry\" id=\"entry\" class=\"form-control\" >
                  <option ";
        // line 59
        if (((isset($context["entry"]) ? $context["entry"] : $this->getContext($context, "entry")) == "e1")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"e1\">1</option>
                  <option ";
        // line 60
        if (((isset($context["entry"]) ? $context["entry"] : $this->getContext($context, "entry")) == "e2")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"e2\">2</option>
                  <option ";
        // line 61
        if (((isset($context["entry"]) ? $context["entry"] : $this->getContext($context, "entry")) == "e3")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"e3\">3</option>
                  <option ";
        // line 62
        if (((isset($context["entry"]) ? $context["entry"] : $this->getContext($context, "entry")) == "total")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"total\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total"), "html", null, true);
        echo "</option>
              </select>
            </div>      
              
              
              
            <div class=\"form-group\">
              <label for=\"freq\">";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Frequence"), "html", null, true);
        echo "</label>
              ";
        // line 71
        echo "              <select name=\"freq\" id=\"frequence\" class=\"form-control\" >
                  <option ";
        // line 72
        if (((isset($context["freq"]) ? $context["freq"] : $this->getContext($context, "freq")) == "year")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"year\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Year"), "html", null, true);
        echo "</option>
                  <option ";
        // line 73
        if (((isset($context["freq"]) ? $context["freq"] : $this->getContext($context, "freq")) == "month")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"month\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Month"), "html", null, true);
        echo "</option>
                  <option ";
        // line 74
        if (((isset($context["freq"]) ? $context["freq"] : $this->getContext($context, "freq")) == "day")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"day\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Day"), "html", null, true);
        echo "</option>
                  <option ";
        // line 75
        if (((isset($context["freq"]) ? $context["freq"] : $this->getContext($context, "freq")) == "hour")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"hour\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hour"), "html", null, true);
        echo "</option>
                  <option ";
        // line 76
        if (((isset($context["freq"]) ? $context["freq"] : $this->getContext($context, "freq")) == "minute")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"minute\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Minute"), "html", null, true);
        echo "</option>
              </select>
            </div>       
              
              
              
            <div class=\"form-group date\">
              
              
              ";
        // line 86
        echo "                <div class=\"form_half\">
                    <label >";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo "</label>
                    <label for=\"from\">";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
                    <input type=\"text\" id=\"from\" name=\"from\" value=\"";
        // line 89
        echo twig_escape_filter($this->env, (isset($context["from"]) ? $context["from"] : $this->getContext($context, "from")), "html", null, true);
        echo "\" class=\"form-control\">
                </div>
                <div class=\"form_half\">    
                  <label for=\"to\">";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
                  <input type=\"text\" id=\"to\" name=\"to\" value=\"";
        // line 93
        echo twig_escape_filter($this->env, (isset($context["to"]) ? $context["to"] : $this->getContext($context, "to")), "html", null, true);
        echo "\" class=\"form-control\">
                </div>  
            </div>  
              
                  
            <p><a class=\"btn btn-primary pull-right\" onclick=\"updateChart();\" href=\"javascript:void(0)\" role=\"button\">";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Update"), "html", null, true);
        echo "</a></p>  
            </form>         
            </div>
        </div>
     
        
        <div class=\"row\" style=\"margin-top:40px;\">
            <div class=\"col-md-12\">
                <div id=\"empty_message\" style=\"display:none;\" class=\"alert alert-info\" role=\"alert\"><span class=\"glyphicon glyphicon-exclamation-sign\">&nbsp;</span>";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No data available using these parameters"), "html", null, true);
        echo "</div>
                <div id=\"chart_container\" style=\"min-width: 310px; height: 400px; margin: 0 auto\"></div>
            
            </div>
        </div>    
  
   </div>

     
</div>
         
     </div>
    </div> 
    
";
        
        $__internal_3c4e9dc25d1771da54bb6807e5a807f2fcaed872de872d90b053d845ada67a81->leave($__internal_3c4e9dc25d1771da54bb6807e5a807f2fcaed872de872d90b053d845ada67a81_prof);

    }

    // line 122
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_316abe4bce3ebaaddf4c95fb22b7ff6d30c3acb0ebff5b51f73ff40304dcb188 = $this->env->getExtension("native_profiler");
        $__internal_316abe4bce3ebaaddf4c95fb22b7ff6d30c3acb0ebff5b51f73ff40304dcb188->enter($__internal_316abe4bce3ebaaddf4c95fb22b7ff6d30c3acb0ebff5b51f73ff40304dcb188_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 123
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "   
<link rel=\"stylesheet\" href=\"http://code.jquery.com/ui/1.9.1/themes/cupertino/jquery-ui.css\">
<link rel=\"stylesheet\" href=\"";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/timepicker/jquery-ui-timepicker-addon.css"), "html", null, true);
        echo "\">

<style>
    div#chart_container div#highcharts-0.highcharts-container > svg > text:last-child{opacity: 0;}
    p span.description-text{font-size: small;}
</style>
";
        
        $__internal_316abe4bce3ebaaddf4c95fb22b7ff6d30c3acb0ebff5b51f73ff40304dcb188->leave($__internal_316abe4bce3ebaaddf4c95fb22b7ff6d30c3acb0ebff5b51f73ff40304dcb188_prof);

    }

    // line 134
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_a7bf6f6ba28e2b9d8dbedefee9cd5236d9f62874f01509189bd77426e502d4bf = $this->env->getExtension("native_profiler");
        $__internal_a7bf6f6ba28e2b9d8dbedefee9cd5236d9f62874f01509189bd77426e502d4bf->enter($__internal_a7bf6f6ba28e2b9d8dbedefee9cd5236d9f62874f01509189bd77426e502d4bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 135
        echo "     ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
     
     <script>
 
    function updateChart(){
        var client_id   = \$(\"#client_id\").val();
        var variable    = \$(\"#variable\").val();
        var freq        = \$(\"#frequence\").val();
        var entry       = \$(\"#entry\").val();
        var from        = \$(\"#from\").val();
        var to          = \$(\"#to\").val();
        var path        = \"";
        // line 146
        echo $this->env->getExtension('routing')->getPath("_clients_chart");
        echo "\";
        var ip          = \"";
        // line 147
        echo twig_escape_filter($this->env, (isset($context["ip"]) ? $context["ip"] : $this->getContext($context, "ip")), "html", null, true);
        echo "\";
        
        var url         =  path+'/'+";
        // line 149
        echo twig_escape_filter($this->env, (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")), "html", null, true);
        echo "+'/'+variable+'/'+entry+'/'+freq+'/'+ip;
        if ( from.length != 0 && to.length != 0){
                                        url = url +'/'+from+'/'+to;   
            }else{
               if ( to.length != 0)     url = url + '/0/'+to;   
               if ( from.length != 0)   url = url + '/'+from;   
            }
        
        //console.log(url);
    window.location = url;
    };
</script>
     
    <script src=\"https://code.highcharts.com/highcharts.js\"></script>
    <script src=\"https://code.highcharts.com/highcharts-more.js\"></script>
    <script src=\"https://code.highcharts.com/modules/exporting.js\"></script>
    <script src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/jquery.blockUI.js"), "html", null, true);
        echo "\"></script> 
    <script src=\"//code.jquery.com/ui/1.11.4/jquery-ui.js\"></script>
    
    <script src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/timepicker/jquery-ui-timepicker-addon.js"), "html", null, true);
        echo "\"></script>
    
    
        
    
    
<script>
\$(function() {
    /*
    \$( \"#from\" ).datepicker({
      defaultDate: \"+1w\",
      changeMonth: true,
      numberOfMonths: 3,
      dateFormat: \"yy-mm-dd\",
      onClose: function( selectedDate ) {
        \$( \"#to\" ).datepicker( \"option\", \"minDate\", selectedDate );
      }
    });
    \$( \"#to\" ).datepicker({
      defaultDate: \"+1w\",
      changeMonth: true,
      numberOfMonths: 3,
      dateFormat: \"yy-mm-dd\",
      onClose: function( selectedDate ) {
        \$( \"#from\" ).datepicker( \"option\", \"maxDate\", selectedDate );
      }
    });
    */
    
    
     \$('#from').datetimepicker({
\taddSliderAccess: true,
\tsliderAccessArgs: { touchonly: false },
        changeMonth: true,
      numberOfMonths: 2,
      dateFormat: \"yy-mm-dd\",
      timeFormat: 'HH:mm:ss',
      onClose: function( selectedDate ) {
        \$( \"#to\" ).datepicker( \"option\", \"minDate\", selectedDate );
      }
    });
    
    \$('#to').datetimepicker({
\taddSliderAccess: true,
\tsliderAccessArgs: { touchonly: false },
        changeMonth: true,
      numberOfMonths: 2,
      dateFormat: \"yy-mm-dd\",
      timeFormat: 'HH:mm:ss',
      onClose: function( selectedDate ) {
        \$( \"#from\" ).datepicker( \"option\", \"maxDate\", selectedDate );
      }
    });
    
    
  });    
</script>    

    
    

     
<script>
   
    /*
    var ranges = [
            [1246406400000, 14.3, 27.7],
            [1246492800000, 14.5, 27.8],
            [1246579200000, 15.5, 29.6],
            [1246665600000, 16.7, 30.7],
            [1246752000000, 16.5, 25.0],
            [1246838400000, 17.8, 25.7],
            [1246924800000, 13.5, 24.8],
            [1247011200000, 10.5, 21.4],
            [1247097600000, 9.2, 23.8],
            [1247184000000, 11.6, 21.8],
            [1247270400000, 10.7, 23.7],
            [1247356800000, 11.0, 23.3],
            [1247443200000, 11.6, 23.7],
            [1247529600000, 11.8, 20.7],
            [1247616000000, 12.6, 22.4],
            [1247702400000, 13.6, 19.6],
            [1247788800000, 11.4, 22.6],
            [1247875200000, 13.2, 25.0],
            [1247961600000, 14.2, 21.6],
            [1248048000000, 13.1, 17.1],
            [1248134400000, 12.2, 15.5],
            [1248220800000, 12.0, 20.8],
            [1248307200000, 12.0, 17.1],
            [1248393600000, 12.7, 18.3],
            [1248480000000, 12.4, 19.4],
            [1248566400000, 12.6, 19.9],
            [1248652800000, 11.9, 20.2],
            [1248739200000, 11.0, 19.3],
            [1248825600000, 10.8, 17.8],
            [1248912000000, 11.8, 18.5],
            [1248998400000, 10.8, 16.1]
        ];
       var averages = [
            [1246406400000, 21.5],
            [1246492800000, 22.1],
            [1246579200000, 23],
            [1246665600000, 23.8],
            [1246752000000, 21.4],
            [1246838400000, 21.3],
            [1246924800000, 18.3],
            [1247011200000, 15.4],
            [1247097600000, 16.4],
            [1247184000000, 17.7],
            [1247270400000, 17.5],
            [1247356800000, 17.6],
            [1247443200000, 17.7],
            [1247529600000, 16.8],
            [1247616000000, 17.7],
            [1247702400000, 16.3],
            [1247788800000, 17.8],
            [1247875200000, 18.1],
            [1247961600000, 17.2],
            [1248048000000, 14.4],
            [1248134400000, 13.7],
            [1248220800000, 15.7],
            [1248307200000, 14.6],
            [1248393600000, 15.3],
            [1248480000000, 15.3],
            [1248566400000, 15.8],
            [1248652800000, 15.2],
            [1248739200000, 14.8],
            [1248825600000, 14.4],
            [1248912000000, 15],
            [1248998400000, 13.6]
        ];
    */
    
    \$(function () {
        
        
        
        
    
    \$.blockUI({ message: null });
    
    
   
        
        
    /*REQUEST DATA TO RESPECTIVE REST SERVICE */    
    \$.get( \"http://";
        // line 314
        echo twig_escape_filter($this->env, (isset($context["ip"]) ? $context["ip"] : $this->getContext($context, "ip")), "html", null, true);
        echo "/lab/raspberry_data_rest.php\", \$(\"form\").serialize() )
    .done(function( data ) {
        
       
        
        var ranges      = data.ranges;//JSON.stringify(data.ranges);
        var averages    = data.averages;//JSON.stringify(data.averages);
        var messure     = \"\"+data.messure;
        var suffix      = \"\"+data.suffix;
        var catg        = data.categories;//JSON.stringify(data.categories);//

        if ( ranges.length == 0){
            \$(\"#empty_message\").show();
            \$('#chart_container').hide();
        }else{
            \$(\"#empty_message\").hide();
            \$('#chart_container').show();
        }

        //console.log(suffix);

        \$('#chart_container').highcharts({

                    title: {
                        text: messure";
        // line 339
        echo "                    },

                    exporting: {
                            enabled: false
                        },        

                    xAxis: {
                        //type: 'datetime'
                        categories: catg//['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    },

                    yAxis: {
                        title: {
                            text: null
                        }
                    },

                    tooltip: {
                        crosshairs: true,
                        shared: true,
                        valueSuffix: suffix
                    },

                    legend: {
                    },

                    series: [{
                        name: messure,";
        // line 367
        echo "                        data: averages,
                        zIndex: 1,
                        marker: {
                            fillColor: 'white',
                            lineWidth: 2,
                            lineColor: Highcharts.getOptions().colors[0]
                        }
                    }, {
                        name: \"";
        // line 375
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Range"), "js", null, true);
        echo "\",
                        data: ranges,
                        type: 'arearange',
                        lineWidth: 0,
                        linkedTo: ':previous',
                        color: Highcharts.getOptions().colors[0],
                        fillOpacity: 0.3,
                        zIndex: 0
                    }]
                });
                
                
            \$.unblockUI();    
     
    });    
        

    /*var ranges = [
            [1246406400000, 14.3, 27.7],
            [1246492800000, 14.5, 27.8],
            [1246579200000, 15.5, 29.6],
            [1246665600000, 16.7, 30.7],
            [1246752000000, 16.5, 25.0],
            [1246838400000, 17.8, 25.7],
            [1246924800000, 13.5, 24.8],
            [1247011200000, 10.5, 21.4],
            [1247097600000, 9.2, 23.8],
            [1247184000000, 11.6, 21.8],
            [1247270400000, 10.7, 23.7],
            [1247356800000, 11.0, 23.3],
            [1247443200000, 11.6, 23.7],
            [1247529600000, 11.8, 20.7],
            [1247616000000, 12.6, 22.4],
            [1247702400000, 13.6, 19.6],
            [1247788800000, 11.4, 22.6],
            [1247875200000, 13.2, 25.0],
            [1247961600000, 14.2, 21.6],
            [1248048000000, 13.1, 17.1],
            [1248134400000, 12.2, 15.5],
            [1248220800000, 12.0, 20.8],
            [1248307200000, 12.0, 17.1],
            [1248393600000, 12.7, 18.3],
            [1248480000000, 12.4, 19.4],
            [1248566400000, 12.6, 19.9],
            [1248652800000, 11.9, 20.2],
            [1248739200000, 11.0, 19.3],
            [1248825600000, 10.8, 17.8],
            [1248912000000, 11.8, 18.5],
            [1248998400000, 10.8, 16.1]
        ],
        averages = [
            [1246406400000, 21.5],
            [1246492800000, 22.1],
            [1246579200000, 23],
            [1246665600000, 23.8],
            [1246752000000, 21.4],
            [1246838400000, 21.3],
            [1246924800000, 18.3],
            [1247011200000, 15.4],
            [1247097600000, 16.4],
            [1247184000000, 17.7],
            [1247270400000, 17.5],
            [1247356800000, 17.6],
            [1247443200000, 17.7],
            [1247529600000, 16.8],
            [1247616000000, 17.7],
            [1247702400000, 16.3],
            [1247788800000, 17.8],
            [1247875200000, 18.1],
            [1247961600000, 17.2],
            [1248048000000, 14.4],
            [1248134400000, 13.7],
            [1248220800000, 15.7],
            [1248307200000, 14.6],
            [1248393600000, 15.3],
            [1248480000000, 15.3],
            [1248566400000, 15.8],
            [1248652800000, 15.2],
            [1248739200000, 14.8],
            [1248825600000, 14.4],
            [1248912000000, 15],
            [1248998400000, 13.6]
        ];

*/
    
});
    
</script>   



     
";
        
        $__internal_a7bf6f6ba28e2b9d8dbedefee9cd5236d9f62874f01509189bd77426e502d4bf->leave($__internal_a7bf6f6ba28e2b9d8dbedefee9cd5236d9f62874f01509189bd77426e502d4bf_prof);

    }

    public function getTemplateName()
    {
        return "clients_chart.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  626 => 375,  616 => 367,  587 => 339,  560 => 314,  411 => 168,  405 => 165,  386 => 149,  381 => 147,  377 => 146,  362 => 135,  356 => 134,  342 => 125,  337 => 123,  331 => 122,  309 => 106,  298 => 98,  290 => 93,  286 => 92,  280 => 89,  276 => 88,  272 => 87,  269 => 86,  253 => 76,  245 => 75,  237 => 74,  229 => 73,  221 => 72,  218 => 71,  214 => 69,  200 => 62,  194 => 61,  188 => 60,  182 => 59,  179 => 58,  175 => 56,  161 => 49,  153 => 48,  145 => 47,  137 => 46,  129 => 45,  121 => 44,  113 => 43,  105 => 42,  97 => 41,  93 => 39,  89 => 29,  81 => 24,  68 => 14,  64 => 12,  62 => 11,  55 => 6,  49 => 5,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block title %}  IMDC Chart    {% endblock %}*/
/* */
/* {% block content %}*/
/* */
/* */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients_v2.html.twig' %}*/
/*     */
/*     */
/*      {% include 'clients_breadcrumb.html.twig' with {'client_id': client.id}%}  */
/*     <div class="col-md-9  main">    */
/*    */
/*     */
/*  <div  class="container-fluid">*/
/*         <div class="row">*/
/*             <div class="col-md-12">*/
/*                 */
/*              <form>*/
/*                  */
/*               <input name="client_id" id="client_id" type="hidden" val="{{client_id}}"/>   */
/*                  */
/*             <div class="form-group">*/
/*                 */
/*                 */
/*               <label for="var">{{"Measure"|trans}}</label>*/
/*                  {#power_consumption,*/
/*                  power_apparent,*/
/*                  power_factor,*/
/*                  utilisation_factor,*/
/*                  tension,*/
/*                  frequency,*/
/*                  thd,*/
/*                  tcdd,*/
/*                  k-factor#}*/
/*               */
/*               <select name="var" id="variable" class="form-control" >*/
/*                   <option {%if var=="power_consumption" %}selected="selected"{%endif%} value="power_consumption">{{"Power Consumption"|trans}}</option>*/
/*                   <option {%if var=="power_apparent" %}selected="selected"{%endif%} value="power_apparent">{{"Apparent Power"|trans}}</option>*/
/*                   <option {%if var=="power_factor" %}selected="selected"{%endif%} value="power_factor">{{"Power Factor"|trans}}</option>*/
/*                   <option {%if var=="utilisation_factor" %}selected="selected"{%endif%} value="utilisation_factor">{{"Utilisation Factor"|trans}}</option>*/
/*                   <option {%if var=="tension" %}selected="selected"{%endif%} value="tension">{{"Tension"|trans}}</option>*/
/*                   <option {%if var=="frequency" %}selected="selected"{%endif%} value="frequency">{{"Frequency"|trans}}</option>*/
/*                   <option {%if var=="thd" %}selected="selected"{%endif%} value="thd">{{"Total Harmonic Distorsion"|trans}}</option>*/
/*                   <option {%if var=="tcdd" %}selected="selected"{%endif%} value="tcdd">{{"Total Current Demand Distorsion"|trans}}</option>*/
/*                   <option {%if var=="k-factor" %}selected="selected"{%endif%} value="k-factor">{{"K-FACTOR"|trans}}</option>*/
/*                   */
/*               </select>*/
/*             </div> */
/*                   */
/*             */
/*             <div class="form-group">*/
/*               <label for="entry">{{"Phase"|trans}}</label>*/
/*               {#e1,e2,e3,total#}*/
/*               <select name="entry" id="entry" class="form-control" >*/
/*                   <option {%if entry=="e1" %}selected="selected"{%endif%} value="e1">1</option>*/
/*                   <option {%if entry=="e2" %}selected="selected"{%endif%} value="e2">2</option>*/
/*                   <option {%if entry=="e3" %}selected="selected"{%endif%} value="e3">3</option>*/
/*                   <option {%if entry=="total" %}selected="selected"{%endif%} value="total">{{"Total"|trans}}</option>*/
/*               </select>*/
/*             </div>      */
/*               */
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="freq">{{"Frequence"|trans}}</label>*/
/*               {#e1,e2,e3,total#}*/
/*               <select name="freq" id="frequence" class="form-control" >*/
/*                   <option {%if freq=="year" %}selected="selected"{%endif%} value="year">{{"Year"|trans}}</option>*/
/*                   <option {%if freq=="month" %}selected="selected"{%endif%} value="month">{{"Month"|trans}}</option>*/
/*                   <option {%if freq=="day" %}selected="selected"{%endif%} value="day">{{"Day"|trans}}</option>*/
/*                   <option {%if freq=="hour" %}selected="selected"{%endif%} value="hour">{{"Hour"|trans}}</option>*/
/*                   <option {%if freq=="minute" %}selected="selected"{%endif%} value="minute">{{"Minute"|trans}}</option>*/
/*               </select>*/
/*             </div>       */
/*               */
/*               */
/*               */
/*             <div class="form-group date">*/
/*               */
/*               */
/*               {#e1,e2,e3,total#}*/
/*                 <div class="form_half">*/
/*                     <label >{{"Date"|trans}}</label>*/
/*                     <label for="from">{{"from"|trans}}</label>*/
/*                     <input type="text" id="from" name="from" value="{{from}}" class="form-control">*/
/*                 </div>*/
/*                 <div class="form_half">    */
/*                   <label for="to">{{"to"|trans}}</label>*/
/*                   <input type="text" id="to" name="to" value="{{to}}" class="form-control">*/
/*                 </div>  */
/*             </div>  */
/*               */
/*                   */
/*             <p><a class="btn btn-primary pull-right" onclick="updateChart();" href="javascript:void(0)" role="button">{{"Update"|trans}}</a></p>  */
/*             </form>         */
/*             </div>*/
/*         </div>*/
/*      */
/*         */
/*         <div class="row" style="margin-top:40px;">*/
/*             <div class="col-md-12">*/
/*                 <div id="empty_message" style="display:none;" class="alert alert-info" role="alert"><span class="glyphicon glyphicon-exclamation-sign">&nbsp;</span>{{"No data available using these parameters"|trans}}</div>*/
/*                 <div id="chart_container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>*/
/*             */
/*             </div>*/
/*         </div>    */
/*   */
/*    </div>*/
/* */
/*      */
/* </div>*/
/*          */
/*      </div>*/
/*     </div> */
/*     */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{ parent() }}   */
/* <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/cupertino/jquery-ui.css">*/
/* <link rel="stylesheet" href="{{ asset('libs/timepicker/jquery-ui-timepicker-addon.css')}}">*/
/* */
/* <style>*/
/*     div#chart_container div#highcharts-0.highcharts-container > svg > text:last-child{opacity: 0;}*/
/*     p span.description-text{font-size: small;}*/
/* </style>*/
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*      {{ parent() }}*/
/*      */
/*      <script>*/
/*  */
/*     function updateChart(){*/
/*         var client_id   = $("#client_id").val();*/
/*         var variable    = $("#variable").val();*/
/*         var freq        = $("#frequence").val();*/
/*         var entry       = $("#entry").val();*/
/*         var from        = $("#from").val();*/
/*         var to          = $("#to").val();*/
/*         var path        = "{{path('_clients_chart')}}";*/
/*         var ip          = "{{ip}}";*/
/*         */
/*         var url         =  path+'/'+{{client_id}}+'/'+variable+'/'+entry+'/'+freq+'/'+ip;*/
/*         if ( from.length != 0 && to.length != 0){*/
/*                                         url = url +'/'+from+'/'+to;   */
/*             }else{*/
/*                if ( to.length != 0)     url = url + '/0/'+to;   */
/*                if ( from.length != 0)   url = url + '/'+from;   */
/*             }*/
/*         */
/*         //console.log(url);*/
/*     window.location = url;*/
/*     };*/
/* </script>*/
/*      */
/*     <script src="https://code.highcharts.com/highcharts.js"></script>*/
/*     <script src="https://code.highcharts.com/highcharts-more.js"></script>*/
/*     <script src="https://code.highcharts.com/modules/exporting.js"></script>*/
/*     <script src="{{ asset('libs/jquery.blockUI.js')}}"></script> */
/*     <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>*/
/*     */
/*     <script src="{{ asset('libs/timepicker/jquery-ui-timepicker-addon.js')}}"></script>*/
/*     */
/*     */
/*         */
/*     */
/*     */
/* <script>*/
/* $(function() {*/
/*     /**/
/*     $( "#from" ).datepicker({*/
/*       defaultDate: "+1w",*/
/*       changeMonth: true,*/
/*       numberOfMonths: 3,*/
/*       dateFormat: "yy-mm-dd",*/
/*       onClose: function( selectedDate ) {*/
/*         $( "#to" ).datepicker( "option", "minDate", selectedDate );*/
/*       }*/
/*     });*/
/*     $( "#to" ).datepicker({*/
/*       defaultDate: "+1w",*/
/*       changeMonth: true,*/
/*       numberOfMonths: 3,*/
/*       dateFormat: "yy-mm-dd",*/
/*       onClose: function( selectedDate ) {*/
/*         $( "#from" ).datepicker( "option", "maxDate", selectedDate );*/
/*       }*/
/*     });*/
/*     *//* */
/*     */
/*     */
/*      $('#from').datetimepicker({*/
/* 	addSliderAccess: true,*/
/* 	sliderAccessArgs: { touchonly: false },*/
/*         changeMonth: true,*/
/*       numberOfMonths: 2,*/
/*       dateFormat: "yy-mm-dd",*/
/*       timeFormat: 'HH:mm:ss',*/
/*       onClose: function( selectedDate ) {*/
/*         $( "#to" ).datepicker( "option", "minDate", selectedDate );*/
/*       }*/
/*     });*/
/*     */
/*     $('#to').datetimepicker({*/
/* 	addSliderAccess: true,*/
/* 	sliderAccessArgs: { touchonly: false },*/
/*         changeMonth: true,*/
/*       numberOfMonths: 2,*/
/*       dateFormat: "yy-mm-dd",*/
/*       timeFormat: 'HH:mm:ss',*/
/*       onClose: function( selectedDate ) {*/
/*         $( "#from" ).datepicker( "option", "maxDate", selectedDate );*/
/*       }*/
/*     });*/
/*     */
/*     */
/*   });    */
/* </script>    */
/* */
/*     */
/*     */
/* */
/*      */
/* <script>*/
/*    */
/*     /**/
/*     var ranges = [*/
/*             [1246406400000, 14.3, 27.7],*/
/*             [1246492800000, 14.5, 27.8],*/
/*             [1246579200000, 15.5, 29.6],*/
/*             [1246665600000, 16.7, 30.7],*/
/*             [1246752000000, 16.5, 25.0],*/
/*             [1246838400000, 17.8, 25.7],*/
/*             [1246924800000, 13.5, 24.8],*/
/*             [1247011200000, 10.5, 21.4],*/
/*             [1247097600000, 9.2, 23.8],*/
/*             [1247184000000, 11.6, 21.8],*/
/*             [1247270400000, 10.7, 23.7],*/
/*             [1247356800000, 11.0, 23.3],*/
/*             [1247443200000, 11.6, 23.7],*/
/*             [1247529600000, 11.8, 20.7],*/
/*             [1247616000000, 12.6, 22.4],*/
/*             [1247702400000, 13.6, 19.6],*/
/*             [1247788800000, 11.4, 22.6],*/
/*             [1247875200000, 13.2, 25.0],*/
/*             [1247961600000, 14.2, 21.6],*/
/*             [1248048000000, 13.1, 17.1],*/
/*             [1248134400000, 12.2, 15.5],*/
/*             [1248220800000, 12.0, 20.8],*/
/*             [1248307200000, 12.0, 17.1],*/
/*             [1248393600000, 12.7, 18.3],*/
/*             [1248480000000, 12.4, 19.4],*/
/*             [1248566400000, 12.6, 19.9],*/
/*             [1248652800000, 11.9, 20.2],*/
/*             [1248739200000, 11.0, 19.3],*/
/*             [1248825600000, 10.8, 17.8],*/
/*             [1248912000000, 11.8, 18.5],*/
/*             [1248998400000, 10.8, 16.1]*/
/*         ];*/
/*        var averages = [*/
/*             [1246406400000, 21.5],*/
/*             [1246492800000, 22.1],*/
/*             [1246579200000, 23],*/
/*             [1246665600000, 23.8],*/
/*             [1246752000000, 21.4],*/
/*             [1246838400000, 21.3],*/
/*             [1246924800000, 18.3],*/
/*             [1247011200000, 15.4],*/
/*             [1247097600000, 16.4],*/
/*             [1247184000000, 17.7],*/
/*             [1247270400000, 17.5],*/
/*             [1247356800000, 17.6],*/
/*             [1247443200000, 17.7],*/
/*             [1247529600000, 16.8],*/
/*             [1247616000000, 17.7],*/
/*             [1247702400000, 16.3],*/
/*             [1247788800000, 17.8],*/
/*             [1247875200000, 18.1],*/
/*             [1247961600000, 17.2],*/
/*             [1248048000000, 14.4],*/
/*             [1248134400000, 13.7],*/
/*             [1248220800000, 15.7],*/
/*             [1248307200000, 14.6],*/
/*             [1248393600000, 15.3],*/
/*             [1248480000000, 15.3],*/
/*             [1248566400000, 15.8],*/
/*             [1248652800000, 15.2],*/
/*             [1248739200000, 14.8],*/
/*             [1248825600000, 14.4],*/
/*             [1248912000000, 15],*/
/*             [1248998400000, 13.6]*/
/*         ];*/
/*     *//* */
/*     */
/*     $(function () {*/
/*         */
/*         */
/*         */
/*         */
/*     */
/*     $.blockUI({ message: null });*/
/*     */
/*     */
/*    */
/*         */
/*         */
/*     /*REQUEST DATA TO RESPECTIVE REST SERVICE *//*     */
/*     $.get( "http://{{ip}}/lab/raspberry_data_rest.php", $("form").serialize() )*/
/*     .done(function( data ) {*/
/*         */
/*        */
/*         */
/*         var ranges      = data.ranges;//JSON.stringify(data.ranges);*/
/*         var averages    = data.averages;//JSON.stringify(data.averages);*/
/*         var messure     = ""+data.messure;*/
/*         var suffix      = ""+data.suffix;*/
/*         var catg        = data.categories;//JSON.stringify(data.categories);//*/
/* */
/*         if ( ranges.length == 0){*/
/*             $("#empty_message").show();*/
/*             $('#chart_container').hide();*/
/*         }else{*/
/*             $("#empty_message").hide();*/
/*             $('#chart_container').show();*/
/*         }*/
/* */
/*         //console.log(suffix);*/
/* */
/*         $('#chart_container').highcharts({*/
/* */
/*                     title: {*/
/*                         text: messure{#{% autoescape 'js' %}{{""|trans}}{% endautoescape %}"#}*/
/*                     },*/
/* */
/*                     exporting: {*/
/*                             enabled: false*/
/*                         },        */
/* */
/*                     xAxis: {*/
/*                         //type: 'datetime'*/
/*                         categories: catg//['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']*/
/*                     },*/
/* */
/*                     yAxis: {*/
/*                         title: {*/
/*                             text: null*/
/*                         }*/
/*                     },*/
/* */
/*                     tooltip: {*/
/*                         crosshairs: true,*/
/*                         shared: true,*/
/*                         valueSuffix: suffix*/
/*                     },*/
/* */
/*                     legend: {*/
/*                     },*/
/* */
/*                     series: [{*/
/*                         name: messure,{#{% autoescape 'js' %}{{''|trans}}{% endautoescape %}",#}*/
/*                         data: averages,*/
/*                         zIndex: 1,*/
/*                         marker: {*/
/*                             fillColor: 'white',*/
/*                             lineWidth: 2,*/
/*                             lineColor: Highcharts.getOptions().colors[0]*/
/*                         }*/
/*                     }, {*/
/*                         name: "{% autoescape 'js' %}{{'Range'|trans}}{% endautoescape %}",*/
/*                         data: ranges,*/
/*                         type: 'arearange',*/
/*                         lineWidth: 0,*/
/*                         linkedTo: ':previous',*/
/*                         color: Highcharts.getOptions().colors[0],*/
/*                         fillOpacity: 0.3,*/
/*                         zIndex: 0*/
/*                     }]*/
/*                 });*/
/*                 */
/*                 */
/*             $.unblockUI();    */
/*      */
/*     });    */
/*         */
/* */
/*     /*var ranges = [*/
/*             [1246406400000, 14.3, 27.7],*/
/*             [1246492800000, 14.5, 27.8],*/
/*             [1246579200000, 15.5, 29.6],*/
/*             [1246665600000, 16.7, 30.7],*/
/*             [1246752000000, 16.5, 25.0],*/
/*             [1246838400000, 17.8, 25.7],*/
/*             [1246924800000, 13.5, 24.8],*/
/*             [1247011200000, 10.5, 21.4],*/
/*             [1247097600000, 9.2, 23.8],*/
/*             [1247184000000, 11.6, 21.8],*/
/*             [1247270400000, 10.7, 23.7],*/
/*             [1247356800000, 11.0, 23.3],*/
/*             [1247443200000, 11.6, 23.7],*/
/*             [1247529600000, 11.8, 20.7],*/
/*             [1247616000000, 12.6, 22.4],*/
/*             [1247702400000, 13.6, 19.6],*/
/*             [1247788800000, 11.4, 22.6],*/
/*             [1247875200000, 13.2, 25.0],*/
/*             [1247961600000, 14.2, 21.6],*/
/*             [1248048000000, 13.1, 17.1],*/
/*             [1248134400000, 12.2, 15.5],*/
/*             [1248220800000, 12.0, 20.8],*/
/*             [1248307200000, 12.0, 17.1],*/
/*             [1248393600000, 12.7, 18.3],*/
/*             [1248480000000, 12.4, 19.4],*/
/*             [1248566400000, 12.6, 19.9],*/
/*             [1248652800000, 11.9, 20.2],*/
/*             [1248739200000, 11.0, 19.3],*/
/*             [1248825600000, 10.8, 17.8],*/
/*             [1248912000000, 11.8, 18.5],*/
/*             [1248998400000, 10.8, 16.1]*/
/*         ],*/
/*         averages = [*/
/*             [1246406400000, 21.5],*/
/*             [1246492800000, 22.1],*/
/*             [1246579200000, 23],*/
/*             [1246665600000, 23.8],*/
/*             [1246752000000, 21.4],*/
/*             [1246838400000, 21.3],*/
/*             [1246924800000, 18.3],*/
/*             [1247011200000, 15.4],*/
/*             [1247097600000, 16.4],*/
/*             [1247184000000, 17.7],*/
/*             [1247270400000, 17.5],*/
/*             [1247356800000, 17.6],*/
/*             [1247443200000, 17.7],*/
/*             [1247529600000, 16.8],*/
/*             [1247616000000, 17.7],*/
/*             [1247702400000, 16.3],*/
/*             [1247788800000, 17.8],*/
/*             [1247875200000, 18.1],*/
/*             [1247961600000, 17.2],*/
/*             [1248048000000, 14.4],*/
/*             [1248134400000, 13.7],*/
/*             [1248220800000, 15.7],*/
/*             [1248307200000, 14.6],*/
/*             [1248393600000, 15.3],*/
/*             [1248480000000, 15.3],*/
/*             [1248566400000, 15.8],*/
/*             [1248652800000, 15.2],*/
/*             [1248739200000, 14.8],*/
/*             [1248825600000, 14.4],*/
/*             [1248912000000, 15],*/
/*             [1248998400000, 13.6]*/
/*         ];*/
/* */
/* *//* */
/*     */
/* });*/
/*     */
/* </script>   */
/* */
/* */
/* */
/*      */
/* {% endblock %}*/
/* */
/* */
