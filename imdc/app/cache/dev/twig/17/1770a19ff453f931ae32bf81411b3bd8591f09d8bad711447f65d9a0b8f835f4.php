<?php

/* sidebar_clients.html.twig */
class __TwigTemplate_26b63a3f0316c414d296c09465e2f4523d0dbe81d19f7a445855227fdf9b315a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9885da7c8ff4c952dbca1ca66552f2344afc1e5c4e33ee75ef88fb55a02c6137 = $this->env->getExtension("native_profiler");
        $__internal_9885da7c8ff4c952dbca1ca66552f2344afc1e5c4e33ee75ef88fb55a02c6137->enter($__internal_9885da7c8ff4c952dbca1ca66552f2344afc1e5c4e33ee75ef88fb55a02c6137_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "sidebar_clients.html.twig"));

        // line 1
        $context["role"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "role"), "method");
        // line 2
        echo "
        <div class=\"col-md-2 sidebar\">
            
           
             
            <ul class=\"nav nav-sidebar\">
            
            ";
        // line 9
        if (((isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")) == "ROLE_ADMIN")) {
            // line 10
            echo "                <li ><a href=\"";
            echo $this->env->getExtension('routing')->getPath("_users");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Users"), "html", null, true);
            echo "</a></li>
            ";
        }
        // line 11
        echo "  
            
            ";
        // line 13
        if ((((isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")) == "ROLE_ADMIN") || ((isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")) == "ROLE_CLIENT"))) {
            // line 14
            echo "                <li style=\"/*display:none;*/\" class=\"active\"><a href=\"";
            echo $this->env->getExtension('routing')->getPath("_clients");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clients"), "html", null, true);
            echo "</a></li>
            ";
        }
        // line 15
        echo "      
                
          </ul>
            
          ";
        // line 38
        echo "            
         
         
        </div>
    
    
    
    ";
        
        $__internal_9885da7c8ff4c952dbca1ca66552f2344afc1e5c4e33ee75ef88fb55a02c6137->leave($__internal_9885da7c8ff4c952dbca1ca66552f2344afc1e5c4e33ee75ef88fb55a02c6137_prof);

    }

    public function getTemplateName()
    {
        return "sidebar_clients.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 38,  57 => 15,  49 => 14,  47 => 13,  43 => 11,  35 => 10,  33 => 9,  24 => 2,  22 => 1,);
    }
}
/* {% set role = app.session.get('role') %}*/
/* */
/*         <div class="col-md-2 sidebar">*/
/*             */
/*            */
/*              */
/*             <ul class="nav nav-sidebar">*/
/*             */
/*             {% if role=="ROLE_ADMIN" %}*/
/*                 <li ><a href="{{path('_users')}}">{{"Users"|trans}}</a></li>*/
/*             {% endif %}  */
/*             */
/*             {% if role=="ROLE_ADMIN" or role=="ROLE_CLIENT" %}*/
/*                 <li style="/*display:none;*//* " class="active"><a href="{{path('_clients')}}">{{"Clients"|trans}}</a></li>*/
/*             {% endif %}      */
/*                 */
/*           </ul>*/
/*             */
/*           {#<div class="dropdown">*/
/*               <a id="dLabel" data-target="#" href="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">*/
/*                 {{"current_language"|trans}}*/
/*                 <span class="caret"></span>*/
/*               </a>*/
/* */
/*               <ul class="dropdown-menu" aria-labelledby="dLabel">*/
/*                 {%if app.request.locale == "en"%}*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), frParams) }}">French</a></li>*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), itParams) }}">Italien</a></li>*/
/*                {%elseif app.request.locale == "fr"%}*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), enParams) }}">English</a></li>*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), itParams) }}">Italien</a></li>*/
/*                {%else%}*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), enParams) }}">English</a></li>*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), frParams) }}">French</a></li>*/
/*                {%endif%}*/
/*               </ul>*/
/*             </div>  #}*/
/*             */
/*          */
/*          */
/*         </div>*/
/*     */
/*     */
/*     */
/*     */
