<?php

/* clients_create.html.twig */
class __TwigTemplate_dabd99c127cc05c690b7f7e1fa8af09658b87f466c533ebfef298e21986915f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_create.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_254fc16618b0be3130e0cf345f02a88e2ded8ae91a48cf9ebc4bb95f8809559b = $this->env->getExtension("native_profiler");
        $__internal_254fc16618b0be3130e0cf345f02a88e2ded8ae91a48cf9ebc4bb95f8809559b->enter($__internal_254fc16618b0be3130e0cf345f02a88e2ded8ae91a48cf9ebc4bb95f8809559b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_create.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_254fc16618b0be3130e0cf345f02a88e2ded8ae91a48cf9ebc4bb95f8809559b->leave($__internal_254fc16618b0be3130e0cf345f02a88e2ded8ae91a48cf9ebc4bb95f8809559b_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_d6354ca715a7d332cf9d1fe223b3ac7fc7fb27f7785bff908ff4f952fe145216 = $this->env->getExtension("native_profiler");
        $__internal_d6354ca715a7d332cf9d1fe223b3ac7fc7fb27f7785bff908ff4f952fe145216->enter($__internal_d6354ca715a7d332cf9d1fe223b3ac7fc7fb27f7785bff908ff4f952fe145216_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
";
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb_reduced.html.twig", "clients_create.html.twig", 13)->display($context);
        // line 14
        echo "    <div class=\"col-md-12 main\">
         
          <h1 class=\"page-header\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clients"), "html", null, true);
        echo "</h1>
          
          <form  action=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("_clients_create");
        echo "\" method=\"POST\" enctype=\"multipart/form-data\">
            <div class=\"form-group\">
              <label for=\"name\">";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "</label>
              <input name=\"name\" data-validation=\"required\" type=\"text\" class=\"form-control\" id=\"name\" placeholder=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "\"  data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">
            </div>
            
            
          ";
        // line 30
        echo "            
            
             <div class=\"control-group\">
                  <label class=\"control-label\" for=\"inputDocument\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Logo"), "html", null, true);
        echo "</label>
                  <div class=\"controls\">
                    <span class=\"btn btn-file\">
                        ";
        // line 37
        echo "                        <input name=\"logo\" type=\"file\" data-validation=\"required\"  id='inputDocument' accept=\"image/*\" data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\"";
        echo "/>
                    </span>

                  </div>

              </div>
              
            
           
            <div class=\"form-group\">
              <label for=\"server_ip\">";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Server IP"), "html", null, true);
        echo "</label>
              <input name=\"server_ip\" data-validation=\"required\" type=\"text\" class=\"form-control\" id=\"server_ip\" placeholder=\"127.0.0.1\"  data-validation-error-msg=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">
            </div>
            
            
           
            <div class=\"form-group\">
              <label for=\"server_id\">";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Server ID"), "html", null, true);
        echo "</label>
              <input name=\"server_id\" data-validation=\"required\" type=\"text\" class=\"form-control\" id=\"server_id\" placeholder=\"\"  data-validation-error-msg=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">
            </div>
            
            
            <div class=\"form-group\">
              <input id=\"address\" name=\"address\" type=\"hidden\" value=\"\" data-validation=\"required\" data-validation-error-msg=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">  
              <label for=\"address_item\">";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Address(es)"), "html", null, true);
        echo "</label>
              <a href=\"javascript:void(0);\" onclick=\"addAddress()\">
                    <span class=\" glyphicon glyphicon-plus\"></span>
                </a>
              
              
              <input name=\"address_item\" type=\"text\" class=\"form-control\" id=\"address_item\" placeholder=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("NUMBER, STREET, PROVINCE, ZIP"), "html", null, true);
        echo "\"  >
              
              <br/>
            
              <div id=\"address_list\" class=\"list-group\">
                ";
        // line 75
        echo "              </div>
            
            
            </div>
              
            
              
              
              
              <div class=\"form-group\">
              <input id=\"contact\" name=\"contact\" type=\"hidden\" value=\"\" data-validation=\"required\" data-validation-error-msg=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">  
              <label for=\"address_item\">";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contact(s)"), "html", null, true);
        echo "</label>
              <a href=\"javascript:void(0);\" onclick=\"addContact()\">
                    <span class=\" glyphicon glyphicon-plus\"></span>
                </a>
              
              
              <input name=\"contact_item\" type=\"text\" class=\"form-control\" id=\"contact_item\" placeholder=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("TITLE, FULLNAME, PHONE NUMBER EXT, EMAIL"), "html", null, true);
        echo "\"  >
              
              <br/>
            
              <div id=\"contact_list\" class=\"list-group\">
                ";
        // line 100
        echo "              </div>
            
            
            </div>
            
              
             ";
        // line 187
        echo "              
              
         
            
             <h3>";
        // line 191
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Entries"), "html", null, true);
        echo " <!-- <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" class=\"glyphicon glyphicon-info-sign\"></span>--></h3>
              <div class=\"well well-lg form-inline\">
                  
                  <h4>";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("First Entry"), "html", null, true);
        echo "</h4>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry1_name\">";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "</label>
                    <input name=\"entries['first']['name']\" id=\"entry1_name\"   type=\"text\" class=\"form-control\" id=\"entry1_name\"  placeholder=\"Entry1\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry1_kw\">";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power"), "html", null, true);
        echo " (Kw)</label>
                    <input name=\"entries['first']['kw']\" id=\"entry1_kw\"   type=\"text\" class=\"form-control\" id=\"entry1_kw\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry1_priority\">";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Priority"), "html", null, true);
        echo "</label>
                    <input value=\"\" name=\"entries['first']['priority']\" id=\"entry1_priority\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry1_percent\">";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percentage (%)"), "html", null, true);
        echo "</label>
                    <input name=\"entries['first']['percent']\" id=\"entry1_percent\"   type=\"text\" class=\"form-control\" id=\"entry1_percent\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry1_temperature\">";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Heat alarm (C)"), "html", null, true);
        echo "</label>
                    <input name=\"entries['first']['temperature']\" id=\"entry1_temperature\"   type=\"text\" class=\"form-control\" id=\"entry1_temperature\"  placeholder=\"25\">
                  </div>
                  <p></p>
                                    
                  <h4>";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Second Entry"), "html", null, true);
        echo "</h4>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry2_name\">";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "</label>
                    <input name=\"entries['second']['name']\" id=\"entry2_name\"   type=\"text\" class=\"form-control\" id=\"entry1_name\"  placeholder=\"Entry2\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry2_kw\">";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power"), "html", null, true);
        echo " (Kw)</label>
                    <input name=\"entries['second']['kw']\" id=\"entry2_kw\"   type=\"text\" class=\"form-control\" id=\"entry1_kw\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry2_priority\">";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Priority"), "html", null, true);
        echo "</label>
                    <input value=\"\" name=\"entries['second']['priority']\" id=\"entry2_priority\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry2_percent\">";
        // line 238
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percentage (%)"), "html", null, true);
        echo "</label>
                    <input name=\"entries['second']['percent']\" id=\"entry2_percent\"   type=\"text\" class=\"form-control\" id=\"entry1_percent\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry2_temperature\">";
        // line 243
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Heat alarm (C)"), "html", null, true);
        echo "</label>
                    <input name=\"entries['second']['temperature']\" id=\"entry2_temperature\"   type=\"text\" class=\"form-control\" id=\"entry1_temperature\"  placeholder=\"25\">
                  </div>
                  <p></p>
                  
                  <h4>";
        // line 248
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Third Entry"), "html", null, true);
        echo "</h4>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry3_name\">";
        // line 250
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "</label>
                    <input name=\"entries['third']['name']\" id=\"entry3_name\"   type=\"text\" class=\"form-control\" id=\"entry1_name\"  placeholder=\"Entry3\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry3_kw\">";
        // line 255
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power"), "html", null, true);
        echo " (Kw)</label>
                    <input name=\"entries['third']['kw']\" id=\"entry3_kw\"   type=\"text\" class=\"form-control\" id=\"entry1_kw\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry3_priority\">";
        // line 260
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Priority"), "html", null, true);
        echo "</label>
                    <input value=\"\" name=\"entries['third']['priority']\" id=\"entry3_priority\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry3_percent\">";
        // line 265
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percentage (%)"), "html", null, true);
        echo "</label>
                    <input name=\"entries['third']['percent']\" id=\"entry3_percent\"   type=\"text\" class=\"form-control\" id=\"entry1_percent\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry3_temperature\">";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Heat alarm (C)"), "html", null, true);
        echo "</label>
                    <input  name=\"entries['third']['temperature']\" id=\"entry3_temperature\"   type=\"text\" class=\"form-control\" id=\"entry1_temperature\"  placeholder=\"25\">
                  </div>
                  <p></p>
                
                  
              </div> 
              
              
              
              
              
              
            
           
            <input type=\"submit\" class=\"btn btn-primary btn-lg pull-right\" value=\"";
        // line 285
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\"/>
          </form>
          
          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_d6354ca715a7d332cf9d1fe223b3ac7fc7fb27f7785bff908ff4f952fe145216->leave($__internal_d6354ca715a7d332cf9d1fe223b3ac7fc7fb27f7785bff908ff4f952fe145216_prof);

    }

    // line 300
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_faba64cd1072baabca71b87587c8f4411c9f2f640724003860e430bde8c79bb4 = $this->env->getExtension("native_profiler");
        $__internal_faba64cd1072baabca71b87587c8f4411c9f2f640724003860e430bde8c79bb4->enter($__internal_faba64cd1072baabca71b87587c8f4411c9f2f640724003860e430bde8c79bb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 301
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    
";
        
        $__internal_faba64cd1072baabca71b87587c8f4411c9f2f640724003860e430bde8c79bb4->leave($__internal_faba64cd1072baabca71b87587c8f4411c9f2f640724003860e430bde8c79bb4_prof);

    }

    // line 308
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e4571df9212df3fb2e032db8e2faa44965cf4671b192309a2229f05c7034e753 = $this->env->getExtension("native_profiler");
        $__internal_e4571df9212df3fb2e032db8e2faa44965cf4671b192309a2229f05c7034e753->enter($__internal_e4571df9212df3fb2e032db8e2faa44965cf4671b192309a2229f05c7034e753_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 309
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js\"></script>
<script> \$.validate(); </script>

<script>
function addAddress(){
        var address_item    = \$(\"#address_item\").val();
        var address         = new Array();
        var address_list    = \$(\"#address\").val();
        
        if (address_item.length == 0){
            return 0;
        }
        
        if (address_list.length > 0){
            address = eval(address_list);
        }
        
        //ADD ADDRESS TO THE LIST
        address.push(address_item);
        
        //SAVE LIST IN  THE INPUT
        \$(\"#address\").val(JSON.stringify(address));
        
        //REBUILD THE LIST
        rebuildAddress();
        
        //REMOVE ADDRESS TEXT
        \$(\"#address_item\").val(\"\");
        
    }
    
    function deleteAddress(id){
        
        var address_list    = \$(\"#address\").val();
        var address = eval(address_list);
        
        //SUPPRIME THE ITEM FROM THE LIST
        address.splice(id, 1);
        
        //SAVE THE LIST WITHOUT THE ITEM
        \$(\"#address\").val(JSON.stringify(address));
        
        //REBUILD THE LIST
        rebuildAddress();
        
    }
    
    function rebuildAddress(){
         var address         = eval(\$(\"#address\").val());
         var id_item         = 0;

        
         \$(\"#address_list\").html(\"\");
         address.forEach(function(item) {
             \$(\"#address_list\").append('<a onclick=\"deleteAddress('+id_item+');\" href=\"javascript:void(0);\" class=\"list-group-item\">'+item+'<span class=\" pull-right glyphicon glyphicon-minus\"></span></a>');
             id_item++;
        //console.log(item);
        });
    }
        
    
</script>    



<script>
    function addContact(){
        var contact_item    = \$(\"#contact_item\").val();
        var contact         = new Array();
        var contact_list    = \$(\"#contact\").val();
        
        if (contact_item.length == 0){
            return 0;
        }
        
        if (contact_list.length > 0){
            contact = eval(contact_list);
        }
        
        //ADD CONTACT TO THE LIST
        contact.push(contact_item);
        
        //SAVE LIST IN  THE INPUT
        \$(\"#contact\").val(JSON.stringify(contact));
        
        //REBUILD THE LIST
        rebuildContact();
        
        //REMOVE CONTACT TEXT
        \$(\"#contact_item\").val(\"\");
        
    }
    
    function deleteContact(id){
        
        var contact_list    = \$(\"#contact\").val();
        var contacts = eval(contact_list);
        
        //SUPPRIME THE ITEM FROM THE LIST
        contacts.splice(id, 1);
        
        //SAVE THE LIST WITHOUT THE ITEM
        \$(\"#contact\").val(JSON.stringify(contacts));
        
        //REBUILD THE LIST
        rebuildContact();
        
    }
    
    function rebuildContact(){
         var contact         = eval(\$(\"#contact\").val());
         var id_item         = 0;

        
         \$(\"#contact_list\").html(\"\");
         contact.forEach(function(item) {
             \$(\"#contact_list\").append('<a onclick=\"deleteContact('+id_item+');\" href=\"javascript:void(0);\" class=\"list-group-item\">'+item+'<span class=\" pull-right glyphicon glyphicon-minus\"></span></a>');
             id_item++;
        //console.log(item);
        });
    }
    
    
    
</script>    

";
        
        $__internal_e4571df9212df3fb2e032db8e2faa44965cf4671b192309a2229f05c7034e753->leave($__internal_e4571df9212df3fb2e032db8e2faa44965cf4671b192309a2229f05c7034e753_prof);

    }

    public function getTemplateName()
    {
        return "clients_create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  396 => 309,  390 => 308,  380 => 301,  374 => 300,  353 => 285,  335 => 270,  327 => 265,  319 => 260,  311 => 255,  303 => 250,  298 => 248,  290 => 243,  282 => 238,  274 => 233,  266 => 228,  258 => 223,  253 => 221,  245 => 216,  237 => 211,  229 => 206,  221 => 201,  213 => 196,  208 => 194,  202 => 191,  196 => 187,  188 => 100,  180 => 92,  171 => 86,  167 => 85,  155 => 75,  147 => 67,  138 => 61,  134 => 60,  126 => 55,  122 => 54,  113 => 48,  109 => 47,  94 => 37,  88 => 33,  83 => 30,  74 => 21,  70 => 20,  65 => 18,  60 => 16,  56 => 14,  54 => 13,  49 => 10,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/* {#        {% include 'sidebar_clients.html.twig' %}#}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb_reduced.html.twig'%}*/
/*     <div class="col-md-12 main">*/
/*          */
/*           <h1 class="page-header">{{"Clients"|trans}}</h1>*/
/*           */
/*           <form  action="{{path('_clients_create')}}" method="POST" enctype="multipart/form-data">*/
/*             <div class="form-group">*/
/*               <label for="name">{{"Name"|trans}}</label>*/
/*               <input name="name" data-validation="required" type="text" class="form-control" id="name" placeholder="{{"Name"|trans}}"  data-validation-error-msg="{{"You have not answered all required fields"|trans}}">*/
/*             </div>*/
/*             */
/*             */
/*           {#  <div class="form-group">*/
/*                 <label for="logo_url">{{"Logo"|trans}}</label>*/
/*                 <input type="file" id="logo_url">*/
/*                 <p class="help-block">Example block-level help text here.</p>*/
/*             </div>#}*/
/*             */
/*             */
/*              <div class="control-group">*/
/*                   <label class="control-label" for="inputDocument">{{"Logo"|trans}}</label>*/
/*                   <div class="controls">*/
/*                     <span class="btn btn-file">*/
/*                         {#<input type="hidden" name="MAX_FILE_SIZE" value="90000" />#}*/
/*                         <input name="logo" type="file" data-validation="required"  id='inputDocument' accept="image/*" data-validation-error-msg="{{"You have not answered all required fields"|trans}}"{#<?=  (isset($query))?'value="default_upload"':'';?>  #}/>*/
/*                     </span>*/
/* */
/*                   </div>*/
/* */
/*               </div>*/
/*               */
/*             */
/*            */
/*             <div class="form-group">*/
/*               <label for="server_ip">{{"Server IP"|trans}}</label>*/
/*               <input name="server_ip" data-validation="required" type="text" class="form-control" id="server_ip" placeholder="127.0.0.1"  data-validation-error-msg="{{"You have not answered all required fields"|trans}}">*/
/*             </div>*/
/*             */
/*             */
/*            */
/*             <div class="form-group">*/
/*               <label for="server_id">{{"Server ID"|trans}}</label>*/
/*               <input name="server_id" data-validation="required" type="text" class="form-control" id="server_id" placeholder=""  data-validation-error-msg="{{"You have not answered all required fields"|trans}}">*/
/*             </div>*/
/*             */
/*             */
/*             <div class="form-group">*/
/*               <input id="address" name="address" type="hidden" value="" data-validation="required" data-validation-error-msg="{{"You have not answered all required fields"|trans}}">  */
/*               <label for="address_item">{{"Address(es)"|trans}}</label>*/
/*               <a href="javascript:void(0);" onclick="addAddress()">*/
/*                     <span class=" glyphicon glyphicon-plus"></span>*/
/*                 </a>*/
/*               */
/*               */
/*               <input name="address_item" type="text" class="form-control" id="address_item" placeholder="{{"NUMBER, STREET, PROVINCE, ZIP"|trans}}"  >*/
/*               */
/*               <br/>*/
/*             */
/*               <div id="address_list" class="list-group">*/
/*                 {#<a href="#" class="list-group-item">Dapibus ac facilisis in*/
/*                     <span class=" pull-right glyphicon glyphicon-minus"></span>*/
/*                 </a>#}*/
/*               </div>*/
/*             */
/*             */
/*             </div>*/
/*               */
/*             */
/*               */
/*               */
/*               */
/*               <div class="form-group">*/
/*               <input id="contact" name="contact" type="hidden" value="" data-validation="required" data-validation-error-msg="{{"You have not answered all required fields"|trans}}">  */
/*               <label for="address_item">{{"Contact(s)"|trans}}</label>*/
/*               <a href="javascript:void(0);" onclick="addContact()">*/
/*                     <span class=" glyphicon glyphicon-plus"></span>*/
/*                 </a>*/
/*               */
/*               */
/*               <input name="contact_item" type="text" class="form-control" id="contact_item" placeholder="{{"TITLE, FULLNAME, PHONE NUMBER EXT, EMAIL"|trans}}"  >*/
/*               */
/*               <br/>*/
/*             */
/*               <div id="contact_list" class="list-group">*/
/*                 {#<a href="#" class="list-group-item">Dapibus ac facilisis in*/
/*                     <span class=" pull-right glyphicon glyphicon-minus"></span>*/
/*                 </a>#}*/
/*               </div>*/
/*             */
/*             */
/*             </div>*/
/*             */
/*               */
/*              {# */
/*              <h3>{{"Previous bills"|trans}}  <span data-toggle="tooltip" data-placement="top" title="{{"All data must be introduced using just numerical values."|trans}}" class="glyphicon glyphicon-info-sign"></span></h3>*/
/*               <div class="well well-lg form-inline">*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_year">{{"Year"|trans}}</label>*/
/*                     <input type="text" class="form-control" id="history_year" name="history['year']" placeholder="2015">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_january">{{"January"|trans}}</label>*/
/*                     <input type="text" name="history['jan']['c']" class="form-control" id="history_consumption_january" placeholder="KW">*/
/*                     <input type="text" name="history['jan']['p']" class="form-control" id="history_payment_january" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_february">{{"February"|trans}}</label>*/
/*                     <input type="text" name="history['feb']['c']" class="form-control" id="history_consumption_february" placeholder="KW">*/
/*                     <input type="text" name="history['feb']['p']" class="form-control" id="history_payment_february" placeholder="$">*/
/*                   </div>  */
/*                    <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_mars">{{"Mars"|trans}}</label>*/
/*                     <input type="text" name="history['mar']['c']" class="form-control" id="history_consumption_mars" placeholder="KW">*/
/*                     <input type="text" name="history['mar']['p']" class="form-control" id="history_payment_mars" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_avril">{{"Avril"|trans}}</label>*/
/*                     <input type="text" name="history['avr']['c']" class="form-control" id="history_consumption_avril" placeholder="KW">*/
/*                     <input type="text" name="history['avr']['p']" class="form-control" id="history_payment_avril" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_may">{{"May"|trans}}</label>*/
/*                     <input type="text" name="history['mai']['c']" class="form-control" id="history_consumption_may" placeholder="KW">*/
/*                     <input type="text" name="history['mai']['p']" class="form-control" id="history_payment_may" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_june">{{"June"|trans}}</label>*/
/*                     <input type="text" name="history['jun']['c']" class="form-control" id="history_consumption_june" placeholder="KW">*/
/*                     <input type="text" name="history['jun']['p']" class="form-control" id="history_payment_june" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_jully">{{"July"|trans}}</label>*/
/*                     <input type="text" name="history['jul']['c']" class="form-control" id="history_consumption_jully" placeholder="KW">*/
/*                     <input type="text" name="history['jul']['p']" class="form-control" id="history_payment_jully" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_august">{{"August"|trans}}</label>*/
/*                     <input type="text" name="history['aug']['c']" class="form-control" id="history_consumption_august" placeholder="KW">*/
/*                     <input type="text" name="history['aug']['p']" class="form-control" id="history_payment_august" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_september">{{"September"|trans}}</label>*/
/*                     <input type="text" name="history['sep']['c']" class="form-control" id="history_consumption_september" placeholder="KW">*/
/*                     <input type="text" name="history['sep']['p']" class="form-control" id="history_payment_september" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_october">{{"October"|trans}}</label>*/
/*                     <input type="text" name="history['oct']['c']" class="form-control" id="history_consumption_october" placeholder="KW">*/
/*                     <input type="text" name="history['oct']['p']" class="form-control" id="history_payment_october" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_november">{{"November"|trans}}</label>*/
/*                     <input type="text" name="history['nov']['c']" class="form-control" id="history_consumption_november" placeholder="KW">*/
/*                     <input type="text" name="history['nov']['p']" class="form-control" id="history_payment_november" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_december">{{"December"|trans}}</label>*/
/*                     <input type="text" name="history['dec']['c']" class="form-control" id="history_consumption_december" placeholder="KW">*/
/*                     <input type="text" name="history['dec']['p']" class="form-control" id="history_payment_december" placeholder="$">*/
/*                   </div>    */
/*                   */
/*               </div>#}*/
/*               */
/*               */
/*          */
/*             */
/*              <h3>{{"Entries"|trans}} <!-- <span data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-info-sign"></span>--></h3>*/
/*               <div class="well well-lg form-inline">*/
/*                   */
/*                   <h4>{{"First Entry"|trans}}</h4>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry1_name">{{"Name"|trans}}</label>*/
/*                     <input name="entries['first']['name']" id="entry1_name"   type="text" class="form-control" id="entry1_name"  placeholder="Entry1">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry1_kw">{{"Power"|trans}} (Kw)</label>*/
/*                     <input name="entries['first']['kw']" id="entry1_kw"   type="text" class="form-control" id="entry1_kw"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry1_priority">{{"Priority"|trans}}</label>*/
/*                     <input value="" name="entries['first']['priority']" id="entry1_priority"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry1_percent">{{"Percentage (%)"|trans}}</label>*/
/*                     <input name="entries['first']['percent']" id="entry1_percent"   type="text" class="form-control" id="entry1_percent"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry1_temperature">{{"Heat alarm (C)"|trans}}</label>*/
/*                     <input name="entries['first']['temperature']" id="entry1_temperature"   type="text" class="form-control" id="entry1_temperature"  placeholder="25">*/
/*                   </div>*/
/*                   <p></p>*/
/*                                     */
/*                   <h4>{{"Second Entry"|trans}}</h4>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry2_name">{{"Name"|trans}}</label>*/
/*                     <input name="entries['second']['name']" id="entry2_name"   type="text" class="form-control" id="entry1_name"  placeholder="Entry2">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry2_kw">{{"Power"|trans}} (Kw)</label>*/
/*                     <input name="entries['second']['kw']" id="entry2_kw"   type="text" class="form-control" id="entry1_kw"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry2_priority">{{"Priority"|trans}}</label>*/
/*                     <input value="" name="entries['second']['priority']" id="entry2_priority"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry2_percent">{{"Percentage (%)"|trans}}</label>*/
/*                     <input name="entries['second']['percent']" id="entry2_percent"   type="text" class="form-control" id="entry1_percent"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry2_temperature">{{"Heat alarm (C)"|trans}}</label>*/
/*                     <input name="entries['second']['temperature']" id="entry2_temperature"   type="text" class="form-control" id="entry1_temperature"  placeholder="25">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   */
/*                   <h4>{{"Third Entry"|trans}}</h4>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry3_name">{{"Name"|trans}}</label>*/
/*                     <input name="entries['third']['name']" id="entry3_name"   type="text" class="form-control" id="entry1_name"  placeholder="Entry3">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry3_kw">{{"Power"|trans}} (Kw)</label>*/
/*                     <input name="entries['third']['kw']" id="entry3_kw"   type="text" class="form-control" id="entry1_kw"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry3_priority">{{"Priority"|trans}}</label>*/
/*                     <input value="" name="entries['third']['priority']" id="entry3_priority"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry3_percent">{{"Percentage (%)"|trans}}</label>*/
/*                     <input name="entries['third']['percent']" id="entry3_percent"   type="text" class="form-control" id="entry1_percent"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry3_temperature">{{"Heat alarm (C)"|trans}}</label>*/
/*                     <input  name="entries['third']['temperature']" id="entry3_temperature"   type="text" class="form-control" id="entry1_temperature"  placeholder="25">*/
/*                   </div>*/
/*                   <p></p>*/
/*                 */
/*                   */
/*               </div> */
/*               */
/*               */
/*               */
/*               */
/*               */
/*               */
/*             */
/*            */
/*             <input type="submit" class="btn btn-primary btn-lg pull-right" value="{{"Submit"|trans}}"/>*/
/*           </form>*/
/*           */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/*     */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* {% block javascripts %}*/
/* {{parent()}}*/
/* <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>*/
/* <script> $.validate(); </script>*/
/* */
/* <script>*/
/* function addAddress(){*/
/*         var address_item    = $("#address_item").val();*/
/*         var address         = new Array();*/
/*         var address_list    = $("#address").val();*/
/*         */
/*         if (address_item.length == 0){*/
/*             return 0;*/
/*         }*/
/*         */
/*         if (address_list.length > 0){*/
/*             address = eval(address_list);*/
/*         }*/
/*         */
/*         //ADD ADDRESS TO THE LIST*/
/*         address.push(address_item);*/
/*         */
/*         //SAVE LIST IN  THE INPUT*/
/*         $("#address").val(JSON.stringify(address));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildAddress();*/
/*         */
/*         //REMOVE ADDRESS TEXT*/
/*         $("#address_item").val("");*/
/*         */
/*     }*/
/*     */
/*     function deleteAddress(id){*/
/*         */
/*         var address_list    = $("#address").val();*/
/*         var address = eval(address_list);*/
/*         */
/*         //SUPPRIME THE ITEM FROM THE LIST*/
/*         address.splice(id, 1);*/
/*         */
/*         //SAVE THE LIST WITHOUT THE ITEM*/
/*         $("#address").val(JSON.stringify(address));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildAddress();*/
/*         */
/*     }*/
/*     */
/*     function rebuildAddress(){*/
/*          var address         = eval($("#address").val());*/
/*          var id_item         = 0;*/
/* */
/*         */
/*          $("#address_list").html("");*/
/*          address.forEach(function(item) {*/
/*              $("#address_list").append('<a onclick="deleteAddress('+id_item+');" href="javascript:void(0);" class="list-group-item">'+item+'<span class=" pull-right glyphicon glyphicon-minus"></span></a>');*/
/*              id_item++;*/
/*         //console.log(item);*/
/*         });*/
/*     }*/
/*         */
/*     */
/* </script>    */
/* */
/* */
/* */
/* <script>*/
/*     function addContact(){*/
/*         var contact_item    = $("#contact_item").val();*/
/*         var contact         = new Array();*/
/*         var contact_list    = $("#contact").val();*/
/*         */
/*         if (contact_item.length == 0){*/
/*             return 0;*/
/*         }*/
/*         */
/*         if (contact_list.length > 0){*/
/*             contact = eval(contact_list);*/
/*         }*/
/*         */
/*         //ADD CONTACT TO THE LIST*/
/*         contact.push(contact_item);*/
/*         */
/*         //SAVE LIST IN  THE INPUT*/
/*         $("#contact").val(JSON.stringify(contact));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildContact();*/
/*         */
/*         //REMOVE CONTACT TEXT*/
/*         $("#contact_item").val("");*/
/*         */
/*     }*/
/*     */
/*     function deleteContact(id){*/
/*         */
/*         var contact_list    = $("#contact").val();*/
/*         var contacts = eval(contact_list);*/
/*         */
/*         //SUPPRIME THE ITEM FROM THE LIST*/
/*         contacts.splice(id, 1);*/
/*         */
/*         //SAVE THE LIST WITHOUT THE ITEM*/
/*         $("#contact").val(JSON.stringify(contacts));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildContact();*/
/*         */
/*     }*/
/*     */
/*     function rebuildContact(){*/
/*          var contact         = eval($("#contact").val());*/
/*          var id_item         = 0;*/
/* */
/*         */
/*          $("#contact_list").html("");*/
/*          contact.forEach(function(item) {*/
/*              $("#contact_list").append('<a onclick="deleteContact('+id_item+');" href="javascript:void(0);" class="list-group-item">'+item+'<span class=" pull-right glyphicon glyphicon-minus"></span></a>');*/
/*              id_item++;*/
/*         //console.log(item);*/
/*         });*/
/*     }*/
/*     */
/*     */
/*     */
/* </script>    */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
