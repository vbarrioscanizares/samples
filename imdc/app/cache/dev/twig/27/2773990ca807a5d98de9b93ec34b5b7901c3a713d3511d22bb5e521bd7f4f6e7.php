<?php

/* clients_board.html.twig */
class __TwigTemplate_6d2f3d645bcdb289acba09bb9042139e05fb074fb9f2161b888076e73255793e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_board.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7d5b75e10fcfe9c288f03a58978cd56a3140659148422d5137848756ebb5417 = $this->env->getExtension("native_profiler");
        $__internal_d7d5b75e10fcfe9c288f03a58978cd56a3140659148422d5137848756ebb5417->enter($__internal_d7d5b75e10fcfe9c288f03a58978cd56a3140659148422d5137848756ebb5417_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_board.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d7d5b75e10fcfe9c288f03a58978cd56a3140659148422d5137848756ebb5417->leave($__internal_d7d5b75e10fcfe9c288f03a58978cd56a3140659148422d5137848756ebb5417_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_c2faeca626a1d1a1fc6de0f188ecd72e9f2937ed4428bfa2ae1f7923a43019c4 = $this->env->getExtension("native_profiler");
        $__internal_c2faeca626a1d1a1fc6de0f188ecd72e9f2937ed4428bfa2ae1f7923a43019c4->enter($__internal_c2faeca626a1d1a1fc6de0f188ecd72e9f2937ed4428bfa2ae1f7923a43019c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "  ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Board"), "html", null, true);
        echo "    ";
        
        $__internal_c2faeca626a1d1a1fc6de0f188ecd72e9f2937ed4428bfa2ae1f7923a43019c4->leave($__internal_c2faeca626a1d1a1fc6de0f188ecd72e9f2937ed4428bfa2ae1f7923a43019c4_prof);

    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        $__internal_9757380b3b4fdabad4f91f7ee5ee6ca9692492dc19bf4543a0a1e086e5e4a2da = $this->env->getExtension("native_profiler");
        $__internal_9757380b3b4fdabad4f91f7ee5ee6ca9692492dc19bf4543a0a1e086e5e4a2da->enter($__internal_9757380b3b4fdabad4f91f7ee5ee6ca9692492dc19bf4543a0a1e086e5e4a2da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "

    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 11
        $this->loadTemplate("sidebar_clients_m.html.twig", "clients_board.html.twig", 11)->display($context);
        // line 12
        echo "    
    
    ";
        // line 14
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_board.html.twig", 14)->display(array_merge($context, array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()))));
        echo "  
    <div class=\" col-md-9s col-md-offset-3 main\">    
    
    

      
    <div class='col-md-12 gauges'>
    
    
     
     <div class=\"col-md-2\" style=\"position: relative;\" id=\"clock\"> <!-- clock -->
         
         
         <div class=\"light_board\" >
             <a href=\"#\" >
                 <img id=\"clock_off\" style=\"display:none;    border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;\" src=\"http://fakeimg.pl/20x20/ff0000/ff0000/\" alt=\"img_off\" class=\"img-rounded thumbnail\">
                 <img id=\"clock_on\"  style=\"display:none; border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;\" src=\"http://fakeimg.pl/20x20/008000/008000/\" alt=\"img_off\" class=\"img-rounded thumbnail\">
                 
             </a>
         </div>
     
     
         <div id=\"container_clock\" style=\"width: 150px; height: 150px; margin-top:30px;\"></div>
         <p></p>
         
     </div>
        
        
        
     <div class=\"col-md-2 text-center\"  style=\"position: relative;\" id=\"gauge1\"> <!-- gauge1 -->
         
         <div class=\"light_board\" >
             <a href=\"#\" >
                 <img id=\"e1_off\"  style=\"display:none; border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;\" src=\"http://fakeimg.pl/20x20/ff0000/ff0000/\" alt=\"img_off\" class=\"img-rounded thumbnail\">
                 <img id=\"e1_on\"   style=\"display:none;    border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;\" src=\"http://fakeimg.pl/20x20/008000/008000/\" alt=\"img_off\" class=\"img-rounded thumbnail\">
             </a>
         </div>
         <div style=\"width:150px;position:absolute;margin-left:50%;left:-75px;margin-top:30px;\" id=\"chart1\"></div>
         
         <div class=\"measures\">
          <p style=\"    \" id=\"e1_pcr\"><span style=\"font-weight: bold;\">";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("P"), "html", null, true);
        echo ":</span><span class=\"value\">s37kW</span></p>   
          <p id=\"e1_t\"><span style=\"font-weight: bold;\">";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("T"), "html", null, true);
        echo ":</span><span class=\"value\">s25C</span></p>
          <p id=\"e1_fu\"><span style=\"font-weight: bold;\">";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("FU"), "html", null, true);
        echo ":</span><span class=\"value\">s100%</span></p>
        </div> 
          
     </div>
        
        
        
     <div class=\"col-md-2  text-center\" style=\"position: relative;\"  id=\"gauge2\"> <!-- gauge2 -->
         
         <div class=\"light_board\" >
             <a href=\"#\" >
                 <img id=\"e2_off\" style=\"display:none; border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;\" src=\"http://fakeimg.pl/20x20/ff0000/ff0000/\" alt=\"img_off\" class=\"img-rounded thumbnail\">
                 <img id=\"e2_on\" style=\"display:none;    border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;\" src=\"http://fakeimg.pl/20x20/008000/008000/\" alt=\"img_off\" class=\"img-rounded thumbnail\">
                 
             </a>
         </div>
         
         
         <div style=\"width:150px;position:absolute;margin-left:50%;left:-75px;margin-top:30px;\" id=\"chart2\"></div>
         <div class=\"measures\">
            <p style=\"\"  id=\"e2_pcr\"><span style=\"font-weight: bold;\">";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("P"), "html", null, true);
        echo ":</span><span class=\"value\">s25.9kW</span></p>   
            <p id=\"e2_t\"><span style=\"font-weight: bold;\">";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("T"), "html", null, true);
        echo ":</span><span class=\"value\">s25C</span></p>
            <p id=\"e2_fu\"><span style=\"font-weight: bold;\">";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("FU"), "html", null, true);
        echo ":</span><span class=\"value\">s70%</span></p>    
         </div> 
     </div>
         
         
         
         
     <div class=\"col-md-2 text-center\" style=\"position: relative;\" id=\"gauge3\"> <!-- gauge3 -->
         <div class=\"light_board\" >
             <a href=\"#\" >
                 <img id=\"e3_off\" style=\"display:none; border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;\" src=\"http://fakeimg.pl/20x20/ff0000/ff0000/\" alt=\"img_off\" class=\"img-rounded thumbnail\">
                 <img id=\"e3_on\" style=\"display:none;    border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;\" src=\"http://fakeimg.pl/20x20/008000/008000/\" alt=\"img_off\" class=\"img-rounded thumbnail\">
             </a>
         </div>
         <div style=\"width:150px;position:absolute;margin-left:50%;left:-75px;margin-top:30px;\" id=\"chart3\"></div>
         <div class=\"measures\">
            <p style=\"\" id=\"e3_pcr\"><span style=\"font-weight: bold;\">";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("P"), "html", null, true);
        echo ":</span><span class=\"value\">s7.4kW</p>   
            <p id=\"e3_t\"><span style=\"font-weight: bold;\">";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("T"), "html", null, true);
        echo ":</span><span class=\"value\">s25C</span></p>
            <p id=\"e3_fu\"><span style=\"font-weight: bold;\">";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("FU"), "html", null, true);
        echo ":</span><span class=\"value\">s20%</span></p>    
         </div>
     </div>
     
     
     
     <div class=\"col-md-2 text-center\" style=\"position: relative;\" id=\"gauge4\"> <!-- gauge4 -->
        
            <div style=\"width:150px;position:absolute;margin-left:50%;left:-75px;margin-top:30px;\" id=\"chart4\"></div>
            
          <div class=\"measures\">
            <p style=\"\" id=\"e4_pcr\"><span style=\"font-weight: bold;\">";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("P"), "html", null, true);
        echo ":</span><span class=\"value\">s70.3kW</p>   
            <p id=\"e4_fu\"><span style=\"font-weight: bold;\">";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("FU"), "html", null, true);
        echo ":</span><span class=\"value\">s72%</span></p>    
          </div>
    </div>
     
         
    </div>    
    
    
     
     
     
     
     
     
     
     
     
     
     
     <div class=\"col-md-12 system_info\"><!-- system_info-->
            
            <div class=\"panel panel-info\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title \">";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("System"), "html", null, true);
        echo "</h3>
                  </div>
                  
                  
                  <div class=\"panel-body\">
                      
                      <div class=\"col-md-12\">
                      
                      <a id=\"live_data_simple_REAL_POWER\" class=\"btn btn-default\" href=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "var" => "power_consumption")), "html", null, true);
        echo "\" role=\"button\">
                          <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Real Power"), "html", null, true);
        echo "\" class=\"description-header value\">s160851Kw</span>
                      </a>  
                      
                       <a id=\"live_data_simple_POWER_CONSUMPTION\" class=\"btn btn-default\" href=\"#\" role=\"button\">
                          <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption"), "html", null, true);
        echo "\" class=\"description-header value\">s</span>
                      </a>  
                    
                      
                      
                      <a id=\"live_data_simple_APPARENT_POWER\" class=\"btn btn-default\" href=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "var" => "power_apparent")), "html", null, true);
        echo "\" role=\"button\">
                          <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apparent Power"), "html", null, true);
        echo "\" class=\"description-header value\">s</span>
                      </a>  
                    
                          
                    <a id=\"live_data_simple_POWER_FACTOR\" class=\"btn btn-default\" href=\"";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "var" => "power_factor")), "html", null, true);
        echo "\" role=\"button\">
                          <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power Factor"), "html", null, true);
        echo "\" class=\"description-header value\">s</span>
                    </a>       
                    
                           
                    <a id=\"live_data_simple_UTILISATION_FACTOR\" class=\"btn btn-default\" href=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "var" => "utilisation_factor")), "html", null, true);
        echo "\" role=\"button\">
                          <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Utilisation Factor"), "html", null, true);
        echo "\"class=\"description-header value\">s</span>
                    </a>
                     
                    
                    <br/>
                    
                           
                    <a id=\"live_data_simple_TENSION\" class=\"btn btn-default\" href=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "var" => "tension")), "html", null, true);
        echo "\" role=\"button\">
                          <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tension"), "html", null, true);
        echo "\"class=\"description-header value\">s</span>
                    </a>
                    
                    <a id=\"live_data_simple_FREQUENCY\" class=\"btn btn-default\" href=\"";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "var" => "frequency")), "html", null, true);
        echo "\" role=\"button\">
                          <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        // line 172
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Frequency"), "html", null, true);
        echo "\"class=\"description-header value\">s</span>
                    </a>
                    
                    
                    
                     <a id=\"live_data_simple_TOTAL_HARMONIC_DISTORSION\" class=\"btn btn-default\" href=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "var" => "thd")), "html", null, true);
        echo "\" role=\"button\">
                          <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("THD"), "html", null, true);
        echo "\"class=\"description-header value\">s</span>
                    </a>
                    
                    
                     <a id=\"live_data_simple_TOTAL_CURRENT_DEMAND_DISTORSION\" class=\"btn btn-default\" href=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "var" => "tcdd")), "html", null, true);
        echo "\" role=\"button\">
                          <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("TCDD"), "html", null, true);
        echo "\"class=\"description-header value\">s</span>
                    </a>
                    
                    
                    <a id=\"live_data_simple_KFACTOR\" class=\"btn btn-default\" href=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "var" => "k-factor")), "html", null, true);
        echo "\" role=\"button\">
                          <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("K-FACTOR"), "html", null, true);
        echo "\"class=\"description-header value\">s</span>
                    </a>
                    
                    
                   ";
        // line 195
        echo "                    
                   </div>
                   
                  </div>
                
            </div>
            
        </div>     
    
    
    
        <div class=\"col-md-12\"></div>
        
        
        
        
    
        <div class=\"col-md-12\" id=\"dynamic_charts\">
            
            <div class=\"col-md-6\" id=\"dynamic_chart1\">
            
                 <!-- Power Chart1 -->
                    <div class=\"panel panel-info\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title \">";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power"), "html", null, true);
        echo " (Kw)</h3>
                          </div>
                          <div class=\"panel-body\">
                               <div id=\"container_power_chart\" style=\"min-width: 310px; height: 400px; margin: 0 auto\"></div> 

                          </div>

                    </div>

              
            
            </div>
            
            
            
            <div class=\"col-md-6\" id=\"dynamic_chart2\">
            
                 <!-- Consumption Chart2 -->
                    <div class=\"panel panel-info\">
                        <div class=\"panel-heading\">
                            <h3 class=\"panel-title \">";
        // line 239
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption"), "html", null, true);
        echo " (Kwh)</h3>
                          </div>
                          <div class=\"panel-body\">
                              <div id=\"container_consumption_chart\" style=\"min-width: 310px; height: 400px; margin: 0 auto\"></div>
";
        // line 244
        echo "                          </div>

                    </div>

                
            </div>
            
            
            
        </div>
        
        
        
        
        
        <div class=\"col-md-12\"></div>
        
        <div class=\"col-md-12\"> <!-- Alertes -->
            <div class=\"panel panel-info\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title \">";
        // line 264
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Alarms"), "html", null, true);
        echo "</h3>
                  </div>
                  <div class=\"panel-body\">
                      <div class=\"alert alert-success\" role=\"alert\">
                          <span id=\"alarm_list\">
                            ";
        // line 269
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("0 Alarms in the system!"), "html", null, true);
        echo "
                          </span>
                      </div>
                    
                  </div>
                
            </div>
            
        </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    

     
      
          ";
        // line 297
        echo "            
        </div>
    </div>
   
     
</div>
         
     
    
";
        
        $__internal_9757380b3b4fdabad4f91f7ee5ee6ca9692492dc19bf4543a0a1e086e5e4a2da->leave($__internal_9757380b3b4fdabad4f91f7ee5ee6ca9692492dc19bf4543a0a1e086e5e4a2da_prof);

    }

    // line 308
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_43894aa918b0aed6669e7e6375efa5248e6990301c82c81529d4bdd7e498abd1 = $this->env->getExtension("native_profiler");
        $__internal_43894aa918b0aed6669e7e6375efa5248e6990301c82c81529d4bdd7e498abd1->enter($__internal_43894aa918b0aed6669e7e6375efa5248e6990301c82c81529d4bdd7e498abd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 309
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "   
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 310
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/jqueryclock/css/analog.css"), "html", null, true);
        echo "\"> 

<style>
 /*HIDE CONTENT UNTIL UPDATE DATA FROM SERVER*/
    .row{opacity: 0;}   
    div#container_power_chart div#highcharts-5.highcharts-container > svg > text:last-child{opacity: 0;}
    p span.description-text{font-size: small;}
</style>
";
        
        $__internal_43894aa918b0aed6669e7e6375efa5248e6990301c82c81529d4bdd7e498abd1->leave($__internal_43894aa918b0aed6669e7e6375efa5248e6990301c82c81529d4bdd7e498abd1_prof);

    }

    // line 321
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_fc48de541e1b8ece421e9250f03f27c46beb12ca43513792b14b9479216f21da = $this->env->getExtension("native_profiler");
        $__internal_fc48de541e1b8ece421e9250f03f27c46beb12ca43513792b14b9479216f21da->enter($__internal_fc48de541e1b8ece421e9250f03f27c46beb12ca43513792b14b9479216f21da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 322
        echo "     ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
     
        <script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>
        <script src=\"";
        // line 325
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/jqueryclock/js/jquery.clock.js"), "html", null, true);
        echo "\"></script>\t
        <script src=\"";
        // line 326
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/jquery.blockUI.js"), "html", null, true);
        echo "\"></script> 
        <script src=\"https://code.highcharts.com/highcharts.js\"></script>
        <script src=\"https://code.highcharts.com/highcharts-more.js\"></script>
        <script src=\"https://code.highcharts.com/modules/exporting.js\"></script>


     <script type='text/javascript'>
            \$.blockUI({ message: null });

             /* SETTING CLOCK VALUES */
             var clock;
            \$(document).ready(function() {
                  /*SETTING SAMPLE CLOCK*/  
                  \$('#analog-clock').clock({
                      offset: '+5', 
                      type: 'analog', 
                      backgroundColor: 'rgba(0,0,0,0)'
                      
                  });
                  /*SETTING ALL CLOCKS*/  
            });
        
        
            jQuery(document).ready(function() {
                    window.setInterval(function(){
                              data_update();
                            }, 1000);
            });
         
         
         
        google.load(\"visualization\", \"1\", {packages:[\"gauge\"]});
\tgoogle.setOnLoadCallback(drawGauges);
\t
\tfunction drawGauges() {
        
        window.data1 = new google.visualization.DataTable();
        window.data1.addColumn('number', '";
        // line 363
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry1"]) ? $context["entry1"] : $this->getContext($context, "entry1")), "'name'", array(), "array"), "html", null, true);
        echo "');
        window.data1.addRows(2);
        
        window.data2 = new google.visualization.DataTable();
        window.data2.addColumn('number', '";
        // line 367
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry2"]) ? $context["entry2"] : $this->getContext($context, "entry2")), "'name'", array(), "array"), "html", null, true);
        echo "');
        window.data2.addRows(2);
        
        window.data3 = new google.visualization.DataTable();
        window.data3.addColumn('number', '";
        // line 371
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry3"]) ? $context["entry3"] : $this->getContext($context, "entry3")), "'name'", array(), "array"), "html", null, true);
        echo "');
        window.data3.addRows(2);
        
        window.data4 = new google.visualization.DataTable();
        window.data4.addColumn('number', 'Total');
        window.data4.addRows(2);
        
\t
\twindow.options = {
\t  width: 150, height: 150,
\t  redFrom: 90, redTo: 100,
\t  yellowFrom:75, yellowTo: 90,
\t  minorTicks: 5
      };
\t
        window.chart1 = new google.visualization.Gauge(document.getElementById('chart1'));
        window.chart2 = new google.visualization.Gauge(document.getElementById('chart2'));
        window.chart3 = new google.visualization.Gauge(document.getElementById('chart3'));
        window.chart4 = new google.visualization.Gauge(document.getElementById('chart4'));
           
       window.chart1.draw(window.data1, options);
       window.chart2.draw(window.data2, options);
       window.chart3.draw(window.data3, options);
       window.chart4.draw(window.data4, options);
\t
";
        // line 418
        echo "\t
\t}
\t
    
    function data_update()
\t{
\t\t\t\t\t\t
                        
                        \$.ajax({
                          url: \"http://";
        // line 427
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getServerIp", array(), "method"), "html", null, true);
        echo "/lab/datalive_client.php\",
                        }).done(function(data) {

                                //pagejson = JSON.parse(data);
                                
                                var pagejson = data;
                                
                                /*SAVING DATA FOR EXTERNAL USE IN THE PAGE*/
                                window.time_hour     = pagejson.time_hour;
                                window.time_minute   = pagejson.time_minute;
                                window.time_second   = pagejson.time_second;
                                window.current_power_consumption    = pagejson.real_power.value;//pagejson.power_consumption;
                                window.current_consumption          = pagejson.power_consumption.value;
                                /*CLIENT DATA*/
                                
                                /*
                                \$(\"#client_logo\").attr(\"src\",pagejson.detail_client.logo);
                                \$(\"#client_name\").html(pagejson.detail_client.nom);
                                \$(\"#client_address\").html(\"\");
                                \$(\"#client_contact\").html(\"\");

                                pagejson.detail_client.address.forEach(function(address)
                                {\$(\"#client_address\").append(address.street + ', ' + address.prov + ', ' + address.pcode + \"<br/>\");});


                                pagejson.detail_client.contact.forEach(function(contact)
                                {\$(\"#client_contact\").append(contact.fonction + '. ' + contact.nom + ', ' + contact.phone + \"<br/>\");\t});
                                */
                                /*REFRESH WM40 MESURES READING*/
                                \$(\"#live_data_simple_REAL_POWER span.value\").html(pagejson.real_power.value+pagejson.real_power.unit);
                                //\$(\"#live_data_simple_ACTIVE_POWER span.value\").html(pagejson.real_power.value+pagejson.real_power.unit);
                                \$(\"#live_data_simple_POWER_CONSUMPTION span.value\").html(pagejson.power_consumption.value+pagejson.power_consumption.unit);
                                \$(\"#live_data_simple_APPARENT_POWER span.value\").html(pagejson.apparent_power.value+pagejson.apparent_power.unit);
                                \$(\"#live_data_simple_POWER_FACTOR span.value\").html(pagejson.power_factor.value+pagejson.power_factor.unit);
                                \$(\"#live_data_simple_UTILISATION_FACTOR span.value\").html(pagejson.utilisation_factor.value+pagejson.utilisation_factor.unit);
                                \$(\"#live_data_simple_TENSION span.value\").html(pagejson.tension.value+pagejson.tension.unit);
                                \$(\"#live_data_simple_FREQUENCY span.value\").html(pagejson.frequency.value+pagejson.frequency.unit);
                                \$(\"#live_data_simple_TOTAL_HARMONIC_DISTORSION span.value\").html(pagejson.total_harmonic_distorsion.value+pagejson.total_harmonic_distorsion.unit);
                                \$(\"#live_data_simple_TOTAL_CURRENT_DEMAND_DISTORSION span.value\").html(pagejson.total_current_demand_distorsion.value+pagejson.total_current_demand_distorsion.unit);
                                \$(\"#live_data_simple_KFACTOR span.value\").html(pagejson.k_factor.value+pagejson.k_factor.unit);
                                
                                /*REFRESH GAUGES INFORMATION*/
                  
                                window.data1 = google.visualization.arrayToDataTable([
                                  ['Label', 'Value'],
                                  [\"";
        // line 472
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry1"]) ? $context["entry1"] : $this->getContext($context, "entry1")), "'name'", array(), "array"), "html", null, true);
        echo "\"/*pagejson.E1.name*/, pagejson.E1.powerConsumptionRealPercent]
                                ]);
                                
                                window.data2 = google.visualization.arrayToDataTable([
                                  ['Label', 'Value'],
                                  [\"";
        // line 477
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry2"]) ? $context["entry2"] : $this->getContext($context, "entry2")), "'name'", array(), "array"), "html", null, true);
        echo "\"/*pagejson.E2.name*/, pagejson.E2.powerConsumptionRealPercent]
                              
                                ]);
                                
                                window.data3 = google.visualization.arrayToDataTable([
                                  ['Label', 'Value'],
                                  [\"";
        // line 483
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry3"]) ? $context["entry3"] : $this->getContext($context, "entry3")), "'name'", array(), "array"), "html", null, true);
        echo "\"/*pagejson.E3.name*/, pagejson.E3.powerConsumptionRealPercent]
                          
                                ]);
                                
                                window.data4 = google.visualization.arrayToDataTable([
                                  ['Label', 'Value'],
                                  [\"Total\"/*pagejson.TOTAL.name*/, pagejson.TOTAL.powerConsumptionRealPercent]
                                ]);

                                window.chart1.draw(window.data1, window.options);
                                window.chart2.draw(window.data2, window.options);
                                window.chart3.draw(window.data3, window.options);
                                window.chart4.draw(window.data4, window.options);
                                
                                \$(\"#e1_pcr span.value\").html(pagejson.E1.powerConsumptionReal+pagejson.E1.powerConsumptionUnit);
                                \$(\"#e1_t span.value\").html(pagejson.E1.temperatureCelcius+\"C\");
                                \$(\"#e1_fu span.value\").html(pagejson.E1.powerConsumptionRealPercent+\"%\");
                                
                                \$(\"#e2_pcr span.value\").html(pagejson.E2.powerConsumptionReal+pagejson.E2.powerConsumptionUnit);
                                \$(\"#e2_t span.value\").html(pagejson.E2.temperatureCelcius+\"C\");
                                \$(\"#e2_fu span.value\").html(pagejson.E2.powerConsumptionRealPercent+\"%\");
                                
                                \$(\"#e3_pcr span.value\").html(pagejson.E3.powerConsumptionReal+pagejson.E3.powerConsumptionUnit);
                                \$(\"#e3_t span.value\").html(pagejson.E3.temperatureCelcius+\"C\");
                                \$(\"#e3_fu span.value\").html(pagejson.E3.powerConsumptionRealPercent+\"%\");
                                
                                \$(\"#e4_pcr span.value\").html(pagejson.TOTAL.powerConsumptionReal+pagejson.TOTAL.powerConsumptionUnit);
                                \$(\"#e4_fu span.value\").html(pagejson.TOTAL.powerConsumptionRealPercent+\"%\");
                                
                                
                                \$('#chart1 circle:nth-child(1)').attr('stroke', '#00B4EB'); //outer-ring
                                \$('#chart1 circle:nth-child(1)').attr('fill', '#00B4EB'); //outer-ring
                                
                                \$('#chart2 circle:nth-child(1)').attr('stroke', '#00B4EB'); //outer-ring
                                \$('#chart2 circle:nth-child(1)').attr('fill', '#00B4EB'); //outer-ring
                                
                                \$('#chart3 circle:nth-child(1)').attr('stroke', '#00B4EB'); //outer-ring
                                \$('#chart3 circle:nth-child(1)').attr('fill', '#00B4EB'); //outer-ring
                                
                                \$('#chart4 circle:nth-child(1)').attr('stroke', '#00B4EB'); //outer-ring
                                \$('#chart4 circle:nth-child(1)').attr('fill', '#00B4EB'); //outer-ring

                                /*REFRESH DATA [GAUGES ON OR OFF...]*/
                                if(pagejson.clock == \"1\"){
                                    \$(\"#clock_on\").show();
                                    \$(\"#clock_off\").hide();
                                }else if(pagejson.clock == \"0\"){
                                    \$(\"#clock_off\").show();
                                    \$(\"#clock_on\").hide()
                                }
                                
                                if(pagejson.E1.active == \"1\"){
                                    \$(\"#e1_on\").show();
                                    \$(\"#e1_off\").hide();
                                }else if(pagejson.E1.active == \"0\"){
                                    \$(\"#e1_off\").show();
                                    \$(\"#e1_on\").hide()
                                }
                                
                                if(pagejson.E2.active == \"1\"){
                                    \$(\"#e2_on\").show();
                                    \$(\"#e2_off\").hide();
                                }else if(pagejson.E2.active == \"0\"){
                                    \$(\"#e2_off\").show();
                                    \$(\"#e2_on\").hide()
                                }     
                                
                                if(pagejson.E3.active == \"1\"){
                                    \$(\"#e3_on\").show();
                                    \$(\"#e3_off\").hide();
                                }else if(pagejson.E3.active == \"0\"){
                                    \$(\"#e3_off\").show();
                                    \$(\"#e3_on\").hide()
                                }     
                                
                                
                                /*SHOW PAGE WITH DATA VALIDATED*/
                                \$(\".row\").css(\"opacity\",\"1\");
                                \$.unblockUI();
                                return false;
                        });
                        
                        
\t\t\t
\t}
\t  
     </script>
     
     
     
     
     <script>
         /******  CLOCK - HIGHCHARTS   ******/
         
         
         
         \$(function () {

    /**
     * Get the current time
     */
    function getNow() {
        var now = new Date();

        return {
            hours: now.getHours() + now.getMinutes() / 60,
            minutes: now.getMinutes() * 12 / 60 + now.getSeconds() * 12 / 3600,
            seconds: now.getSeconds() * 12 / 60
        };
    }

    /**
     * Pad numbers
     */
   /* function pad(number, length) {
        // Create an array of the remaining length + 1 and join it with 0's
        return new Array((length || 2) + 1 - String(number).length).join(0) + number;
    }*/

    var now = getNow();

    // Create the chart
    \$('#container_clock').highcharts({

        chart: {
            type: 'gauge',
            backgroundColor: 'none',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            height: 160,
            width: 160
        },

        credits: {
            enabled: false
        },
        
        exporting: {
                enabled: false
            },

        title: {
            text: ''
        },

        pane: {
            background: [{
                // default background
            }, {
                // reflex for supported browsers
                backgroundColor: Highcharts.svg ? {
                    radialGradient: {
                        cx: 0.5,
                        cy: -0.4,
                        r: 1.9
                    },
                    stops: [
                        [0.5, 'rgba(255, 255, 255, 0.2)'],
                        [0.5, 'rgba(200, 200, 200, 0.2)']
                    ]
                } : null
            }]
        },

        yAxis: {
            labels: {
                distance: -20
            },
            min: 0,
            max: 12,
            lineWidth: 0,
            showFirstLabel: false,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 5,
            minorTickPosition: 'inside',
            minorGridLineWidth: 0,
            minorTickColor: '#666',

            tickInterval: 1,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            title: {
                text: 'HQ',
                style: {
                    color: '#BBB',
                    fontWeight: 'normal',
                    fontSize: '8px',
                    lineHeight: '10px'
                },
                y: 10
            }
        },

        tooltip: {
            formatter: function () {
                return this.series.chart.tooltipText;
            }
        },

        series: [{
            data: [{
                id: 'hour',
                y: now.hours,
                dial: {
                    radius: '60%',
                    baseWidth: 4,
                    baseLength: '95%',
                    rearLength: 0
                }
            }, {
                id: 'minute',
                y: now.minutes,
                dial: {
                    baseLength: '95%',
                    rearLength: 0
                }
            }, {
                id: 'second',
                y: now.seconds,
                dial: {
                    radius: '100%',
                    baseWidth: 1,
                    rearLength: '20%'
                }
            }],
            animation: false,
            dataLabels: {
                enabled: false
            }
        }]
    },

        // Move
        function (chart) {
            setInterval(function () {

                now = getNow();

                var hour = chart.get('hour'),
                    minute = chart.get('minute'),
                    second = chart.get('second'),
                    // run animation unless we're wrapping around from 59 to 0
                    animation = now.seconds === 0 ?
                        false :
                        {
                            easing: 'easeOutElastic'
                        };

                // Cache the tooltip text
                chart.tooltipText = window.time_hour + ':' + window.time_minute + ':' +window.time_second;
                    ";
        // line 744
        echo "                            
                            
                var hours =  parseInt(window.time_hour);
                var minutes = parseInt(window.time_minute)  * 12 / 60 + parseInt(window.time_second) * 12 / 3600;
                var seconds =  parseInt(window.time_second) * 12 / 60;
                            
                hour.update(hours , true, animation);
                minute.update(minutes , true, animation);
                second.update(seconds, true, animation);

            }, 1000);

        });
});

// Extend jQuery with some easing (copied from jQuery UI)
\$.extend(\$.easing, {
    /* eslint-disable */
    easeOutElastic: function (x, t, b, c, d) {
        var s=1.70158;var p=0;var a=c;
        if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
        if (a < Math.abs(c)) { a=c; var s=p/4; }
        else var s = p/(2*Math.PI) * Math.asin (c/a);
        return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
    }
    /* eslint-enable */
});
         
         
     </script>    
    
     
     
     
     
    <script>
    /*   CHART POWER REAL CONSUMPTION   */  
    
    
    
    
    \$(function () {
    \$(document).ready(function () {
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
        
        
        
        \$('#container_consumption_chart').highcharts({
            chart: {
                type: 'area',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = window.current_consumption;//Math.random();
                            series.addPoint([x, y], true, true);
                        }, 1000);
                    }
                }
            },
            title: {
                text: '";
        // line 814
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption"), "html", null, true);
        echo "'
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Kwh'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
             plotOptions: {
             area: {
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 2
                    }
                },
                marker: {
                    enabled: false
                }, 
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                }
            }
            ";
        // line 873
        echo "        },        
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'P',
                data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: Math.random()
                        });
                    }
                    return data;
                }())
            }]
        });
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

        \$('#container_power_chart').highcharts({
            chart: {
                type: 'area',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function () {

                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function () {
                            var x = (new Date()).getTime(), // current time
                                y = window.current_power_consumption;//Math.random();
                            series.addPoint([x, y], true, true);
                        }, 1000);
                    }
                }
            },
            
            
            plotOptions: {
           area: {
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 2
                    }
                },
                marker: {
                    enabled: false
                }, 
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                }
            }
            ";
        // line 970
        echo "        },     
       
            
            title: {
                text: '";
        // line 974
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power"), "html", null, true);
        echo "'
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 150
            },
            yAxis: {
                title: {
                    text: 'Kw'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                        Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                        Highcharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: 'P',
                data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: Math.random()
                        });
                    }
                    return data;
                }())
            }]
        });
    });
}); 
        
        
        
        
    </script>
     
     
     
     
     
     
";
        
        $__internal_fc48de541e1b8ece421e9250f03f27c46beb12ca43513792b14b9479216f21da->leave($__internal_fc48de541e1b8ece421e9250f03f27c46beb12ca43513792b14b9479216f21da_prof);

    }

    public function getTemplateName()
    {
        return "clients_board.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1150 => 974,  1144 => 970,  1057 => 873,  1007 => 814,  935 => 744,  676 => 483,  667 => 477,  659 => 472,  611 => 427,  600 => 418,  572 => 371,  565 => 367,  558 => 363,  518 => 326,  514 => 325,  507 => 322,  501 => 321,  485 => 310,  481 => 309,  475 => 308,  459 => 297,  430 => 269,  422 => 264,  400 => 244,  393 => 239,  370 => 219,  344 => 195,  337 => 188,  333 => 187,  326 => 183,  322 => 182,  315 => 178,  311 => 177,  303 => 172,  299 => 171,  293 => 168,  289 => 167,  279 => 160,  275 => 159,  268 => 155,  264 => 154,  257 => 150,  253 => 149,  245 => 144,  238 => 140,  234 => 139,  223 => 131,  197 => 108,  193 => 107,  179 => 96,  175 => 95,  171 => 94,  152 => 78,  148 => 77,  144 => 76,  121 => 56,  117 => 55,  113 => 54,  70 => 14,  66 => 12,  64 => 11,  57 => 6,  51 => 5,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block title %}  {{"Board"|trans}}    {% endblock %}*/
/* */
/* {% block content %}*/
/* */
/* */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients_m.html.twig' %}*/
/*     */
/*     */
/*     {% include 'clients_breadcrumb.html.twig' with {'client_id': client.id}%}  */
/*     <div class=" col-md-9s col-md-offset-3 main">    */
/*     */
/*     */
/* */
/*       */
/*     <div class='col-md-12 gauges'>*/
/*     */
/*     */
/*      */
/*      <div class="col-md-2" style="position: relative;" id="clock"> <!-- clock -->*/
/*          */
/*          */
/*          <div class="light_board" >*/
/*              <a href="#" >*/
/*                  <img id="clock_off" style="display:none;    border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;" src="http://fakeimg.pl/20x20/ff0000/ff0000/" alt="img_off" class="img-rounded thumbnail">*/
/*                  <img id="clock_on"  style="display:none; border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;" src="http://fakeimg.pl/20x20/008000/008000/" alt="img_off" class="img-rounded thumbnail">*/
/*                  */
/*              </a>*/
/*          </div>*/
/*      */
/*      */
/*          <div id="container_clock" style="width: 150px; height: 150px; margin-top:30px;"></div>*/
/*          <p></p>*/
/*          */
/*      </div>*/
/*         */
/*         */
/*         */
/*      <div class="col-md-2 text-center"  style="position: relative;" id="gauge1"> <!-- gauge1 -->*/
/*          */
/*          <div class="light_board" >*/
/*              <a href="#" >*/
/*                  <img id="e1_off"  style="display:none; border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;" src="http://fakeimg.pl/20x20/ff0000/ff0000/" alt="img_off" class="img-rounded thumbnail">*/
/*                  <img id="e1_on"   style="display:none;    border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;" src="http://fakeimg.pl/20x20/008000/008000/" alt="img_off" class="img-rounded thumbnail">*/
/*              </a>*/
/*          </div>*/
/*          <div style="width:150px;position:absolute;margin-left:50%;left:-75px;margin-top:30px;" id="chart1"></div>*/
/*          */
/*          <div class="measures">*/
/*           <p style="    " id="e1_pcr"><span style="font-weight: bold;">{{"P"|trans}}:</span><span class="value">s37kW</span></p>   */
/*           <p id="e1_t"><span style="font-weight: bold;">{{"T"|trans}}:</span><span class="value">s25C</span></p>*/
/*           <p id="e1_fu"><span style="font-weight: bold;">{{"FU"|trans}}:</span><span class="value">s100%</span></p>*/
/*         </div> */
/*           */
/*      </div>*/
/*         */
/*         */
/*         */
/*      <div class="col-md-2  text-center" style="position: relative;"  id="gauge2"> <!-- gauge2 -->*/
/*          */
/*          <div class="light_board" >*/
/*              <a href="#" >*/
/*                  <img id="e2_off" style="display:none; border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;" src="http://fakeimg.pl/20x20/ff0000/ff0000/" alt="img_off" class="img-rounded thumbnail">*/
/*                  <img id="e2_on" style="display:none;    border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;" src="http://fakeimg.pl/20x20/008000/008000/" alt="img_off" class="img-rounded thumbnail">*/
/*                  */
/*              </a>*/
/*          </div>*/
/*          */
/*          */
/*          <div style="width:150px;position:absolute;margin-left:50%;left:-75px;margin-top:30px;" id="chart2"></div>*/
/*          <div class="measures">*/
/*             <p style=""  id="e2_pcr"><span style="font-weight: bold;">{{"P"|trans}}:</span><span class="value">s25.9kW</span></p>   */
/*             <p id="e2_t"><span style="font-weight: bold;">{{"T"|trans}}:</span><span class="value">s25C</span></p>*/
/*             <p id="e2_fu"><span style="font-weight: bold;">{{"FU"|trans}}:</span><span class="value">s70%</span></p>    */
/*          </div> */
/*      </div>*/
/*          */
/*          */
/*          */
/*          */
/*      <div class="col-md-2 text-center" style="position: relative;" id="gauge3"> <!-- gauge3 -->*/
/*          <div class="light_board" >*/
/*              <a href="#" >*/
/*                  <img id="e3_off" style="display:none; border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;" src="http://fakeimg.pl/20x20/ff0000/ff0000/" alt="img_off" class="img-rounded thumbnail">*/
/*                  <img id="e3_on" style="display:none;    border-radius: 20px;-webkit-border-radius: 20px;-moz-border-radius: 20px;" src="http://fakeimg.pl/20x20/008000/008000/" alt="img_off" class="img-rounded thumbnail">*/
/*              </a>*/
/*          </div>*/
/*          <div style="width:150px;position:absolute;margin-left:50%;left:-75px;margin-top:30px;" id="chart3"></div>*/
/*          <div class="measures">*/
/*             <p style="" id="e3_pcr"><span style="font-weight: bold;">{{"P"|trans}}:</span><span class="value">s7.4kW</p>   */
/*             <p id="e3_t"><span style="font-weight: bold;">{{"T"|trans}}:</span><span class="value">s25C</span></p>*/
/*             <p id="e3_fu"><span style="font-weight: bold;">{{"FU"|trans}}:</span><span class="value">s20%</span></p>    */
/*          </div>*/
/*      </div>*/
/*      */
/*      */
/*      */
/*      <div class="col-md-2 text-center" style="position: relative;" id="gauge4"> <!-- gauge4 -->*/
/*         */
/*             <div style="width:150px;position:absolute;margin-left:50%;left:-75px;margin-top:30px;" id="chart4"></div>*/
/*             */
/*           <div class="measures">*/
/*             <p style="" id="e4_pcr"><span style="font-weight: bold;">{{"P"|trans}}:</span><span class="value">s70.3kW</p>   */
/*             <p id="e4_fu"><span style="font-weight: bold;">{{"FU"|trans}}:</span><span class="value">s72%</span></p>    */
/*           </div>*/
/*     </div>*/
/*      */
/*          */
/*     </div>    */
/*     */
/*     */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/*      <div class="col-md-12 system_info"><!-- system_info-->*/
/*             */
/*             <div class="panel panel-info">*/
/*                 <div class="panel-heading">*/
/*                     <h3 class="panel-title ">{{"System"|trans}}</h3>*/
/*                   </div>*/
/*                   */
/*                   */
/*                   <div class="panel-body">*/
/*                       */
/*                       <div class="col-md-12">*/
/*                       */
/*                       <a id="live_data_simple_REAL_POWER" class="btn btn-default" href="{{path('_clients_chart', {'client_id':client.id,'var': 'power_consumption'})}}" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{"Real Power"|trans}}" class="description-header value">s160851Kw</span>*/
/*                       </a>  */
/*                       */
/*                        <a id="live_data_simple_POWER_CONSUMPTION" class="btn btn-default" href="#" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{"Consumption"|trans}}" class="description-header value">s</span>*/
/*                       </a>  */
/*                     */
/*                       */
/*                       */
/*                       <a id="live_data_simple_APPARENT_POWER" class="btn btn-default" href="{{path('_clients_chart', {'client_id':client.id,'var': 'power_apparent'})}}" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{"Apparent Power"|trans}}" class="description-header value">s</span>*/
/*                       </a>  */
/*                     */
/*                           */
/*                     <a id="live_data_simple_POWER_FACTOR" class="btn btn-default" href="{{path('_clients_chart', {'client_id':client.id,'var': 'power_factor'})}}" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{"Power Factor"|trans}}" class="description-header value">s</span>*/
/*                     </a>       */
/*                     */
/*                            */
/*                     <a id="live_data_simple_UTILISATION_FACTOR" class="btn btn-default" href="{{path('_clients_chart', {'client_id':client.id,'var': 'utilisation_factor'})}}" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{"Utilisation Factor"|trans}}"class="description-header value">s</span>*/
/*                     </a>*/
/*                      */
/*                     */
/*                     <br/>*/
/*                     */
/*                            */
/*                     <a id="live_data_simple_TENSION" class="btn btn-default" href="{{path('_clients_chart', {'client_id':client.id,'var': 'tension'})}}" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{"Tension"|trans}}"class="description-header value">s</span>*/
/*                     </a>*/
/*                     */
/*                     <a id="live_data_simple_FREQUENCY" class="btn btn-default" href="{{path('_clients_chart', {'client_id':client.id,'var': 'frequency'})}}" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{"Frequency"|trans}}"class="description-header value">s</span>*/
/*                     </a>*/
/*                     */
/*                     */
/*                     */
/*                      <a id="live_data_simple_TOTAL_HARMONIC_DISTORSION" class="btn btn-default" href="{{path('_clients_chart', {'client_id':client.id,'var': 'thd'})}}" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{"THD"|trans}}"class="description-header value">s</span>*/
/*                     </a>*/
/*                     */
/*                     */
/*                      <a id="live_data_simple_TOTAL_CURRENT_DEMAND_DISTORSION" class="btn btn-default" href="{{path('_clients_chart', {'client_id':client.id,'var': 'tcdd'})}}" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{"TCDD"|trans}}"class="description-header value">s</span>*/
/*                     </a>*/
/*                     */
/*                     */
/*                     <a id="live_data_simple_KFACTOR" class="btn btn-default" href="{{path('_clients_chart', {'client_id':client.id,'var': 'k-factor'})}}" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{"K-FACTOR"|trans}}"class="description-header value">s</span>*/
/*                     </a>*/
/*                     */
/*                     */
/*                    {# <a id="" class="btn btn-default" href="{{path('_clients_chart', {'client_id':client.id,'var': 'tension'})}}" role="button">*/
/*                           <span data-toggle="tooltip" data-placement="right" title="{{""|trans}}"class="description-header value">s</span>*/
/*                     </a>#}*/
/*                     */
/*                    </div>*/
/*                    */
/*                   </div>*/
/*                 */
/*             </div>*/
/*             */
/*         </div>     */
/*     */
/*     */
/*     */
/*         <div class="col-md-12"></div>*/
/*         */
/*         */
/*         */
/*         */
/*     */
/*         <div class="col-md-12" id="dynamic_charts">*/
/*             */
/*             <div class="col-md-6" id="dynamic_chart1">*/
/*             */
/*                  <!-- Power Chart1 -->*/
/*                     <div class="panel panel-info">*/
/*                         <div class="panel-heading">*/
/*                             <h3 class="panel-title ">{{"Power"|trans}} (Kw)</h3>*/
/*                           </div>*/
/*                           <div class="panel-body">*/
/*                                <div id="container_power_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div> */
/* */
/*                           </div>*/
/* */
/*                     </div>*/
/* */
/*               */
/*             */
/*             </div>*/
/*             */
/*             */
/*             */
/*             <div class="col-md-6" id="dynamic_chart2">*/
/*             */
/*                  <!-- Consumption Chart2 -->*/
/*                     <div class="panel panel-info">*/
/*                         <div class="panel-heading">*/
/*                             <h3 class="panel-title ">{{"Consumption"|trans}} (Kwh)</h3>*/
/*                           </div>*/
/*                           <div class="panel-body">*/
/*                               <div id="container_consumption_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>*/
/* {#                             <div id="container_power_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div> #}*/
/*                           </div>*/
/* */
/*                     </div>*/
/* */
/*                 */
/*             </div>*/
/*             */
/*             */
/*             */
/*         </div>*/
/*         */
/*         */
/*         */
/*         */
/*         */
/*         <div class="col-md-12"></div>*/
/*         */
/*         <div class="col-md-12"> <!-- Alertes -->*/
/*             <div class="panel panel-info">*/
/*                 <div class="panel-heading">*/
/*                     <h3 class="panel-title ">{{"Alarms"|trans}}</h3>*/
/*                   </div>*/
/*                   <div class="panel-body">*/
/*                       <div class="alert alert-success" role="alert">*/
/*                           <span id="alarm_list">*/
/*                             {{"0 Alarms in the system!"|trans}}*/
/*                           </span>*/
/*                       </div>*/
/*                     */
/*                   </div>*/
/*                 */
/*             </div>*/
/*             */
/*         </div>*/
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/*     */
/* */
/*      */
/*       */
/*           {#  <div class="col-md-6">*/
/*                 <div id="container_power_chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>*/
/*             </div>  #}            */
/*         </div>*/
/*     </div>*/
/*    */
/*      */
/* </div>*/
/*          */
/*      */
/*     */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{ parent() }}   */
/* <link rel="stylesheet" type="text/css" href="{{ asset('libs/jqueryclock/css/analog.css') }}"> */
/* */
/* <style>*/
/*  /*HIDE CONTENT UNTIL UPDATE DATA FROM SERVER*//* */
/*     .row{opacity: 0;}   */
/*     div#container_power_chart div#highcharts-5.highcharts-container > svg > text:last-child{opacity: 0;}*/
/*     p span.description-text{font-size: small;}*/
/* </style>*/
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*      {{ parent() }}*/
/*      */
/*         <script type="text/javascript" src="https://www.google.com/jsapi"></script>*/
/*         <script src="{{ asset('libs/jqueryclock/js/jquery.clock.js') }}"></script>	*/
/*         <script src="{{ asset('libs/jquery.blockUI.js')}}"></script> */
/*         <script src="https://code.highcharts.com/highcharts.js"></script>*/
/*         <script src="https://code.highcharts.com/highcharts-more.js"></script>*/
/*         <script src="https://code.highcharts.com/modules/exporting.js"></script>*/
/* */
/* */
/*      <script type='text/javascript'>*/
/*             $.blockUI({ message: null });*/
/* */
/*              /* SETTING CLOCK VALUES *//* */
/*              var clock;*/
/*             $(document).ready(function() {*/
/*                   /*SETTING SAMPLE CLOCK*//*   */
/*                   $('#analog-clock').clock({*/
/*                       offset: '+5', */
/*                       type: 'analog', */
/*                       backgroundColor: 'rgba(0,0,0,0)'*/
/*                       */
/*                   });*/
/*                   /*SETTING ALL CLOCKS*//*   */
/*             });*/
/*         */
/*         */
/*             jQuery(document).ready(function() {*/
/*                     window.setInterval(function(){*/
/*                               data_update();*/
/*                             }, 1000);*/
/*             });*/
/*          */
/*          */
/*          */
/*         google.load("visualization", "1", {packages:["gauge"]});*/
/* 	google.setOnLoadCallback(drawGauges);*/
/* 	*/
/* 	function drawGauges() {*/
/*         */
/*         window.data1 = new google.visualization.DataTable();*/
/*         window.data1.addColumn('number', '{{entry1["'name'"]}}');*/
/*         window.data1.addRows(2);*/
/*         */
/*         window.data2 = new google.visualization.DataTable();*/
/*         window.data2.addColumn('number', '{{entry2["'name'"]}}');*/
/*         window.data2.addRows(2);*/
/*         */
/*         window.data3 = new google.visualization.DataTable();*/
/*         window.data3.addColumn('number', '{{entry3["'name'"]}}');*/
/*         window.data3.addRows(2);*/
/*         */
/*         window.data4 = new google.visualization.DataTable();*/
/*         window.data4.addColumn('number', 'Total');*/
/*         window.data4.addRows(2);*/
/*         */
/* 	*/
/* 	window.options = {*/
/* 	  width: 150, height: 150,*/
/* 	  redFrom: 90, redTo: 100,*/
/* 	  yellowFrom:75, yellowTo: 90,*/
/* 	  minorTicks: 5*/
/*       };*/
/* 	*/
/*         window.chart1 = new google.visualization.Gauge(document.getElementById('chart1'));*/
/*         window.chart2 = new google.visualization.Gauge(document.getElementById('chart2'));*/
/*         window.chart3 = new google.visualization.Gauge(document.getElementById('chart3'));*/
/*         window.chart4 = new google.visualization.Gauge(document.getElementById('chart4'));*/
/*            */
/*        window.chart1.draw(window.data1, options);*/
/*        window.chart2.draw(window.data2, options);*/
/*        window.chart3.draw(window.data3, options);*/
/*        window.chart4.draw(window.data4, options);*/
/* 	*/
/* {#        setInterval(function() {*/
/*             */
/*             var jsonData = $.ajax({*/
/*                     url: "http://{{ip_rest_api_sample}}/lab/data1.php",*/
/*                     data: {'jsonrequest':'dial'},*/
/*                     dataType:"json",*/
/*                     async: false*/
/*             }).responseText;*/
/*             jdata = JSON.parse(jsonData);*/
/* */
/*             data1.setCell(0, 0, jdata.rows[0].c[2].v );*/
/*             data2.setCell(0, 0, jdata.rows[1].c[2].v );*/
/*             data3.setCell(0, 0, jdata.rows[2].c[2].v );*/
/*             data4.setCell(0, 0, jdata.rows[3].c[2].v );*/
/* */
/*             chart1.draw(data1, options);*/
/*             chart2.draw(data2, options);*/
/*             chart3.draw(data3, options);*/
/*             chart4.draw(data4, options);*/
/* 			*/
/*         }, 1000);*/
/* 		#}*/
/* 	*/
/* 	}*/
/* 	*/
/*     */
/*     function data_update()*/
/* 	{*/
/* 						*/
/*                         */
/*                         $.ajax({*/
/*                           url: "http://{{client.getServerIp()}}/lab/datalive_client.php",*/
/*                         }).done(function(data) {*/
/* */
/*                                 //pagejson = JSON.parse(data);*/
/*                                 */
/*                                 var pagejson = data;*/
/*                                 */
/*                                 /*SAVING DATA FOR EXTERNAL USE IN THE PAGE*//* */
/*                                 window.time_hour     = pagejson.time_hour;*/
/*                                 window.time_minute   = pagejson.time_minute;*/
/*                                 window.time_second   = pagejson.time_second;*/
/*                                 window.current_power_consumption    = pagejson.real_power.value;//pagejson.power_consumption;*/
/*                                 window.current_consumption          = pagejson.power_consumption.value;*/
/*                                 /*CLIENT DATA*//* */
/*                                 */
/*                                 /**/
/*                                 $("#client_logo").attr("src",pagejson.detail_client.logo);*/
/*                                 $("#client_name").html(pagejson.detail_client.nom);*/
/*                                 $("#client_address").html("");*/
/*                                 $("#client_contact").html("");*/
/* */
/*                                 pagejson.detail_client.address.forEach(function(address)*/
/*                                 {$("#client_address").append(address.street + ', ' + address.prov + ', ' + address.pcode + "<br/>");});*/
/* */
/* */
/*                                 pagejson.detail_client.contact.forEach(function(contact)*/
/*                                 {$("#client_contact").append(contact.fonction + '. ' + contact.nom + ', ' + contact.phone + "<br/>");	});*/
/*                                 *//* */
/*                                 /*REFRESH WM40 MESURES READING*//* */
/*                                 $("#live_data_simple_REAL_POWER span.value").html(pagejson.real_power.value+pagejson.real_power.unit);*/
/*                                 //$("#live_data_simple_ACTIVE_POWER span.value").html(pagejson.real_power.value+pagejson.real_power.unit);*/
/*                                 $("#live_data_simple_POWER_CONSUMPTION span.value").html(pagejson.power_consumption.value+pagejson.power_consumption.unit);*/
/*                                 $("#live_data_simple_APPARENT_POWER span.value").html(pagejson.apparent_power.value+pagejson.apparent_power.unit);*/
/*                                 $("#live_data_simple_POWER_FACTOR span.value").html(pagejson.power_factor.value+pagejson.power_factor.unit);*/
/*                                 $("#live_data_simple_UTILISATION_FACTOR span.value").html(pagejson.utilisation_factor.value+pagejson.utilisation_factor.unit);*/
/*                                 $("#live_data_simple_TENSION span.value").html(pagejson.tension.value+pagejson.tension.unit);*/
/*                                 $("#live_data_simple_FREQUENCY span.value").html(pagejson.frequency.value+pagejson.frequency.unit);*/
/*                                 $("#live_data_simple_TOTAL_HARMONIC_DISTORSION span.value").html(pagejson.total_harmonic_distorsion.value+pagejson.total_harmonic_distorsion.unit);*/
/*                                 $("#live_data_simple_TOTAL_CURRENT_DEMAND_DISTORSION span.value").html(pagejson.total_current_demand_distorsion.value+pagejson.total_current_demand_distorsion.unit);*/
/*                                 $("#live_data_simple_KFACTOR span.value").html(pagejson.k_factor.value+pagejson.k_factor.unit);*/
/*                                 */
/*                                 /*REFRESH GAUGES INFORMATION*//* */
/*                   */
/*                                 window.data1 = google.visualization.arrayToDataTable([*/
/*                                   ['Label', 'Value'],*/
/*                                   ["{{entry1["'name'"]}}"/*pagejson.E1.name*//* , pagejson.E1.powerConsumptionRealPercent]*/
/*                                 ]);*/
/*                                 */
/*                                 window.data2 = google.visualization.arrayToDataTable([*/
/*                                   ['Label', 'Value'],*/
/*                                   ["{{entry2["'name'"]}}"/*pagejson.E2.name*//* , pagejson.E2.powerConsumptionRealPercent]*/
/*                               */
/*                                 ]);*/
/*                                 */
/*                                 window.data3 = google.visualization.arrayToDataTable([*/
/*                                   ['Label', 'Value'],*/
/*                                   ["{{entry3["'name'"]}}"/*pagejson.E3.name*//* , pagejson.E3.powerConsumptionRealPercent]*/
/*                           */
/*                                 ]);*/
/*                                 */
/*                                 window.data4 = google.visualization.arrayToDataTable([*/
/*                                   ['Label', 'Value'],*/
/*                                   ["Total"/*pagejson.TOTAL.name*//* , pagejson.TOTAL.powerConsumptionRealPercent]*/
/*                                 ]);*/
/* */
/*                                 window.chart1.draw(window.data1, window.options);*/
/*                                 window.chart2.draw(window.data2, window.options);*/
/*                                 window.chart3.draw(window.data3, window.options);*/
/*                                 window.chart4.draw(window.data4, window.options);*/
/*                                 */
/*                                 $("#e1_pcr span.value").html(pagejson.E1.powerConsumptionReal+pagejson.E1.powerConsumptionUnit);*/
/*                                 $("#e1_t span.value").html(pagejson.E1.temperatureCelcius+"C");*/
/*                                 $("#e1_fu span.value").html(pagejson.E1.powerConsumptionRealPercent+"%");*/
/*                                 */
/*                                 $("#e2_pcr span.value").html(pagejson.E2.powerConsumptionReal+pagejson.E2.powerConsumptionUnit);*/
/*                                 $("#e2_t span.value").html(pagejson.E2.temperatureCelcius+"C");*/
/*                                 $("#e2_fu span.value").html(pagejson.E2.powerConsumptionRealPercent+"%");*/
/*                                 */
/*                                 $("#e3_pcr span.value").html(pagejson.E3.powerConsumptionReal+pagejson.E3.powerConsumptionUnit);*/
/*                                 $("#e3_t span.value").html(pagejson.E3.temperatureCelcius+"C");*/
/*                                 $("#e3_fu span.value").html(pagejson.E3.powerConsumptionRealPercent+"%");*/
/*                                 */
/*                                 $("#e4_pcr span.value").html(pagejson.TOTAL.powerConsumptionReal+pagejson.TOTAL.powerConsumptionUnit);*/
/*                                 $("#e4_fu span.value").html(pagejson.TOTAL.powerConsumptionRealPercent+"%");*/
/*                                 */
/*                                 */
/*                                 $('#chart1 circle:nth-child(1)').attr('stroke', '#00B4EB'); //outer-ring*/
/*                                 $('#chart1 circle:nth-child(1)').attr('fill', '#00B4EB'); //outer-ring*/
/*                                 */
/*                                 $('#chart2 circle:nth-child(1)').attr('stroke', '#00B4EB'); //outer-ring*/
/*                                 $('#chart2 circle:nth-child(1)').attr('fill', '#00B4EB'); //outer-ring*/
/*                                 */
/*                                 $('#chart3 circle:nth-child(1)').attr('stroke', '#00B4EB'); //outer-ring*/
/*                                 $('#chart3 circle:nth-child(1)').attr('fill', '#00B4EB'); //outer-ring*/
/*                                 */
/*                                 $('#chart4 circle:nth-child(1)').attr('stroke', '#00B4EB'); //outer-ring*/
/*                                 $('#chart4 circle:nth-child(1)').attr('fill', '#00B4EB'); //outer-ring*/
/* */
/*                                 /*REFRESH DATA [GAUGES ON OR OFF...]*//* */
/*                                 if(pagejson.clock == "1"){*/
/*                                     $("#clock_on").show();*/
/*                                     $("#clock_off").hide();*/
/*                                 }else if(pagejson.clock == "0"){*/
/*                                     $("#clock_off").show();*/
/*                                     $("#clock_on").hide()*/
/*                                 }*/
/*                                 */
/*                                 if(pagejson.E1.active == "1"){*/
/*                                     $("#e1_on").show();*/
/*                                     $("#e1_off").hide();*/
/*                                 }else if(pagejson.E1.active == "0"){*/
/*                                     $("#e1_off").show();*/
/*                                     $("#e1_on").hide()*/
/*                                 }*/
/*                                 */
/*                                 if(pagejson.E2.active == "1"){*/
/*                                     $("#e2_on").show();*/
/*                                     $("#e2_off").hide();*/
/*                                 }else if(pagejson.E2.active == "0"){*/
/*                                     $("#e2_off").show();*/
/*                                     $("#e2_on").hide()*/
/*                                 }     */
/*                                 */
/*                                 if(pagejson.E3.active == "1"){*/
/*                                     $("#e3_on").show();*/
/*                                     $("#e3_off").hide();*/
/*                                 }else if(pagejson.E3.active == "0"){*/
/*                                     $("#e3_off").show();*/
/*                                     $("#e3_on").hide()*/
/*                                 }     */
/*                                 */
/*                                 */
/*                                 /*SHOW PAGE WITH DATA VALIDATED*//* */
/*                                 $(".row").css("opacity","1");*/
/*                                 $.unblockUI();*/
/*                                 return false;*/
/*                         });*/
/*                         */
/*                         */
/* 			*/
/* 	}*/
/* 	  */
/*      </script>*/
/*      */
/*      */
/*      */
/*      */
/*      <script>*/
/*          /******  CLOCK - HIGHCHARTS   ******//* */
/*          */
/*          */
/*          */
/*          $(function () {*/
/* */
/*     /***/
/*      * Get the current time*/
/*      *//* */
/*     function getNow() {*/
/*         var now = new Date();*/
/* */
/*         return {*/
/*             hours: now.getHours() + now.getMinutes() / 60,*/
/*             minutes: now.getMinutes() * 12 / 60 + now.getSeconds() * 12 / 3600,*/
/*             seconds: now.getSeconds() * 12 / 60*/
/*         };*/
/*     }*/
/* */
/*     /***/
/*      * Pad numbers*/
/*      *//* */
/*    /* function pad(number, length) {*/
/*         // Create an array of the remaining length + 1 and join it with 0's*/
/*         return new Array((length || 2) + 1 - String(number).length).join(0) + number;*/
/*     }*//* */
/* */
/*     var now = getNow();*/
/* */
/*     // Create the chart*/
/*     $('#container_clock').highcharts({*/
/* */
/*         chart: {*/
/*             type: 'gauge',*/
/*             backgroundColor: 'none',*/
/*             plotBackgroundColor: null,*/
/*             plotBackgroundImage: null,*/
/*             plotBorderWidth: 0,*/
/*             plotShadow: false,*/
/*             height: 160,*/
/*             width: 160*/
/*         },*/
/* */
/*         credits: {*/
/*             enabled: false*/
/*         },*/
/*         */
/*         exporting: {*/
/*                 enabled: false*/
/*             },*/
/* */
/*         title: {*/
/*             text: ''*/
/*         },*/
/* */
/*         pane: {*/
/*             background: [{*/
/*                 // default background*/
/*             }, {*/
/*                 // reflex for supported browsers*/
/*                 backgroundColor: Highcharts.svg ? {*/
/*                     radialGradient: {*/
/*                         cx: 0.5,*/
/*                         cy: -0.4,*/
/*                         r: 1.9*/
/*                     },*/
/*                     stops: [*/
/*                         [0.5, 'rgba(255, 255, 255, 0.2)'],*/
/*                         [0.5, 'rgba(200, 200, 200, 0.2)']*/
/*                     ]*/
/*                 } : null*/
/*             }]*/
/*         },*/
/* */
/*         yAxis: {*/
/*             labels: {*/
/*                 distance: -20*/
/*             },*/
/*             min: 0,*/
/*             max: 12,*/
/*             lineWidth: 0,*/
/*             showFirstLabel: false,*/
/* */
/*             minorTickInterval: 'auto',*/
/*             minorTickWidth: 1,*/
/*             minorTickLength: 5,*/
/*             minorTickPosition: 'inside',*/
/*             minorGridLineWidth: 0,*/
/*             minorTickColor: '#666',*/
/* */
/*             tickInterval: 1,*/
/*             tickWidth: 2,*/
/*             tickPosition: 'inside',*/
/*             tickLength: 10,*/
/*             tickColor: '#666',*/
/*             title: {*/
/*                 text: 'HQ',*/
/*                 style: {*/
/*                     color: '#BBB',*/
/*                     fontWeight: 'normal',*/
/*                     fontSize: '8px',*/
/*                     lineHeight: '10px'*/
/*                 },*/
/*                 y: 10*/
/*             }*/
/*         },*/
/* */
/*         tooltip: {*/
/*             formatter: function () {*/
/*                 return this.series.chart.tooltipText;*/
/*             }*/
/*         },*/
/* */
/*         series: [{*/
/*             data: [{*/
/*                 id: 'hour',*/
/*                 y: now.hours,*/
/*                 dial: {*/
/*                     radius: '60%',*/
/*                     baseWidth: 4,*/
/*                     baseLength: '95%',*/
/*                     rearLength: 0*/
/*                 }*/
/*             }, {*/
/*                 id: 'minute',*/
/*                 y: now.minutes,*/
/*                 dial: {*/
/*                     baseLength: '95%',*/
/*                     rearLength: 0*/
/*                 }*/
/*             }, {*/
/*                 id: 'second',*/
/*                 y: now.seconds,*/
/*                 dial: {*/
/*                     radius: '100%',*/
/*                     baseWidth: 1,*/
/*                     rearLength: '20%'*/
/*                 }*/
/*             }],*/
/*             animation: false,*/
/*             dataLabels: {*/
/*                 enabled: false*/
/*             }*/
/*         }]*/
/*     },*/
/* */
/*         // Move*/
/*         function (chart) {*/
/*             setInterval(function () {*/
/* */
/*                 now = getNow();*/
/* */
/*                 var hour = chart.get('hour'),*/
/*                     minute = chart.get('minute'),*/
/*                     second = chart.get('second'),*/
/*                     // run animation unless we're wrapping around from 59 to 0*/
/*                     animation = now.seconds === 0 ?*/
/*                         false :*/
/*                         {*/
/*                             easing: 'easeOutElastic'*/
/*                         };*/
/* */
/*                 // Cache the tooltip text*/
/*                 chart.tooltipText = window.time_hour + ':' + window.time_minute + ':' +window.time_second;*/
/*                     {#*/
/*                 var seconds = window.time_second;*/
/*                 var minutes =  seconds / 60;*/
/*                 var hours = : minutes / 60;*/
/*             #}*/
/*                             */
/*                             */
/*                 var hours =  parseInt(window.time_hour);*/
/*                 var minutes = parseInt(window.time_minute)  * 12 / 60 + parseInt(window.time_second) * 12 / 3600;*/
/*                 var seconds =  parseInt(window.time_second) * 12 / 60;*/
/*                             */
/*                 hour.update(hours , true, animation);*/
/*                 minute.update(minutes , true, animation);*/
/*                 second.update(seconds, true, animation);*/
/* */
/*             }, 1000);*/
/* */
/*         });*/
/* });*/
/* */
/* // Extend jQuery with some easing (copied from jQuery UI)*/
/* $.extend($.easing, {*/
/*     /* eslint-disable *//* */
/*     easeOutElastic: function (x, t, b, c, d) {*/
/*         var s=1.70158;var p=0;var a=c;*/
/*         if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;*/
/*         if (a < Math.abs(c)) { a=c; var s=p/4; }*/
/*         else var s = p/(2*Math.PI) * Math.asin (c/a);*/
/*         return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;*/
/*     }*/
/*     /* eslint-enable *//* */
/* });*/
/*          */
/*          */
/*      </script>    */
/*     */
/*      */
/*      */
/*      */
/*      */
/*     <script>*/
/*     /*   CHART POWER REAL CONSUMPTION   *//*   */
/*     */
/*     */
/*     */
/*     */
/*     $(function () {*/
/*     $(document).ready(function () {*/
/*         Highcharts.setOptions({*/
/*             global: {*/
/*                 useUTC: false*/
/*             }*/
/*         });*/
/*         */
/*         */
/*         */
/*         $('#container_consumption_chart').highcharts({*/
/*             chart: {*/
/*                 type: 'area',*/
/*                 animation: Highcharts.svg, // don't animate in old IE*/
/*                 marginRight: 10,*/
/*                 events: {*/
/*                     load: function () {*/
/* */
/*                         // set up the updating of the chart each second*/
/*                         var series = this.series[0];*/
/*                         setInterval(function () {*/
/*                             var x = (new Date()).getTime(), // current time*/
/*                                 y = window.current_consumption;//Math.random();*/
/*                             series.addPoint([x, y], true, true);*/
/*                         }, 1000);*/
/*                     }*/
/*                 }*/
/*             },*/
/*             title: {*/
/*                 text: '{{"Consumption"|trans}}'*/
/*             },*/
/*             xAxis: {*/
/*                 type: 'datetime',*/
/*                 tickPixelInterval: 150*/
/*             },*/
/*             yAxis: {*/
/*                 title: {*/
/*                     text: 'Kwh'*/
/*                 },*/
/*                 plotLines: [{*/
/*                     value: 0,*/
/*                     width: 1,*/
/*                     color: '#808080'*/
/*                 }]*/
/*             },*/
/*             tooltip: {*/
/*                 formatter: function () {*/
/*                     return '<b>' + this.series.name + '</b><br/>' +*/
/*                         Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +*/
/*                         Highcharts.numberFormat(this.y, 2);*/
/*                 }*/
/*             },*/
/*              plotOptions: {*/
/*              area: {*/
/*                 lineWidth: 1,*/
/*                 states: {*/
/*                     hover: {*/
/*                         lineWidth: 2*/
/*                     }*/
/*                 },*/
/*                 marker: {*/
/*                     enabled: false*/
/*                 }, */
/*                 fillColor: {*/
/*                     linearGradient: {*/
/*                         x1: 0,*/
/*                         y1: 0,*/
/*                         x2: 0,*/
/*                         y2: 1*/
/*                     },*/
/*                     stops: [*/
/*                         [0, Highcharts.getOptions().colors[0]],*/
/*                         [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]*/
/*                     ]*/
/*                 }*/
/*             }*/
/*             {#spline: {*/
/*                 lineWidth: 1,*/
/*                 states: {*/
/*                     hover: {*/
/*                         lineWidth: 3*/
/*                     }*/
/*                 },*/
/*                 marker: {*/
/*                     enabled: false*/
/*                 }*/
/*                 */
/*             }#}*/
/*         },        */
/*             legend: {*/
/*                 enabled: false*/
/*             },*/
/*             exporting: {*/
/*                 enabled: false*/
/*             },*/
/*             series: [{*/
/*                 name: 'P',*/
/*                 data: (function () {*/
/*                     // generate an array of random data*/
/*                     var data = [],*/
/*                         time = (new Date()).getTime(),*/
/*                         i;*/
/* */
/*                     for (i = -19; i <= 0; i += 1) {*/
/*                         data.push({*/
/*                             x: time + i * 1000,*/
/*                             y: Math.random()*/
/*                         });*/
/*                     }*/
/*                     return data;*/
/*                 }())*/
/*             }]*/
/*         });*/
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/*         */
/* */
/*         $('#container_power_chart').highcharts({*/
/*             chart: {*/
/*                 type: 'area',*/
/*                 animation: Highcharts.svg, // don't animate in old IE*/
/*                 marginRight: 10,*/
/*                 events: {*/
/*                     load: function () {*/
/* */
/*                         // set up the updating of the chart each second*/
/*                         var series = this.series[0];*/
/*                         setInterval(function () {*/
/*                             var x = (new Date()).getTime(), // current time*/
/*                                 y = window.current_power_consumption;//Math.random();*/
/*                             series.addPoint([x, y], true, true);*/
/*                         }, 1000);*/
/*                     }*/
/*                 }*/
/*             },*/
/*             */
/*             */
/*             plotOptions: {*/
/*            area: {*/
/*                 lineWidth: 1,*/
/*                 states: {*/
/*                     hover: {*/
/*                         lineWidth: 2*/
/*                     }*/
/*                 },*/
/*                 marker: {*/
/*                     enabled: false*/
/*                 }, */
/*                 fillColor: {*/
/*                     linearGradient: {*/
/*                         x1: 0,*/
/*                         y1: 0,*/
/*                         x2: 0,*/
/*                         y2: 1*/
/*                     },*/
/*                     stops: [*/
/*                         [0, Highcharts.getOptions().colors[0]],*/
/*                         [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]*/
/*                     ]*/
/*                 }*/
/*             }*/
/*             {#spline: {*/
/*                 lineWidth: 1,*/
/*                 states: {*/
/*                     hover: {*/
/*                         lineWidth: 3*/
/*                     }*/
/*                 },*/
/*                 marker: {*/
/*                     enabled: false*/
/*                 }*/
/*                 */
/*             }#}*/
/*         },     */
/*        */
/*             */
/*             title: {*/
/*                 text: '{{"Power"|trans}}'*/
/*             },*/
/*             xAxis: {*/
/*                 type: 'datetime',*/
/*                 tickPixelInterval: 150*/
/*             },*/
/*             yAxis: {*/
/*                 title: {*/
/*                     text: 'Kw'*/
/*                 },*/
/*                 plotLines: [{*/
/*                     value: 0,*/
/*                     width: 1,*/
/*                     color: '#808080'*/
/*                 }]*/
/*             },*/
/*             tooltip: {*/
/*                 formatter: function () {*/
/*                     return '<b>' + this.series.name + '</b><br/>' +*/
/*                         Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +*/
/*                         Highcharts.numberFormat(this.y, 2);*/
/*                 }*/
/*             },*/
/*             legend: {*/
/*                 enabled: false*/
/*             },*/
/*             exporting: {*/
/*                 enabled: false*/
/*             },*/
/*             series: [{*/
/*                 name: 'P',*/
/*                 data: (function () {*/
/*                     // generate an array of random data*/
/*                     var data = [],*/
/*                         time = (new Date()).getTime(),*/
/*                         i;*/
/* */
/*                     for (i = -19; i <= 0; i += 1) {*/
/*                         data.push({*/
/*                             x: time + i * 1000,*/
/*                             y: Math.random()*/
/*                         });*/
/*                     }*/
/*                     return data;*/
/*                 }())*/
/*             }]*/
/*         });*/
/*     });*/
/* }); */
/*         */
/*         */
/*         */
/*         */
/*     </script>*/
/*      */
/*      */
/*      */
/*      */
/*      */
/*      */
/* {% endblock %}*/
/* */
/* */
