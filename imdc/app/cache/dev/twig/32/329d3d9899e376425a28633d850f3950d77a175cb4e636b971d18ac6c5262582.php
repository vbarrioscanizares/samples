<?php

/* clients_bills.html.twig */
class __TwigTemplate_1c03b0939bae013e86c7bdf8547c66ebb1bb78560d583d812e81a46ba4f6530e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_bills.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1621fd53efb2c3692d74a30018bdca1463291dfc450c0b9218a3a6dea0141bc8 = $this->env->getExtension("native_profiler");
        $__internal_1621fd53efb2c3692d74a30018bdca1463291dfc450c0b9218a3a6dea0141bc8->enter($__internal_1621fd53efb2c3692d74a30018bdca1463291dfc450c0b9218a3a6dea0141bc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_bills.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1621fd53efb2c3692d74a30018bdca1463291dfc450c0b9218a3a6dea0141bc8->leave($__internal_1621fd53efb2c3692d74a30018bdca1463291dfc450c0b9218a3a6dea0141bc8_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_f39d074ad3922fda4d93bf0379893abd47016ec4547dcb467c85b8f0a2a11bc3 = $this->env->getExtension("native_profiler");
        $__internal_f39d074ad3922fda4d93bf0379893abd47016ec4547dcb467c85b8f0a2a11bc3->enter($__internal_f39d074ad3922fda4d93bf0379893abd47016ec4547dcb467c85b8f0a2a11bc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 9
        $this->loadTemplate("sidebar_clients_v2.html.twig", "clients_bills.html.twig", 9)->display($context);
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_bills.html.twig", 13)->display(array_merge($context, array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))));
        echo " 
    <div class=\"col-md-9  main\">
           
          <h1 class=\"page-header\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("General information"), "html", null, true);
        echo "</h1>
          
          <form  action=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills_general", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
        echo "\" method=\"POST\">
              <input type=\"hidden\" value=\"1\" name=\"posting\">
           
           <div class=\"well well-lg col-sm-4  col-md-5\">   
              
              
            <div class=\"form-group\">
              <label for=\"bill_customer_no\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Customer number"), "html", null, true);
        echo "</label>
              <input name=\"bill_customer_no\" value=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getBillCustomerNo", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"bill_customer_no\" placeholder=\"\"  >
            </div>
              
              
            <div class=\"form-group\">
              <label for=\"bill_account_no\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Account number"), "html", null, true);
        echo "</label>
              <input name=\"bill_account_no\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getBillAccountNo", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"bill_account_no\" placeholder=\"\"  >
            </div>  
            
            <div class=\"form-group\">
              <label for=\"bill_contrat_no\">";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contrat number"), "html", null, true);
        echo "</label>
              <input name=\"bill_contrat_no\" value=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getBillContratNo", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"bill_contrat_no\" placeholder=\"\"  >
            </div>   
             
              
            <div class=\"form-group\">
              <label for=\"bill_meter_compteur_id\">";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Meter counter"), "html", null, true);
        echo "</label>
              <input name=\"bill_meter_compteur_id\" value=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getBillMeterCompteurId", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"bill_meter_compteur_id\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"bill_hq_code_access\">";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("HQ access code"), "html", null, true);
        echo "</label>
              <input name=\"bill_hq_code_access\" value=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getBillHqCodeAccess", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"bill_hq_code_access\" placeholder=\"\"  >
            </div>  
              
            <div class=\"form-group\">
              <label for=\"bill_hq_code_password\">";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("HQ password"), "html", null, true);
        echo "</label>
              <input name=\"bill_hq_code_password\" value=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getBillHqPassword", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"bill_hq_code_password\" placeholder=\"\"  >
            </div>    
             </div><!--well-->
             
             
             <div class=\"well well-lg col-sm-4 col-md-offset-1  col-md-5\" style=\"overflow-y: scroll;height: 495px\">   
              
              
            <div class=\"form-group\">
              <label for=\"bill_notes\">";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Notes"), "html", null, true);
        echo "</label>
              <a href=\"javascript:void(0);\" onclick=\"add()\">
                    <span class=\" glyphicon glyphicon-plus\"></span>
               </a>
              
              
              <input value=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getBillNotes", array(), "method"), "html", null, true);
        echo "\" id=\"bill_notes\" name=\"bill_notes\" type=\"hidden\">  
              <textarea  rows=\"5\" class=\"form-control\" id=\"item\" ></textarea>
            
               <br/>
               <div id=\"list\" class=\"list-group\"></div> 
               
            </div>
           </div>   
           
                
            
           <p>
            <input type=\"submit\" class=\"btn btn-primary btn-lg pull-right\" value=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\"/>
            </p>
            
           
          </form>
          
          
          
          
          <p></p>
          <p></p>
          </div> <!-- main -->
          
          
          
          <div class=\" col-md-9  main\">
          <h1 class=\"page-header\">";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Bills"), "html", null, true);
        echo "</h1>
          
          
          
          
          <div class=\"table-responsive\">
            <table class=\"table table-hover\">
               <thead>
                <tr>
";
        // line 106
        echo "                   
                   <th style=\"width:16%;\">";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</th>
                   <th style=\"width:16%;\">";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</th>
                   <th style=\"width:16%;\">";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption"), "html", null, true);
        echo " (Kw)</th>
                   <th style=\"width:16%;\">";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Amount"), "html", null, true);
        echo " (\$)</th>
                   <th style=\"width:16%;\">";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("File"), "html", null, true);
        echo "</th>
                   <th style=\"width:20%;\"></th>
                </tr>
               </thead>  
            <tbody>
                
                
            ";
        // line 118
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getBills", array(), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["bill"]) {
            // line 119
            echo "                    
                        <tr style=\"cursor:pointer;\">

";
            // line 123
            echo "                        
                        <td>";
            // line 124
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "getDateFrom", array(), "method"), "html", null, true);
            echo "</td>
                        <td>";
            // line 125
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "getDateTo", array(), "method"), "html", null, true);
            echo "</td>
                        <td>";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "getKw", array(), "method"), "html", null, true);
            echo "</td>
                        <td>";
            // line 127
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "getAmmountKw", array(), "method"), "html", null, true);
            echo "</td>
                        <td><a target=\"_blank\"  href=\"";
            // line 128
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("upload/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "getFilename", array(), "method"), "html", null, true);
            echo "\"><img width=\"30\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/pdf-icon.png"), "html", null, true);
            echo "\" alt=\"pdf\"/></a></td>
                      
                        
                        <td>
                            <a href='";
            // line 132
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills_edit", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")), "bill_id" => $this->getAttribute($context["bill"], "id", array()))), "html", null, true);
            echo "'><span class=\"glyphicon glyphicon-pencil\"></span></a>   
                            
                            
                            <a href=\"";
            // line 135
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills_delete", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")), "bill_id" => $this->getAttribute($context["bill"], "id", array()))), "html", null, true);
            echo "\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Delete"), "html", null, true);
            echo "\" class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></a>
                            
                        </td>
                      </tr>
                    
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['bill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 140
        echo "    
              
            
            
            </tbody>  
            </table>
            
            <p>
                <a class=\"btn btn-primary  btn-lg pull-right\" href=\"";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills_create", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
        echo "\" role=\"button\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Add new bill"), "html", null, true);
        echo "</a>
                <a style=\"margin-right: 10px;\" class=\"btn btn-primary  btn-lg pull-right\" href=\"";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills_create_m", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
        echo "\" role=\"button\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Add new 'M' bill"), "html", null, true);
        echo "</a>
            
            </p>
           
            
          </div>
          
          </div>
          
          
          
          
          
          
          
          
          
          

          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_f39d074ad3922fda4d93bf0379893abd47016ec4547dcb467c85b8f0a2a11bc3->leave($__internal_f39d074ad3922fda4d93bf0379893abd47016ec4547dcb467c85b8f0a2a11bc3_prof);

    }

    // line 180
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7bd748a1d9240a6737cbdc9a2a45581e0c155cafd2680b022092fc301e5ea4e7 = $this->env->getExtension("native_profiler");
        $__internal_7bd748a1d9240a6737cbdc9a2a45581e0c155cafd2680b022092fc301e5ea4e7->enter($__internal_7bd748a1d9240a6737cbdc9a2a45581e0c155cafd2680b022092fc301e5ea4e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 181
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    
";
        
        $__internal_7bd748a1d9240a6737cbdc9a2a45581e0c155cafd2680b022092fc301e5ea4e7->leave($__internal_7bd748a1d9240a6737cbdc9a2a45581e0c155cafd2680b022092fc301e5ea4e7_prof);

    }

    // line 187
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_df8cf16278407c31943d9a6b78c39a4191019a03c51ae4a369f97b1263fc150a = $this->env->getExtension("native_profiler");
        $__internal_df8cf16278407c31943d9a6b78c39a4191019a03c51ae4a369f97b1263fc150a->enter($__internal_df8cf16278407c31943d9a6b78c39a4191019a03c51ae4a369f97b1263fc150a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 188
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

<script>
\$(document).ready(function() {
                  rebuild();
            });    
    
    
    
    
function add(){
        var item            = \$(\"#item\").val();
        var items           = new Array();
        var list            = \$(\"#bill_notes\").val();
        var d               = new Date();
        var date_string     = d.toLocaleString();
        
        item = date_string + \": \"+item;
        if (item.length == 0){
            return 0;
        }
        
        if (list.length > 0){
            items = eval(list);
        }
        
        //ADD ADDRESS TO THE LIST
        items.push(item);
        
        
        //SAVE LIST IN  THE INPUT
        \$(\"#bill_notes\").val(JSON.stringify(items));
        
        //REBUILD THE LIST
        rebuild();
        
        //REMOVE ADDRESS TEXT
        \$(\"#item\").val(\"\");
        
    }
    
    function del(id){
        
        var list    = \$(\"#bill_notes\").val();
        var items = eval(list);
        
        //SUPPRIME THE ITEM FROM THE LIST
        items.splice(id, 1);
        
        //SAVE THE LIST WITHOUT THE ITEM
        \$(\"#bill_notes\").val(JSON.stringify(items));
        
        //REBUILD THE LIST
        rebuild();
        
    }
    
    function rebuild(){
         var items            = eval(\$(\"#bill_notes\").val());
         var id_item            = 0;
     
         
        if (typeof items === 'undefined'){
            return 0;
        }
        
         \$(\"#list\").html(\"\");
         items.forEach(function(item) {
             \$(\"#list\").append('<div class=\"list-group-item\">'+item+'<a onclick=\"del('+id_item+');\" href=\"javascript:void(0);\" >'+'<span class=\" pull-right glyphicon glyphicon-minus\"></span></a></div>');
             id_item++;
        //console.log(item);
        });
    }
        
    
</script>     
";
        
        $__internal_df8cf16278407c31943d9a6b78c39a4191019a03c51ae4a369f97b1263fc150a->leave($__internal_df8cf16278407c31943d9a6b78c39a4191019a03c51ae4a369f97b1263fc150a_prof);

    }

    public function getTemplateName()
    {
        return "clients_bills.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  373 => 188,  367 => 187,  357 => 181,  351 => 180,  312 => 149,  306 => 148,  296 => 140,  282 => 135,  276 => 132,  266 => 128,  262 => 127,  258 => 126,  254 => 125,  250 => 124,  247 => 123,  242 => 119,  238 => 118,  228 => 111,  224 => 110,  220 => 109,  214 => 108,  208 => 107,  205 => 106,  193 => 96,  174 => 80,  159 => 68,  150 => 62,  138 => 53,  134 => 52,  127 => 48,  123 => 47,  116 => 43,  112 => 42,  104 => 37,  100 => 36,  93 => 32,  89 => 31,  81 => 26,  77 => 25,  67 => 18,  62 => 16,  56 => 13,  51 => 10,  49 => 9,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients_v2.html.twig' %}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb.html.twig' with {'client_id': client_id}%} */
/*     <div class="col-md-9  main">*/
/*            */
/*           <h1 class="page-header">{{"General information"|trans}}</h1>*/
/*           */
/*           <form  action="{{path('_clients_bills_general',{ 'client_id': client_id})}}" method="POST">*/
/*               <input type="hidden" value="1" name="posting">*/
/*            */
/*            <div class="well well-lg col-sm-4  col-md-5">   */
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="bill_customer_no">{{"Customer number"|trans}}</label>*/
/*               <input name="bill_customer_no" value="{{client.getBillCustomerNo()}}" type="text" class="form-control" id="bill_customer_no" placeholder=""  >*/
/*             </div>*/
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="bill_account_no">{{"Account number"|trans}}</label>*/
/*               <input name="bill_account_no" value="{{client.getBillAccountNo()}}" type="text" class="form-control" id="bill_account_no" placeholder=""  >*/
/*             </div>  */
/*             */
/*             <div class="form-group">*/
/*               <label for="bill_contrat_no">{{"Contrat number"|trans}}</label>*/
/*               <input name="bill_contrat_no" value="{{client.getBillContratNo()}}" type="text" class="form-control" id="bill_contrat_no" placeholder=""  >*/
/*             </div>   */
/*              */
/*               */
/*             <div class="form-group">*/
/*               <label for="bill_meter_compteur_id">{{"Meter counter"|trans}}</label>*/
/*               <input name="bill_meter_compteur_id" value="{{client.getBillMeterCompteurId()}}" type="text" class="form-control" id="bill_meter_compteur_id" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="bill_hq_code_access">{{"HQ access code"|trans}}</label>*/
/*               <input name="bill_hq_code_access" value="{{client.getBillHqCodeAccess()}}" type="text" class="form-control" id="bill_hq_code_access" placeholder=""  >*/
/*             </div>  */
/*               */
/*             <div class="form-group">*/
/*               <label for="bill_hq_code_password">{{"HQ password"|trans}}</label>*/
/*               <input name="bill_hq_code_password" value="{{client.getBillHqPassword()}}" type="text" class="form-control" id="bill_hq_code_password" placeholder=""  >*/
/*             </div>    */
/*              </div><!--well-->*/
/*              */
/*              */
/*              <div class="well well-lg col-sm-4 col-md-offset-1  col-md-5" style="overflow-y: scroll;height: 495px">   */
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="bill_notes">{{"Notes"|trans}}</label>*/
/*               <a href="javascript:void(0);" onclick="add()">*/
/*                     <span class=" glyphicon glyphicon-plus"></span>*/
/*                </a>*/
/*               */
/*               */
/*               <input value="{{client.getBillNotes()}}" id="bill_notes" name="bill_notes" type="hidden">  */
/*               <textarea  rows="5" class="form-control" id="item" ></textarea>*/
/*             */
/*                <br/>*/
/*                <div id="list" class="list-group"></div> */
/*                */
/*             </div>*/
/*            </div>   */
/*            */
/*                 */
/*             */
/*            <p>*/
/*             <input type="submit" class="btn btn-primary btn-lg pull-right" value="{{"Submit"|trans}}"/>*/
/*             </p>*/
/*             */
/*            */
/*           </form>*/
/*           */
/*           */
/*           */
/*           */
/*           <p></p>*/
/*           <p></p>*/
/*           </div> <!-- main -->*/
/*           */
/*           */
/*           */
/*           <div class=" col-md-9  main">*/
/*           <h1 class="page-header">{{"Bills"|trans}}</h1>*/
/*           */
/*           */
/*           */
/*           */
/*           <div class="table-responsive">*/
/*             <table class="table table-hover">*/
/*                <thead>*/
/*                 <tr>*/
/* {#                   <th style="width:5%;">{{"Id"|trans}}</th>#}*/
/*                    */
/*                    <th style="width:16%;">{{"Date"|trans}} {{"from"|trans}}</th>*/
/*                    <th style="width:16%;">{{"Date"|trans}} {{"to"|trans}}</th>*/
/*                    <th style="width:16%;">{{"Consumption"|trans}} (Kw)</th>*/
/*                    <th style="width:16%;">{{"Amount"|trans}} ($)</th>*/
/*                    <th style="width:16%;">{{"File"|trans}}</th>*/
/*                    <th style="width:20%;"></th>*/
/*                 </tr>*/
/*                </thead>  */
/*             <tbody>*/
/*                 */
/*                 */
/*             {% for bill in client.getBills() %}*/
/*                     */
/*                         <tr style="cursor:pointer;">*/
/* */
/* {#                        <td>{{bill.id}}</td>#}*/
/*                         */
/*                         <td>{{bill.getDateFrom()}}</td>*/
/*                         <td>{{bill.getDateTo()}}</td>*/
/*                         <td>{{bill.getKw()}}</td>*/
/*                         <td>{{bill.getAmmountKw()}}</td>*/
/*                         <td><a target="_blank"  href="{{asset('upload/')}}{{bill.getFilename()}}"><img width="30" src="{{asset('img/pdf-icon.png')}}" alt="pdf"/></a></td>*/
/*                       */
/*                         */
/*                         <td>*/
/*                             <a href='{{path("_clients_bills_edit", { 'client_id': client_id, 'bill_id':bill.id })}}'><span class="glyphicon glyphicon-pencil"></span></a>   */
/*                             */
/*                             */
/*                             <a href="{{path("_clients_bills_delete", { 'client_id': client_id, 'bill_id':bill.id })}}"><span data-toggle="tooltip" data-placement="top" title="{{"Delete"|trans}}" class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>*/
/*                             */
/*                         </td>*/
/*                       </tr>*/
/*                     */
/*             {% endfor %}    */
/*               */
/*             */
/*             */
/*             </tbody>  */
/*             </table>*/
/*             */
/*             <p>*/
/*                 <a class="btn btn-primary  btn-lg pull-right" href="{{path("_clients_bills_create", { 'client_id': client_id })}}" role="button">{{"Add new bill"|trans}}</a>*/
/*                 <a style="margin-right: 10px;" class="btn btn-primary  btn-lg pull-right" href="{{path("_clients_bills_create_m", { 'client_id': client_id })}}" role="button">{{"Add new 'M' bill"|trans}}</a>*/
/*             */
/*             </p>*/
/*            */
/*             */
/*           </div>*/
/*           */
/*           </div>*/
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/* */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/*     */
/* {% endblock %}*/
/* */
/* */
/* */
/* {% block javascripts %}*/
/* {{parent()}}*/
/* */
/* <script>*/
/* $(document).ready(function() {*/
/*                   rebuild();*/
/*             });    */
/*     */
/*     */
/*     */
/*     */
/* function add(){*/
/*         var item            = $("#item").val();*/
/*         var items           = new Array();*/
/*         var list            = $("#bill_notes").val();*/
/*         var d               = new Date();*/
/*         var date_string     = d.toLocaleString();*/
/*         */
/*         item = date_string + ": "+item;*/
/*         if (item.length == 0){*/
/*             return 0;*/
/*         }*/
/*         */
/*         if (list.length > 0){*/
/*             items = eval(list);*/
/*         }*/
/*         */
/*         //ADD ADDRESS TO THE LIST*/
/*         items.push(item);*/
/*         */
/*         */
/*         //SAVE LIST IN  THE INPUT*/
/*         $("#bill_notes").val(JSON.stringify(items));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuild();*/
/*         */
/*         //REMOVE ADDRESS TEXT*/
/*         $("#item").val("");*/
/*         */
/*     }*/
/*     */
/*     function del(id){*/
/*         */
/*         var list    = $("#bill_notes").val();*/
/*         var items = eval(list);*/
/*         */
/*         //SUPPRIME THE ITEM FROM THE LIST*/
/*         items.splice(id, 1);*/
/*         */
/*         //SAVE THE LIST WITHOUT THE ITEM*/
/*         $("#bill_notes").val(JSON.stringify(items));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuild();*/
/*         */
/*     }*/
/*     */
/*     function rebuild(){*/
/*          var items            = eval($("#bill_notes").val());*/
/*          var id_item            = 0;*/
/*      */
/*          */
/*         if (typeof items === 'undefined'){*/
/*             return 0;*/
/*         }*/
/*         */
/*          $("#list").html("");*/
/*          items.forEach(function(item) {*/
/*              $("#list").append('<div class="list-group-item">'+item+'<a onclick="del('+id_item+');" href="javascript:void(0);" >'+'<span class=" pull-right glyphicon glyphicon-minus"></span></a></div>');*/
/*              id_item++;*/
/*         //console.log(item);*/
/*         });*/
/*     }*/
/*         */
/*     */
/* </script>     */
/* {% endblock %}*/
