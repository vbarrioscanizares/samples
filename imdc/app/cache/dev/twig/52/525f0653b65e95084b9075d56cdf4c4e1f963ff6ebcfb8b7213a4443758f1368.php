<?php

/* users_create.html.twig */
class __TwigTemplate_420877d040f217c5c3dab98cb3e46033043d4732fc92dfbcf092d9224a6b7f0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "users_create.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4327ca9ac1b355df55092aa131e82e33829cb40c4b33761b5003cbe5a76cd608 = $this->env->getExtension("native_profiler");
        $__internal_4327ca9ac1b355df55092aa131e82e33829cb40c4b33761b5003cbe5a76cd608->enter($__internal_4327ca9ac1b355df55092aa131e82e33829cb40c4b33761b5003cbe5a76cd608_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users_create.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4327ca9ac1b355df55092aa131e82e33829cb40c4b33761b5003cbe5a76cd608->leave($__internal_4327ca9ac1b355df55092aa131e82e33829cb40c4b33761b5003cbe5a76cd608_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_e959820d056770a59690bfd3ef38952e50cdbfe6b6996d10052dc94fb19cabe2 = $this->env->getExtension("native_profiler");
        $__internal_e959820d056770a59690bfd3ef38952e50cdbfe6b6996d10052dc94fb19cabe2->enter($__internal_e959820d056770a59690bfd3ef38952e50cdbfe6b6996d10052dc94fb19cabe2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
";
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb_reduced.html.twig", "users_create.html.twig", 13)->display($context);
        // line 14
        echo "    <div class=\"col-md-12 main\">
          <h1 class=\"page-header\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Users"), "html", null, true);
        echo "</h1>
          
          <form  action=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("_users_create");
        echo "\" method=\"POST\">
            <div class=\"form-group\">
              <label for=\"fullname\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fullname"), "html", null, true);
        echo "</label>
              <input name=\"fullname\" data-validation=\"required\" type=\"text\" class=\"form-control\" id=\"fullname\" placeholder=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fullname"), "html", null, true);
        echo "\"  data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">
            </div>
            
            <div class=\"form-group\">
              <label for=\"email\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
              <input name=\"email\" data-validation=\"email\" type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "\" data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not given a correct e-mail address"), "html", null, true);
        echo "\">
            </div>
              
              
            <div class=\"form-group\">
              <label for=\"password\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Password"), "html", null, true);
        echo "</label>
              <input name=\"password\" data-validation=\"required\" type=\"password\" class=\"form-control\" id=\"password\" placeholder=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Password"), "html", null, true);
        echo "\"  data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">
            </div>
  
              
              
              
            <div class=\"form-group\">
              <label for=\"address\">";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Address"), "html", null, true);
        echo "</label>
              <input name=\"address\" type=\"text\" class=\"form-control\" id=\"address\" placeholder=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Address"), "html", null, true);
        echo "\">
            </div>
              
              
            <div class=\"form-group\">
              <label for=\"phone\">";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Phone"), "html", null, true);
        echo "</label>
              <input name=\"phone\" type=\"text\" class=\"form-control\" id=\"phone\" placeholder=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Phone"), "html", null, true);
        echo "\">
            </div>
              
            
            
            <div class=\"form-group\">
              <label for=\"role\">";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Role"), "html", null, true);
        echo "</label>
              <select name=\"role\" id=\"role\" class=\"form-control\" >
                  <option value=\"ROLE_ADMIN\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Admin"), "html", null, true);
        echo "</option>
                  <option value=\"ROLE_CLIENT\">";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Client"), "html", null, true);
        echo "</option>
                  <option value=\"ROLE_GUEST\">";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guest"), "html", null, true);
        echo "</option>
                  
              </select>
              
            </div>
            
                  
                  
            <div class=\"form-group\" id=\"role_client\" style=\"display:none;\">
              <label for=\"clients\">";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clients"), "html", null, true);
        echo "</label>
               <select multiple=\"multiple\" name=\"clients[]\" id=\"clients\" style=\"width:250px;\" >
                    ";
        // line 66
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clients_list"]) ? $context["clients_list"] : $this->getContext($context, "clients_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["client"]) {
            // line 67
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["client"], "name", array()), "html", null, true);
            echo "</option>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['client'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo " 
                   
                </select>
                
              
            </div>      
                  
                  
            
            
            
            
            
           
            <input type=\"submit\" class=\"btn btn-primary btn-lg pull-right\" value=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\"/>
          </form>
          
          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_e959820d056770a59690bfd3ef38952e50cdbfe6b6996d10052dc94fb19cabe2->leave($__internal_e959820d056770a59690bfd3ef38952e50cdbfe6b6996d10052dc94fb19cabe2_prof);

    }

    // line 97
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_fa791e9fd6abdd053a1bce1078f4b918565c5bfa2f484f5e9ad2337f4940aeeb = $this->env->getExtension("native_profiler");
        $__internal_fa791e9fd6abdd053a1bce1078f4b918565c5bfa2f484f5e9ad2337f4940aeeb->enter($__internal_fa791e9fd6abdd053a1bce1078f4b918565c5bfa2f484f5e9ad2337f4940aeeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 98
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/multipleselect/multiple-select.css"), "html", null, true);
        echo "\">    
";
        
        $__internal_fa791e9fd6abdd053a1bce1078f4b918565c5bfa2f484f5e9ad2337f4940aeeb->leave($__internal_fa791e9fd6abdd053a1bce1078f4b918565c5bfa2f484f5e9ad2337f4940aeeb_prof);

    }

    // line 105
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_77510e5085e74c9aba0887747ce744deb754fd8a081ec9aa75c813478d2383fa = $this->env->getExtension("native_profiler");
        $__internal_77510e5085e74c9aba0887747ce744deb754fd8a081ec9aa75c813478d2383fa->enter($__internal_77510e5085e74c9aba0887747ce744deb754fd8a081ec9aa75c813478d2383fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 106
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js\"></script>
<script> \$.validate(); </script>
<script src=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/multipleselect/multiple-select.js"), "html", null, true);
        echo "\"></script> 
<script>
    \$('#role_client select').multipleSelect();

    //\$(\"select\").multipleSelect(\"getSelects\"); //values
    //\$(\"select\").multipleSelect(\"getSelects\", \"text\");//text

    \$('select#role').on(\"change\",function(){
        var option = \$(\"select#role  option:selected\").val();
        if (option == \"ROLE_CLIENT\"){
                    \$('#role_client').show();
        }else       \$('#role_client').hide();
        
    });
</script>
";
        
        $__internal_77510e5085e74c9aba0887747ce744deb754fd8a081ec9aa75c813478d2383fa->leave($__internal_77510e5085e74c9aba0887747ce744deb754fd8a081ec9aa75c813478d2383fa_prof);

    }

    public function getTemplateName()
    {
        return "users_create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  250 => 109,  244 => 106,  238 => 105,  229 => 99,  225 => 98,  219 => 97,  198 => 82,  182 => 68,  171 => 67,  167 => 66,  162 => 64,  150 => 55,  146 => 54,  142 => 53,  137 => 51,  128 => 45,  124 => 44,  116 => 39,  112 => 38,  100 => 31,  96 => 30,  86 => 25,  82 => 24,  73 => 20,  69 => 19,  64 => 17,  59 => 15,  56 => 14,  54 => 13,  49 => 10,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/* {#        {% include 'sidebar_users.html.twig' %}#}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb_reduced.html.twig'%}*/
/*     <div class="col-md-12 main">*/
/*           <h1 class="page-header">{{"Users"|trans}}</h1>*/
/*           */
/*           <form  action="{{path('_users_create')}}" method="POST">*/
/*             <div class="form-group">*/
/*               <label for="fullname">{{"Fullname"|trans}}</label>*/
/*               <input name="fullname" data-validation="required" type="text" class="form-control" id="fullname" placeholder="{{"Fullname"|trans}}"  data-validation-error-msg="{{"You have not answered all required fields"|trans}}">*/
/*             </div>*/
/*             */
/*             <div class="form-group">*/
/*               <label for="email">{{"Email"|trans}}</label>*/
/*               <input name="email" data-validation="email" type="email" class="form-control" id="email" placeholder="{{"Email"|trans}}" data-validation-error-msg="{{"You have not given a correct e-mail address"|trans}}">*/
/*             </div>*/
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="password">{{"Password"|trans}}</label>*/
/*               <input name="password" data-validation="required" type="password" class="form-control" id="password" placeholder="{{"Password"|trans}}"  data-validation-error-msg="{{"You have not answered all required fields"|trans}}">*/
/*             </div>*/
/*   */
/*               */
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="address">{{"Address"|trans}}</label>*/
/*               <input name="address" type="text" class="form-control" id="address" placeholder="{{"Address"|trans}}">*/
/*             </div>*/
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="phone">{{"Phone"|trans}}</label>*/
/*               <input name="phone" type="text" class="form-control" id="phone" placeholder="{{"Phone"|trans}}">*/
/*             </div>*/
/*               */
/*             */
/*             */
/*             <div class="form-group">*/
/*               <label for="role">{{"Role"|trans}}</label>*/
/*               <select name="role" id="role" class="form-control" >*/
/*                   <option value="ROLE_ADMIN">{{"Admin"|trans}}</option>*/
/*                   <option value="ROLE_CLIENT">{{"Client"|trans}}</option>*/
/*                   <option value="ROLE_GUEST">{{"Guest"|trans}}</option>*/
/*                   */
/*               </select>*/
/*               */
/*             </div>*/
/*             */
/*                   */
/*                   */
/*             <div class="form-group" id="role_client" style="display:none;">*/
/*               <label for="clients">{{"Clients"|trans}}</label>*/
/*                <select multiple="multiple" name="clients[]" id="clients" style="width:250px;" >*/
/*                     {% for client in clients_list %}*/
/*                         <option value="{{client.id}}">{{client.name}}</option>*/
/*                     {% endfor %} */
/*                    */
/*                 </select>*/
/*                 */
/*               */
/*             </div>      */
/*                   */
/*                   */
/*             */
/*             */
/*             */
/*             */
/*             */
/*            */
/*             <input type="submit" class="btn btn-primary btn-lg pull-right" value="{{"Submit"|trans}}"/>*/
/*           </form>*/
/*           */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/* <link rel="stylesheet" href="{{asset('libs/multipleselect/multiple-select.css')}}">    */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* {% block javascripts %}*/
/* {{parent()}}*/
/* <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>*/
/* <script> $.validate(); </script>*/
/* <script src="{{asset('libs/multipleselect/multiple-select.js')}}"></script> */
/* <script>*/
/*     $('#role_client select').multipleSelect();*/
/* */
/*     //$("select").multipleSelect("getSelects"); //values*/
/*     //$("select").multipleSelect("getSelects", "text");//text*/
/* */
/*     $('select#role').on("change",function(){*/
/*         var option = $("select#role  option:selected").val();*/
/*         if (option == "ROLE_CLIENT"){*/
/*                     $('#role_client').show();*/
/*         }else       $('#role_client').hide();*/
/*         */
/*     });*/
/* </script>*/
/* {% endblock %}*/
/* */
/* */
/* */
