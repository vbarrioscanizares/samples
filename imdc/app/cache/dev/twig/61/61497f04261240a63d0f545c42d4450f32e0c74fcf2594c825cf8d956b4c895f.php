<?php

/* sidebar_clients_v2.html.twig */
class __TwigTemplate_bcbf527168c7d085dd52097844c4df300b63723872e8c23870dbc8d66ec62940 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8b96d545671f832239cd47dc274bdd0a277ecb4ed8a1bcac17ed87ec615378c5 = $this->env->getExtension("native_profiler");
        $__internal_8b96d545671f832239cd47dc274bdd0a277ecb4ed8a1bcac17ed87ec615378c5->enter($__internal_8b96d545671f832239cd47dc274bdd0a277ecb4ed8a1bcac17ed87ec615378c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "sidebar_clients_v2.html.twig"));

        // line 1
        $context["role"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "role"), "method");
        // line 2
        echo "
        <div class=\"col-md-3 _sidebar\">
            
           
            
            
         <div class=\"col-md-12\"><!-- CLIENT INFO-->
             
            <img class='img-responsive center-block' id=\"client_logo\"  style='max-height: 200px;' src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("upload/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getLogoUrl", array(), "method"), "html", null, true);
        echo "\" alt=\"logo\"/>
            ";
        // line 12
        echo "            
            <h2 id=\"client_name\" class='text-center'>";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "name", array()), "html", null, true);
        echo "</h2>
            
            <h5 class='text-center' id=\"client_address\">
                ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["addresses"]) ? $context["addresses"] : $this->getContext($context, "addresses")));
        foreach ($context['_seq'] as $context["_key"] => $context["address"]) {
            // line 17
            echo "                    ";
            echo twig_escape_filter($this->env, $context["address"], "html", null, true);
            echo " <br/>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['address'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "            </h5>
            
            <h6 class='text-center' id=\"client_contact\">
                ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["contacts"]) ? $context["contacts"] : $this->getContext($context, "contacts")));
        foreach ($context['_seq'] as $context["_key"] => $context["contact"]) {
            // line 23
            echo "                    ";
            echo twig_escape_filter($this->env, $context["contact"], "html", null, true);
            echo " <br/>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contact'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "            </h6>
        </div>   
            
        <br>    
         
            
        <div class=\"col-md-12 billing\"><!-- Facturation-->
            
            <div class=\"panel panel-info\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title \">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Billing"), "html", null, true);
        echo " - Mai 2015</h3>
                  </div>
                  <div class=\"panel-body\">
                    <span class=\"label label-info\">5748.50\$</span>
                    <span class=\"label label-info\">400kWh</span>
                    <span class=\"label label-info\">14139.99\$</span>
                    <span class=\"label label-info\">300851kW</span>
                    <span class=\"label label-info\">19888.48\$</span>
                  </div>
                
            </div>
            
        </div>     
            
            
         <div class=\"col-md-12 forecast\"><!-- Prevision-->
             <div class=\"panel panel-success\">
                 <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Forecast"), "html", null, true);
        echo " - Mai 2016</h3>
                  </div>
                  <div class=\"panel-body\">
                    <span class=\"label label-info\">5029.50\$</span>
                     <span class=\"label label-info\">350kWh</span>
                     <span class=\"label label-info\">12259.99\$</span>
                     <span class=\"label label-info\">260851kW</span>
                     <span class=\"label label-info\">17289.50\$</span>
                  </div>
                 
                 
             </div>
            
        </div>        
            
           
                  
        <div class=\"col-md-12 video\">
            
            
            <div class=\"panel panel-info\">
                 <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Video"), "html", null, true);
        echo "</h3>
                  </div>
                  <div class=\"panel-body\">
                    <iframe width=\"100%\" src=\"https://www.youtube.com/embed/VYMjSule0Bw\" frameborder=\"0\" allowfullscreen></iframe>
                  </div>
                 
                 
             </div>
            
        
        </div>    
                
            
         
         
        </div>
    
    
    
    ";
        
        $__internal_8b96d545671f832239cd47dc274bdd0a277ecb4ed8a1bcac17ed87ec615378c5->leave($__internal_8b96d545671f832239cd47dc274bdd0a277ecb4ed8a1bcac17ed87ec615378c5_prof);

    }

    public function getTemplateName()
    {
        return "sidebar_clients_v2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 75,  112 => 53,  91 => 35,  79 => 25,  70 => 23,  66 => 22,  61 => 19,  52 => 17,  48 => 16,  42 => 13,  39 => 12,  34 => 10,  24 => 2,  22 => 1,);
    }
}
/* {% set role = app.session.get('role') %}*/
/* */
/*         <div class="col-md-3 _sidebar">*/
/*             */
/*            */
/*             */
/*             */
/*          <div class="col-md-12"><!-- CLIENT INFO-->*/
/*              */
/*             <img class='img-responsive center-block' id="client_logo"  style='max-height: 200px;' src="{{asset('upload/')}}{{client.getLogoUrl()}}" alt="logo"/>*/
/*             {#<img id="client_logo"  style='max-height: 200px;' src="https://image.freepik.com/free-vector/logo-sample-text_355-558.jpg" class='img-responsive center-block' alt='logo' />#}*/
/*             */
/*             <h2 id="client_name" class='text-center'>{{client.name}}</h2>*/
/*             */
/*             <h5 class='text-center' id="client_address">*/
/*                 {% for address in addresses%}*/
/*                     {{address}} <br/>*/
/*                 {% endfor %}*/
/*             </h5>*/
/*             */
/*             <h6 class='text-center' id="client_contact">*/
/*                 {% for contact in contacts%}*/
/*                     {{contact}} <br/>*/
/*                 {% endfor %}*/
/*             </h6>*/
/*         </div>   */
/*             */
/*         <br>    */
/*          */
/*             */
/*         <div class="col-md-12 billing"><!-- Facturation-->*/
/*             */
/*             <div class="panel panel-info">*/
/*                 <div class="panel-heading">*/
/*                     <h3 class="panel-title ">{{"Billing"|trans}} - Mai 2015</h3>*/
/*                   </div>*/
/*                   <div class="panel-body">*/
/*                     <span class="label label-info">5748.50$</span>*/
/*                     <span class="label label-info">400kWh</span>*/
/*                     <span class="label label-info">14139.99$</span>*/
/*                     <span class="label label-info">300851kW</span>*/
/*                     <span class="label label-info">19888.48$</span>*/
/*                   </div>*/
/*                 */
/*             </div>*/
/*             */
/*         </div>     */
/*             */
/*             */
/*          <div class="col-md-12 forecast"><!-- Prevision-->*/
/*              <div class="panel panel-success">*/
/*                  <div class="panel-heading">*/
/*                     <h3 class="panel-title">{{"Forecast"|trans}} - Mai 2016</h3>*/
/*                   </div>*/
/*                   <div class="panel-body">*/
/*                     <span class="label label-info">5029.50$</span>*/
/*                      <span class="label label-info">350kWh</span>*/
/*                      <span class="label label-info">12259.99$</span>*/
/*                      <span class="label label-info">260851kW</span>*/
/*                      <span class="label label-info">17289.50$</span>*/
/*                   </div>*/
/*                  */
/*                  */
/*              </div>*/
/*             */
/*         </div>        */
/*             */
/*            */
/*                   */
/*         <div class="col-md-12 video">*/
/*             */
/*             */
/*             <div class="panel panel-info">*/
/*                  <div class="panel-heading">*/
/*                     <h3 class="panel-title">{{"Video"|trans}}</h3>*/
/*                   </div>*/
/*                   <div class="panel-body">*/
/*                     <iframe width="100%" src="https://www.youtube.com/embed/VYMjSule0Bw" frameborder="0" allowfullscreen></iframe>*/
/*                   </div>*/
/*                  */
/*                  */
/*              </div>*/
/*             */
/*         */
/*         </div>    */
/*                 */
/*             */
/*          */
/*          */
/*         </div>*/
/*     */
/*     */
/*     */
/*     */
