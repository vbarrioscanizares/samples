<?php

/* clients_alarms.html.twig */
class __TwigTemplate_d6098c005c87b26a428b3c0cc052c03ab51855f8a0de13bac3d56d9882134f16 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_alarms.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7081fddbb12d408b011680cf968211583b28114e6046914c430096042e720442 = $this->env->getExtension("native_profiler");
        $__internal_7081fddbb12d408b011680cf968211583b28114e6046914c430096042e720442->enter($__internal_7081fddbb12d408b011680cf968211583b28114e6046914c430096042e720442_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_alarms.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7081fddbb12d408b011680cf968211583b28114e6046914c430096042e720442->leave($__internal_7081fddbb12d408b011680cf968211583b28114e6046914c430096042e720442_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_759762b6c3d761670999ce5d0f79c7ee6764a2a4957bfede0dade73a9828ef2f = $this->env->getExtension("native_profiler");
        $__internal_759762b6c3d761670999ce5d0f79c7ee6764a2a4957bfede0dade73a9828ef2f->enter($__internal_759762b6c3d761670999ce5d0f79c7ee6764a2a4957bfede0dade73a9828ef2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 9
        $this->loadTemplate("sidebar_clients_v2.html.twig", "clients_alarms.html.twig", 9)->display($context);
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_alarms.html.twig", 13)->display(array_merge($context, array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()))));
        // line 14
        echo "    <div class=\" col-md-9 main\">
           
        
          <h1 class=\"page-header\">";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Alarms"), "html", null, true);
        echo "</h1>
          
          
          
          <p></p>
          
          ";
        // line 23
        if ((twig_length_filter($this->env, (isset($context["alarms"]) ? $context["alarms"] : $this->getContext($context, "alarms"))) == 0)) {
            // line 24
            echo "              <div class=\"alert alert-success\" role=\"alert\">
                  <span id=\"alarm_list\">
                    ";
            // line 26
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("0 Alarms in the system!"), "html", null, true);
            echo "
                  </span>
              </div>
              
          ";
        } else {
            // line 30
            echo "    
          
          <div class=\"table-responsive\">
            <table class=\"table table-hover\">
               <thead>
                <tr>
                   <th style=\"width:20%;\">";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
            echo "</th>
                   <th style=\"width:80%;\">";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Event"), "html", null, true);
            echo "</th>
                   
                </tr>
               </thead>  
            <tbody>
                
                
            ";
            // line 44
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["alarms"]) ? $context["alarms"] : $this->getContext($context, "alarms")));
            foreach ($context['_seq'] as $context["_key"] => $context["alarm"]) {
                // line 45
                echo "                    
                        <tr style=\"cursor:pointer;\" >

                        <td>";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($context["alarm"], "datetime", array()), "html", null, true);
                echo "</td>
                        <td>";
                // line 49
                echo twig_escape_filter($this->env, $this->getAttribute($context["alarm"], "event", array()), "html", null, true);
                echo "</td>
                        
                      </tr>
                    
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['alarm'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "    
              
            
            
            </tbody>  
            </table>
            
            
          </div>
          ";
        }
        // line 63
        echo "            
            ";
        // line 64
        if (((isset($context["role"]) ? $context["role"] : $this->getContext($context, "role")) == "ROLE_ADMIN")) {
            // line 65
            echo "                <p><a class=\"btn btn-primary btn-lg pull-right\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_alarms_reset", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()))), "html", null, true);
            echo "\" role=\"button\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Reset"), "html", null, true);
            echo "</a></p>
            ";
        }
        // line 67
        echo "          
          
          
          
          
          
          
          
          
          
          

          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_759762b6c3d761670999ce5d0f79c7ee6764a2a4957bfede0dade73a9828ef2f->leave($__internal_759762b6c3d761670999ce5d0f79c7ee6764a2a4957bfede0dade73a9828ef2f_prof);

    }

    // line 91
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_725f27a21795a4c7bebd8c0e619a345c4f271af5f99651d4620f9844c6d7d33b = $this->env->getExtension("native_profiler");
        $__internal_725f27a21795a4c7bebd8c0e619a345c4f271af5f99651d4620f9844c6d7d33b->enter($__internal_725f27a21795a4c7bebd8c0e619a345c4f271af5f99651d4620f9844c6d7d33b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 92
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    
";
        
        $__internal_725f27a21795a4c7bebd8c0e619a345c4f271af5f99651d4620f9844c6d7d33b->leave($__internal_725f27a21795a4c7bebd8c0e619a345c4f271af5f99651d4620f9844c6d7d33b_prof);

    }

    public function getTemplateName()
    {
        return "clients_alarms.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 92,  185 => 91,  156 => 67,  148 => 65,  146 => 64,  143 => 63,  131 => 53,  120 => 49,  116 => 48,  111 => 45,  107 => 44,  97 => 37,  93 => 36,  85 => 30,  77 => 26,  73 => 24,  71 => 23,  62 => 17,  57 => 14,  55 => 13,  50 => 10,  48 => 9,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients_v2.html.twig' %}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb.html.twig' with {'client_id': client.id}%}*/
/*     <div class=" col-md-9 main">*/
/*            */
/*         */
/*           <h1 class="page-header">{{"Alarms"|trans}}</h1>*/
/*           */
/*           */
/*           */
/*           <p></p>*/
/*           */
/*           {% if alarms|length == 0%}*/
/*               <div class="alert alert-success" role="alert">*/
/*                   <span id="alarm_list">*/
/*                     {{"0 Alarms in the system!"|trans}}*/
/*                   </span>*/
/*               </div>*/
/*               */
/*           {%else%}    */
/*           */
/*           <div class="table-responsive">*/
/*             <table class="table table-hover">*/
/*                <thead>*/
/*                 <tr>*/
/*                    <th style="width:20%;">{{"Date"|trans}}</th>*/
/*                    <th style="width:80%;">{{"Event"|trans}}</th>*/
/*                    */
/*                 </tr>*/
/*                </thead>  */
/*             <tbody>*/
/*                 */
/*                 */
/*             {% for alarm in alarms %}*/
/*                     */
/*                         <tr style="cursor:pointer;" >*/
/* */
/*                         <td>{{alarm.datetime}}</td>*/
/*                         <td>{{alarm.event}}</td>*/
/*                         */
/*                       </tr>*/
/*                     */
/*             {% endfor %}    */
/*               */
/*             */
/*             */
/*             </tbody>  */
/*             </table>*/
/*             */
/*             */
/*           </div>*/
/*           {%endif%}*/
/*             */
/*             {%if role == "ROLE_ADMIN"%}*/
/*                 <p><a class="btn btn-primary btn-lg pull-right" href="{{path("_clients_alarms_reset", { 'client_id': client.id })}}" role="button">{{"Reset"|trans}}</a></p>*/
/*             {%endif%}*/
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/* */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/*     */
/* {% endblock %}*/
/* */
