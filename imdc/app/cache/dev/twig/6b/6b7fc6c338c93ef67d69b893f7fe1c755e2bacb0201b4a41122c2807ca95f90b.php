<?php

/* default/login.html.twig */
class __TwigTemplate_bc5d2f15be99579b5a5061c322761ad1b6ba2cdb0df27155dd4099ac6ba95ad2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "default/login.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_901fdcf644f5ee95108e2d682a1e3339987a66c799946bd1b4fd80ffb3b49ae9 = $this->env->getExtension("native_profiler");
        $__internal_901fdcf644f5ee95108e2d682a1e3339987a66c799946bd1b4fd80ffb3b49ae9->enter($__internal_901fdcf644f5ee95108e2d682a1e3339987a66c799946bd1b4fd80ffb3b49ae9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_901fdcf644f5ee95108e2d682a1e3339987a66c799946bd1b4fd80ffb3b49ae9->leave($__internal_901fdcf644f5ee95108e2d682a1e3339987a66c799946bd1b4fd80ffb3b49ae9_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_95f547fc83375e3497c5e4c7f53ddb26aba75e0c2887a792f6dc9a92f20ee1d3 = $this->env->getExtension("native_profiler");
        $__internal_95f547fc83375e3497c5e4c7f53ddb26aba75e0c2887a792f6dc9a92f20ee1d3->enter($__internal_95f547fc83375e3497c5e4c7f53ddb26aba75e0c2887a792f6dc9a92f20ee1d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Login"), "html", null, true);
        echo "
";
        
        $__internal_95f547fc83375e3497c5e4c7f53ddb26aba75e0c2887a792f6dc9a92f20ee1d3->leave($__internal_95f547fc83375e3497c5e4c7f53ddb26aba75e0c2887a792f6dc9a92f20ee1d3_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_9c9a498843c015adffc8dbf5a813f473095b921ae2d4f94022f2c1c06a70a735 = $this->env->getExtension("native_profiler");
        $__internal_9c9a498843c015adffc8dbf5a813f473095b921ae2d4f94022f2c1c06a70a735->enter($__internal_9c9a498843c015adffc8dbf5a813f473095b921ae2d4f94022f2c1c06a70a735_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        $context["params"] = twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method"), $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "all", array(), "method"));
        // line 11
        $context["frParams"] = twig_array_merge((isset($context["params"]) ? $context["params"] : $this->getContext($context, "params")), array("_locale" => "fr"));
        // line 12
        $context["enParams"] = twig_array_merge((isset($context["params"]) ? $context["params"] : $this->getContext($context, "params")), array("_locale" => "en"));
        // line 13
        $context["itParams"] = twig_array_merge((isset($context["params"]) ? $context["params"] : $this->getContext($context, "params")), array("_locale" => "it"));
        // line 14
        echo "    


<div class=\"logo\"></div>
<div class=\"login-block\">
    <h1>";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Login"), "html", null, true);
        echo "</h1>
         
        <input  name=\"username\" type=\"text\" value=\"\" placeholder=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Username"), "html", null, true);
        echo "\" id=\"username\" />
        <input name=\"password\" type=\"password\" value=\"\" placeholder=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Password"), "html", null, true);
        echo "\" id=\"password\" />
        <p  id=\"msg_verify\" style=\"color:red;display: none;\">";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("* Please, verify the information!"), "html", null, true);
        echo "</p>
        <button onclick=\"submitF();\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "</button>
      
    <a style=\"display:none;\" href=\"\">";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Forgot your password?"), "html", null, true);
        echo "</a>
            ";
        // line 45
        echo "   
</div>
</body>    
    
    
    
";
        
        $__internal_9c9a498843c015adffc8dbf5a813f473095b921ae2d4f94022f2c1c06a70a735->leave($__internal_9c9a498843c015adffc8dbf5a813f473095b921ae2d4f94022f2c1c06a70a735_prof);

    }

    // line 53
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f88f8aee963c4beccb99f4d471fe57078b38923aa6de066de43937194a9f0bc1 = $this->env->getExtension("native_profiler");
        $__internal_f88f8aee963c4beccb99f4d471fe57078b38923aa6de066de43937194a9f0bc1->enter($__internal_f88f8aee963c4beccb99f4d471fe57078b38923aa6de066de43937194a9f0bc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 54
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    
<style>    
    
 </style>   
    
";
        
        $__internal_f88f8aee963c4beccb99f4d471fe57078b38923aa6de066de43937194a9f0bc1->leave($__internal_f88f8aee963c4beccb99f4d471fe57078b38923aa6de066de43937194a9f0bc1_prof);

    }

    // line 65
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_10efaf85721012270fdd4d3af4ea18f37362b8770c20bfd31a356c69ca41c5a1 = $this->env->getExtension("native_profiler");
        $__internal_10efaf85721012270fdd4d3af4ea18f37362b8770c20bfd31a356c69ca41c5a1->enter($__internal_10efaf85721012270fdd4d3af4ea18f37362b8770c20bfd31a356c69ca41c5a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 66
        echo "
";
        // line 67
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
 
<script>
 \$(document).ready(function(){
    
    \$('input').keypress(function(e){
      if(e.keyCode==13)
            submitF();
    });
});   
    
    
    
    
 function submitF(){
    var username = \$(\"#username\").val();
    var password = \$(\"#password\").val();
    
    \$.post( \"";
        // line 85
        echo $this->env->getExtension('routing')->getPath("_login_call");
        echo "\", 
        { username: \"\"+username, password: \"\"+password }, 
        function( data ) { 
            
     
            if (data == \"done\"){
                 window.location.replace(\"";
        // line 91
        echo $this->env->getExtension('routing')->getUrl("_users");
        echo "\");    
            }else{
                \$(\"#msg_verify\").show();
            }
    });
 
 }
</script>    
    
";
        
        $__internal_10efaf85721012270fdd4d3af4ea18f37362b8770c20bfd31a356c69ca41c5a1->leave($__internal_10efaf85721012270fdd4d3af4ea18f37362b8770c20bfd31a356c69ca41c5a1_prof);

    }

    public function getTemplateName()
    {
        return "default/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 91,  163 => 85,  142 => 67,  139 => 66,  133 => 65,  118 => 54,  112 => 53,  99 => 45,  95 => 26,  90 => 24,  86 => 23,  82 => 22,  78 => 21,  73 => 19,  66 => 14,  64 => 13,  62 => 12,  60 => 11,  58 => 10,  52 => 8,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block title %}*/
/* {{"Login"|trans}}*/
/* {%endblock%}    */
/* */
/* */
/* {% block body %}*/
/* {# language#}*/
/* {% set params = app.request.attributes.get('_route_params')|merge(app.request.query.all()) %}*/
/* {% set frParams = params|merge({'_locale': 'fr'}) %}*/
/* {% set enParams = params|merge({'_locale': 'en'}) %}*/
/* {% set itParams = params|merge({'_locale': 'it'}) %}*/
/*     */
/* */
/* */
/* <div class="logo"></div>*/
/* <div class="login-block">*/
/*     <h1>{{"Login"|trans}}</h1>*/
/*          */
/*         <input  name="username" type="text" value="" placeholder="{{'Username'|trans}}" id="username" />*/
/*         <input name="password" type="password" value="" placeholder="{{'Password'|trans}}" id="password" />*/
/*         <p  id="msg_verify" style="color:red;display: none;">{{"* Please, verify the information!"|trans}}</p>*/
/*         <button onclick="submitF();">{{"Submit"|trans}}</button>*/
/*       */
/*     <a style="display:none;" href="">{{"Forgot your password?"|trans}}</a>*/
/*             {#<div class="dropdown pull-right">*/
/*               <a id="dLabel" data-target="#" href="" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">*/
/*                 {{"current_language"|trans}}*/
/*                 <span class="caret"></span>*/
/*               </a>*/
/* */
/*               <ul class="dropdown-menu" aria-labelledby="dLabel">*/
/*                 {%if app.request.locale == "en"%}*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), frParams) }}">French</a></li>*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), itParams) }}">Italien</a></li>*/
/*                {%elseif app.request.locale == "fr"%}*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), enParams) }}">English</a></li>*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), itParams) }}">Italien</a></li>*/
/*                {%else%}*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), enParams) }}">English</a></li>*/
/*                 <li><a href="{{ path(app.request.attributes.get('_route'), frParams) }}">French</a></li>*/
/*                {%endif%}*/
/*               </ul>*/
/*             </div>#}   */
/* </div>*/
/* </body>    */
/*     */
/*     */
/*     */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{parent()}}*/
/*     */
/* <style>    */
/*     */
/*  </style>   */
/*     */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* {%block javascripts %}*/
/* */
/* {{parent()}}*/
/*  */
/* <script>*/
/*  $(document).ready(function(){*/
/*     */
/*     $('input').keypress(function(e){*/
/*       if(e.keyCode==13)*/
/*             submitF();*/
/*     });*/
/* });   */
/*     */
/*     */
/*     */
/*     */
/*  function submitF(){*/
/*     var username = $("#username").val();*/
/*     var password = $("#password").val();*/
/*     */
/*     $.post( "{{path('_login_call')}}", */
/*         { username: ""+username, password: ""+password }, */
/*         function( data ) { */
/*             */
/*      */
/*             if (data == "done"){*/
/*                  window.location.replace("{{url('_users')}}");    */
/*             }else{*/
/*                 $("#msg_verify").show();*/
/*             }*/
/*     });*/
/*  */
/*  }*/
/* </script>    */
/*     */
/* {% endblock%}*/
