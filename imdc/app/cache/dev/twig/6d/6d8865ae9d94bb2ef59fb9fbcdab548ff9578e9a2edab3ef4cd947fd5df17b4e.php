<?php

/* clients_breadcrumb_reduced.html.twig */
class __TwigTemplate_d3335f52edefccff2a75c3bf69611bbd0bbfca6c699bc668ed734f7edd28ec7e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6392024ae7f628be23fda7da67abfb1f516a907fbf053b5762a4880341f3f0da = $this->env->getExtension("native_profiler");
        $__internal_6392024ae7f628be23fda7da67abfb1f516a907fbf053b5762a4880341f3f0da->enter($__internal_6392024ae7f628be23fda7da67abfb1f516a907fbf053b5762a4880341f3f0da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_breadcrumb_reduced.html.twig"));

        // line 1
        $context["route"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method");
        // line 2
        echo "

<ol class=\"breadcrumb\">
  ";
        // line 6
        echo "  
  
  
  ";
        // line 9
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "role"), "method") == "ROLE_ADMIN")) {
            // line 10
            echo "  
        ";
            // line 11
            if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_users")) {
                // line 12
                echo "            <li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Users"), "html", null, true);
                echo "</li>
        ";
            } else {
                // line 14
                echo "            <li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("_users");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Users"), "html", null, true);
                echo "</a></li>
        ";
            }
            // line 16
            echo "   
   ";
        }
        // line 18
        echo "        
   
   ";
        // line 20
        if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_clients")) {
            // line 21
            echo "            <li>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clients"), "html", null, true);
            echo "</li>
        ";
        } else {
            // line 23
            echo "            <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("_clients");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clients"), "html", null, true);
            echo "</a></li>
        ";
        }
        // line 25
        echo "  
  
   
</ol>";
        
        $__internal_6392024ae7f628be23fda7da67abfb1f516a907fbf053b5762a4880341f3f0da->leave($__internal_6392024ae7f628be23fda7da67abfb1f516a907fbf053b5762a4880341f3f0da_prof);

    }

    public function getTemplateName()
    {
        return "clients_breadcrumb_reduced.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 25,  71 => 23,  65 => 21,  63 => 20,  59 => 18,  55 => 16,  47 => 14,  41 => 12,  39 => 11,  36 => 10,  34 => 9,  29 => 6,  24 => 2,  22 => 1,);
    }
}
/* {%set route = app.request.get('_route') %}*/
/* */
/* */
/* <ol class="breadcrumb">*/
/*   {#<li><a href="{{path('_clients')}}">{{"List"|trans}}</a></li>#}*/
/*   */
/*   */
/*   */
/*   {%if app.session.get('role') == "ROLE_ADMIN"%}*/
/*   */
/*         {% if route=="_users" %}*/
/*             <li>{{"Users"|trans}}</li>*/
/*         {% else %}*/
/*             <li><a href="{{path("_users")}}">{{"Users"|trans}}</a></li>*/
/*         {% endif %}*/
/*    */
/*    {%endif%}*/
/*         */
/*    */
/*    {% if route=="_clients" %}*/
/*             <li>{{"Clients"|trans}}</li>*/
/*         {% else %}*/
/*             <li><a href="{{path("_clients")}}">{{"Clients"|trans}}</a></li>*/
/*         {% endif %}*/
/*   */
/*   */
/*    */
/* </ol>*/
