<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_5eb57cdcf9843174379b526cfb4ff7f7ee3a5235a7a124bf9f218575fa5a8e3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_185c0e3af066840f30ae7f88b7cd4b0844fa38a0a431f039af881997a7826f81 = $this->env->getExtension("native_profiler");
        $__internal_185c0e3af066840f30ae7f88b7cd4b0844fa38a0a431f039af881997a7826f81->enter($__internal_185c0e3af066840f30ae7f88b7cd4b0844fa38a0a431f039af881997a7826f81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_185c0e3af066840f30ae7f88b7cd4b0844fa38a0a431f039af881997a7826f81->leave($__internal_185c0e3af066840f30ae7f88b7cd4b0844fa38a0a431f039af881997a7826f81_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e8c3b47a169fdd6c267df7a89dcc87d6bf09d08f401bd182b78ddb29db8a3f20 = $this->env->getExtension("native_profiler");
        $__internal_e8c3b47a169fdd6c267df7a89dcc87d6bf09d08f401bd182b78ddb29db8a3f20->enter($__internal_e8c3b47a169fdd6c267df7a89dcc87d6bf09d08f401bd182b78ddb29db8a3f20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_e8c3b47a169fdd6c267df7a89dcc87d6bf09d08f401bd182b78ddb29db8a3f20->leave($__internal_e8c3b47a169fdd6c267df7a89dcc87d6bf09d08f401bd182b78ddb29db8a3f20_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_1c4fc74fad2f50766b22343e1f9a3c15c2fda2d6b063031f8fec3ec82574cefb = $this->env->getExtension("native_profiler");
        $__internal_1c4fc74fad2f50766b22343e1f9a3c15c2fda2d6b063031f8fec3ec82574cefb->enter($__internal_1c4fc74fad2f50766b22343e1f9a3c15c2fda2d6b063031f8fec3ec82574cefb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_1c4fc74fad2f50766b22343e1f9a3c15c2fda2d6b063031f8fec3ec82574cefb->leave($__internal_1c4fc74fad2f50766b22343e1f9a3c15c2fda2d6b063031f8fec3ec82574cefb_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_2d7a36ab2598c406bd2c320d6825ef09dd1c79fbd05fad86304f2593de3b4bad = $this->env->getExtension("native_profiler");
        $__internal_2d7a36ab2598c406bd2c320d6825ef09dd1c79fbd05fad86304f2593de3b4bad->enter($__internal_2d7a36ab2598c406bd2c320d6825ef09dd1c79fbd05fad86304f2593de3b4bad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_2d7a36ab2598c406bd2c320d6825ef09dd1c79fbd05fad86304f2593de3b4bad->leave($__internal_2d7a36ab2598c406bd2c320d6825ef09dd1c79fbd05fad86304f2593de3b4bad_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
