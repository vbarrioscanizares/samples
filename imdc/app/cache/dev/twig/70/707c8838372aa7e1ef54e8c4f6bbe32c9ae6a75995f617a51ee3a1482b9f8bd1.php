<?php

/* clients_power_presets_edit.html.twig */
class __TwigTemplate_98a50229803bbb9752e05f3e83bace99865aaa5eead68b19b59e4434402dc654 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_power_presets_edit.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0aa090843e6fddb7a7d62b2d07b9b403b0571eccfd5369df70463ddfa370672f = $this->env->getExtension("native_profiler");
        $__internal_0aa090843e6fddb7a7d62b2d07b9b403b0571eccfd5369df70463ddfa370672f->enter($__internal_0aa090843e6fddb7a7d62b2d07b9b403b0571eccfd5369df70463ddfa370672f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_power_presets_edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0aa090843e6fddb7a7d62b2d07b9b403b0571eccfd5369df70463ddfa370672f->leave($__internal_0aa090843e6fddb7a7d62b2d07b9b403b0571eccfd5369df70463ddfa370672f_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_d54d5babe8ec19b17b4b9ae0f97265fc9c27eecec8c7c762772cf60558788892 = $this->env->getExtension("native_profiler");
        $__internal_d54d5babe8ec19b17b4b9ae0f97265fc9c27eecec8c7c762772cf60558788892->enter($__internal_d54d5babe8ec19b17b4b9ae0f97265fc9c27eecec8c7c762772cf60558788892_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 9
        $this->loadTemplate("sidebar_clients.html.twig", "clients_power_presets_edit.html.twig", 9)->display($context);
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_power_presets_edit.html.twig", 13)->display(array_merge($context, array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()))));
        // line 14
        echo "    <div class=\"col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main\">
            
          <h1 class=\"page-header\">";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "name", array()), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Edit"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Power presets"), "html", null, true);
        echo "</h1>
          
          <form  action=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_active_power_presets_edit", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "bill_id" => $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "id", array()))), "html", null, true);
        echo "\" method=\"POST\">
              <input type=\"hidden\" value=\"1\" name=\"posting\">
           
           <div class=\"well well-lg\">   
                
                
            <div class=\"form-group inline\">
              <label for=\"date_from\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <p><span id=\"date_from\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getDateFrom", array(), "method"), "html", null, true);
        echo "</span></p>
            </div>     
               
            
           <div class=\"form-group inline\">
              <label for=\"date_to\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <p><span id=\"date_to\">";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getDateTo", array(), "method"), "html", null, true);
        echo "</span></p>
            </div>     
                
                
           <div class=\"form-group\">
              <label for=\"h_power\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Historic Power"), "html", null, true);
        echo " (Kw) </label>
              <p><span id=\"h_power\">";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getKw", array(), "method"), "html", null, true);
        echo "</span></p>
            </div>   
               
                
                
           <div class=\"form-group\">
              <label for=\"av_power\">";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Available Power"), "html", null, true);
        echo " (Kw) <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Sum of power in entries"), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></label>
              <p><span id=\"av_power\">";
        // line 45
        echo twig_escape_filter($this->env, (isset($context["sum"]) ? $context["sum"] : $this->getContext($context, "sum")), "html", null, true);
        echo "</span></p>
            </div>     
                
                
           <div class=\"form-group\">
              <label for=\"min_power\">";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Minimal Power"), "html", null, true);
        echo " (Kw) <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"[";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Historic Power"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Available Power"), "html", null, true);
        echo "] ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("or Custom Value"), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></label>
              <p><span id=\"min_power\">";
        // line 51
        echo twig_escape_filter($this->env, (isset($context["max"]) ? $context["max"] : $this->getContext($context, "max")), "html", null, true);
        echo "</span></p>
            </div>     
               
              
            <div class=\"form-group\">
              <label for=\"bill_preset\">";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Preset"), "html", null, true);
        echo " <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"[";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Historic Power"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Available Power"), "html", null, true);
        echo "] ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("or Custom Value"), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></label>
              <input name=\"bill_preset\" value=\"";
        // line 57
        echo twig_escape_filter($this->env, (isset($context["preset"]) ? $context["preset"] : $this->getContext($context, "preset")), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"bill_preset\" placeholder=\"\"  >
            </div>
              
                
             </div><!--well-->
                
            
           <p>
            <input type=\"submit\" class=\"btn btn-primary btn-lg pull-right\" value=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\"/>
            </p>
            
           
          </form>
          
          
          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_d54d5babe8ec19b17b4b9ae0f97265fc9c27eecec8c7c762772cf60558788892->leave($__internal_d54d5babe8ec19b17b4b9ae0f97265fc9c27eecec8c7c762772cf60558788892_prof);

    }

    // line 82
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_422892da732fe7786ec65388c155d2543f8ea998f97a20dfa033cb30efd645c1 = $this->env->getExtension("native_profiler");
        $__internal_422892da732fe7786ec65388c155d2543f8ea998f97a20dfa033cb30efd645c1->enter($__internal_422892da732fe7786ec65388c155d2543f8ea998f97a20dfa033cb30efd645c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 83
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    
";
        
        $__internal_422892da732fe7786ec65388c155d2543f8ea998f97a20dfa033cb30efd645c1->leave($__internal_422892da732fe7786ec65388c155d2543f8ea998f97a20dfa033cb30efd645c1_prof);

    }

    public function getTemplateName()
    {
        return "clients_power_presets_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 83,  197 => 82,  174 => 65,  163 => 57,  153 => 56,  145 => 51,  135 => 50,  127 => 45,  121 => 44,  112 => 38,  108 => 37,  100 => 32,  94 => 31,  86 => 26,  80 => 25,  70 => 18,  61 => 16,  57 => 14,  55 => 13,  50 => 10,  48 => 9,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients.html.twig' %}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb.html.twig' with {'client_id': client.id}%}*/
/*     <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">*/
/*             */
/*           <h1 class="page-header">{{client.name}} - {{"Edit"|trans}} {{"Total Power presets"|trans}}</h1>*/
/*           */
/*           <form  action="{{path('_active_power_presets_edit',{ 'client_id': client.id, 'bill_id': bill.id})}}" method="POST">*/
/*               <input type="hidden" value="1" name="posting">*/
/*            */
/*            <div class="well well-lg">   */
/*                 */
/*                 */
/*             <div class="form-group inline">*/
/*               <label for="date_from">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <p><span id="date_from">{{bill.getDateFrom()}}</span></p>*/
/*             </div>     */
/*                */
/*             */
/*            <div class="form-group inline">*/
/*               <label for="date_to">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <p><span id="date_to">{{bill.getDateTo()}}</span></p>*/
/*             </div>     */
/*                 */
/*                 */
/*            <div class="form-group">*/
/*               <label for="h_power">{{"Historic Power"|trans}} (Kw) </label>*/
/*               <p><span id="h_power">{{bill.getKw()}}</span></p>*/
/*             </div>   */
/*                */
/*                 */
/*                 */
/*            <div class="form-group">*/
/*               <label for="av_power">{{"Available Power"|trans}} (Kw) <span data-toggle="tooltip" data-placement="right" title="{{"Sum of power in entries"|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></label>*/
/*               <p><span id="av_power">{{sum}}</span></p>*/
/*             </div>     */
/*                 */
/*                 */
/*            <div class="form-group">*/
/*               <label for="min_power">{{"Minimal Power"|trans}} (Kw) <span data-toggle="tooltip" data-placement="right" title="[{{"Historic Power"|trans}} - {{"Available Power"|trans}}] {{"or Custom Value"|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></label>*/
/*               <p><span id="min_power">{{max}}</span></p>*/
/*             </div>     */
/*                */
/*               */
/*             <div class="form-group">*/
/*               <label for="bill_preset">{{"Preset"|trans}} <span data-toggle="tooltip" data-placement="right" title="[{{"Historic Power"|trans}} - {{"Available Power"|trans}}] {{"or Custom Value"|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></label>*/
/*               <input name="bill_preset" value="{{preset}}" type="text" class="form-control" id="bill_preset" placeholder=""  >*/
/*             </div>*/
/*               */
/*                 */
/*              </div><!--well-->*/
/*                 */
/*             */
/*            <p>*/
/*             <input type="submit" class="btn btn-primary btn-lg pull-right" value="{{"Submit"|trans}}"/>*/
/*             </p>*/
/*             */
/*            */
/*           </form>*/
/*           */
/*           */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/*     */
/* {% endblock %}*/
/* */
