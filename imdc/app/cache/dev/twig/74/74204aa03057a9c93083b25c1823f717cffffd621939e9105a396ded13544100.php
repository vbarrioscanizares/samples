<?php

/* users.html.twig */
class __TwigTemplate_2e0a437e97160fd3e73bfb01184baed19b2a2f48120b506e90512ef83ac29c1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "users.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0bc3c21020ab89f1a55db5dbc2fbeeea23fe1ff7f968b5c1f956bcc28baa33c4 = $this->env->getExtension("native_profiler");
        $__internal_0bc3c21020ab89f1a55db5dbc2fbeeea23fe1ff7f968b5c1f956bcc28baa33c4->enter($__internal_0bc3c21020ab89f1a55db5dbc2fbeeea23fe1ff7f968b5c1f956bcc28baa33c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "users.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0bc3c21020ab89f1a55db5dbc2fbeeea23fe1ff7f968b5c1f956bcc28baa33c4->leave($__internal_0bc3c21020ab89f1a55db5dbc2fbeeea23fe1ff7f968b5c1f956bcc28baa33c4_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_071e400d93528e2c437298a431f996d65ecaffcc096d625e7bedeb14dea0f5cd = $this->env->getExtension("native_profiler");
        $__internal_071e400d93528e2c437298a431f996d65ecaffcc096d625e7bedeb14dea0f5cd->enter($__internal_071e400d93528e2c437298a431f996d65ecaffcc096d625e7bedeb14dea0f5cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
";
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb_reduced.html.twig", "users.html.twig", 13)->display($context);
        // line 14
        echo "    <div class=\"col-md-12 main\">
          <h1 class=\"page-header\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Users"), "html", null, true);
        echo "</h1>
          
          <div class=\"table-responsive\">
            <table class=\"table table-hover\">
               <thead>
                <tr>
                   <th>";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Id"), "html", null, true);
        echo "</th>
                   <th>";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fullname"), "html", null, true);
        echo "</th>
                   <th>";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</th>
                   <th>";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Role"), "html", null, true);
        echo "</th>
                   <th></th>
                </tr>
               </thead>  
            <tbody>
                
                
            ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users_list"]) ? $context["users_list"] : $this->getContext($context, "users_list")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 32
            echo "                    
                        <tr style=\"cursor:pointer;\" onclick=\"document.location = '";
            // line 33
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_users_edit", array("user_id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "';\">

                        <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "fullname", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo "</td>
                        <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "role", array()), "html", null, true);
            echo "</td>
                        <td><a href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_users_delete", array("user_id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span></a></td>
                      </tr>
                    
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "    
              
            
            
            </tbody>  
            </table>
            
            <p><a class=\"btn btn-primary btn-lg pull-right\" href=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("_users_create");
        echo "\" role=\"button\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("New User"), "html", null, true);
        echo "</a></p>
   
            
          </div>
          
          
          
          
          
          
          
          
          
          
          
          

          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_071e400d93528e2c437298a431f996d65ecaffcc096d625e7bedeb14dea0f5cd->leave($__internal_071e400d93528e2c437298a431f996d65ecaffcc096d625e7bedeb14dea0f5cd_prof);

    }

    // line 78
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_adfbaf7205378c35eb2c72b7b164c6b6e392468cb07da639f749eb0022cce980 = $this->env->getExtension("native_profiler");
        $__internal_adfbaf7205378c35eb2c72b7b164c6b6e392468cb07da639f749eb0022cce980->enter($__internal_adfbaf7205378c35eb2c72b7b164c6b6e392468cb07da639f749eb0022cce980_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 79
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    
";
        
        $__internal_adfbaf7205378c35eb2c72b7b164c6b6e392468cb07da639f749eb0022cce980->leave($__internal_adfbaf7205378c35eb2c72b7b164c6b6e392468cb07da639f749eb0022cce980_prof);

    }

    public function getTemplateName()
    {
        return "users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 79,  173 => 78,  136 => 49,  127 => 42,  117 => 39,  113 => 38,  109 => 37,  105 => 36,  101 => 35,  96 => 33,  93 => 32,  89 => 31,  79 => 24,  75 => 23,  71 => 22,  67 => 21,  58 => 15,  55 => 14,  53 => 13,  48 => 10,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/* {#        {% include 'sidebar_users.html.twig' %}#}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb_reduced.html.twig'%}*/
/*     <div class="col-md-12 main">*/
/*           <h1 class="page-header">{{"Users"|trans}}</h1>*/
/*           */
/*           <div class="table-responsive">*/
/*             <table class="table table-hover">*/
/*                <thead>*/
/*                 <tr>*/
/*                    <th>{{"Id"|trans}}</th>*/
/*                    <th>{{"Fullname"|trans}}</th>*/
/*                    <th>{{"Email"|trans}}</th>*/
/*                    <th>{{"Role"|trans}}</th>*/
/*                    <th></th>*/
/*                 </tr>*/
/*                </thead>  */
/*             <tbody>*/
/*                 */
/*                 */
/*             {% for user in users_list %}*/
/*                     */
/*                         <tr style="cursor:pointer;" onclick="document.location = '{{path("_users_edit", { 'user_id': user.id })}}';">*/
/* */
/*                         <td>{{user.id}}</td>*/
/*                         <td>{{user.fullname}}</td>*/
/*                         <td>{{user.email}}</td>*/
/*                         <td>{{user.role}}</td>*/
/*                         <td><a href="{{path("_users_delete", { 'user_id': user.id })}}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>*/
/*                       </tr>*/
/*                     */
/*             {% endfor %}    */
/*               */
/*             */
/*             */
/*             </tbody>  */
/*             </table>*/
/*             */
/*             <p><a class="btn btn-primary btn-lg pull-right" href="{{path("_users_create")}}" role="button">{{"New User"|trans}}</a></p>*/
/*    */
/*             */
/*           </div>*/
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/*           */
/* */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/*     */
/* {% endblock %}*/
/* */
