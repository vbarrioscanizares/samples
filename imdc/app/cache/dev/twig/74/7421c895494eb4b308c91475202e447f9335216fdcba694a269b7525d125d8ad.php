<?php

/* clients_bills_edit.html.twig */
class __TwigTemplate_ec580e1d528135dfcdf3bc159aeb58c30119a7189505bd4d320214a75b7d735c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_bills_edit.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d26ec8ee56e34a78308db8a96f259aea61a48c3e8e47290f469545745ba6aa0c = $this->env->getExtension("native_profiler");
        $__internal_d26ec8ee56e34a78308db8a96f259aea61a48c3e8e47290f469545745ba6aa0c->enter($__internal_d26ec8ee56e34a78308db8a96f259aea61a48c3e8e47290f469545745ba6aa0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_bills_edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d26ec8ee56e34a78308db8a96f259aea61a48c3e8e47290f469545745ba6aa0c->leave($__internal_d26ec8ee56e34a78308db8a96f259aea61a48c3e8e47290f469545745ba6aa0c_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_3a67fcd8cb6f0f68e9a0ea2c556e164d9fdf87bdbabec7c0132886f7b7fdec99 = $this->env->getExtension("native_profiler");
        $__internal_3a67fcd8cb6f0f68e9a0ea2c556e164d9fdf87bdbabec7c0132886f7b7fdec99->enter($__internal_3a67fcd8cb6f0f68e9a0ea2c556e164d9fdf87bdbabec7c0132886f7b7fdec99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 9
        $this->loadTemplate("sidebar_clients_v2.html.twig", "clients_bills_edit.html.twig", 9)->display($context);
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_bills_edit.html.twig", 13)->display(array_merge($context, array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))));
        // line 14
        echo "    <div class=\"col-md-9  main\">
        
          <h1 class=\"page-header\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Edit"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("bill"), "html", null, true);
        echo "</h1>
          
          <form  action=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills_edit", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")), "bill_id" => $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getId", array(), "method"))), "html", null, true);
        echo "\" method=\"POST\" enctype=\"multipart/form-data\" >
              <input type=\"hidden\" value=\"1\" name=\"posting\">
              <input type=\"hidden\" value=\"\" name=\"ammount_total\">
              <input type=\"hidden\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getFilename", array(), "method"), "html", null, true);
        echo "\"  name=\"current_file\">
           
           <div class=\"well well-lg\">     
           <div class=\"control-group\">
                  <label class=\"control-label\" for=\"inputDocument\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("File"), "html", null, true);
        echo "</label>
                  
                  <div class=\"controls\">
                    ";
        // line 28
        if (($this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getFilename", array(), "method") != "")) {
            // line 29
            echo "                        <a target=\"_blank\"  href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("upload/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getFilename", array(), "method"), "html", null, true);
            echo "\"><img id=\"logo_img\" class=\"img-thumbnail\" style=\"max-width:150px;\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/pdf-icon.png"), "html", null, true);
            echo "\" alt=\"filename\"/></a>  
                    ";
        }
        // line 30
        echo "  
                    
                    <span class=\"btn btn-file\">
                        ";
        // line 34
        echo "                        <input value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getFilename", array(), "method"), "html", null, true);
        echo "\"  onchange=\"hideLogoImg();\" name=\"bill_file\" type=\"file\" id='inputDocument' accept=\"application/pdf\" ";
        echo "/>
                        
                    </span>
                    
                  </div>  

              </div>   
            </div><!--well-->  
              
           <div class=\"well well-lg\">   
              
              
            <div class=\"form-group\">
              <label for=\"date_from\">";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input name=\"date_from\" value=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getDateFrom", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"date_from\" placeholder=\"\"  >
            </div>
              
              
             <div class=\"form-group\">
              <label for=\"date_to\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <input name=\"date_to\" value=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getDateTo", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"date_to\" placeholder=\"\"  >
            </div>
              
              
             <div class=\"form-group\">
              <label for=\"days_no\">";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days number"), "html", null, true);
        echo "</label>
              <input name=\"days_no\" value=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getDaysNo", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"days_no\" placeholder=\"\"  >
            </div>
              
              
              <div class=\"form-group\">
              <label for=\"kwh\">";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total power consumption (KWh)"), "html", null, true);
        echo "</label>
              <input name=\"kwh\" value=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getKwh", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"kwh\" placeholder=\"\"  >
            </div> 
            
              
            <div class=\"form-group\">
              <label for=\"ammount_kwh\">";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Amount of total consumption (\$)"), "html", null, true);
        echo "</label>
              <input name=\"ammount_kwh\" value=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getAmmountKwh", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"ammount_kwh\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"kw\">";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total power billed (kW)"), "html", null, true);
        echo "</label>
              <input name=\"kw\" value=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getKw", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"kw\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"ammount_kw\">";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Amount billed (\$)"), "html", null, true);
        echo "</label>
              <input name=\"ammount_kw\" value=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getAmmountKw", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"ammount_kw\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"bill_type\">";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Bill type"), "html", null, true);
        echo "</label>
              <select name=\"bill_type\" class=\"form-control\">
                  <option ";
        // line 88
        if (($this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getBillType", array(), "method") == "A")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"A\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Actual"), "html", null, true);
        echo "</option>
                  <option ";
        // line 89
        if (($this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getBillType", array(), "method") == "E")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"E\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estimated"), "html", null, true);
        echo "</option>
              </select>    
             </div> 
              
              
             <div class=\"form-group\">
              <label for=\"tarif_type\">";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rate type"), "html", null, true);
        echo "</label>
              <select name=\"tarif_type\" class=\"form-control\">
                  <option ";
        // line 97
        if (($this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getTarifType", array(), "method") == "M")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"M\">M</option>
                  <option ";
        // line 98
        if (($this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getTarifType", array(), "method") == "G")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"G\">G</option>
                  <option ";
        // line 99
        if (($this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getTarifType", array(), "method") == "D")) {
            echo "selected=\"selected\"";
        }
        echo " value=\"D\">D</option>
              </select>    
             </div>  
              
              
            ";
        // line 109
        echo "  
            <div class=\"form-group\">
              <label for=\"fa\">";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power factor (Fa)"), "html", null, true);
        echo "</label>
              <input name=\"fa\" value=\"";
        // line 112
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getFa", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"fa\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"fu\">";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Utilization Factor (Fu)"), "html", null, true);
        echo "</label>
              <input name=\"fu\" value=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getFu", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"fu\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"kw_min\">";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Minimal power (kW)"), "html", null, true);
        echo "</label>
              <input name=\"kw_min\" value=\"";
        // line 122
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getKwMin", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"kw_min\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"kva\">";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apparent Power (Kva)"), "html", null, true);
        echo "</label>
              <input name=\"kva\" value=\"";
        // line 127
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getKva", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"kva\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"kw_real\">";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Real Power (Kw)"), "html", null, true);
        echo "</label>
              <input name=\"kw_real\" value=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getKwReal", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"kw_real\" placeholder=\"\"  >
            </div> 
              
            </div><!-- well -->  
              
            <br>
            <h1 class=\"page-header\">";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption Cost (\$)"), "html", null, true);
        echo "</h1>
            <div class=\"well well-lg\">
            
            <h3>";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("First X Kws"), "html", null, true);
        echo "</h3>    
                 
            <div class=\"form-group\">
              <label for=\"costA_dateFrom\">";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input name=\"costA_dateFrom\" value=\"";
        // line 145
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostADateFrom", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costA_dateFrom\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costA_dateTo\">";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <input name=\"costA_dateTo\" value=\"";
        // line 150
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostADateTo", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costA_dateTo\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costA_Kw\">";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption"), "html", null, true);
        echo " (Kw)</label>
              <input name=\"costA_Kw\" value=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostAKw", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costA_Kw\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costA_Ammount\">";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption amount"), "html", null, true);
        echo " (\$)</label>
              <input name=\"costA_Ammount\" value=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostAAmmount", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costA_Ammount\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costA_division_days\">";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days number"), "html", null, true);
        echo "</label>
              <input name=\"costA_division_days\" value=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostADivisionDays", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costA_division_days\" placeholder=\"\"  >
            </div>
              
            <div class=\"form-group\">
              <label for=\"costA_total_ammount\">";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total amount"), "html", null, true);
        echo " (\$)</label>
              <input name=\"costA_total_ammount\" value=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostATotalAmmount", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costA_total_ammount\" placeholder=\"\"  >
            </div>
              
            <hr>
            
             <h3>";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rest of Kws"), "html", null, true);
        echo "</h3>  
            
             <div class=\"form-group\">
              <label for=\"costB_dateFrom\">";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input name=\"costB_dateFrom\" value=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostBDateFrom", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costB_dateFrom\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costB_dateTo\">";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <input name=\"costB_dateTo\" value=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostBDateTo", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costB_dateTo\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costB_Kw\">";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption"), "html", null, true);
        echo " (Kw)</label>
              <input name=\"costB_Kw\" value=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostBKw", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costB_Kw\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costB_Ammount\">";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption amount"), "html", null, true);
        echo " (\$)</label>
              <input name=\"costB_Ammount\" value=\"";
        // line 194
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostBAmmount", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costB_Ammount\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costB_division_days\">";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days number"), "html", null, true);
        echo "</label>
              <input name=\"costB_division_days\" value=\"";
        // line 199
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostBDivisionDays", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costB_division_days\" placeholder=\"\"  >
            </div>
              
            <div class=\"form-group\">
              <label for=\"costB_total_ammount\">";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total amount"), "html", null, true);
        echo " (\$)</label>
              <input name=\"costB_total_ammount\" value=\"";
        // line 204
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostBTotalAmmount", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costB_total_ammount\" placeholder=\"\"  >
            </div>
            
            <hr>
            
            <div class=\"form-group\">
              <label for=\"costApluscostB\">";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Consumption Amount"), "html", null, true);
        echo " (\$)</label>
              <input name=\"costApluscostB\" value=\"";
        // line 211
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getCostAPlusCostB", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"costApluscostB\" placeholder=\"\"  >
            </div> 
              
              
              
              
              
              
              
            </div> <!-- well -->
            
            <br>
            <h1 class=\"page-header\">";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total consumption"), "html", null, true);
        echo "</h1>
            <div class=\"well well-lg\">
                 
                 
             <h3>";
        // line 227
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("First X Kwhs"), "html", null, true);
        echo "</h3>    
                 
            <div class=\"form-group\">
              <label for=\"consf_dateFrom\">";
        // line 230
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input name=\"consf_dateFrom\" value=\"";
        // line 231
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsfDateFrom", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consf_dateFrom\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consf_dateTo\">";
        // line 235
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <input name=\"consf_dateTo\" value=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsfDateTo", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consf_dateTo\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consf_Kwh\">";
        // line 240
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption KWh"), "html", null, true);
        echo "</label>
              <input name=\"consf_Kwh\" value=\"";
        // line 241
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsfKwh", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consf_Kwh\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consf_ammount\">";
        // line 245
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption amount"), "html", null, true);
        echo " (\$)</label>
              <input name=\"consf_ammount\" value=\"";
        // line 246
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsfAmmount", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consf_ammount\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consf_total_ammount\">";
        // line 250
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total amount"), "html", null, true);
        echo " (\$)</label>
              <input name=\"consf_total_ammount\" value=\"";
        // line 251
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsfTotalAmmount", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consf_total_ammount\" placeholder=\"\"  >
            </div>
              
            
              
            <hr>
            
             <h3>";
        // line 258
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rest of Kwhs"), "html", null, true);
        echo "</h3>  
            
             <div class=\"form-group\">
              <label for=\"consr_dateFrom\">";
        // line 261
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input name=\"consr_dateFrom\" value=\"";
        // line 262
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsrDateFrom", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consr_dateFrom\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consr_dateTo\">";
        // line 266
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <input name=\"consr_dateTo\" value=\"";
        // line 267
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsrDateTo", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consr_dateTo\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consr_Kwh\">";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption KWh"), "html", null, true);
        echo "</label>
              <input name=\"consr_Kwh\" value=\"";
        // line 272
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsrKwh", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consr_Kwh\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consr_ammount\">";
        // line 276
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption amount"), "html", null, true);
        echo " (\$)</label>
              <input name=\"consr_ammount\" value=\"";
        // line 277
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsrAmmount", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consr_ammount\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consr_total_ammount\">";
        // line 281
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total amount"), "html", null, true);
        echo " (\$)</label>
              <input name=\"consr_total_ammount\" value=\"";
        // line 282
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsrTotalAmmount", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consr_total_ammount\" placeholder=\"\"  >
            </div>
            
            <hr>
            
            <div class=\"form-group\">
              <label for=\"consf_consr_visilec_ammount\">";
        // line 288
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Service Visilec"), "html", null, true);
        echo " (\$)</label>
              <input name=\"consf_consr_visilec_ammount\" value=\"";
        // line 289
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsfConsrVisilecAmmount", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consf_consr_visilec_ammount\" placeholder=\"\"  >
            </div>
            
            
            <div class=\"form-group\">
              <label for=\"consf_plus_consr\">";
        // line 294
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Consumption Amount"), "html", null, true);
        echo " (\$)</label>
              <input name=\"consf_plus_consr\" value=\"";
        // line 295
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["bill"]) ? $context["bill"] : $this->getContext($context, "bill")), "getConsfPlusConsr", array(), "method"), "html", null, true);
        echo "\" type=\"text\" class=\"form-control\" id=\"consf_plus_consr\" placeholder=\"\"  >
            </div> 
                
                
                
                
                
            </div> <!-- well -->
              
            <a href=\"";
        // line 304
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
        echo "\" class=\"btn btn-default btn-lg pull-right \">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cancel"), "html", null, true);
        echo "</a>
           
            <input type=\"submit\" class=\"btn btn-primary btn-lg pull-right \" value=\"";
        // line 306
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\"/>
          
            </div>
          </form>
          
          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_3a67fcd8cb6f0f68e9a0ea2c556e164d9fdf87bdbabec7c0132886f7b7fdec99->leave($__internal_3a67fcd8cb6f0f68e9a0ea2c556e164d9fdf87bdbabec7c0132886f7b7fdec99_prof);

    }

    // line 323
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_27d6db6dbc0652df287505b7dc7436fd069dfa97add0aa59336132d0fedd2cec = $this->env->getExtension("native_profiler");
        $__internal_27d6db6dbc0652df287505b7dc7436fd069dfa97add0aa59336132d0fedd2cec->enter($__internal_27d6db6dbc0652df287505b7dc7436fd069dfa97add0aa59336132d0fedd2cec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 324
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"http://code.jquery.com/ui/1.9.1/themes/cupertino/jquery-ui.css\">
<link rel=\"stylesheet\" href=\"";
        // line 326
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/timepicker/jquery-ui-timepicker-addon.css"), "html", null, true);
        echo "\">     
";
        
        $__internal_27d6db6dbc0652df287505b7dc7436fd069dfa97add0aa59336132d0fedd2cec->leave($__internal_27d6db6dbc0652df287505b7dc7436fd069dfa97add0aa59336132d0fedd2cec_prof);

    }

    // line 332
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_2e03cac3811238e1afeb03d980439ddf09c34ee82c277868c38be8ccd66fedc4 = $this->env->getExtension("native_profiler");
        $__internal_2e03cac3811238e1afeb03d980439ddf09c34ee82c277868c38be8ccd66fedc4->enter($__internal_2e03cac3811238e1afeb03d980439ddf09c34ee82c277868c38be8ccd66fedc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 333
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js\"></script>

<script src=\"//code.jquery.com/ui/1.11.4/jquery-ui.js\"></script>
<script src=\"";
        // line 337
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/timepicker/jquery-ui-timepicker-addon.js"), "html", null, true);
        echo "\"></script>

<script> \$.validate(); </script>

<script>
    function hideLogoImg(){
     \$(\"#logo_img\").hide();
 }           
      
    
</script>    

<script>
 \$(document).ready(function() {         
        
                    \$('#date_from').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#date_to\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });    
                    \$('#date_to').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#date_from\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });
                    \$('#costA_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costA_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });   
                    \$('#costA_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costA_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });   
                    \$('#costB_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costB_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });      
                    \$('#costB_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costB_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });  
                    \$('#consf_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consf_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });   
                    \$('#consf_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consf_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });
                    \$('#consr_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consr_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });   
                    \$('#consr_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consr_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        }); 
                    
            });
            

              
        
</script>  


";
        
        $__internal_2e03cac3811238e1afeb03d980439ddf09c34ee82c277868c38be8ccd66fedc4->leave($__internal_2e03cac3811238e1afeb03d980439ddf09c34ee82c277868c38be8ccd66fedc4_prof);

    }

    public function getTemplateName()
    {
        return "clients_bills_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  734 => 337,  727 => 333,  721 => 332,  712 => 326,  707 => 324,  701 => 323,  678 => 306,  671 => 304,  659 => 295,  655 => 294,  647 => 289,  643 => 288,  634 => 282,  630 => 281,  623 => 277,  619 => 276,  612 => 272,  608 => 271,  601 => 267,  595 => 266,  588 => 262,  582 => 261,  576 => 258,  566 => 251,  562 => 250,  555 => 246,  551 => 245,  544 => 241,  540 => 240,  533 => 236,  527 => 235,  520 => 231,  514 => 230,  508 => 227,  501 => 223,  486 => 211,  482 => 210,  473 => 204,  469 => 203,  462 => 199,  458 => 198,  451 => 194,  447 => 193,  440 => 189,  436 => 188,  429 => 184,  423 => 183,  416 => 179,  410 => 178,  404 => 175,  396 => 170,  392 => 169,  385 => 165,  381 => 164,  374 => 160,  370 => 159,  363 => 155,  359 => 154,  352 => 150,  346 => 149,  339 => 145,  333 => 144,  327 => 141,  321 => 138,  312 => 132,  308 => 131,  301 => 127,  297 => 126,  290 => 122,  286 => 121,  279 => 117,  275 => 116,  268 => 112,  264 => 111,  260 => 109,  250 => 99,  244 => 98,  238 => 97,  233 => 95,  220 => 89,  212 => 88,  207 => 86,  200 => 82,  196 => 81,  189 => 77,  185 => 76,  178 => 72,  174 => 71,  166 => 66,  162 => 65,  154 => 60,  150 => 59,  142 => 54,  136 => 53,  128 => 48,  122 => 47,  104 => 34,  99 => 30,  90 => 29,  88 => 28,  82 => 25,  75 => 21,  69 => 18,  62 => 16,  58 => 14,  56 => 13,  51 => 10,  49 => 9,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients_v2.html.twig' %}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb.html.twig' with {'client_id': client_id}%}*/
/*     <div class="col-md-9  main">*/
/*         */
/*           <h1 class="page-header">{{"Edit"|trans}} {{"bill"|trans}}</h1>*/
/*           */
/*           <form  action="{{path('_clients_bills_edit',{ 'client_id': client_id, 'bill_id':bill.getId()})}}" method="POST" enctype="multipart/form-data" >*/
/*               <input type="hidden" value="1" name="posting">*/
/*               <input type="hidden" value="" name="ammount_total">*/
/*               <input type="hidden" value="{{bill.getFilename()}}"  name="current_file">*/
/*            */
/*            <div class="well well-lg">     */
/*            <div class="control-group">*/
/*                   <label class="control-label" for="inputDocument">{{"File"|trans}}</label>*/
/*                   */
/*                   <div class="controls">*/
/*                     {%if bill.getFilename()!="" %}*/
/*                         <a target="_blank"  href="{{asset('upload/')}}{{bill.getFilename()}}"><img id="logo_img" class="img-thumbnail" style="max-width:150px;" src="{{asset('img/pdf-icon.png')}}" alt="filename"/></a>  */
/*                     {%endif%}  */
/*                     */
/*                     <span class="btn btn-file">*/
/*                         {#<input type="hidden" name="MAX_FILE_SIZE" value="90000" />#}*/
/*                         <input value="{{bill.getFilename()}}"  onchange="hideLogoImg();" name="bill_file" type="file" id='inputDocument' accept="application/pdf" {#<?=  (isset($query))?'value="default_upload"':'';?>  #}/>*/
/*                         */
/*                     </span>*/
/*                     */
/*                   </div>  */
/* */
/*               </div>   */
/*             </div><!--well-->  */
/*               */
/*            <div class="well well-lg">   */
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="date_from">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="date_from" value="{{bill.getDateFrom()}}" type="text" class="form-control" id="date_from" placeholder=""  >*/
/*             </div>*/
/*               */
/*               */
/*              <div class="form-group">*/
/*               <label for="date_to">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="date_to" value="{{bill.getDateTo()}}" type="text" class="form-control" id="date_to" placeholder=""  >*/
/*             </div>*/
/*               */
/*               */
/*              <div class="form-group">*/
/*               <label for="days_no">{{"Days number"|trans}}</label>*/
/*               <input name="days_no" value="{{bill.getDaysNo()}}" type="text" class="form-control" id="days_no" placeholder=""  >*/
/*             </div>*/
/*               */
/*               */
/*               <div class="form-group">*/
/*               <label for="kwh">{{"Total power consumption (KWh)"|trans}}</label>*/
/*               <input name="kwh" value="{{bill.getKwh()}}" type="text" class="form-control" id="kwh" placeholder=""  >*/
/*             </div> */
/*             */
/*               */
/*             <div class="form-group">*/
/*               <label for="ammount_kwh">{{"Amount of total consumption ($)"|trans}}</label>*/
/*               <input name="ammount_kwh" value="{{bill.getAmmountKwh()}}" type="text" class="form-control" id="ammount_kwh" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="kw">{{"Total power billed (kW)"|trans}}</label>*/
/*               <input name="kw" value="{{bill.getKw()}}" type="text" class="form-control" id="kw" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="ammount_kw">{{"Amount billed ($)"|trans}}</label>*/
/*               <input name="ammount_kw" value="{{bill.getAmmountKw()}}" type="text" class="form-control" id="ammount_kw" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="bill_type">{{"Bill type"|trans}}</label>*/
/*               <select name="bill_type" class="form-control">*/
/*                   <option {%if bill.getBillType()=="A" %}selected="selected"{%endif%} value="A">{{"Actual"|trans}}</option>*/
/*                   <option {%if bill.getBillType()=="E" %}selected="selected"{%endif%} value="E">{{"Estimated"|trans}}</option>*/
/*               </select>    */
/*              </div> */
/*               */
/*               */
/*              <div class="form-group">*/
/*               <label for="tarif_type">{{"Rate type"|trans}}</label>*/
/*               <select name="tarif_type" class="form-control">*/
/*                   <option {%if bill.getTarifType()=="M" %}selected="selected"{%endif%} value="M">M</option>*/
/*                   <option {%if bill.getTarifType()=="G" %}selected="selected"{%endif%} value="G">G</option>*/
/*                   <option {%if bill.getTarifType()=="D" %}selected="selected"{%endif%} value="D">D</option>*/
/*               </select>    */
/*              </div>  */
/*               */
/*               */
/*             {#  */
/*             <div class="form-group">*/
/*               <label for="dollar">{{"Amount billed ($)"|trans}} maybe double?</label>*/
/*               <input name="dollar" value="{{bill.get()}}" type="text" class="form-control" id="dollar" placeholder=""  >*/
/*             </div> */
/*             #}  */
/*             <div class="form-group">*/
/*               <label for="fa">{{"Power factor (Fa)"|trans}}</label>*/
/*               <input name="fa" value="{{bill.getFa()}}" type="text" class="form-control" id="fa" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="fu">{{"Utilization Factor (Fu)"|trans}}</label>*/
/*               <input name="fu" value="{{bill.getFu()}}" type="text" class="form-control" id="fu" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="kw_min">{{"Minimal power (kW)"|trans}}</label>*/
/*               <input name="kw_min" value="{{bill.getKwMin()}}" type="text" class="form-control" id="kw_min" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="kva">{{"Apparent Power (Kva)"|trans}}</label>*/
/*               <input name="kva" value="{{bill.getKva()}}" type="text" class="form-control" id="kva" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="kw_real">{{"Real Power (Kw)"|trans}}</label>*/
/*               <input name="kw_real" value="{{bill.getKwReal()}}" type="text" class="form-control" id="kw_real" placeholder=""  >*/
/*             </div> */
/*               */
/*             </div><!-- well -->  */
/*               */
/*             <br>*/
/*             <h1 class="page-header">{{"Consumption Cost ($)"|trans}}</h1>*/
/*             <div class="well well-lg">*/
/*             */
/*             <h3>{{"First X Kws"|trans}}</h3>    */
/*                  */
/*             <div class="form-group">*/
/*               <label for="costA_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="costA_dateFrom" value="{{bill.getCostADateFrom()}}" type="text" class="form-control" id="costA_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="costA_dateTo" value="{{bill.getCostADateTo()}}" type="text" class="form-control" id="costA_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_Kw">{{"Consumption"|trans}} (Kw)</label>*/
/*               <input name="costA_Kw" value="{{bill.getCostAKw()}}" type="text" class="form-control" id="costA_Kw" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_Ammount">{{"Consumption amount"|trans}} ($)</label>*/
/*               <input name="costA_Ammount" value="{{bill.getCostAAmmount()}}" type="text" class="form-control" id="costA_Ammount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_division_days">{{"Days number"|trans}}</label>*/
/*               <input name="costA_division_days" value="{{bill.getCostADivisionDays()}}" type="text" class="form-control" id="costA_division_days" placeholder=""  >*/
/*             </div>*/
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_total_ammount">{{"Total amount"|trans}} ($)</label>*/
/*               <input name="costA_total_ammount" value="{{bill.getCostATotalAmmount()}}" type="text" class="form-control" id="costA_total_ammount" placeholder=""  >*/
/*             </div>*/
/*               */
/*             <hr>*/
/*             */
/*              <h3>{{"Rest of Kws"|trans}}</h3>  */
/*             */
/*              <div class="form-group">*/
/*               <label for="costB_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="costB_dateFrom" value="{{bill.getCostBDateFrom()}}" type="text" class="form-control" id="costB_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="costB_dateTo" value="{{bill.getCostBDateTo()}}" type="text" class="form-control" id="costB_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_Kw">{{"Consumption"|trans}} (Kw)</label>*/
/*               <input name="costB_Kw" value="{{bill.getCostBKw()}}" type="text" class="form-control" id="costB_Kw" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_Ammount">{{"Consumption amount"|trans}} ($)</label>*/
/*               <input name="costB_Ammount" value="{{bill.getCostBAmmount()}}" type="text" class="form-control" id="costB_Ammount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_division_days">{{"Days number"|trans}}</label>*/
/*               <input name="costB_division_days" value="{{bill.getCostBDivisionDays()}}" type="text" class="form-control" id="costB_division_days" placeholder=""  >*/
/*             </div>*/
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_total_ammount">{{"Total amount"|trans}} ($)</label>*/
/*               <input name="costB_total_ammount" value="{{bill.getCostBTotalAmmount()}}" type="text" class="form-control" id="costB_total_ammount" placeholder=""  >*/
/*             </div>*/
/*             */
/*             <hr>*/
/*             */
/*             <div class="form-group">*/
/*               <label for="costApluscostB">{{"Total Consumption Amount"|trans}} ($)</label>*/
/*               <input name="costApluscostB" value="{{bill.getCostAPlusCostB()}}" type="text" class="form-control" id="costApluscostB" placeholder=""  >*/
/*             </div> */
/*               */
/*               */
/*               */
/*               */
/*               */
/*               */
/*               */
/*             </div> <!-- well -->*/
/*             */
/*             <br>*/
/*             <h1 class="page-header">{{"Total consumption"|trans}}</h1>*/
/*             <div class="well well-lg">*/
/*                  */
/*                  */
/*              <h3>{{"First X Kwhs"|trans}}</h3>    */
/*                  */
/*             <div class="form-group">*/
/*               <label for="consf_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="consf_dateFrom" value="{{bill.getConsfDateFrom()}}" type="text" class="form-control" id="consf_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="consf_dateTo" value="{{bill.getConsfDateTo()}}" type="text" class="form-control" id="consf_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_Kwh">{{"Consumption KWh"|trans}}</label>*/
/*               <input name="consf_Kwh" value="{{bill.getConsfKwh()}}" type="text" class="form-control" id="consf_Kwh" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_ammount">{{"Consumption amount"|trans}} ($)</label>*/
/*               <input name="consf_ammount" value="{{bill.getConsfAmmount()}}" type="text" class="form-control" id="consf_ammount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_total_ammount">{{"Total amount"|trans}} ($)</label>*/
/*               <input name="consf_total_ammount" value="{{bill.getConsfTotalAmmount()}}" type="text" class="form-control" id="consf_total_ammount" placeholder=""  >*/
/*             </div>*/
/*               */
/*             */
/*               */
/*             <hr>*/
/*             */
/*              <h3>{{"Rest of Kwhs"|trans}}</h3>  */
/*             */
/*              <div class="form-group">*/
/*               <label for="consr_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="consr_dateFrom" value="{{bill.getConsrDateFrom()}}" type="text" class="form-control" id="consr_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="consr_dateTo" value="{{bill.getConsrDateTo()}}" type="text" class="form-control" id="consr_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_Kwh">{{"Consumption KWh"|trans}}</label>*/
/*               <input name="consr_Kwh" value="{{bill.getConsrKwh()}}" type="text" class="form-control" id="consr_Kwh" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_ammount">{{"Consumption amount"|trans}} ($)</label>*/
/*               <input name="consr_ammount" value="{{bill.getConsrAmmount()}}" type="text" class="form-control" id="consr_ammount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_total_ammount">{{"Total amount"|trans}} ($)</label>*/
/*               <input name="consr_total_ammount" value="{{bill.getConsrTotalAmmount()}}" type="text" class="form-control" id="consr_total_ammount" placeholder=""  >*/
/*             </div>*/
/*             */
/*             <hr>*/
/*             */
/*             <div class="form-group">*/
/*               <label for="consf_consr_visilec_ammount">{{"Service Visilec"|trans}} ($)</label>*/
/*               <input name="consf_consr_visilec_ammount" value="{{bill.getConsfConsrVisilecAmmount()}}" type="text" class="form-control" id="consf_consr_visilec_ammount" placeholder=""  >*/
/*             </div>*/
/*             */
/*             */
/*             <div class="form-group">*/
/*               <label for="consf_plus_consr">{{"Total Consumption Amount"|trans}} ($)</label>*/
/*               <input name="consf_plus_consr" value="{{bill.getConsfPlusConsr()}}" type="text" class="form-control" id="consf_plus_consr" placeholder=""  >*/
/*             </div> */
/*                 */
/*                 */
/*                 */
/*                 */
/*                 */
/*             </div> <!-- well -->*/
/*               */
/*             <a href="{{path("_clients_bills", { 'client_id': client_id })}}" class="btn btn-default btn-lg pull-right ">{{"Cancel"|trans}}</a>*/
/*            */
/*             <input type="submit" class="btn btn-primary btn-lg pull-right " value="{{"Submit"|trans}}"/>*/
/*           */
/*             </div>*/
/*           </form>*/
/*           */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/* <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/cupertino/jquery-ui.css">*/
/* <link rel="stylesheet" href="{{ asset('libs/timepicker/jquery-ui-timepicker-addon.css')}}">     */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* {% block javascripts %}*/
/* {{parent()}}*/
/* <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>*/
/* */
/* <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>*/
/* <script src="{{ asset('libs/timepicker/jquery-ui-timepicker-addon.js')}}"></script>*/
/* */
/* <script> $.validate(); </script>*/
/* */
/* <script>*/
/*     function hideLogoImg(){*/
/*      $("#logo_img").hide();*/
/*  }           */
/*       */
/*     */
/* </script>    */
/* */
/* <script>*/
/*  $(document).ready(function() {         */
/*         */
/*                     $('#date_from').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#date_to" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });    */
/*                     $('#date_to').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#date_from" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });*/
/*                     $('#costA_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costA_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#costA_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costA_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#costB_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costB_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });      */
/*                     $('#costB_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costB_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });  */
/*                     $('#consf_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consf_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#consf_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consf_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });*/
/*                     $('#consr_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consr_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#consr_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consr_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         }); */
/*                     */
/*             });*/
/*             */
/* */
/*               */
/*         */
/* </script>  */
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
