<?php

/* clients_presets.html.twig */
class __TwigTemplate_dbf6ac4cf44486f82890346a67c427cd8709358ed71a7f32337448e39f3215f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_presets.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_29d1a630f80a4feae3c1bae6cc624bce841971e3f0eb043a83d942cf65279952 = $this->env->getExtension("native_profiler");
        $__internal_29d1a630f80a4feae3c1bae6cc624bce841971e3f0eb043a83d942cf65279952->enter($__internal_29d1a630f80a4feae3c1bae6cc624bce841971e3f0eb043a83d942cf65279952_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_presets.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_29d1a630f80a4feae3c1bae6cc624bce841971e3f0eb043a83d942cf65279952->leave($__internal_29d1a630f80a4feae3c1bae6cc624bce841971e3f0eb043a83d942cf65279952_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_7a96775fc30215caa4f096776a32db0a3a9d1dc7ac55ce7530f689e0b843c12a = $this->env->getExtension("native_profiler");
        $__internal_7a96775fc30215caa4f096776a32db0a3a9d1dc7ac55ce7530f689e0b843c12a->enter($__internal_7a96775fc30215caa4f096776a32db0a3a9d1dc7ac55ce7530f689e0b843c12a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 9
        $this->loadTemplate("sidebar_clients_v2.html.twig", "clients_presets.html.twig", 9)->display($context);
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_presets.html.twig", 13)->display(array_merge($context, array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()))));
        echo " 
    <div class=\"col-md-9  main\">
         
        
        <h1 class=\"page-header\">";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Presets"), "html", null, true);
        echo "</h1>
        
        
        
        
          <form  action=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_presets", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()))), "html", null, true);
        echo "\" method=\"POST\" enctype=\"multipart/form-data\">
               <input type=\"hidden\" name=\"sended\" value=\"1\"/>
              
              
               <h3>";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Presets"), "html", null, true);
        echo " <!-- <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" class=\"glyphicon glyphicon-info-sign\"></span>--></h3>
               <input value=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getPresetSystem", array(), "method"), "html", null, true);
        echo "\" type=\"hidden\" name=\"preset\" id=\"preset\"/>
               
               <div class=\"well well-lg form-inline\">
                  
                   
                   
                   <h4>";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("System"), "html", null, true);
        echo " <a href=\"javascript:void(0);\" onclick=\"addPresets()\">
                                                    <span class=\" glyphicon glyphicon-plus\"></span>
                                                </a>
                    </h4>
                                                    
                                                    
                  <div class=\"form-group\">
                    <label  for=\"time_on\">";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Time ON"), "html", null, true);
        echo "</label>
                    <input value=\"\"  id=\"system_time_on\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  
                  <div class=\"form-group\">
                    <label for=\"time_off\">";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Time OFF"), "html", null, true);
        echo "</label>
                    <input value=\"\" id=\"system_time_off\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  
                  <div class=\"form-group\">
                    <label  for=\"system_preset\">";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Preset"), "html", null, true);
        echo "&nbsp;<span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Maximum consumption allowed. Just numbers allowed."), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></label>
                    <input value=\"\" id=\"system_preset\"   type=\"text\" class=\"form-control\"   placeholder=\"Kw\">
                  </div>
                  
                    
                   <div class=\"form-group\">
                    <label  for=\"system_preset\">";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days"), "html", null, true);
        echo "&nbsp;</label>
                    <input id=\"system_sunday\" checked=\"checked\"  type=\"checkbox\" name=\"system_days[]\" value=\"Sunday\" />";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Sunday"), "html", null, true);
        echo "
                    <input id=\"system_monday\" checked=\"checked\"  type=\"checkbox\" name=\"system_days[]\" value=\"Monday\" />";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Monday"), "html", null, true);
        echo "
                    <input id=\"system_tuesday\" checked=\"checked\"  type=\"checkbox\" name=\"system_days[]\" value=\"Tuesday\" />";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tuesday"), "html", null, true);
        echo "
                    <input id=\"system_wednesday\" checked=\"checked\"  type=\"checkbox\" name=\"system_days[]\" value=\"Wednesday\" />";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wednesday"), "html", null, true);
        echo "
                    <input id=\"system_thursday\" checked=\"checked\"  type=\"checkbox\" name=\"system_days[]\" value=\"Thursday\" />";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Thursday"), "html", null, true);
        echo "
                    <input id=\"system_friday\" checked=\"checked\"  type=\"checkbox\" name=\"system_days[]\" value=\"Friday\" />";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Friday"), "html", null, true);
        echo "
                    <input id=\"system_saturday\" checked=\"checked\"  type=\"checkbox\" name=\"system_days[]\" value=\"Saturday\" />";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Saturday"), "html", null, true);
        echo "
                  </div>
                    
                    
                
                  <p></p>
            
                  <div id=\"system_preset_list\" class=\"list-group\">
                    ";
        // line 74
        echo "                  </div>
                  <p></p>
                   
                  
                  
                  <h4>";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("First Entry"), "html", null, true);
        echo " <a href=\"javascript:void(0);\" onclick=\"addPresetsE1('e1');\">
                                                    <span class=\" glyphicon glyphicon-plus\"></span>
                                                </a>
                    </h4>
                                                    
                  <input value=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getPresetEntry1", array(), "method"), "html", null, true);
        echo "\" type=\"hidden\" name=\"e1_preset\" id=\"e1_preset\"/>                                  
                  <div class=\"form-group\">
                    <label  for=\"e1_time_on\">";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Time ON"), "html", null, true);
        echo "</label>
                    <input value=\"\"  id=\"e1_time_on\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  
                  <div class=\"form-group\">
                    <label for=\"e1_time_off\">";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Time OFF"), "html", null, true);
        echo "</label>
                    <input value=\"\" id=\"e1_time_off\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  
                  <div class=\"form-group\">
                    <label  for=\"e1_percent\">";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percent"), "html", null, true);
        echo "&nbsp;<span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percent. Just numbers allowed."), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></label>
                    <input value=\"\" id=\"e1_percent\"   type=\"text\" class=\"form-control\"   placeholder=\"%\">
                  </div>
                  
                    
                 <div class=\"form-group\">
                    <label  for=\"system_preset\">";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days"), "html", null, true);
        echo "&nbsp;</label>
                    <input id=\"e1_sunday\" checked=\"checked\"  type=\"checkbox\" name=\"e1_days[]\" value=\"Sunday\" />";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Sunday"), "html", null, true);
        echo "
                    <input id=\"e1_monday\" checked=\"checked\"  type=\"checkbox\" name=\"e1_days[]\" value=\"Monday\" />";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Monday"), "html", null, true);
        echo "
                    <input id=\"e1_tuesday\" checked=\"checked\"  type=\"checkbox\" name=\"e1_days[]\" value=\"Tuesday\" />";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tuesday"), "html", null, true);
        echo "
                    <input id=\"e1_wednesday\" checked=\"checked\"  type=\"checkbox\" name=\"e1_days[]\" value=\"Wednesday\" />";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wednesday"), "html", null, true);
        echo "
                    <input id=\"e1_thursday\" checked=\"checked\"  type=\"checkbox\" name=\"e1_days[]\" value=\"Thursday\" />";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Thursday"), "html", null, true);
        echo "
                    <input id=\"e1_friday\" checked=\"checked\"  type=\"checkbox\" name=\"e1_days[]\" value=\"Friday\" />";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Friday"), "html", null, true);
        echo "
                    <input id=\"e1_saturday\" checked=\"checked\"  type=\"checkbox\" name=\"e1_days[]\" value=\"Saturday\" />";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Saturday"), "html", null, true);
        echo "
                  </div>
                
                  <p></p>
            
                  <div id=\"e1_preset_list\" class=\"list-group\">
                    ";
        // line 118
        echo "                  </div>
                  <p></p>
                  
                  
                  
                  
                  
                  
                  
                   <h4>";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Second Entry"), "html", null, true);
        echo " <a href=\"javascript:void(0);\" onclick=\"addPresetsE1('e2');\">
                                                    <span class=\" glyphicon glyphicon-plus\"></span>
                                                </a>
                    </h4>
                                                    
                  <input value=\"";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getPresetEntry2", array(), "method"), "html", null, true);
        echo "\" type=\"hidden\" name=\"e2_preset\" id=\"e2_preset\"/>                                  
                  <div class=\"form-group\">
                    <label  for=\"e2_time_on\">";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Time ON"), "html", null, true);
        echo "</label>
                    <input value=\"\"  id=\"e2_time_on\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  
                  <div class=\"form-group\">
                    <label for=\"e2_time_off\">";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Time OFF"), "html", null, true);
        echo "</label>
                    <input value=\"\" id=\"e2_time_off\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  
                  <div class=\"form-group\">
                    <label  for=\"e2_percent\">";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percent"), "html", null, true);
        echo "&nbsp;<span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percent. Just numbers allowed."), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></label>
                    <input value=\"\" id=\"e2_percent\"   type=\"text\" class=\"form-control\"   placeholder=\"%\">
                  </div>
                  
                  <div class=\"form-group\">
                    <label  for=\"system_preset\">";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days"), "html", null, true);
        echo "&nbsp;</label>
                    <input id=\"e2_sunday\" checked=\"checked\"  type=\"checkbox\" name=\"e2_days[]\" value=\"Sunday\" />";
        // line 150
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Sunday"), "html", null, true);
        echo "
                    <input id=\"e2_monday\" checked=\"checked\"  type=\"checkbox\" name=\"e2_days[]\" value=\"Monday\" />";
        // line 151
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Monday"), "html", null, true);
        echo "
                    <input id=\"e2_tuesday\" checked=\"checked\"  type=\"checkbox\" name=\"e2_days[]\" value=\"Tuesday\" />";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tuesday"), "html", null, true);
        echo "
                    <input id=\"e2_wednesday\" checked=\"checked\"  type=\"checkbox\" name=\"e2_days[]\" value=\"Wednesday\" />";
        // line 153
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wednesday"), "html", null, true);
        echo "
                    <input id=\"e2_thursday\" checked=\"checked\"  type=\"checkbox\" name=\"e2_days[]\" value=\"Thursday\" />";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Thursday"), "html", null, true);
        echo "
                    <input id=\"e2_friday\" checked=\"checked\"  type=\"checkbox\" name=\"e2_days[]\" value=\"Friday\" />";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Friday"), "html", null, true);
        echo "
                    <input id=\"e2_saturday\" checked=\"checked\"  type=\"checkbox\" name=\"e2_days[]\" value=\"Saturday\" />";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Saturday"), "html", null, true);
        echo "
                  </div>      
                  <p></p>
            
                  <div id=\"e2_preset_list\" class=\"list-group\">
                    ";
        // line 164
        echo "                  </div>
                  <p></p>
                  
                  
                  
                  
                  <h4>";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Third Entry"), "html", null, true);
        echo " <a href=\"javascript:void(0);\" onclick=\"addPresetsE1('e3');\">
                                                    <span class=\" glyphicon glyphicon-plus\"></span>
                                                </a>
                    </h4>
                                                    
                  <input value=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getPresetEntry3", array(), "method"), "html", null, true);
        echo "\" type=\"hidden\" name=\"e3_preset\" id=\"e3_preset\"/>                                  
                  <div class=\"form-group\">
                    <label  for=\"e3_time_on\">";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Time ON"), "html", null, true);
        echo "</label>
                    <input value=\"\"  id=\"e3_time_on\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  
                  <div class=\"form-group\">
                    <label for=\"e3_time_off\">";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Time OFF"), "html", null, true);
        echo "</label>
                    <input value=\"\" id=\"e3_time_off\"   type=\"text\" class=\"form-control\"   placeholder=\"\">
                  </div>
                  
                  <div class=\"form-group\">
                    <label  for=\"e3_percent\">";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percent"), "html", null, true);
        echo "&nbsp;<span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percent. Just numbers allowed."), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></label>
                    <input value=\"\" id=\"e3_percent\"   type=\"text\" class=\"form-control\"   placeholder=\"%\">
                  </div>
                  
                    
                  <div class=\"form-group\">
                    <label  for=\"system_preset\">";
        // line 193
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days"), "html", null, true);
        echo "&nbsp;</label>
                    <input id=\"e3_sunday\" checked=\"checked\"  type=\"checkbox\" name=\"e3_days[]\" value=\"Sunday\" />";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Sunday"), "html", null, true);
        echo "
                    <input id=\"e3_monday\" checked=\"checked\"  type=\"checkbox\" name=\"e3_days[]\" value=\"Monday\" />";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Monday"), "html", null, true);
        echo "
                    <input id=\"e3_tuesday\" checked=\"checked\"  type=\"checkbox\" name=\"e3_days[]\" value=\"Tuesday\" />";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tuesday"), "html", null, true);
        echo "
                    <input id=\"e3_wednesday\" checked=\"checked\"  type=\"checkbox\" name=\"e3_days[]\" value=\"Wednesday\" />";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wednesday"), "html", null, true);
        echo "
                    <input id=\"e3_thursday\" checked=\"checked\"  type=\"checkbox\" name=\"e3_days[]\" value=\"Thursday\" />";
        // line 198
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Thursday"), "html", null, true);
        echo "
                    <input id=\"e3_friday\" checked=\"checked\"  type=\"checkbox\" name=\"e3_days[]\" value=\"Friday\" />";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Friday"), "html", null, true);
        echo "
                    <input id=\"e3_saturday\" checked=\"checked\"  type=\"checkbox\" name=\"e3_days[]\" value=\"Saturday\" />";
        // line 200
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Saturday"), "html", null, true);
        echo "
                  </div>  
                
                  <p></p>
            
                  <div id=\"e3_preset_list\" class=\"list-group\">
                    ";
        // line 209
        echo "                  </div>
                  <p></p>
                   
                   
                   
                  
                
                  
              </div>
         
              
              
              
              
            
              
              
            <input type=\"submit\" class=\"btn btn-primary btn-lg pull-right\" value=\"";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\"/>
          </form>
          
          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_7a96775fc30215caa4f096776a32db0a3a9d1dc7ac55ce7530f689e0b843c12a->leave($__internal_7a96775fc30215caa4f096776a32db0a3a9d1dc7ac55ce7530f689e0b843c12a_prof);

    }

    // line 241
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_5028d9415996e8a428e7e7445968896306fca98e9ab0cb41114252c93fed6e46 = $this->env->getExtension("native_profiler");
        $__internal_5028d9415996e8a428e7e7445968896306fca98e9ab0cb41114252c93fed6e46->enter($__internal_5028d9415996e8a428e7e7445968896306fca98e9ab0cb41114252c93fed6e46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 242
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"http://code.jquery.com/ui/1.9.1/themes/cupertino/jquery-ui.css\">
<link rel=\"stylesheet\" href=\"";
        // line 244
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/timepicker/jquery-ui-timepicker-addon.css"), "html", null, true);
        echo "\">    

<style>
    input[type=\"text\"]{    max-width: 100px;}
</style>    
";
        
        $__internal_5028d9415996e8a428e7e7445968896306fca98e9ab0cb41114252c93fed6e46->leave($__internal_5028d9415996e8a428e7e7445968896306fca98e9ab0cb41114252c93fed6e46_prof);

    }

    // line 254
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1743d929323f4f9242b75fdcfd8358e1d25628d4f07e0fa79ba75188bca247e1 = $this->env->getExtension("native_profiler");
        $__internal_1743d929323f4f9242b75fdcfd8358e1d25628d4f07e0fa79ba75188bca247e1->enter($__internal_1743d929323f4f9242b75fdcfd8358e1d25628d4f07e0fa79ba75188bca247e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 255
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js\"></script>
<script src=\"//code.jquery.com/ui/1.11.4/jquery-ui.js\"></script>
<script src=\"";
        // line 258
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/timepicker/jquery-ui-timepicker-addon.js"), "html", null, true);
        echo "\"></script>

<script> \$.validate(); </script>

<script>
 \$(document).ready(function() {
                    rebuildPresets();
                  
                    \$('#system_time_on').timepicker({});   
                    \$('#system_time_off').timepicker({}); 

                    \$('#e1_time_on').timepicker({});   
                    \$('#e1_time_off').timepicker({}); 

                    \$('#e2_time_on').timepicker({});   
                    \$('#e2_time_off').timepicker({}); 

                    \$('#e3_time_on').timepicker({});   
                    \$('#e3_time_off').timepicker({}); 
                    
                    //RECREATE THE DATA OF THE CLIENT USING THE DATA STORED
                    rebuildPresets();
                    rebuildPresetsE1('e1');
                    rebuildPresetsE1('e2');
                    rebuildPresetsE1('e3');


            });
            

              
        
</script>    


<script>
function addPresets(){
        var preset_item    = new Array();
            
        preset_item[\"system_time_on\"]   = \$(\"#system_time_on\").val();
        preset_item[\"system_time_off\"]  = \$(\"#system_time_off\").val();
        preset_item[\"system_preset\"]    = \$(\"#system_preset\").val();
        
        var preset         = new Array();
        var preset_list    = \$(\"#preset\").val();
        
        if (preset_item[\"system_time_on\"].length == 0 ||
            preset_item[\"system_time_off\"].length == 0 ||
            preset_item[\"system_preset\"].length == 0){
            
            return 0;
        }
        
        var days = \"[\";
        var days_a = \$('input[name=\"system_days[]\"]:checked');
        var i = 0;
        days_a.each(function(){
            if (i>0) days += \",\";
            
            days += '\"'+this.value+'\"';  
            i++;
        });
        days += \"]\";
        
        
        if (preset_list.length > 0){
            preset = eval(preset_list);
        }
        
        //ADD ADDRESS TO THE LIST
        //preset.push(preset_item);
        preset.push(\"{'time_on':'\"+preset_item[\"system_time_on\"]+\"','time_off':'\"+preset_item[\"system_time_off\"]+\"','preset':'\"+preset_item[\"system_preset\"]+\"', 'days':'\"+days+\"'}\");

        //SAVE LIST IN  THE INPUT
        \$(\"#preset\").val(JSON.stringify(preset));
        
        //console.log(preset); return 0;
        
        //REBUILD THE LIST
        rebuildPresets();
        
        //REMOVE ADDRESS TEXT
        \$(\"#system_time_on\").val(\"\");
        \$(\"#system_time_off\").val(\"\");
        \$(\"#system_preset\").val(\"\");
        
    }
    
    function deletePresets(id){
        
        var preset_list    = \$(\"#preset\").val();
        var preset = eval(preset_list);
        
        //SUPPRIME THE ITEM FROM THE LIST
        preset.splice(id, 1);
        
        //SAVE THE LIST WITHOUT THE ITEM
        \$(\"#preset\").val(JSON.stringify(preset));
        
        //REBUILD THE LIST
        rebuildPresets();
        
    }
    
    function rebuildPresets(){
         var preset            = eval(\$(\"#preset\").val());
         var id_item            = 0;
     
         
        if (typeof preset === 'undefined'){
            return 0;
        }
        
         \$(\"#system_preset_list\").html(\"\");
         preset.forEach(function(item) {
             
              var current = eval(\"(\"+item+\")\");
            
             \$(\"#system_preset_list\").append('<div class=\"list-group-item\">Time ON: '+current.time_on+', Time OFF: '+current.time_off+', Preset: '+current.preset+', Days: '+current.days+'<a onclick=\"deletePresets('+id_item+');\" href=\"javascript:void(0);\" > <span class=\" pull-right glyphicon glyphicon-minus\"></span></a></div>');
            
             id_item++;
       
        });
    }
        
    
</script>    



<script>
function addPresetsE1(listId){
        var preset_item    = new Array();
            
        preset_item[\"time_on\"]   = \$(\"#\"+listId+\"_time_on\").val();
        preset_item[\"time_off\"]  = \$(\"#\"+listId+\"_time_off\").val();
        preset_item[\"percent\"]    = \$(\"#\"+listId+\"_percent\").val();
        
        var preset         = new Array();
        var preset_list    = \$(\"#\"+listId+\"_preset\").val();
        
        //console.log(preset_item); return 0;
        
        if (preset_item[\"time_on\"].length == 0 ||
            preset_item[\"time_off\"].length == 0 ||
            preset_item[\"percent\"].length == 0){
            
            return 0;
        }
        
        
        var days = \"[\";
        var days_a = \$('input[name=\"'+listId+'_days[]\"]:checked');
        var i = 0;
        days_a.each(function(){
            if (i>0) days += \",\";
            
            days += '\"'+this.value+'\"';  
            i++;
        });
        days += \"]\";
        
        
        if (preset_list.length > 0){
            preset = eval(preset_list);
        }
        
        //ADD ADDRESS TO THE LIST
        preset.push(\"{'time_on':'\"+preset_item[\"time_on\"]+\"','time_off':'\"+preset_item[\"time_off\"]+\"','percent':'\"+preset_item[\"percent\"]+\"', 'days':'\"+days+\"'}\");
       

        //SAVE LIST IN  THE INPUT
        \$(\"#\"+listId+\"_preset\").val(JSON.stringify(preset));
        
        //console.log(preset); return 0;
        
        //REBUILD THE LIST
        rebuildPresetsE1(listId);
        
        //REMOVE ADDRESS TEXT
        \$(\"#\"+listId+\"_time_on\").val(\"\");
        \$(\"#\"+listId+\"_time_off\").val(\"\");
        \$(\"#\"+listId+\"_percent\").val(\"\");
        
    }
    
    function deletePresetsE1(id,listId){
        
        var preset_list    = \$(\"#\"+listId+\"_preset\").val();
        var preset = eval(preset_list);
        
        //SUPPRIME THE ITEM FROM THE LIST
        preset.splice(id, 1);
        
        //SAVE THE LIST WITHOUT THE ITEM
        \$(\"#\"+listId+\"_preset\").val(JSON.stringify(preset));
        
        //REBUILD THE LIST
        rebuildPresetsE1(listId);
        
    }
    
    function rebuildPresetsE1(listId){
         var preset            = eval(\$(\"#\"+listId+\"_preset\").val());
         var id_item            = 0;
     
         
        if (typeof preset === 'undefined'){
            return 0;
        }
        
         \$(\"#\"+listId+\"_preset_list\").html(\"\");
         preset.forEach(function(item) {
             var current = eval(\"(\"+item+\")\");
            // console.log(current);
             \$(\"#\"+listId+\"_preset_list\").append('<div class=\"list-group-item\">Time ON: '+current.time_on+', Time OFF: '+current.time_off+', Percent: '+current.percent+', Days: '+current.days+'<a onclick=\"deletePresetsE1('+id_item+',\\''+listId+'\\');\" href=\"javascript:void(0);\" > <span class=\" pull-right glyphicon glyphicon-minus\"></span></a></div>');
             id_item++;
        //console.log(item);
        });
    }
        
    
</script> 



";
        
        $__internal_1743d929323f4f9242b75fdcfd8358e1d25628d4f07e0fa79ba75188bca247e1->leave($__internal_1743d929323f4f9242b75fdcfd8358e1d25628d4f07e0fa79ba75188bca247e1_prof);

    }

    public function getTemplateName()
    {
        return "clients_presets.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  498 => 258,  492 => 255,  486 => 254,  473 => 244,  468 => 242,  462 => 241,  441 => 226,  422 => 209,  413 => 200,  409 => 199,  405 => 198,  401 => 197,  397 => 196,  393 => 195,  389 => 194,  385 => 193,  374 => 187,  366 => 182,  358 => 177,  353 => 175,  345 => 170,  337 => 164,  329 => 156,  325 => 155,  321 => 154,  317 => 153,  313 => 152,  309 => 151,  305 => 150,  301 => 149,  291 => 144,  283 => 139,  275 => 134,  270 => 132,  262 => 127,  251 => 118,  242 => 109,  238 => 108,  234 => 107,  230 => 106,  226 => 105,  222 => 104,  218 => 103,  214 => 102,  203 => 96,  195 => 91,  187 => 86,  182 => 84,  174 => 79,  167 => 74,  156 => 63,  152 => 62,  148 => 61,  144 => 60,  140 => 59,  136 => 58,  132 => 57,  128 => 56,  117 => 50,  109 => 45,  101 => 40,  91 => 33,  82 => 27,  78 => 26,  71 => 22,  63 => 17,  56 => 13,  51 => 10,  49 => 9,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients_v2.html.twig' %}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb.html.twig' with {'client_id': client.id}%} */
/*     <div class="col-md-9  main">*/
/*          */
/*         */
/*         <h1 class="page-header">{{"Presets"|trans}}</h1>*/
/*         */
/*         */
/*         */
/*         */
/*           <form  action="{{path('_clients_presets',{ 'client_id': client.id})}}" method="POST" enctype="multipart/form-data">*/
/*                <input type="hidden" name="sended" value="1"/>*/
/*               */
/*               */
/*                <h3>{{"Presets"|trans}} <!-- <span data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-info-sign"></span>--></h3>*/
/*                <input value="{{client.getPresetSystem()}}" type="hidden" name="preset" id="preset"/>*/
/*                */
/*                <div class="well well-lg form-inline">*/
/*                   */
/*                    */
/*                    */
/*                    <h4>{{"System"|trans}} <a href="javascript:void(0);" onclick="addPresets()">*/
/*                                                     <span class=" glyphicon glyphicon-plus"></span>*/
/*                                                 </a>*/
/*                     </h4>*/
/*                                                     */
/*                                                     */
/*                   <div class="form-group">*/
/*                     <label  for="time_on">{{"Time ON"|trans}}</label>*/
/*                     <input value=""  id="system_time_on"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   */
/*                   <div class="form-group">*/
/*                     <label for="time_off">{{"Time OFF"|trans}}</label>*/
/*                     <input value="" id="system_time_off"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   */
/*                   <div class="form-group">*/
/*                     <label  for="system_preset">{{"Preset"|trans}}&nbsp;<span data-toggle="tooltip" data-placement="right" title="{{"Maximum consumption allowed. Just numbers allowed."|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></label>*/
/*                     <input value="" id="system_preset"   type="text" class="form-control"   placeholder="Kw">*/
/*                   </div>*/
/*                   */
/*                     */
/*                    <div class="form-group">*/
/*                     <label  for="system_preset">{{"Days"|trans}}&nbsp;</label>*/
/*                     <input id="system_sunday" checked="checked"  type="checkbox" name="system_days[]" value="Sunday" />{{"Sunday"|trans}}*/
/*                     <input id="system_monday" checked="checked"  type="checkbox" name="system_days[]" value="Monday" />{{"Monday"|trans}}*/
/*                     <input id="system_tuesday" checked="checked"  type="checkbox" name="system_days[]" value="Tuesday" />{{"Tuesday"|trans}}*/
/*                     <input id="system_wednesday" checked="checked"  type="checkbox" name="system_days[]" value="Wednesday" />{{"Wednesday"|trans}}*/
/*                     <input id="system_thursday" checked="checked"  type="checkbox" name="system_days[]" value="Thursday" />{{"Thursday"|trans}}*/
/*                     <input id="system_friday" checked="checked"  type="checkbox" name="system_days[]" value="Friday" />{{"Friday"|trans}}*/
/*                     <input id="system_saturday" checked="checked"  type="checkbox" name="system_days[]" value="Saturday" />{{"Saturday"|trans}}*/
/*                   </div>*/
/*                     */
/*                     */
/*                 */
/*                   <p></p>*/
/*             */
/*                   <div id="system_preset_list" class="list-group">*/
/*                     {#<a href="#" class="list-group-item">Dapibus ac facilisis in*/
/*                         <span class=" pull-right glyphicon glyphicon-minus"></span>*/
/*                     </a>#}*/
/*                   </div>*/
/*                   <p></p>*/
/*                    */
/*                   */
/*                   */
/*                   <h4>{{"First Entry"|trans}} <a href="javascript:void(0);" onclick="addPresetsE1('e1');">*/
/*                                                     <span class=" glyphicon glyphicon-plus"></span>*/
/*                                                 </a>*/
/*                     </h4>*/
/*                                                     */
/*                   <input value="{{client.getPresetEntry1()}}" type="hidden" name="e1_preset" id="e1_preset"/>                                  */
/*                   <div class="form-group">*/
/*                     <label  for="e1_time_on">{{"Time ON"|trans}}</label>*/
/*                     <input value=""  id="e1_time_on"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   */
/*                   <div class="form-group">*/
/*                     <label for="e1_time_off">{{"Time OFF"|trans}}</label>*/
/*                     <input value="" id="e1_time_off"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   */
/*                   <div class="form-group">*/
/*                     <label  for="e1_percent">{{"Percent"|trans}}&nbsp;<span data-toggle="tooltip" data-placement="right" title="{{"Percent. Just numbers allowed."|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></label>*/
/*                     <input value="" id="e1_percent"   type="text" class="form-control"   placeholder="%">*/
/*                   </div>*/
/*                   */
/*                     */
/*                  <div class="form-group">*/
/*                     <label  for="system_preset">{{"Days"|trans}}&nbsp;</label>*/
/*                     <input id="e1_sunday" checked="checked"  type="checkbox" name="e1_days[]" value="Sunday" />{{"Sunday"|trans}}*/
/*                     <input id="e1_monday" checked="checked"  type="checkbox" name="e1_days[]" value="Monday" />{{"Monday"|trans}}*/
/*                     <input id="e1_tuesday" checked="checked"  type="checkbox" name="e1_days[]" value="Tuesday" />{{"Tuesday"|trans}}*/
/*                     <input id="e1_wednesday" checked="checked"  type="checkbox" name="e1_days[]" value="Wednesday" />{{"Wednesday"|trans}}*/
/*                     <input id="e1_thursday" checked="checked"  type="checkbox" name="e1_days[]" value="Thursday" />{{"Thursday"|trans}}*/
/*                     <input id="e1_friday" checked="checked"  type="checkbox" name="e1_days[]" value="Friday" />{{"Friday"|trans}}*/
/*                     <input id="e1_saturday" checked="checked"  type="checkbox" name="e1_days[]" value="Saturday" />{{"Saturday"|trans}}*/
/*                   </div>*/
/*                 */
/*                   <p></p>*/
/*             */
/*                   <div id="e1_preset_list" class="list-group">*/
/*                     {#<a href="#" class="list-group-item">Dapibus ac facilisis in*/
/*                         <span class=" pull-right glyphicon glyphicon-minus"></span>*/
/*                     </a>#}*/
/*                   </div>*/
/*                   <p></p>*/
/*                   */
/*                   */
/*                   */
/*                   */
/*                   */
/*                   */
/*                   */
/*                    <h4>{{"Second Entry"|trans}} <a href="javascript:void(0);" onclick="addPresetsE1('e2');">*/
/*                                                     <span class=" glyphicon glyphicon-plus"></span>*/
/*                                                 </a>*/
/*                     </h4>*/
/*                                                     */
/*                   <input value="{{client.getPresetEntry2()}}" type="hidden" name="e2_preset" id="e2_preset"/>                                  */
/*                   <div class="form-group">*/
/*                     <label  for="e2_time_on">{{"Time ON"|trans}}</label>*/
/*                     <input value=""  id="e2_time_on"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   */
/*                   <div class="form-group">*/
/*                     <label for="e2_time_off">{{"Time OFF"|trans}}</label>*/
/*                     <input value="" id="e2_time_off"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   */
/*                   <div class="form-group">*/
/*                     <label  for="e2_percent">{{"Percent"|trans}}&nbsp;<span data-toggle="tooltip" data-placement="right" title="{{"Percent. Just numbers allowed."|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></label>*/
/*                     <input value="" id="e2_percent"   type="text" class="form-control"   placeholder="%">*/
/*                   </div>*/
/*                   */
/*                   <div class="form-group">*/
/*                     <label  for="system_preset">{{"Days"|trans}}&nbsp;</label>*/
/*                     <input id="e2_sunday" checked="checked"  type="checkbox" name="e2_days[]" value="Sunday" />{{"Sunday"|trans}}*/
/*                     <input id="e2_monday" checked="checked"  type="checkbox" name="e2_days[]" value="Monday" />{{"Monday"|trans}}*/
/*                     <input id="e2_tuesday" checked="checked"  type="checkbox" name="e2_days[]" value="Tuesday" />{{"Tuesday"|trans}}*/
/*                     <input id="e2_wednesday" checked="checked"  type="checkbox" name="e2_days[]" value="Wednesday" />{{"Wednesday"|trans}}*/
/*                     <input id="e2_thursday" checked="checked"  type="checkbox" name="e2_days[]" value="Thursday" />{{"Thursday"|trans}}*/
/*                     <input id="e2_friday" checked="checked"  type="checkbox" name="e2_days[]" value="Friday" />{{"Friday"|trans}}*/
/*                     <input id="e2_saturday" checked="checked"  type="checkbox" name="e2_days[]" value="Saturday" />{{"Saturday"|trans}}*/
/*                   </div>      */
/*                   <p></p>*/
/*             */
/*                   <div id="e2_preset_list" class="list-group">*/
/*                     {#<a href="#" class="list-group-item">Dapibus ac facilisis in*/
/*                         <span class=" pull-right glyphicon glyphicon-minus"></span>*/
/*                     </a>#}*/
/*                   </div>*/
/*                   <p></p>*/
/*                   */
/*                   */
/*                   */
/*                   */
/*                   <h4>{{"Third Entry"|trans}} <a href="javascript:void(0);" onclick="addPresetsE1('e3');">*/
/*                                                     <span class=" glyphicon glyphicon-plus"></span>*/
/*                                                 </a>*/
/*                     </h4>*/
/*                                                     */
/*                   <input value="{{client.getPresetEntry3()}}" type="hidden" name="e3_preset" id="e3_preset"/>                                  */
/*                   <div class="form-group">*/
/*                     <label  for="e3_time_on">{{"Time ON"|trans}}</label>*/
/*                     <input value=""  id="e3_time_on"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   */
/*                   <div class="form-group">*/
/*                     <label for="e3_time_off">{{"Time OFF"|trans}}</label>*/
/*                     <input value="" id="e3_time_off"   type="text" class="form-control"   placeholder="">*/
/*                   </div>*/
/*                   */
/*                   <div class="form-group">*/
/*                     <label  for="e3_percent">{{"Percent"|trans}}&nbsp;<span data-toggle="tooltip" data-placement="right" title="{{"Percent. Just numbers allowed."|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></label>*/
/*                     <input value="" id="e3_percent"   type="text" class="form-control"   placeholder="%">*/
/*                   </div>*/
/*                   */
/*                     */
/*                   <div class="form-group">*/
/*                     <label  for="system_preset">{{"Days"|trans}}&nbsp;</label>*/
/*                     <input id="e3_sunday" checked="checked"  type="checkbox" name="e3_days[]" value="Sunday" />{{"Sunday"|trans}}*/
/*                     <input id="e3_monday" checked="checked"  type="checkbox" name="e3_days[]" value="Monday" />{{"Monday"|trans}}*/
/*                     <input id="e3_tuesday" checked="checked"  type="checkbox" name="e3_days[]" value="Tuesday" />{{"Tuesday"|trans}}*/
/*                     <input id="e3_wednesday" checked="checked"  type="checkbox" name="e3_days[]" value="Wednesday" />{{"Wednesday"|trans}}*/
/*                     <input id="e3_thursday" checked="checked"  type="checkbox" name="e3_days[]" value="Thursday" />{{"Thursday"|trans}}*/
/*                     <input id="e3_friday" checked="checked"  type="checkbox" name="e3_days[]" value="Friday" />{{"Friday"|trans}}*/
/*                     <input id="e3_saturday" checked="checked"  type="checkbox" name="e3_days[]" value="Saturday" />{{"Saturday"|trans}}*/
/*                   </div>  */
/*                 */
/*                   <p></p>*/
/*             */
/*                   <div id="e3_preset_list" class="list-group">*/
/*                     {#<a href="#" class="list-group-item">Dapibus ac facilisis in*/
/*                         <span class=" pull-right glyphicon glyphicon-minus"></span>*/
/*                     </a>#}*/
/*                   </div>*/
/*                   <p></p>*/
/*                    */
/*                    */
/*                    */
/*                   */
/*                 */
/*                   */
/*               </div>*/
/*          */
/*               */
/*               */
/*               */
/*               */
/*             */
/*               */
/*               */
/*             <input type="submit" class="btn btn-primary btn-lg pull-right" value="{{"Submit"|trans}}"/>*/
/*           </form>*/
/*           */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/* <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/cupertino/jquery-ui.css">*/
/* <link rel="stylesheet" href="{{ asset('libs/timepicker/jquery-ui-timepicker-addon.css')}}">    */
/* */
/* <style>*/
/*     input[type="text"]{    max-width: 100px;}*/
/* </style>    */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* {% block javascripts %}*/
/* {{parent()}}*/
/* <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>*/
/* <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>*/
/* <script src="{{ asset('libs/timepicker/jquery-ui-timepicker-addon.js')}}"></script>*/
/* */
/* <script> $.validate(); </script>*/
/* */
/* <script>*/
/*  $(document).ready(function() {*/
/*                     rebuildPresets();*/
/*                   */
/*                     $('#system_time_on').timepicker({});   */
/*                     $('#system_time_off').timepicker({}); */
/* */
/*                     $('#e1_time_on').timepicker({});   */
/*                     $('#e1_time_off').timepicker({}); */
/* */
/*                     $('#e2_time_on').timepicker({});   */
/*                     $('#e2_time_off').timepicker({}); */
/* */
/*                     $('#e3_time_on').timepicker({});   */
/*                     $('#e3_time_off').timepicker({}); */
/*                     */
/*                     //RECREATE THE DATA OF THE CLIENT USING THE DATA STORED*/
/*                     rebuildPresets();*/
/*                     rebuildPresetsE1('e1');*/
/*                     rebuildPresetsE1('e2');*/
/*                     rebuildPresetsE1('e3');*/
/* */
/* */
/*             });*/
/*             */
/* */
/*               */
/*         */
/* </script>    */
/* */
/* */
/* <script>*/
/* function addPresets(){*/
/*         var preset_item    = new Array();*/
/*             */
/*         preset_item["system_time_on"]   = $("#system_time_on").val();*/
/*         preset_item["system_time_off"]  = $("#system_time_off").val();*/
/*         preset_item["system_preset"]    = $("#system_preset").val();*/
/*         */
/*         var preset         = new Array();*/
/*         var preset_list    = $("#preset").val();*/
/*         */
/*         if (preset_item["system_time_on"].length == 0 ||*/
/*             preset_item["system_time_off"].length == 0 ||*/
/*             preset_item["system_preset"].length == 0){*/
/*             */
/*             return 0;*/
/*         }*/
/*         */
/*         var days = "[";*/
/*         var days_a = $('input[name="system_days[]"]:checked');*/
/*         var i = 0;*/
/*         days_a.each(function(){*/
/*             if (i>0) days += ",";*/
/*             */
/*             days += '"'+this.value+'"';  */
/*             i++;*/
/*         });*/
/*         days += "]";*/
/*         */
/*         */
/*         if (preset_list.length > 0){*/
/*             preset = eval(preset_list);*/
/*         }*/
/*         */
/*         //ADD ADDRESS TO THE LIST*/
/*         //preset.push(preset_item);*/
/*         preset.push("{'time_on':'"+preset_item["system_time_on"]+"','time_off':'"+preset_item["system_time_off"]+"','preset':'"+preset_item["system_preset"]+"', 'days':'"+days+"'}");*/
/* */
/*         //SAVE LIST IN  THE INPUT*/
/*         $("#preset").val(JSON.stringify(preset));*/
/*         */
/*         //console.log(preset); return 0;*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildPresets();*/
/*         */
/*         //REMOVE ADDRESS TEXT*/
/*         $("#system_time_on").val("");*/
/*         $("#system_time_off").val("");*/
/*         $("#system_preset").val("");*/
/*         */
/*     }*/
/*     */
/*     function deletePresets(id){*/
/*         */
/*         var preset_list    = $("#preset").val();*/
/*         var preset = eval(preset_list);*/
/*         */
/*         //SUPPRIME THE ITEM FROM THE LIST*/
/*         preset.splice(id, 1);*/
/*         */
/*         //SAVE THE LIST WITHOUT THE ITEM*/
/*         $("#preset").val(JSON.stringify(preset));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildPresets();*/
/*         */
/*     }*/
/*     */
/*     function rebuildPresets(){*/
/*          var preset            = eval($("#preset").val());*/
/*          var id_item            = 0;*/
/*      */
/*          */
/*         if (typeof preset === 'undefined'){*/
/*             return 0;*/
/*         }*/
/*         */
/*          $("#system_preset_list").html("");*/
/*          preset.forEach(function(item) {*/
/*              */
/*               var current = eval("("+item+")");*/
/*             */
/*              $("#system_preset_list").append('<div class="list-group-item">Time ON: '+current.time_on+', Time OFF: '+current.time_off+', Preset: '+current.preset+', Days: '+current.days+'<a onclick="deletePresets('+id_item+');" href="javascript:void(0);" > <span class=" pull-right glyphicon glyphicon-minus"></span></a></div>');*/
/*             */
/*              id_item++;*/
/*        */
/*         });*/
/*     }*/
/*         */
/*     */
/* </script>    */
/* */
/* */
/* */
/* <script>*/
/* function addPresetsE1(listId){*/
/*         var preset_item    = new Array();*/
/*             */
/*         preset_item["time_on"]   = $("#"+listId+"_time_on").val();*/
/*         preset_item["time_off"]  = $("#"+listId+"_time_off").val();*/
/*         preset_item["percent"]    = $("#"+listId+"_percent").val();*/
/*         */
/*         var preset         = new Array();*/
/*         var preset_list    = $("#"+listId+"_preset").val();*/
/*         */
/*         //console.log(preset_item); return 0;*/
/*         */
/*         if (preset_item["time_on"].length == 0 ||*/
/*             preset_item["time_off"].length == 0 ||*/
/*             preset_item["percent"].length == 0){*/
/*             */
/*             return 0;*/
/*         }*/
/*         */
/*         */
/*         var days = "[";*/
/*         var days_a = $('input[name="'+listId+'_days[]"]:checked');*/
/*         var i = 0;*/
/*         days_a.each(function(){*/
/*             if (i>0) days += ",";*/
/*             */
/*             days += '"'+this.value+'"';  */
/*             i++;*/
/*         });*/
/*         days += "]";*/
/*         */
/*         */
/*         if (preset_list.length > 0){*/
/*             preset = eval(preset_list);*/
/*         }*/
/*         */
/*         //ADD ADDRESS TO THE LIST*/
/*         preset.push("{'time_on':'"+preset_item["time_on"]+"','time_off':'"+preset_item["time_off"]+"','percent':'"+preset_item["percent"]+"', 'days':'"+days+"'}");*/
/*        */
/* */
/*         //SAVE LIST IN  THE INPUT*/
/*         $("#"+listId+"_preset").val(JSON.stringify(preset));*/
/*         */
/*         //console.log(preset); return 0;*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildPresetsE1(listId);*/
/*         */
/*         //REMOVE ADDRESS TEXT*/
/*         $("#"+listId+"_time_on").val("");*/
/*         $("#"+listId+"_time_off").val("");*/
/*         $("#"+listId+"_percent").val("");*/
/*         */
/*     }*/
/*     */
/*     function deletePresetsE1(id,listId){*/
/*         */
/*         var preset_list    = $("#"+listId+"_preset").val();*/
/*         var preset = eval(preset_list);*/
/*         */
/*         //SUPPRIME THE ITEM FROM THE LIST*/
/*         preset.splice(id, 1);*/
/*         */
/*         //SAVE THE LIST WITHOUT THE ITEM*/
/*         $("#"+listId+"_preset").val(JSON.stringify(preset));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildPresetsE1(listId);*/
/*         */
/*     }*/
/*     */
/*     function rebuildPresetsE1(listId){*/
/*          var preset            = eval($("#"+listId+"_preset").val());*/
/*          var id_item            = 0;*/
/*      */
/*          */
/*         if (typeof preset === 'undefined'){*/
/*             return 0;*/
/*         }*/
/*         */
/*          $("#"+listId+"_preset_list").html("");*/
/*          preset.forEach(function(item) {*/
/*              var current = eval("("+item+")");*/
/*             // console.log(current);*/
/*              $("#"+listId+"_preset_list").append('<div class="list-group-item">Time ON: '+current.time_on+', Time OFF: '+current.time_off+', Percent: '+current.percent+', Days: '+current.days+'<a onclick="deletePresetsE1('+id_item+',\''+listId+'\');" href="javascript:void(0);" > <span class=" pull-right glyphicon glyphicon-minus"></span></a></div>');*/
/*              id_item++;*/
/*         //console.log(item);*/
/*         });*/
/*     }*/
/*         */
/*     */
/* </script> */
/* */
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
