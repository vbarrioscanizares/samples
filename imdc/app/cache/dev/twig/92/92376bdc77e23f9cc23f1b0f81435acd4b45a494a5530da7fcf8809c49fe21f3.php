<?php

/* base.html.twig */
class __TwigTemplate_2f3449af46cff78af7d190bd5d999f7a0f5760d75bc724b52d235504afb3086f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f17a8de72d01f35267b79713868ff1ffebe023a7b735fd1ec60264395468541 = $this->env->getExtension("native_profiler");
        $__internal_6f17a8de72d01f35267b79713868ff1ffebe023a7b735fd1ec60264395468541->enter($__internal_6f17a8de72d01f35267b79713868ff1ffebe023a7b735fd1ec60264395468541_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        
        ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 13
        echo "        
        
    </head>
    <body>
            
        ";
        // line 20
        echo "        
        ";
        // line 21
        $this->displayBlock('body', $context, $blocks);
        // line 22
        echo "        
        
        ";
        // line 24
        $this->displayBlock('javascripts', $context, $blocks);
        // line 28
        echo "    </body>
</html>
";
        
        $__internal_6f17a8de72d01f35267b79713868ff1ffebe023a7b735fd1ec60264395468541->leave($__internal_6f17a8de72d01f35267b79713868ff1ffebe023a7b735fd1ec60264395468541_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_f28f8aee7ee834c3a31bbd8a690ff7b1e82414802c3deb72e98217ccca869fe7 = $this->env->getExtension("native_profiler");
        $__internal_f28f8aee7ee834c3a31bbd8a690ff7b1e82414802c3deb72e98217ccca869fe7->enter($__internal_f28f8aee7ee834c3a31bbd8a690ff7b1e82414802c3deb72e98217ccca869fe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_f28f8aee7ee834c3a31bbd8a690ff7b1e82414802c3deb72e98217ccca869fe7->leave($__internal_f28f8aee7ee834c3a31bbd8a690ff7b1e82414802c3deb72e98217ccca869fe7_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_edcff7dddccadc057a64d0434cdfec2a3a9dee52dde4e3b1685f268a5e92f7f8 = $this->env->getExtension("native_profiler");
        $__internal_edcff7dddccadc057a64d0434cdfec2a3a9dee52dde4e3b1685f268a5e92f7f8->enter($__internal_edcff7dddccadc057a64d0434cdfec2a3a9dee52dde4e3b1685f268a5e92f7f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "                <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
                <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\">
                <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/recast.css"), "html", null, true);
        echo "\">
        ";
        
        $__internal_edcff7dddccadc057a64d0434cdfec2a3a9dee52dde4e3b1685f268a5e92f7f8->leave($__internal_edcff7dddccadc057a64d0434cdfec2a3a9dee52dde4e3b1685f268a5e92f7f8_prof);

    }

    // line 21
    public function block_body($context, array $blocks = array())
    {
        $__internal_8aa669a93dd1f5782195987aa5fd903b98f2e586e92207f09ecf68a12200c182 = $this->env->getExtension("native_profiler");
        $__internal_8aa669a93dd1f5782195987aa5fd903b98f2e586e92207f09ecf68a12200c182->enter($__internal_8aa669a93dd1f5782195987aa5fd903b98f2e586e92207f09ecf68a12200c182_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_8aa669a93dd1f5782195987aa5fd903b98f2e586e92207f09ecf68a12200c182->leave($__internal_8aa669a93dd1f5782195987aa5fd903b98f2e586e92207f09ecf68a12200c182_prof);

    }

    // line 24
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ed06b9dda3af6336506928f6180aaf8b4f53f417cc03e1d300c766c731a08d7d = $this->env->getExtension("native_profiler");
        $__internal_ed06b9dda3af6336506928f6180aaf8b4f53f417cc03e1d300c766c731a08d7d->enter($__internal_ed06b9dda3af6336506928f6180aaf8b4f53f417cc03e1d300c766c731a08d7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 25
        echo "                <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/jQuery/jQuery-2.1.4.min.js"), "html", null, true);
        echo "\"></script>
                <script src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script> 
        ";
        
        $__internal_ed06b9dda3af6336506928f6180aaf8b4f53f417cc03e1d300c766c731a08d7d->leave($__internal_ed06b9dda3af6336506928f6180aaf8b4f53f417cc03e1d300c766c731a08d7d_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  125 => 26,  120 => 25,  114 => 24,  103 => 21,  94 => 11,  90 => 10,  85 => 9,  79 => 8,  67 => 6,  58 => 28,  56 => 24,  52 => 22,  50 => 21,  47 => 20,  40 => 13,  38 => 8,  33 => 6,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         */
/*         {% block stylesheets %}*/
/*                 <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*                 <link rel="stylesheet" href="{{asset('libs/bootstrap/css/bootstrap.min.css')}}">*/
/*                 <link rel="stylesheet" href="{{asset('css/recast.css')}}">*/
/*         {% endblock %}*/
/*         */
/*         */
/*     </head>*/
/*     <body>*/
/*             */
/*         {#<p class="bg-success">...</p>*/
/*         <p class="bg-danger">...</p>#}*/
/*         */
/*         {% block body %}{% endblock %}*/
/*         */
/*         */
/*         {% block javascripts %}*/
/*                 <script src="{{asset('libs/jQuery/jQuery-2.1.4.min.js')}}"></script>*/
/*                 <script src="{{asset('libs/bootstrap/js/bootstrap.min.js')}}"></script> */
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
