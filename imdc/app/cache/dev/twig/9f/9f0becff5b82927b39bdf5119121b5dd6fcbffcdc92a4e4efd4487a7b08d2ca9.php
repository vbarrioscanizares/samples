<?php

/* clients_bills_create_m.html.twig */
class __TwigTemplate_ffb1aee203afc458a1b93a618e5ed9423921e2412ef4b8434b0f939570c041c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_bills_create_m.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6809fb2fe3b96b5196d00e3becb1e5cc0add2b96d230143d8ae0d0f97f4725d0 = $this->env->getExtension("native_profiler");
        $__internal_6809fb2fe3b96b5196d00e3becb1e5cc0add2b96d230143d8ae0d0f97f4725d0->enter($__internal_6809fb2fe3b96b5196d00e3becb1e5cc0add2b96d230143d8ae0d0f97f4725d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_bills_create_m.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6809fb2fe3b96b5196d00e3becb1e5cc0add2b96d230143d8ae0d0f97f4725d0->leave($__internal_6809fb2fe3b96b5196d00e3becb1e5cc0add2b96d230143d8ae0d0f97f4725d0_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_ea4cf722deb9eb8169785a380062fe57ec208f32b470c973df6abebebd37d618 = $this->env->getExtension("native_profiler");
        $__internal_ea4cf722deb9eb8169785a380062fe57ec208f32b470c973df6abebebd37d618->enter($__internal_ea4cf722deb9eb8169785a380062fe57ec208f32b470c973df6abebebd37d618_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 9
        $this->loadTemplate("sidebar_clients_v2.html.twig", "clients_bills_create_m.html.twig", 9)->display($context);
        // line 10
        echo "    
    
    
     ";
        // line 13
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_bills_create_m.html.twig", 13)->display(array_merge($context, array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))));
        // line 14
        echo "    <div class=\" col-md-9 main\">
       
          <h1 class=\"page-header\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("New bill"), "html", null, true);
        echo "</h1>
          
          <form  action=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills_create_m", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
        echo "\" method=\"POST\" enctype=\"multipart/form-data\" >
              <input type=\"hidden\" value=\"1\" name=\"posting\">
              <input type=\"hidden\" value=\"\" name=\"ammount_total\">
              <input type=\"hidden\" value=\"\" name=\"filename\">
           
          ";
        // line 36
        echo "              
           <div class=\"well well-lg\">   
              
              
            <div class=\"form-group\">
              <label for=\"date_from\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input data-validation=\"required\" data-validation-error-msg=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\"  name=\"date_from\" value=\"\" type=\"text\" class=\"form-control\" id=\"date_from\" placeholder=\"\"  >
            </div>
              
             ";
        // line 51
        echo "              
             <div class=\"form-group\">
              <label for=\"days_no\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days number"), "html", null, true);
        echo "</label>
              <input data-validation=\"required number\" data-validation-error-msg=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\"  name=\"days_no\" value=\"\" type=\"text\" class=\"form-control\" id=\"days_no\" placeholder=\"\"  >
            </div>
              
              
              <div class=\"form-group\">
              <label for=\"kwh\">";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total power consumption (KWh)"), "html", null, true);
        echo "</label>
              <input data-validation=\"required number\" data-validation-error-msg=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required number fields"), "html", null, true);
        echo "\"  name=\"kwh\" value=\"\" type=\"text\" class=\"form-control\" id=\"kwh\" placeholder=\"\"  >
            </div> 
            
              ";
        // line 68
        echo " 
            <div class=\"form-group\">
              <label for=\"kw\">";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total power billed (kW)"), "html", null, true);
        echo "</label>
              <input data-validation=\"required number\" data-validation-error-msg=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required number fields"), "html", null, true);
        echo "\"  name=\"kw\" value=\"\" type=\"text\" class=\"form-control\" id=\"kw\" placeholder=\"\"  >
            </div> 
             ";
        // line 78
        echo "             ";
        // line 86
        echo "            
            ";
        // line 96
        echo " 
            <div class=\"form-group\">
              <label for=\"kw_min\">";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Minimal power (kW)"), "html", null, true);
        echo "</label>
              <input data-validation=\"required number\" data-validation-error-msg=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required number fields"), "html", null, true);
        echo "\"  name=\"kw_min\" value=\"\" type=\"text\" class=\"form-control\" id=\"kw_min\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"kva\">";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apparent Power (Kva)"), "html", null, true);
        echo "</label>
              <input data-validation=\"required number\" data-validation-error-msg=\"";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required number fields"), "html", null, true);
        echo "\"  name=\"kva\" value=\"\" type=\"text\" class=\"form-control\" id=\"kva\" placeholder=\"\"  >
            </div> 
              
           ";
        // line 111
        echo "              
            </div><!-- well -->  
            ";
        // line 282
        echo "           <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
        echo "\" class=\"btn btn-default btn-lg pull-right \">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cancel"), "html", null, true);
        echo "</a>
            <input type=\"submit\" class=\"btn btn-primary btn-lg pull-right\" value=\"";
        // line 283
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\"/>
          
            </div>
          </form>
          
          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_ea4cf722deb9eb8169785a380062fe57ec208f32b470c973df6abebebd37d618->leave($__internal_ea4cf722deb9eb8169785a380062fe57ec208f32b470c973df6abebebd37d618_prof);

    }

    // line 300
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_07c08db9deb6fa355a2f1549f3834d8a5f85e7a108ac3b26bceef5a8bae15dcd = $this->env->getExtension("native_profiler");
        $__internal_07c08db9deb6fa355a2f1549f3834d8a5f85e7a108ac3b26bceef5a8bae15dcd->enter($__internal_07c08db9deb6fa355a2f1549f3834d8a5f85e7a108ac3b26bceef5a8bae15dcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 301
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"http://code.jquery.com/ui/1.9.1/themes/cupertino/jquery-ui.css\">
<link rel=\"stylesheet\" href=\"";
        // line 303
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/timepicker/jquery-ui-timepicker-addon.css"), "html", null, true);
        echo "\">     
";
        
        $__internal_07c08db9deb6fa355a2f1549f3834d8a5f85e7a108ac3b26bceef5a8bae15dcd->leave($__internal_07c08db9deb6fa355a2f1549f3834d8a5f85e7a108ac3b26bceef5a8bae15dcd_prof);

    }

    // line 309
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ba863295ebda62013987d507d7fc86d690334a5658efc025f18550d7356974ab = $this->env->getExtension("native_profiler");
        $__internal_ba863295ebda62013987d507d7fc86d690334a5658efc025f18550d7356974ab->enter($__internal_ba863295ebda62013987d507d7fc86d690334a5658efc025f18550d7356974ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 310
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js\"></script>

<script src=\"//code.jquery.com/ui/1.11.4/jquery-ui.js\"></script>
<script src=\"";
        // line 314
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/timepicker/jquery-ui-timepicker-addon.js"), "html", null, true);
        echo "\"></script>

<script> \$.validate(); </script>

<script>
 \$(document).ready(function() {         
        
                    \$('#date_from').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#date_to\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });    
                    \$('#date_to').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#date_from\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });
                    \$('#costA_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costA_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });   
                    \$('#costA_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costA_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });   
                    \$('#costB_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costB_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });      
                    \$('#costB_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costB_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });  
                    \$('#consf_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consf_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });   
                    \$('#consf_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consf_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });
                    \$('#consr_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consr_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });   
                    \$('#consr_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consr_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        }); 
                    
            });
            

              
        
</script>  


";
        
        $__internal_ba863295ebda62013987d507d7fc86d690334a5658efc025f18550d7356974ab->leave($__internal_ba863295ebda62013987d507d7fc86d690334a5658efc025f18550d7356974ab_prof);

    }

    public function getTemplateName()
    {
        return "clients_bills_create_m.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 314,  223 => 310,  217 => 309,  208 => 303,  203 => 301,  197 => 300,  174 => 283,  167 => 282,  163 => 111,  157 => 104,  153 => 103,  146 => 99,  142 => 98,  138 => 96,  135 => 86,  133 => 78,  128 => 71,  124 => 70,  120 => 68,  114 => 60,  110 => 59,  102 => 54,  98 => 53,  94 => 51,  88 => 42,  82 => 41,  75 => 36,  67 => 18,  62 => 16,  58 => 14,  56 => 13,  51 => 10,  49 => 9,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients_v2.html.twig' %}*/
/*     */
/*     */
/*     */
/*      {% include 'clients_breadcrumb.html.twig' with {'client_id': client_id}%}*/
/*     <div class=" col-md-9 main">*/
/*        */
/*           <h1 class="page-header">{{"New bill"|trans}}</h1>*/
/*           */
/*           <form  action="{{path('_clients_bills_create_m',{ 'client_id': client_id})}}" method="POST" enctype="multipart/form-data" >*/
/*               <input type="hidden" value="1" name="posting">*/
/*               <input type="hidden" value="" name="ammount_total">*/
/*               <input type="hidden" value="" name="filename">*/
/*            */
/*           {# <div class="well well-lg">     */
/*            <div class="control-group">*/
/*                   <label class="control-label" for="inputDocument">{{"File"|trans}}</label>*/
/*                   <div class="controls">*/
/*                     <span class="btn btn-file">*/
/*                         */
/*                         <input name="bill_file" type="file" id='inputDocument' accept="application/pdf" />*/
/*                     </span>*/
/* */
/*                   </div>*/
/* */
/*               </div>   */
/*             </div><!--well-->  #}*/
/*               */
/*            <div class="well well-lg">   */
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="date_from">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input data-validation="required" data-validation-error-msg="{{"You have not answered all required fields"|trans}}"  name="date_from" value="" type="text" class="form-control" id="date_from" placeholder=""  >*/
/*             </div>*/
/*               */
/*              {# */
/*              <div class="form-group">*/
/*               <label for="date_to">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="date_to" value="" type="text" class="form-control" id="date_to" placeholder=""  >*/
/*             </div>*/
/*               #}*/
/*               */
/*              <div class="form-group">*/
/*               <label for="days_no">{{"Days number"|trans}}</label>*/
/*               <input data-validation="required number" data-validation-error-msg="{{"You have not answered all required fields"|trans}}"  name="days_no" value="" type="text" class="form-control" id="days_no" placeholder=""  >*/
/*             </div>*/
/*               */
/*               */
/*               <div class="form-group">*/
/*               <label for="kwh">{{"Total power consumption (KWh)"|trans}}</label>*/
/*               <input data-validation="required number" data-validation-error-msg="{{"You have not answered all required number fields"|trans}}"  name="kwh" value="" type="text" class="form-control" id="kwh" placeholder=""  >*/
/*             </div> */
/*             */
/*               {#*/
/*             <div class="form-group">*/
/*               <label for="ammount_kwh">{{"Amount of total consumption ($)"|trans}}</label>*/
/*               <input name="ammount_kwh" value="" type="text" class="form-control" id="ammount_kwh" placeholder=""  >*/
/*             </div> */
/*              #} */
/*             <div class="form-group">*/
/*               <label for="kw">{{"Total power billed (kW)"|trans}}</label>*/
/*               <input data-validation="required number" data-validation-error-msg="{{"You have not answered all required number fields"|trans}}"  name="kw" value="" type="text" class="form-control" id="kw" placeholder=""  >*/
/*             </div> */
/*              {# */
/*             <div class="form-group">*/
/*               <label for="ammount_kw">{{"Amount billed ($)"|trans}}</label>*/
/*               <input name="ammount_kw" value="" type="text" class="form-control" id="ammount_kw" placeholder=""  >*/
/*             </div> #}*/
/*              {# */
/*             <div class="form-group">*/
/*               <label for="bill_type">{{"Bill type"|trans}}</label>*/
/*               <select name="bill_type" class="form-control">*/
/*                   <option value="A">{{"Actual"|trans}}</option>*/
/*                   <option value="E">{{"Estimated"|trans}}</option>*/
/*               </select>    */
/*              </div> #}*/
/*             */
/*             {#<div class="form-group">*/
/*               <label for="fa">{{"Power factor (Fa)"|trans}}</label>*/
/*               <input name="fa" value="" type="text" class="form-control" id="fa" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="fu">{{"Utilization Factor (Fu)"|trans}}</label>*/
/*               <input name="fu" value="" type="text" class="form-control" id="fu" placeholder=""  >*/
/*             </div> */
/*              #} */
/*             <div class="form-group">*/
/*               <label for="kw_min">{{"Minimal power (kW)"|trans}}</label>*/
/*               <input data-validation="required number" data-validation-error-msg="{{"You have not answered all required number fields"|trans}}"  name="kw_min" value="" type="text" class="form-control" id="kw_min" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="kva">{{"Apparent Power (Kva)"|trans}}</label>*/
/*               <input data-validation="required number" data-validation-error-msg="{{"You have not answered all required number fields"|trans}}"  name="kva" value="" type="text" class="form-control" id="kva" placeholder=""  >*/
/*             </div> */
/*               */
/*            {# <div class="form-group">*/
/*               <label for="kw_real">{{"Real Power (Kw)"|trans}} maybe double?</label>*/
/*               <input name="kw_real" value="" type="text" class="form-control" id="kw_real" placeholder=""  >*/
/*             </div> #}*/
/*               */
/*             </div><!-- well -->  */
/*             {#  */
/*             <br>*/
/*             <h1 class="page-header">{{"Consumption Cost ($)"|trans}}</h1>*/
/*             <div class="well well-lg">*/
/*             */
/*             <h3>{{"First X Kws"|trans}}</h3>    */
/*                  */
/*             <div class="form-group">*/
/*               <label for="costA_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="costA_dateFrom" value="" type="text" class="form-control" id="costA_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="costA_dateTo" value="" type="text" class="form-control" id="costA_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_Kw">{{"Consumption"|trans}}</label>*/
/*               <input name="costA_Kw" value="" type="text" class="form-control" id="costA_Kw" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_Amount">{{"Consumption amount"|trans}}</label>*/
/*               <input name="costA_Amount" value="" type="text" class="form-control" id="costA_Amount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_division_days">{{"Days number"|trans}}</label>*/
/*               <input name="costA_division_days" value="" type="text" class="form-control" id="costA_division_days" placeholder=""  >*/
/*             </div>*/
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_total_ammount">{{"Total amount"|trans}}</label>*/
/*               <input name="costA_total_ammount" value="" type="text" class="form-control" id="costA_total_ammount" placeholder=""  >*/
/*             </div>*/
/*               */
/*             <hr>*/
/*             */
/*              <h3>{{"Rest of Kws"|trans}}</h3>  */
/*             */
/*              <div class="form-group">*/
/*               <label for="costB_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="costB_dateFrom" value="" type="text" class="form-control" id="costB_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="costB_dateTo" value="" type="text" class="form-control" id="costB_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_Kw">{{"Consumption"|trans}}</label>*/
/*               <input name="costB_Kw" value="" type="text" class="form-control" id="costB_Kw" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_Amount">{{"Consumption amount"|trans}}</label>*/
/*               <input name="costB_Amount" value="" type="text" class="form-control" id="costB_Amount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_division_days">{{"Days number"|trans}}</label>*/
/*               <input name="costB_division_days" value="" type="text" class="form-control" id="costB_division_days" placeholder=""  >*/
/*             </div>*/
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_total_ammount">{{"Total amount"|trans}}</label>*/
/*               <input name="costB_total_ammount" value="" type="text" class="form-control" id="costB_total_ammount" placeholder=""  >*/
/*             </div>*/
/*             */
/*             <hr>*/
/*             */
/*             <div class="form-group">*/
/*               <label for="costApluscostB">{{"Total Consumption Amount"|trans}}</label>*/
/*               <input name="costApluscostB" value="" type="text" class="form-control" id="costApluscostB" placeholder=""  >*/
/*             </div> */
/*               */
/*               */
/*               */
/*               */
/*               */
/*               */
/*               */
/*             </div> <!-- well -->*/
/*             */
/*             <br>*/
/*             <h1 class="page-header">{{"Total consumption"|trans}}</h1>*/
/*             <div class="well well-lg">*/
/*                  */
/*                  */
/*              <h3>{{"First X Kwhs"|trans}}</h3>    */
/*                  */
/*             <div class="form-group">*/
/*               <label for="consf_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="consf_dateFrom" value="" type="text" class="form-control" id="consf_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="consf_dateTo" value="" type="text" class="form-control" id="consf_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_Kwh">{{"Consumption KWh"|trans}}</label>*/
/*               <input name="consf_Kwh" value="" type="text" class="form-control" id="consf_Kwh" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_ammount">{{"Consumption amount"|trans}}</label>*/
/*               <input name="consf_ammount" value="" type="text" class="form-control" id="consf_ammount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_total_ammount">{{"Total amount"|trans}}</label>*/
/*               <input name="consf_total_ammount" value="" type="text" class="form-control" id="consf_total_ammount" placeholder=""  >*/
/*             </div>*/
/*               */
/*             */
/*               */
/*             <hr>*/
/*             */
/*              <h3>{{"Rest of Kwhs"|trans}}</h3>  */
/*             */
/*              <div class="form-group">*/
/*               <label for="consr_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="consr_dateFrom" value="" type="text" class="form-control" id="consr_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="consr_dateTo" value="" type="text" class="form-control" id="consr_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_Kwh">{{"Consumption KWh"|trans}}</label>*/
/*               <input name="consr_Kwh" value="" type="text" class="form-control" id="consr_Kwh" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_ammount">{{"Consumption amount"|trans}}</label>*/
/*               <input name="consr_ammount" value="" type="text" class="form-control" id="consr_ammount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_total_ammount">{{"Total amount"|trans}}</label>*/
/*               <input name="consr_total_ammount" value="" type="text" class="form-control" id="consr_total_ammount" placeholder=""  >*/
/*             </div>*/
/*             */
/*             <hr>*/
/*             */
/*             <div class="form-group">*/
/*               <label for="consf_consr_visilec_ammount">{{"Service Visilec"|trans}}</label>*/
/*               <input name="consf_consr_visilec_ammount" value="" type="text" class="form-control" id="consf_consr_visilec_ammount" placeholder=""  >*/
/*             </div>*/
/*             */
/*             */
/*             <div class="form-group">*/
/*               <label for="consf_plus_consr">{{"Total Consumption Amount"|trans}}</label>*/
/*               <input name="consf_plus_consr" value="" type="text" class="form-control" id="consf_plus_consr" placeholder=""  >*/
/*             </div> */
/*                 */
/*                 */
/*                 */
/*                 */
/*                 */
/*             </div> <!-- well -->*/
/*               */
/*             #}*/
/*            <a href="{{path("_clients_bills", { 'client_id': client_id })}}" class="btn btn-default btn-lg pull-right ">{{"Cancel"|trans}}</a>*/
/*             <input type="submit" class="btn btn-primary btn-lg pull-right" value="{{"Submit"|trans}}"/>*/
/*           */
/*             </div>*/
/*           </form>*/
/*           */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/* <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/cupertino/jquery-ui.css">*/
/* <link rel="stylesheet" href="{{ asset('libs/timepicker/jquery-ui-timepicker-addon.css')}}">     */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* {% block javascripts %}*/
/* {{parent()}}*/
/* <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>*/
/* */
/* <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>*/
/* <script src="{{ asset('libs/timepicker/jquery-ui-timepicker-addon.js')}}"></script>*/
/* */
/* <script> $.validate(); </script>*/
/* */
/* <script>*/
/*  $(document).ready(function() {         */
/*         */
/*                     $('#date_from').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#date_to" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });    */
/*                     $('#date_to').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#date_from" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });*/
/*                     $('#costA_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costA_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#costA_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costA_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#costB_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costB_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });      */
/*                     $('#costB_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costB_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });  */
/*                     $('#consf_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consf_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#consf_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consf_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });*/
/*                     $('#consr_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consr_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#consr_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consr_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         }); */
/*                     */
/*             });*/
/*             */
/* */
/*               */
/*         */
/* </script>  */
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
