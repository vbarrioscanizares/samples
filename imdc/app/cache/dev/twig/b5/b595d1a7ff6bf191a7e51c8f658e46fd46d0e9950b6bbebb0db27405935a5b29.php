<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_becd3b382202ce73f2bf0eb34b2de9da819ecd39129936cc40f90b89ea26bf2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ad5a023c3d880d3e1b333addbeca820041533681bb17a4c105218bac7ec69a1d = $this->env->getExtension("native_profiler");
        $__internal_ad5a023c3d880d3e1b333addbeca820041533681bb17a4c105218bac7ec69a1d->enter($__internal_ad5a023c3d880d3e1b333addbeca820041533681bb17a4c105218bac7ec69a1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ad5a023c3d880d3e1b333addbeca820041533681bb17a4c105218bac7ec69a1d->leave($__internal_ad5a023c3d880d3e1b333addbeca820041533681bb17a4c105218bac7ec69a1d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_d2eeab8fdffc0db0a4771f12273b87d344f790f83511350de0eb9ce884e6ab62 = $this->env->getExtension("native_profiler");
        $__internal_d2eeab8fdffc0db0a4771f12273b87d344f790f83511350de0eb9ce884e6ab62->enter($__internal_d2eeab8fdffc0db0a4771f12273b87d344f790f83511350de0eb9ce884e6ab62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_d2eeab8fdffc0db0a4771f12273b87d344f790f83511350de0eb9ce884e6ab62->leave($__internal_d2eeab8fdffc0db0a4771f12273b87d344f790f83511350de0eb9ce884e6ab62_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_e1c04db72217db8cc483568cdc5be0e2a8bba9b3e22e97698d89064f067b5277 = $this->env->getExtension("native_profiler");
        $__internal_e1c04db72217db8cc483568cdc5be0e2a8bba9b3e22e97698d89064f067b5277->enter($__internal_e1c04db72217db8cc483568cdc5be0e2a8bba9b3e22e97698d89064f067b5277_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_e1c04db72217db8cc483568cdc5be0e2a8bba9b3e22e97698d89064f067b5277->leave($__internal_e1c04db72217db8cc483568cdc5be0e2a8bba9b3e22e97698d89064f067b5277_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_4a683ead7626d1be74f0e584ea42685c303d047c7534d539303f4a26993e3784 = $this->env->getExtension("native_profiler");
        $__internal_4a683ead7626d1be74f0e584ea42685c303d047c7534d539303f4a26993e3784->enter($__internal_4a683ead7626d1be74f0e584ea42685c303d047c7534d539303f4a26993e3784_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_4a683ead7626d1be74f0e584ea42685c303d047c7534d539303f4a26993e3784->leave($__internal_4a683ead7626d1be74f0e584ea42685c303d047c7534d539303f4a26993e3784_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
