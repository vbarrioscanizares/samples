<?php

/* clients_power_presets.html.twig */
class __TwigTemplate_e361731a8b6ea4c5945868f90c81e09401886eeedc259d6e21e9e9b270613a43 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_power_presets.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5284cc5eb5bafb12c5a87971841c0f57612c2b65a38039afeca40b18e026c30a = $this->env->getExtension("native_profiler");
        $__internal_5284cc5eb5bafb12c5a87971841c0f57612c2b65a38039afeca40b18e026c30a->enter($__internal_5284cc5eb5bafb12c5a87971841c0f57612c2b65a38039afeca40b18e026c30a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_power_presets.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5284cc5eb5bafb12c5a87971841c0f57612c2b65a38039afeca40b18e026c30a->leave($__internal_5284cc5eb5bafb12c5a87971841c0f57612c2b65a38039afeca40b18e026c30a_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_8341a3f4170cd3f509a3517646379f5296cdb61ebe90a13cc6b603ea6ec3d191 = $this->env->getExtension("native_profiler");
        $__internal_8341a3f4170cd3f509a3517646379f5296cdb61ebe90a13cc6b603ea6ec3d191->enter($__internal_8341a3f4170cd3f509a3517646379f5296cdb61ebe90a13cc6b603ea6ec3d191_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 9
        $this->loadTemplate("sidebar_clients_v2.html.twig", "clients_power_presets.html.twig", 9)->display($context);
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_power_presets.html.twig", 13)->display(array_merge($context, array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()))));
        echo "  
    <div class=\" col-md-9 main\">
          
          <h1 class=\"page-header\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Power presets"), "html", null, true);
        echo "</h1>
          
          
          
          <div class=\"table-responsive\">
            <table class=\"table table-hover\">
               <thead>
                <tr>
";
        // line 25
        echo "                   
                   <th style=\"width:16%;\">";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</th>
                   <th style=\"width:16%;\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</th>
                   <th style=\"width:16%;\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Historic Power"), "html", null, true);
        echo " <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total power billed"), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></th>
                   <th style=\"width:16%;\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Available Power"), "html", null, true);
        echo " <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Sum of power in entries"), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></th>
                   <th style=\"width:16%;\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Minimal Power"), "html", null, true);
        echo " <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Maximal power in entries"), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></th>
                   <th style=\"width:16%;\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Preset"), "html", null, true);
        echo " <span data-toggle=\"tooltip\" data-placement=\"right\" title=\"[";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Historic Power"), "html", null, true);
        echo " - ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Available Power"), "html", null, true);
        echo "] ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("or Custom Value"), "html", null, true);
        echo "\" class=\"glyphicon glyphicon-info-sign\" aria-hidden=\"true\"></span></th>
                   <th style=\"width:4%;\"></th>
                </tr>
               </thead>  
            <tbody>
                
                
            ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["bills"]) ? $context["bills"] : $this->getContext($context, "bills")));
        foreach ($context['_seq'] as $context["_key"] => $context["bill"]) {
            // line 39
            echo "                    
                        <tr style=\"cursor:pointer;\">

";
            // line 43
            echo "                        
                        <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "date_from", array(), "array"), "html", null, true);
            echo "</td>
                        <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "date_to", array(), "array"), "html", null, true);
            echo "</td>
                        <td>";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "kw", array(), "array"), "html", null, true);
            echo "</td>
                        <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "total_entries", array(), "array"), "html", null, true);
            echo "</td><!-- Consumption available for entries E1+E2+E3. Set in Client-Edit Page-->
                        <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "max_entry", array(), "array"), "html", null, true);
            echo "</td><!-- Consumption minimal bigger between entries [E1,E2,E3]. Set in Client-Edit Page-->
                        <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["bill"], "bill_preset", array(), "array"), "html", null, true);
            echo "</td><!-- Third Column - Fourth Column or... one edited manually-->
                        
                        <td>
                            <a href='";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_active_power_presets_edit", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()), "bill_id" => $this->getAttribute($context["bill"], "id", array(), "array"))), "html", null, true);
            echo "'><span class=\"glyphicon glyphicon-pencil\"></span></a>      
                        </td>
                      </tr>
                    
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['bill'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "    
              
            
            
            </tbody>  
            </table>
               
          </div>
          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_8341a3f4170cd3f509a3517646379f5296cdb61ebe90a13cc6b603ea6ec3d191->leave($__internal_8341a3f4170cd3f509a3517646379f5296cdb61ebe90a13cc6b603ea6ec3d191_prof);

    }

    // line 74
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_fd34ad76bee8038c840ba0f814090c32661c9c03ca5f3b28015ec078d1b26909 = $this->env->getExtension("native_profiler");
        $__internal_fd34ad76bee8038c840ba0f814090c32661c9c03ca5f3b28015ec078d1b26909->enter($__internal_fd34ad76bee8038c840ba0f814090c32661c9c03ca5f3b28015ec078d1b26909_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 75
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    
";
        
        $__internal_fd34ad76bee8038c840ba0f814090c32661c9c03ca5f3b28015ec078d1b26909->leave($__internal_fd34ad76bee8038c840ba0f814090c32661c9c03ca5f3b28015ec078d1b26909_prof);

    }

    public function getTemplateName()
    {
        return "clients_power_presets.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 75,  193 => 74,  170 => 56,  159 => 52,  153 => 49,  149 => 48,  145 => 47,  141 => 46,  137 => 45,  133 => 44,  130 => 43,  125 => 39,  121 => 38,  105 => 31,  99 => 30,  93 => 29,  87 => 28,  81 => 27,  75 => 26,  72 => 25,  61 => 16,  55 => 13,  50 => 10,  48 => 9,  41 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients_v2.html.twig' %}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb.html.twig' with {'client_id': client.id}%}  */
/*     <div class=" col-md-9 main">*/
/*           */
/*           <h1 class="page-header">{{"Total Power presets"|trans}}</h1>*/
/*           */
/*           */
/*           */
/*           <div class="table-responsive">*/
/*             <table class="table table-hover">*/
/*                <thead>*/
/*                 <tr>*/
/* {#                   <th style="width:5%;">{{"Id"|trans}}</th>#}*/
/*                    */
/*                    <th style="width:16%;">{{"Date"|trans}} {{"from"|trans}}</th>*/
/*                    <th style="width:16%;">{{"Date"|trans}} {{"to"|trans}}</th>*/
/*                    <th style="width:16%;">{{"Historic Power"|trans}} <span data-toggle="tooltip" data-placement="right" title="{{"Total power billed"|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></th>*/
/*                    <th style="width:16%;">{{"Available Power"|trans}} <span data-toggle="tooltip" data-placement="right" title="{{"Sum of power in entries"|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></th>*/
/*                    <th style="width:16%;">{{"Minimal Power"|trans}} <span data-toggle="tooltip" data-placement="right" title="{{"Maximal power in entries"|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></th>*/
/*                    <th style="width:16%;">{{"Preset"|trans}} <span data-toggle="tooltip" data-placement="right" title="[{{"Historic Power"|trans}} - {{"Available Power"|trans}}] {{"or Custom Value"|trans}}" class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></th>*/
/*                    <th style="width:4%;"></th>*/
/*                 </tr>*/
/*                </thead>  */
/*             <tbody>*/
/*                 */
/*                 */
/*             {% for bill in bills %}*/
/*                     */
/*                         <tr style="cursor:pointer;">*/
/* */
/* {#                        <td>{{bill.id}}</td>#}*/
/*                         */
/*                         <td>{{bill["date_from"]}}</td>*/
/*                         <td>{{bill["date_to"]}}</td>*/
/*                         <td>{{bill["kw"]}}</td>*/
/*                         <td>{{bill["total_entries"]}}</td><!-- Consumption available for entries E1+E2+E3. Set in Client-Edit Page-->*/
/*                         <td>{{bill["max_entry"]}}</td><!-- Consumption minimal bigger between entries [E1,E2,E3]. Set in Client-Edit Page-->*/
/*                         <td>{{bill["bill_preset"]}}</td><!-- Third Column - Fourth Column or... one edited manually-->*/
/*                         */
/*                         <td>*/
/*                             <a href='{{path("_active_power_presets_edit", { 'client_id': client.id, 'bill_id':bill["id"] })}}'><span class="glyphicon glyphicon-pencil"></span></a>      */
/*                         </td>*/
/*                       </tr>*/
/*                     */
/*             {% endfor %}    */
/*               */
/*             */
/*             */
/*             </tbody>  */
/*             </table>*/
/*                */
/*           </div>*/
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/*     */
/* {% endblock %}*/
/* */
