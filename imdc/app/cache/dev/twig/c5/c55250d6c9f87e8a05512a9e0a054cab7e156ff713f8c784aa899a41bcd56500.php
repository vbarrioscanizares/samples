<?php

/* imdc.html.twig */
class __TwigTemplate_a8d849c5e85abbdae575d00d178474504d243ae9165d44b4bf88e7d33c5597bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8286d8d9794203384333fedf91c8b17f51d7254b8f77ace0e66772fd7f7c4483 = $this->env->getExtension("native_profiler");
        $__internal_8286d8d9794203384333fedf91c8b17f51d7254b8f77ace0e66772fd7f7c4483->enter($__internal_8286d8d9794203384333fedf91c8b17f51d7254b8f77ace0e66772fd7f7c4483_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "imdc.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        
        ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 14
        echo "        
        
    </head>
    <body  id=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), "html", null, true);
        echo "\">
        
     ";
        // line 20
        echo "    ";
        $context["params"] = twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method"), $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "all", array(), "method"));
        // line 21
        echo "    ";
        $context["frParams"] = twig_array_merge((isset($context["params"]) ? $context["params"] : $this->getContext($context, "params")), array("_locale" => "fr"));
        // line 22
        echo "    ";
        $context["enParams"] = twig_array_merge((isset($context["params"]) ? $context["params"] : $this->getContext($context, "params")), array("_locale" => "en"));
        // line 23
        echo "    ";
        $context["itParams"] = twig_array_merge((isset($context["params"]) ? $context["params"] : $this->getContext($context, "params")), array("_locale" => "it"));
        echo "   
        
        
    <nav class=\"navbar navbar-inverse navbar-fixed-top\">
      <div class=\"container-fluid\">
        <div class=\"navbar-header\">
          <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
            <span class=\"sr-only\">&nbsp;</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
          <a id=\"topbar_imdc\"  href=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("_root");
        echo "\"><span id=\"topbar_imdc\">IMDC 1.0 </span>by</a>
          <a id=\"topbar_logo\"  target=\"_blank\" href=\"http://powerquebec.ca\"><img src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/logo.png"), "html", null, true);
        echo "\" alt=\"\"/></a>
        
        
        
        
        </div>
        <div id=\"navbar\" >
          <ul class=\"nav navbar-nav navbar-right\">
           
              
            
              <li id=\"logout\">
                  
                    ";
        // line 49
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method") == "_login")) {
            // line 50
            echo "                
               
                    ";
        } else {
            // line 53
            echo "                        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("_login");
            echo "\">
                         <span>";
            // line 54
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Sign out"), "html", null, true);
            echo "</span>
                         <img src=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("img/logout.png"), "html", null, true);
            echo "\" alt=\"logout\" />
                        </a>    
                    ";
        }
        // line 58
        echo "                  
                   
              </li>
            
              
              
            <li id=\"langues\">
                        
                    <span><a ";
        // line 66
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "locale", array()) == "fr")) {
            echo " class=\"active\" ";
        }
        echo " href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), (isset($context["frParams"]) ? $context["frParams"] : $this->getContext($context, "frParams"))), "html", null, true);
        echo "\">FR</a>|</span>
                    <span><a ";
        // line 67
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "locale", array()) == "en")) {
            echo " class=\"active\" ";
        }
        echo " href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), (isset($context["enParams"]) ? $context["enParams"] : $this->getContext($context, "enParams"))), "html", null, true);
        echo "\">EN</a>|</span>
                    <span><a ";
        // line 68
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "locale", array()) == "it")) {
            echo " class=\"active\" ";
        }
        echo " href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method"), (isset($context["itParams"]) ? $context["itParams"] : $this->getContext($context, "itParams"))), "html", null, true);
        echo "\">IT</a></span>
                  
              </li>  
              
            <li></li>
            
            
            <li>
                                     
            </li>
          </ul>
          
        </div>
      </div>
    </nav>

        
    ";
        // line 85
        $this->displayBlock('content', $context, $blocks);
        echo "     
    
    ";
        // line 87
        $this->displayBlock('body', $context, $blocks);
        // line 88
        echo "        
        
        ";
        // line 90
        $this->displayBlock('javascripts', $context, $blocks);
        // line 98
        echo "    </body>
</html>
";
        
        $__internal_8286d8d9794203384333fedf91c8b17f51d7254b8f77ace0e66772fd7f7c4483->leave($__internal_8286d8d9794203384333fedf91c8b17f51d7254b8f77ace0e66772fd7f7c4483_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_573faca77f5a6eb259485de3a26d9d55fc33f4c291ccbafef809d8633e06d8fb = $this->env->getExtension("native_profiler");
        $__internal_573faca77f5a6eb259485de3a26d9d55fc33f4c291ccbafef809d8633e06d8fb->enter($__internal_573faca77f5a6eb259485de3a26d9d55fc33f4c291ccbafef809d8633e06d8fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "IMDC 1.0";
        
        $__internal_573faca77f5a6eb259485de3a26d9d55fc33f4c291ccbafef809d8633e06d8fb->leave($__internal_573faca77f5a6eb259485de3a26d9d55fc33f4c291ccbafef809d8633e06d8fb_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_1a2a3ab350bac4895e97844d8d01a4f2de063992f11fcfdfd51073066cc49bf7 = $this->env->getExtension("native_profiler");
        $__internal_1a2a3ab350bac4895e97844d8d01a4f2de063992f11fcfdfd51073066cc49bf7->enter($__internal_1a2a3ab350bac4895e97844d8d01a4f2de063992f11fcfdfd51073066cc49bf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 9
        echo "                <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
                <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/bootstrap-3.3.5/dist/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
                <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/bootstrap-3.3.5/docs/examples/dashboard/dashboard.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
                <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/recast.css"), "html", null, true);
        echo "\">
        ";
        
        $__internal_1a2a3ab350bac4895e97844d8d01a4f2de063992f11fcfdfd51073066cc49bf7->leave($__internal_1a2a3ab350bac4895e97844d8d01a4f2de063992f11fcfdfd51073066cc49bf7_prof);

    }

    // line 85
    public function block_content($context, array $blocks = array())
    {
        $__internal_02beb17e51841bb5153cf27fcd0533ace1a87bbd734100e36049cce2bfcf0f3c = $this->env->getExtension("native_profiler");
        $__internal_02beb17e51841bb5153cf27fcd0533ace1a87bbd734100e36049cce2bfcf0f3c->enter($__internal_02beb17e51841bb5153cf27fcd0533ace1a87bbd734100e36049cce2bfcf0f3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        echo "      ";
        
        $__internal_02beb17e51841bb5153cf27fcd0533ace1a87bbd734100e36049cce2bfcf0f3c->leave($__internal_02beb17e51841bb5153cf27fcd0533ace1a87bbd734100e36049cce2bfcf0f3c_prof);

    }

    // line 87
    public function block_body($context, array $blocks = array())
    {
        $__internal_b976a69a43e6aa3b394aadec4008799ae06015a2fac03f38a56de8896223dceb = $this->env->getExtension("native_profiler");
        $__internal_b976a69a43e6aa3b394aadec4008799ae06015a2fac03f38a56de8896223dceb->enter($__internal_b976a69a43e6aa3b394aadec4008799ae06015a2fac03f38a56de8896223dceb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b976a69a43e6aa3b394aadec4008799ae06015a2fac03f38a56de8896223dceb->leave($__internal_b976a69a43e6aa3b394aadec4008799ae06015a2fac03f38a56de8896223dceb_prof);

    }

    // line 90
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_805f3785ecea3a4899ff5429c823b5568790bd697393425190cac08aa41b77b4 = $this->env->getExtension("native_profiler");
        $__internal_805f3785ecea3a4899ff5429c823b5568790bd697393425190cac08aa41b77b4->enter($__internal_805f3785ecea3a4899ff5429c823b5568790bd697393425190cac08aa41b77b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 91
        echo "                <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/jQuery/jQuery-2.1.4.min.js"), "html", null, true);
        echo "\"></script>
                
                
                <script src=\"";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/bootstrap-3.3.5/dist/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
                <script src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/bootstrap-3.3.5/docs/assets/js/vendor/holder.min.js"), "html", null, true);
        echo "\"></script>
               
        ";
        
        $__internal_805f3785ecea3a4899ff5429c823b5568790bd697393425190cac08aa41b77b4->leave($__internal_805f3785ecea3a4899ff5429c823b5568790bd697393425190cac08aa41b77b4_prof);

    }

    public function getTemplateName()
    {
        return "imdc.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  270 => 95,  266 => 94,  259 => 91,  253 => 90,  242 => 87,  230 => 85,  221 => 12,  217 => 11,  213 => 10,  208 => 9,  202 => 8,  190 => 6,  181 => 98,  179 => 90,  175 => 88,  173 => 87,  168 => 85,  144 => 68,  136 => 67,  128 => 66,  118 => 58,  112 => 55,  108 => 54,  103 => 53,  98 => 50,  96 => 49,  80 => 36,  76 => 35,  60 => 23,  57 => 22,  54 => 21,  51 => 20,  46 => 17,  41 => 14,  39 => 8,  34 => 6,  27 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">*/
/*         <title>{% block title %}IMDC 1.0{% endblock %}</title>*/
/*         */
/*         {% block stylesheets %}*/
/*                 <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*                 <link href="{{asset('libs/bootstrap-3.3.5/dist/css/bootstrap.min.css')}}" rel="stylesheet">*/
/*                 <link href="{{asset('libs/bootstrap-3.3.5/docs/examples/dashboard/dashboard.css')}}" rel="stylesheet">*/
/*                 <link rel="stylesheet" href="{{asset('css/recast.css')}}">*/
/*         {% endblock %}*/
/*         */
/*         */
/*     </head>*/
/*     <body  id="{{ app.request.attributes.get('_route')}}">*/
/*         */
/*      {# language#}*/
/*     {% set params = app.request.attributes.get('_route_params')|merge(app.request.query.all()) %}*/
/*     {% set frParams = params|merge({'_locale': 'fr'}) %}*/
/*     {% set enParams = params|merge({'_locale': 'en'}) %}*/
/*     {% set itParams = params|merge({'_locale': 'it'}) %}   */
/*         */
/*         */
/*     <nav class="navbar navbar-inverse navbar-fixed-top">*/
/*       <div class="container-fluid">*/
/*         <div class="navbar-header">*/
/*           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">*/
/*             <span class="sr-only">&nbsp;</span>*/
/*             <span class="icon-bar"></span>*/
/*             <span class="icon-bar"></span>*/
/*             <span class="icon-bar"></span>*/
/*           </button>*/
/*           <a id="topbar_imdc"  href="{{path("_root")}}"><span id="topbar_imdc">IMDC 1.0 </span>by</a>*/
/*           <a id="topbar_logo"  target="_blank" href="http://powerquebec.ca"><img src="{{asset('img/logo.png')}}" alt=""/></a>*/
/*         */
/*         */
/*         */
/*         */
/*         </div>*/
/*         <div id="navbar" >*/
/*           <ul class="nav navbar-nav navbar-right">*/
/*            */
/*               */
/*             */
/*               <li id="logout">*/
/*                   */
/*                     {%if app.request.attributes.get('_route') == "_login"%}*/
/*                 */
/*                */
/*                     {%else%}*/
/*                         <a href="{{path('_login')}}">*/
/*                          <span>{{"Sign out"|trans}}</span>*/
/*                          <img src="{{asset('img/logout.png')}}" alt="logout" />*/
/*                         </a>    */
/*                     {%endif%}*/
/*                   */
/*                    */
/*               </li>*/
/*             */
/*               */
/*               */
/*             <li id="langues">*/
/*                         */
/*                     <span><a {%if app.request.locale == "fr"%} class="active" {%endif%} href="{{ path(app.request.attributes.get('_route'), frParams) }}">FR</a>|</span>*/
/*                     <span><a {%if app.request.locale == "en"%} class="active" {%endif%} href="{{ path(app.request.attributes.get('_route'), enParams) }}">EN</a>|</span>*/
/*                     <span><a {%if app.request.locale == "it"%} class="active" {%endif%} href="{{ path(app.request.attributes.get('_route'), itParams) }}">IT</a></span>*/
/*                   */
/*               </li>  */
/*               */
/*             <li></li>*/
/*             */
/*             */
/*             <li>*/
/*                                      */
/*             </li>*/
/*           </ul>*/
/*           */
/*         </div>*/
/*       </div>*/
/*     </nav>*/
/* */
/*         */
/*     {% block content %}      {% endblock %}     */
/*     */
/*     {% block body %}{% endblock %}*/
/*         */
/*         */
/*         {% block javascripts %}*/
/*                 <script src="{{asset('libs/jQuery/jQuery-2.1.4.min.js')}}"></script>*/
/*                 */
/*                 */
/*                 <script src="{{asset('libs/bootstrap-3.3.5/dist/js/bootstrap.min.js')}}"></script>*/
/*                 <script src="{{asset('libs/bootstrap-3.3.5/docs/assets/js/vendor/holder.min.js')}}"></script>*/
/*                */
/*         {% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
