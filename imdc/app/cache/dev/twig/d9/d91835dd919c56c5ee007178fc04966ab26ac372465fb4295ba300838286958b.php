<?php

/* sidebar_clients_m.html.twig */
class __TwigTemplate_cfe95251479f286eb794db494d86472f855d01ab2256cd8bbd01f6dec0f9e82d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d6fac07a91fb03893f197fa3acf3a57b23b1bb23375068b6678cb9af11b6244b = $this->env->getExtension("native_profiler");
        $__internal_d6fac07a91fb03893f197fa3acf3a57b23b1bb23375068b6678cb9af11b6244b->enter($__internal_d6fac07a91fb03893f197fa3acf3a57b23b1bb23375068b6678cb9af11b6244b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "sidebar_clients_m.html.twig"));

        // line 1
        $context["role"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "role"), "method");
        // line 2
        echo "
        <div class=\"col-sm-3 col-md-3 _sidebar\">
            
            
         <div class=\"col-md-12\"><!-- CLIENT INFO-->
             
            <img class='img-responsive center-block' id=\"client_logo\"  style='max-height: 200px;' src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("upload/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getLogoUrl", array(), "method"), "html", null, true);
        echo "\" alt=\"logo\"/>
            ";
        // line 10
        echo "            
            <h2 id=\"client_name\" class='text-center'>";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "name", array()), "html", null, true);
        echo "</h2>
            
            <h5 class='text-center' id=\"client_address\">
                ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["addresses"]) ? $context["addresses"] : $this->getContext($context, "addresses")));
        foreach ($context['_seq'] as $context["_key"] => $context["address"]) {
            // line 15
            echo "                    ";
            echo twig_escape_filter($this->env, $context["address"], "html", null, true);
            echo " <br/>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['address'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "            </h5>
            
            <h6 class='text-center' id=\"client_contact\">
                ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["contacts"]) ? $context["contacts"] : $this->getContext($context, "contacts")));
        foreach ($context['_seq'] as $context["_key"] => $context["contact"]) {
            // line 21
            echo "                    ";
            echo twig_escape_filter($this->env, $context["contact"], "html", null, true);
            echo " <br/>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contact'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "            </h6>
        </div>   
            
        <br>    
         
            
        <div class=\"col-md-12 billing\"><!-- Facturation-->
            
            <div class=\"panel panel-info\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title \">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Billing"), "html", null, true);
        echo " - Mai 2015</h3>
                  </div>
                  <div class=\"panel-body\">
                    <span class=\"label label-info\">5748.50\$</span>
                    <span class=\"label label-info\">400kWh</span>
                    <span class=\"label label-info\">14139.99\$</span>
                    <span class=\"label label-info\">300851kW</span>
                    <span class=\"label label-info\">19888.48\$</span>
                   
                  </div>
                
            </div>
            
        </div>     
            
            
         <div class=\"col-md-12 forecast\"><!-- Prevision-->
             <div class=\"panel panel-success\">
                 <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Forecast"), "html", null, true);
        echo " - Mai 2016</h3>
                  </div>
                  <div class=\"panel-body\">
                     <span class=\"label label-info\">5029.50\$</span>
                     <span class=\"label label-info\">350kWh</span>
                     <span class=\"label label-info\">12259.99\$</span>
                     <span class=\"label label-info\">260851kW</span>
                     <span class=\"label label-info\">17289.50\$</span>
                     
                  </div>
                 
                 
             </div>
            
        </div>        
            
           
                  
        <div class=\"col-md-12 video\">
            
            
            <div class=\"panel panel-info\">
                 <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Video"), "html", null, true);
        echo "</h3>
                  </div>
                  <div class=\"panel-body\">
                    <iframe width=\"100%\" src=\"https://www.youtube.com/embed/VYMjSule0Bw\" frameborder=\"0\" allowfullscreen></iframe>
                  </div>
                 
                 
             </div>
            
        
        </div>    
                  
                  
                  
        </div>
    
    
    
    ";
        
        $__internal_d6fac07a91fb03893f197fa3acf3a57b23b1bb23375068b6678cb9af11b6244b->leave($__internal_d6fac07a91fb03893f197fa3acf3a57b23b1bb23375068b6678cb9af11b6244b_prof);

    }

    public function getTemplateName()
    {
        return "sidebar_clients_m.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 75,  111 => 52,  89 => 33,  77 => 23,  68 => 21,  64 => 20,  59 => 17,  50 => 15,  46 => 14,  40 => 11,  37 => 10,  32 => 8,  24 => 2,  22 => 1,);
    }
}
/* {% set role = app.session.get('role') %}*/
/* */
/*         <div class="col-sm-3 col-md-3 _sidebar">*/
/*             */
/*             */
/*          <div class="col-md-12"><!-- CLIENT INFO-->*/
/*              */
/*             <img class='img-responsive center-block' id="client_logo"  style='max-height: 200px;' src="{{asset('upload/')}}{{client.getLogoUrl()}}" alt="logo"/>*/
/*             {#<img id="client_logo"  style='max-height: 200px;' src="https://image.freepik.com/free-vector/logo-sample-text_355-558.jpg" class='img-responsive center-block' alt='logo' />#}*/
/*             */
/*             <h2 id="client_name" class='text-center'>{{client.name}}</h2>*/
/*             */
/*             <h5 class='text-center' id="client_address">*/
/*                 {% for address in addresses%}*/
/*                     {{address}} <br/>*/
/*                 {% endfor %}*/
/*             </h5>*/
/*             */
/*             <h6 class='text-center' id="client_contact">*/
/*                 {% for contact in contacts%}*/
/*                     {{contact}} <br/>*/
/*                 {% endfor %}*/
/*             </h6>*/
/*         </div>   */
/*             */
/*         <br>    */
/*          */
/*             */
/*         <div class="col-md-12 billing"><!-- Facturation-->*/
/*             */
/*             <div class="panel panel-info">*/
/*                 <div class="panel-heading">*/
/*                     <h3 class="panel-title ">{{"Billing"|trans}} - Mai 2015</h3>*/
/*                   </div>*/
/*                   <div class="panel-body">*/
/*                     <span class="label label-info">5748.50$</span>*/
/*                     <span class="label label-info">400kWh</span>*/
/*                     <span class="label label-info">14139.99$</span>*/
/*                     <span class="label label-info">300851kW</span>*/
/*                     <span class="label label-info">19888.48$</span>*/
/*                    */
/*                   </div>*/
/*                 */
/*             </div>*/
/*             */
/*         </div>     */
/*             */
/*             */
/*          <div class="col-md-12 forecast"><!-- Prevision-->*/
/*              <div class="panel panel-success">*/
/*                  <div class="panel-heading">*/
/*                     <h3 class="panel-title">{{"Forecast"|trans}} - Mai 2016</h3>*/
/*                   </div>*/
/*                   <div class="panel-body">*/
/*                      <span class="label label-info">5029.50$</span>*/
/*                      <span class="label label-info">350kWh</span>*/
/*                      <span class="label label-info">12259.99$</span>*/
/*                      <span class="label label-info">260851kW</span>*/
/*                      <span class="label label-info">17289.50$</span>*/
/*                      */
/*                   </div>*/
/*                  */
/*                  */
/*              </div>*/
/*             */
/*         </div>        */
/*             */
/*            */
/*                   */
/*         <div class="col-md-12 video">*/
/*             */
/*             */
/*             <div class="panel panel-info">*/
/*                  <div class="panel-heading">*/
/*                     <h3 class="panel-title">{{"Video"|trans}}</h3>*/
/*                   </div>*/
/*                   <div class="panel-body">*/
/*                     <iframe width="100%" src="https://www.youtube.com/embed/VYMjSule0Bw" frameborder="0" allowfullscreen></iframe>*/
/*                   </div>*/
/*                  */
/*                  */
/*              </div>*/
/*             */
/*         */
/*         </div>    */
/*                   */
/*                   */
/*                   */
/*         </div>*/
/*     */
/*     */
/*     */
/*     */
