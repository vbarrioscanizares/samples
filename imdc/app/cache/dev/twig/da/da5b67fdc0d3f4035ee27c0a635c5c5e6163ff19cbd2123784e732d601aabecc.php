<?php

/* clients_bills_create.html.twig */
class __TwigTemplate_0187077ef1a0eda7a909bae70de471f4fca7f10d62ca879b5916d1d14eeefe3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_bills_create.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bda2f9c7a4478c56cf938c912d1fe7f9baf0b2ea3139cd05fee5d2fe810db576 = $this->env->getExtension("native_profiler");
        $__internal_bda2f9c7a4478c56cf938c912d1fe7f9baf0b2ea3139cd05fee5d2fe810db576->enter($__internal_bda2f9c7a4478c56cf938c912d1fe7f9baf0b2ea3139cd05fee5d2fe810db576_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_bills_create.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bda2f9c7a4478c56cf938c912d1fe7f9baf0b2ea3139cd05fee5d2fe810db576->leave($__internal_bda2f9c7a4478c56cf938c912d1fe7f9baf0b2ea3139cd05fee5d2fe810db576_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_5ed82ac4cb6a66703b44e72aebba8fe2a366ddb0c03967c5945265e60a35a0ba = $this->env->getExtension("native_profiler");
        $__internal_5ed82ac4cb6a66703b44e72aebba8fe2a366ddb0c03967c5945265e60a35a0ba->enter($__internal_5ed82ac4cb6a66703b44e72aebba8fe2a366ddb0c03967c5945265e60a35a0ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 9
        $this->loadTemplate("sidebar_clients_v2.html.twig", "clients_bills_create.html.twig", 9)->display($context);
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_bills_create.html.twig", 13)->display(array_merge($context, array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))));
        // line 14
        echo "    <div class=\" col-md-9 main\">
        
          <h1 class=\"page-header\">";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("New bill"), "html", null, true);
        echo "</h1>
          
          <form  action=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills_create", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
        echo "\" method=\"POST\" enctype=\"multipart/form-data\" >
              <input type=\"hidden\" value=\"1\" name=\"posting\">
              <input type=\"hidden\" value=\"\" name=\"ammount_total\">
              <input type=\"hidden\" value=\"\" name=\"filename\">
           
           <div class=\"well well-lg\">     
           <div class=\"control-group\">
                  <label class=\"control-label\" for=\"inputDocument\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("File"), "html", null, true);
        echo "</label>
                  <div class=\"controls\">
                    <span class=\"btn btn-file\">
                        ";
        // line 29
        echo "                        <input name=\"bill_file\" type=\"file\" id='inputDocument' accept=\"application/pdf\" ";
        echo "/>
                    </span>

                  </div>

              </div>   
            </div><!--well-->  
              
           <div class=\"well well-lg\">   
              
              
            <div class=\"form-group\">
              <label for=\"date_from\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input name=\"date_from\" value=\"\" type=\"text\" class=\"form-control\" id=\"date_from\" placeholder=\"\"  >
            </div>
              
              
             <div class=\"form-group\">
              <label for=\"date_to\">";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <input name=\"date_to\" value=\"\" type=\"text\" class=\"form-control\" id=\"date_to\" placeholder=\"\"  >
            </div>
              
              
             <div class=\"form-group\">
              <label for=\"days_no\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days number"), "html", null, true);
        echo "</label>
              <input name=\"days_no\" value=\"\" type=\"text\" class=\"form-control\" id=\"days_no\" placeholder=\"\"  >
            </div>
              
              
              <div class=\"form-group\">
              <label for=\"kwh\">";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total power consumption (KWh)"), "html", null, true);
        echo "</label>
              <input name=\"kwh\" value=\"\" type=\"text\" class=\"form-control\" id=\"kwh\" placeholder=\"\"  >
            </div> 
            
              
            <div class=\"form-group\">
              <label for=\"ammount_kwh\">";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Amount of total consumption (\$)"), "html", null, true);
        echo "</label>
              <input name=\"ammount_kwh\" value=\"\" type=\"text\" class=\"form-control\" id=\"ammount_kwh\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"kw\">";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total power billed (kW)"), "html", null, true);
        echo "</label>
              <input name=\"kw\" value=\"\" type=\"text\" class=\"form-control\" id=\"kw\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"ammount_kw\">";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Amount billed (\$)"), "html", null, true);
        echo "</label>
              <input name=\"ammount_kw\" value=\"\" type=\"text\" class=\"form-control\" id=\"ammount_kw\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"bill_type\">";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Bill type"), "html", null, true);
        echo "</label>
              <select name=\"bill_type\" class=\"form-control\">
                  <option value=\"A\">";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Actual"), "html", null, true);
        echo "</option>
                  <option value=\"E\">";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estimated"), "html", null, true);
        echo "</option>
              </select>    
             </div> 
              
              
            <div class=\"form-group\">
              <label for=\"tarif_type\">";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rate type"), "html", null, true);
        echo "</label>
              <select name=\"tarif_type\" class=\"form-control\">
                  <option value=\"M\">M</option>
                  <option value=\"G\">G</option>
                  <option value=\"D\">D</option>
              </select>    
             </div>   
              
            ";
        // line 102
        echo "  
            <div class=\"form-group\">
              <label for=\"fa\">";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power factor (Fa)"), "html", null, true);
        echo "</label>
              <input name=\"fa\" value=\"\" type=\"text\" class=\"form-control\" id=\"fa\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"fu\">";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Utilization Factor (Fu)"), "html", null, true);
        echo "</label>
              <input name=\"fu\" value=\"\" type=\"text\" class=\"form-control\" id=\"fu\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"kw_min\">";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Minimal power (kW)"), "html", null, true);
        echo "</label>
              <input name=\"kw_min\" value=\"\" type=\"text\" class=\"form-control\" id=\"kw_min\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"kva\">";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apparent Power (Kva)"), "html", null, true);
        echo "</label>
              <input name=\"kva\" value=\"\" type=\"text\" class=\"form-control\" id=\"kva\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"kw_real\">";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Real Power (Kw)"), "html", null, true);
        echo " maybe double?</label>
              <input name=\"kw_real\" value=\"\" type=\"text\" class=\"form-control\" id=\"kw_real\" placeholder=\"\"  >
            </div> 
              
            </div><!-- well -->  
              
            <br>
            <h1 class=\"page-header\">";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption Cost (\$)"), "html", null, true);
        echo "</h1>
            <div class=\"well well-lg\">
            
            <h3>";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("First X Kws"), "html", null, true);
        echo "</h3>    
                 
            <div class=\"form-group\">
              <label for=\"costA_dateFrom\">";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input name=\"costA_dateFrom\" value=\"\" type=\"text\" class=\"form-control\" id=\"costA_dateFrom\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costA_dateTo\">";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <input name=\"costA_dateTo\" value=\"\" type=\"text\" class=\"form-control\" id=\"costA_dateTo\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costA_Kw\">";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption"), "html", null, true);
        echo "</label>
              <input name=\"costA_Kw\" value=\"\" type=\"text\" class=\"form-control\" id=\"costA_Kw\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costA_Amount\">";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption amount"), "html", null, true);
        echo "</label>
              <input name=\"costA_Amount\" value=\"\" type=\"text\" class=\"form-control\" id=\"costA_Amount\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costA_division_days\">";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days number"), "html", null, true);
        echo "</label>
              <input name=\"costA_division_days\" value=\"\" type=\"text\" class=\"form-control\" id=\"costA_division_days\" placeholder=\"\"  >
            </div>
              
            <div class=\"form-group\">
              <label for=\"costA_total_ammount\">";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total amount"), "html", null, true);
        echo "</label>
              <input name=\"costA_total_ammount\" value=\"\" type=\"text\" class=\"form-control\" id=\"costA_total_ammount\" placeholder=\"\"  >
            </div>
              
            <hr>
            
             <h3>";
        // line 168
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rest of Kws"), "html", null, true);
        echo "</h3>  
            
             <div class=\"form-group\">
              <label for=\"costB_dateFrom\">";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input name=\"costB_dateFrom\" value=\"\" type=\"text\" class=\"form-control\" id=\"costB_dateFrom\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costB_dateTo\">";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <input name=\"costB_dateTo\" value=\"\" type=\"text\" class=\"form-control\" id=\"costB_dateTo\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costB_Kw\">";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption"), "html", null, true);
        echo "</label>
              <input name=\"costB_Kw\" value=\"\" type=\"text\" class=\"form-control\" id=\"costB_Kw\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costB_Amount\">";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption amount"), "html", null, true);
        echo "</label>
              <input name=\"costB_Amount\" value=\"\" type=\"text\" class=\"form-control\" id=\"costB_Amount\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"costB_division_days\">";
        // line 191
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Days number"), "html", null, true);
        echo "</label>
              <input name=\"costB_division_days\" value=\"\" type=\"text\" class=\"form-control\" id=\"costB_division_days\" placeholder=\"\"  >
            </div>
              
            <div class=\"form-group\">
              <label for=\"costB_total_ammount\">";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total amount"), "html", null, true);
        echo "</label>
              <input name=\"costB_total_ammount\" value=\"\" type=\"text\" class=\"form-control\" id=\"costB_total_ammount\" placeholder=\"\"  >
            </div>
            
            <hr>
            
            <div class=\"form-group\">
              <label for=\"costApluscostB\">";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Consumption Amount"), "html", null, true);
        echo "</label>
              <input name=\"costApluscostB\" value=\"\" type=\"text\" class=\"form-control\" id=\"costApluscostB\" placeholder=\"\"  >
            </div> 
              
              
              
              
              
              
              
            </div> <!-- well -->
            
            <br>
            <h1 class=\"page-header\">";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total consumption"), "html", null, true);
        echo "</h1>
            <div class=\"well well-lg\">
                 
                 
             <h3>";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("First X Kwhs"), "html", null, true);
        echo "</h3>    
                 
            <div class=\"form-group\">
              <label for=\"consf_dateFrom\">";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input name=\"consf_dateFrom\" value=\"\" type=\"text\" class=\"form-control\" id=\"consf_dateFrom\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consf_dateTo\">";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <input name=\"consf_dateTo\" value=\"\" type=\"text\" class=\"form-control\" id=\"consf_dateTo\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consf_Kwh\">";
        // line 233
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption KWh"), "html", null, true);
        echo "</label>
              <input name=\"consf_Kwh\" value=\"\" type=\"text\" class=\"form-control\" id=\"consf_Kwh\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consf_ammount\">";
        // line 238
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption amount"), "html", null, true);
        echo "</label>
              <input name=\"consf_ammount\" value=\"\" type=\"text\" class=\"form-control\" id=\"consf_ammount\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consf_total_ammount\">";
        // line 243
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total amount"), "html", null, true);
        echo "</label>
              <input name=\"consf_total_ammount\" value=\"\" type=\"text\" class=\"form-control\" id=\"consf_total_ammount\" placeholder=\"\"  >
            </div>
              
            
              
            <hr>
            
             <h3>";
        // line 251
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rest of Kwhs"), "html", null, true);
        echo "</h3>  
            
             <div class=\"form-group\">
              <label for=\"consr_dateFrom\">";
        // line 254
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("from"), "html", null, true);
        echo "</label>
              <input name=\"consr_dateFrom\" value=\"\" type=\"text\" class=\"form-control\" id=\"consr_dateFrom\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consr_dateTo\">";
        // line 259
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Date"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("to"), "html", null, true);
        echo "</label>
              <input name=\"consr_dateTo\" value=\"\" type=\"text\" class=\"form-control\" id=\"consr_dateTo\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consr_Kwh\">";
        // line 264
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption KWh"), "html", null, true);
        echo "</label>
              <input name=\"consr_Kwh\" value=\"\" type=\"text\" class=\"form-control\" id=\"consr_Kwh\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consr_ammount\">";
        // line 269
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Consumption amount"), "html", null, true);
        echo "</label>
              <input name=\"consr_ammount\" value=\"\" type=\"text\" class=\"form-control\" id=\"consr_ammount\" placeholder=\"\"  >
            </div> 
              
            <div class=\"form-group\">
              <label for=\"consr_total_ammount\">";
        // line 274
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total amount"), "html", null, true);
        echo "</label>
              <input name=\"consr_total_ammount\" value=\"\" type=\"text\" class=\"form-control\" id=\"consr_total_ammount\" placeholder=\"\"  >
            </div>
            
            <hr>
            
            <div class=\"form-group\">
              <label for=\"consf_consr_visilec_ammount\">";
        // line 281
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Service Visilec"), "html", null, true);
        echo "</label>
              <input name=\"consf_consr_visilec_ammount\" value=\"\" type=\"text\" class=\"form-control\" id=\"consf_consr_visilec_ammount\" placeholder=\"\"  >
            </div>
            
            
            <div class=\"form-group\">
              <label for=\"consf_plus_consr\">";
        // line 287
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Consumption Amount"), "html", null, true);
        echo "</label>
              <input name=\"consf_plus_consr\" value=\"\" type=\"text\" class=\"form-control\" id=\"consf_plus_consr\" placeholder=\"\"  >
            </div> 
                
                
                
                
                
            </div> <!-- well -->
              
            
            <a href=\"";
        // line 298
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
        echo "\" class=\"btn btn-default btn-lg pull-right \">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cancel"), "html", null, true);
        echo "</a>
            <input type=\"submit\" class=\"btn btn-primary btn-lg pull-right\" value=\"";
        // line 299
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\"/>
          
            </div>
          </form>
          
          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_5ed82ac4cb6a66703b44e72aebba8fe2a366ddb0c03967c5945265e60a35a0ba->leave($__internal_5ed82ac4cb6a66703b44e72aebba8fe2a366ddb0c03967c5945265e60a35a0ba_prof);

    }

    // line 316
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_88bb9890dd5ea3da979b4790651ab639f4df0f6df9bc08f5e0dd4830f38a6641 = $this->env->getExtension("native_profiler");
        $__internal_88bb9890dd5ea3da979b4790651ab639f4df0f6df9bc08f5e0dd4830f38a6641->enter($__internal_88bb9890dd5ea3da979b4790651ab639f4df0f6df9bc08f5e0dd4830f38a6641_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 317
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" href=\"http://code.jquery.com/ui/1.9.1/themes/cupertino/jquery-ui.css\">
<link rel=\"stylesheet\" href=\"";
        // line 319
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/timepicker/jquery-ui-timepicker-addon.css"), "html", null, true);
        echo "\">     
";
        
        $__internal_88bb9890dd5ea3da979b4790651ab639f4df0f6df9bc08f5e0dd4830f38a6641->leave($__internal_88bb9890dd5ea3da979b4790651ab639f4df0f6df9bc08f5e0dd4830f38a6641_prof);

    }

    // line 325
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_825a013bad37efb741b33d7d78815c5944ab7a7e03dab6d560df9bb5ebe98ded = $this->env->getExtension("native_profiler");
        $__internal_825a013bad37efb741b33d7d78815c5944ab7a7e03dab6d560df9bb5ebe98ded->enter($__internal_825a013bad37efb741b33d7d78815c5944ab7a7e03dab6d560df9bb5ebe98ded_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 326
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js\"></script>

<script src=\"//code.jquery.com/ui/1.11.4/jquery-ui.js\"></script>
<script src=\"";
        // line 330
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("libs/timepicker/jquery-ui-timepicker-addon.js"), "html", null, true);
        echo "\"></script>

<script> \$.validate(); </script>

<script>
 \$(document).ready(function() {         
        
                    \$('#date_from').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#date_to\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });    
                    \$('#date_to').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#date_from\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });
                    \$('#costA_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costA_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });   
                    \$('#costA_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costA_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });   
                    \$('#costB_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costB_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });      
                    \$('#costB_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#costB_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });  
                    \$('#consf_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consf_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });   
                    \$('#consf_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consf_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        });
                    \$('#consr_dateFrom').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consr_dateTo\" ).datepicker( \"option\", \"minDate\", selectedDate );
                          }
                        });   
                    \$('#consr_dateTo').datepicker({
                            addSliderAccess: true,
                            sliderAccessArgs: { touchonly: false },
                            changeMonth: true,
                          numberOfMonths: 3,
                          dateFormat: \"yy-mm-dd\",
                          timeFormat: 'HH:mm:ss',
                          onClose: function( selectedDate ) {
                            \$( \"#consr_dateFrom\" ).datepicker( \"option\", \"maxDate\", selectedDate );
                          }
                        }); 
                    
            });
            

              
        
</script>  


";
        
        $__internal_825a013bad37efb741b33d7d78815c5944ab7a7e03dab6d560df9bb5ebe98ded->leave($__internal_825a013bad37efb741b33d7d78815c5944ab7a7e03dab6d560df9bb5ebe98ded_prof);

    }

    public function getTemplateName()
    {
        return "clients_bills_create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  575 => 330,  568 => 326,  562 => 325,  553 => 319,  548 => 317,  542 => 316,  519 => 299,  513 => 298,  499 => 287,  490 => 281,  480 => 274,  472 => 269,  464 => 264,  454 => 259,  444 => 254,  438 => 251,  427 => 243,  419 => 238,  411 => 233,  401 => 228,  391 => 223,  385 => 220,  378 => 216,  362 => 203,  352 => 196,  344 => 191,  336 => 186,  328 => 181,  318 => 176,  308 => 171,  302 => 168,  293 => 162,  285 => 157,  277 => 152,  269 => 147,  259 => 142,  249 => 137,  243 => 134,  237 => 131,  227 => 124,  219 => 119,  211 => 114,  203 => 109,  195 => 104,  191 => 102,  180 => 89,  171 => 83,  167 => 82,  162 => 80,  154 => 75,  146 => 70,  138 => 65,  129 => 59,  120 => 53,  109 => 47,  98 => 41,  83 => 29,  77 => 25,  67 => 18,  62 => 16,  58 => 14,  56 => 13,  51 => 10,  49 => 9,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients_v2.html.twig' %}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb.html.twig' with {'client_id': client_id}%}*/
/*     <div class=" col-md-9 main">*/
/*         */
/*           <h1 class="page-header">{{"New bill"|trans}}</h1>*/
/*           */
/*           <form  action="{{path('_clients_bills_create',{ 'client_id': client_id})}}" method="POST" enctype="multipart/form-data" >*/
/*               <input type="hidden" value="1" name="posting">*/
/*               <input type="hidden" value="" name="ammount_total">*/
/*               <input type="hidden" value="" name="filename">*/
/*            */
/*            <div class="well well-lg">     */
/*            <div class="control-group">*/
/*                   <label class="control-label" for="inputDocument">{{"File"|trans}}</label>*/
/*                   <div class="controls">*/
/*                     <span class="btn btn-file">*/
/*                         {#<input type="hidden" name="MAX_FILE_SIZE" value="90000" />#}*/
/*                         <input name="bill_file" type="file" id='inputDocument' accept="application/pdf" {#<?=  (isset($query))?'value="default_upload"':'';?>  #}/>*/
/*                     </span>*/
/* */
/*                   </div>*/
/* */
/*               </div>   */
/*             </div><!--well-->  */
/*               */
/*            <div class="well well-lg">   */
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="date_from">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="date_from" value="" type="text" class="form-control" id="date_from" placeholder=""  >*/
/*             </div>*/
/*               */
/*               */
/*              <div class="form-group">*/
/*               <label for="date_to">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="date_to" value="" type="text" class="form-control" id="date_to" placeholder=""  >*/
/*             </div>*/
/*               */
/*               */
/*              <div class="form-group">*/
/*               <label for="days_no">{{"Days number"|trans}}</label>*/
/*               <input name="days_no" value="" type="text" class="form-control" id="days_no" placeholder=""  >*/
/*             </div>*/
/*               */
/*               */
/*               <div class="form-group">*/
/*               <label for="kwh">{{"Total power consumption (KWh)"|trans}}</label>*/
/*               <input name="kwh" value="" type="text" class="form-control" id="kwh" placeholder=""  >*/
/*             </div> */
/*             */
/*               */
/*             <div class="form-group">*/
/*               <label for="ammount_kwh">{{"Amount of total consumption ($)"|trans}}</label>*/
/*               <input name="ammount_kwh" value="" type="text" class="form-control" id="ammount_kwh" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="kw">{{"Total power billed (kW)"|trans}}</label>*/
/*               <input name="kw" value="" type="text" class="form-control" id="kw" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="ammount_kw">{{"Amount billed ($)"|trans}}</label>*/
/*               <input name="ammount_kw" value="" type="text" class="form-control" id="ammount_kw" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="bill_type">{{"Bill type"|trans}}</label>*/
/*               <select name="bill_type" class="form-control">*/
/*                   <option value="A">{{"Actual"|trans}}</option>*/
/*                   <option value="E">{{"Estimated"|trans}}</option>*/
/*               </select>    */
/*              </div> */
/*               */
/*               */
/*             <div class="form-group">*/
/*               <label for="tarif_type">{{"Rate type"|trans}}</label>*/
/*               <select name="tarif_type" class="form-control">*/
/*                   <option value="M">M</option>*/
/*                   <option value="G">G</option>*/
/*                   <option value="D">D</option>*/
/*               </select>    */
/*              </div>   */
/*               */
/*             {#  */
/*             <div class="form-group">*/
/*               <label for="dollar">{{"Amount billed ($)"|trans}} maybe double?</label>*/
/*               <input name="dollar" value="" type="text" class="form-control" id="dollar" placeholder=""  >*/
/*             </div> */
/*             #}  */
/*             <div class="form-group">*/
/*               <label for="fa">{{"Power factor (Fa)"|trans}}</label>*/
/*               <input name="fa" value="" type="text" class="form-control" id="fa" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="fu">{{"Utilization Factor (Fu)"|trans}}</label>*/
/*               <input name="fu" value="" type="text" class="form-control" id="fu" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="kw_min">{{"Minimal power (kW)"|trans}}</label>*/
/*               <input name="kw_min" value="" type="text" class="form-control" id="kw_min" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="kva">{{"Apparent Power (Kva)"|trans}}</label>*/
/*               <input name="kva" value="" type="text" class="form-control" id="kva" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="kw_real">{{"Real Power (Kw)"|trans}} maybe double?</label>*/
/*               <input name="kw_real" value="" type="text" class="form-control" id="kw_real" placeholder=""  >*/
/*             </div> */
/*               */
/*             </div><!-- well -->  */
/*               */
/*             <br>*/
/*             <h1 class="page-header">{{"Consumption Cost ($)"|trans}}</h1>*/
/*             <div class="well well-lg">*/
/*             */
/*             <h3>{{"First X Kws"|trans}}</h3>    */
/*                  */
/*             <div class="form-group">*/
/*               <label for="costA_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="costA_dateFrom" value="" type="text" class="form-control" id="costA_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="costA_dateTo" value="" type="text" class="form-control" id="costA_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_Kw">{{"Consumption"|trans}}</label>*/
/*               <input name="costA_Kw" value="" type="text" class="form-control" id="costA_Kw" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_Amount">{{"Consumption amount"|trans}}</label>*/
/*               <input name="costA_Amount" value="" type="text" class="form-control" id="costA_Amount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_division_days">{{"Days number"|trans}}</label>*/
/*               <input name="costA_division_days" value="" type="text" class="form-control" id="costA_division_days" placeholder=""  >*/
/*             </div>*/
/*               */
/*             <div class="form-group">*/
/*               <label for="costA_total_ammount">{{"Total amount"|trans}}</label>*/
/*               <input name="costA_total_ammount" value="" type="text" class="form-control" id="costA_total_ammount" placeholder=""  >*/
/*             </div>*/
/*               */
/*             <hr>*/
/*             */
/*              <h3>{{"Rest of Kws"|trans}}</h3>  */
/*             */
/*              <div class="form-group">*/
/*               <label for="costB_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="costB_dateFrom" value="" type="text" class="form-control" id="costB_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="costB_dateTo" value="" type="text" class="form-control" id="costB_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_Kw">{{"Consumption"|trans}}</label>*/
/*               <input name="costB_Kw" value="" type="text" class="form-control" id="costB_Kw" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_Amount">{{"Consumption amount"|trans}}</label>*/
/*               <input name="costB_Amount" value="" type="text" class="form-control" id="costB_Amount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_division_days">{{"Days number"|trans}}</label>*/
/*               <input name="costB_division_days" value="" type="text" class="form-control" id="costB_division_days" placeholder=""  >*/
/*             </div>*/
/*               */
/*             <div class="form-group">*/
/*               <label for="costB_total_ammount">{{"Total amount"|trans}}</label>*/
/*               <input name="costB_total_ammount" value="" type="text" class="form-control" id="costB_total_ammount" placeholder=""  >*/
/*             </div>*/
/*             */
/*             <hr>*/
/*             */
/*             <div class="form-group">*/
/*               <label for="costApluscostB">{{"Total Consumption Amount"|trans}}</label>*/
/*               <input name="costApluscostB" value="" type="text" class="form-control" id="costApluscostB" placeholder=""  >*/
/*             </div> */
/*               */
/*               */
/*               */
/*               */
/*               */
/*               */
/*               */
/*             </div> <!-- well -->*/
/*             */
/*             <br>*/
/*             <h1 class="page-header">{{"Total consumption"|trans}}</h1>*/
/*             <div class="well well-lg">*/
/*                  */
/*                  */
/*              <h3>{{"First X Kwhs"|trans}}</h3>    */
/*                  */
/*             <div class="form-group">*/
/*               <label for="consf_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="consf_dateFrom" value="" type="text" class="form-control" id="consf_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="consf_dateTo" value="" type="text" class="form-control" id="consf_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_Kwh">{{"Consumption KWh"|trans}}</label>*/
/*               <input name="consf_Kwh" value="" type="text" class="form-control" id="consf_Kwh" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_ammount">{{"Consumption amount"|trans}}</label>*/
/*               <input name="consf_ammount" value="" type="text" class="form-control" id="consf_ammount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consf_total_ammount">{{"Total amount"|trans}}</label>*/
/*               <input name="consf_total_ammount" value="" type="text" class="form-control" id="consf_total_ammount" placeholder=""  >*/
/*             </div>*/
/*               */
/*             */
/*               */
/*             <hr>*/
/*             */
/*              <h3>{{"Rest of Kwhs"|trans}}</h3>  */
/*             */
/*              <div class="form-group">*/
/*               <label for="consr_dateFrom">{{"Date"|trans}} {{"from"|trans}}</label>*/
/*               <input name="consr_dateFrom" value="" type="text" class="form-control" id="consr_dateFrom" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_dateTo">{{"Date"|trans}} {{"to"|trans}}</label>*/
/*               <input name="consr_dateTo" value="" type="text" class="form-control" id="consr_dateTo" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_Kwh">{{"Consumption KWh"|trans}}</label>*/
/*               <input name="consr_Kwh" value="" type="text" class="form-control" id="consr_Kwh" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_ammount">{{"Consumption amount"|trans}}</label>*/
/*               <input name="consr_ammount" value="" type="text" class="form-control" id="consr_ammount" placeholder=""  >*/
/*             </div> */
/*               */
/*             <div class="form-group">*/
/*               <label for="consr_total_ammount">{{"Total amount"|trans}}</label>*/
/*               <input name="consr_total_ammount" value="" type="text" class="form-control" id="consr_total_ammount" placeholder=""  >*/
/*             </div>*/
/*             */
/*             <hr>*/
/*             */
/*             <div class="form-group">*/
/*               <label for="consf_consr_visilec_ammount">{{"Service Visilec"|trans}}</label>*/
/*               <input name="consf_consr_visilec_ammount" value="" type="text" class="form-control" id="consf_consr_visilec_ammount" placeholder=""  >*/
/*             </div>*/
/*             */
/*             */
/*             <div class="form-group">*/
/*               <label for="consf_plus_consr">{{"Total Consumption Amount"|trans}}</label>*/
/*               <input name="consf_plus_consr" value="" type="text" class="form-control" id="consf_plus_consr" placeholder=""  >*/
/*             </div> */
/*                 */
/*                 */
/*                 */
/*                 */
/*                 */
/*             </div> <!-- well -->*/
/*               */
/*             */
/*             <a href="{{path("_clients_bills", { 'client_id': client_id })}}" class="btn btn-default btn-lg pull-right ">{{"Cancel"|trans}}</a>*/
/*             <input type="submit" class="btn btn-primary btn-lg pull-right" value="{{"Submit"|trans}}"/>*/
/*           */
/*             </div>*/
/*           </form>*/
/*           */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/* <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/cupertino/jquery-ui.css">*/
/* <link rel="stylesheet" href="{{ asset('libs/timepicker/jquery-ui-timepicker-addon.css')}}">     */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* {% block javascripts %}*/
/* {{parent()}}*/
/* <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>*/
/* */
/* <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>*/
/* <script src="{{ asset('libs/timepicker/jquery-ui-timepicker-addon.js')}}"></script>*/
/* */
/* <script> $.validate(); </script>*/
/* */
/* <script>*/
/*  $(document).ready(function() {         */
/*         */
/*                     $('#date_from').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#date_to" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });    */
/*                     $('#date_to').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#date_from" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });*/
/*                     $('#costA_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costA_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#costA_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costA_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#costB_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costB_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });      */
/*                     $('#costB_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#costB_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });  */
/*                     $('#consf_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consf_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#consf_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consf_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         });*/
/*                     $('#consr_dateFrom').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consr_dateTo" ).datepicker( "option", "minDate", selectedDate );*/
/*                           }*/
/*                         });   */
/*                     $('#consr_dateTo').datepicker({*/
/*                             addSliderAccess: true,*/
/*                             sliderAccessArgs: { touchonly: false },*/
/*                             changeMonth: true,*/
/*                           numberOfMonths: 3,*/
/*                           dateFormat: "yy-mm-dd",*/
/*                           timeFormat: 'HH:mm:ss',*/
/*                           onClose: function( selectedDate ) {*/
/*                             $( "#consr_dateFrom" ).datepicker( "option", "maxDate", selectedDate );*/
/*                           }*/
/*                         }); */
/*                     */
/*             });*/
/*             */
/* */
/*               */
/*         */
/* </script>  */
/* */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
