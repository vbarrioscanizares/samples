<?php

/* clients_edit.html.twig */
class __TwigTemplate_9eea4a862c856e4d7bd70d9a9c9f8574248e31ddd873b1aa27ae62e0fb7b9608 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("imdc.html.twig", "clients_edit.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "imdc.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b700300f9e1a6fdc7e91cfe805c0e422d86696520b17c56f0de2d9754accac5 = $this->env->getExtension("native_profiler");
        $__internal_4b700300f9e1a6fdc7e91cfe805c0e422d86696520b17c56f0de2d9754accac5->enter($__internal_4b700300f9e1a6fdc7e91cfe805c0e422d86696520b17c56f0de2d9754accac5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4b700300f9e1a6fdc7e91cfe805c0e422d86696520b17c56f0de2d9754accac5->leave($__internal_4b700300f9e1a6fdc7e91cfe805c0e422d86696520b17c56f0de2d9754accac5_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_b0730c44acebddeecb62f1b1fcbd2db995744e67ac93d339767b1ccd0ab53f24 = $this->env->getExtension("native_profiler");
        $__internal_b0730c44acebddeecb62f1b1fcbd2db995744e67ac93d339767b1ccd0ab53f24->enter($__internal_b0730c44acebddeecb62f1b1fcbd2db995744e67ac93d339767b1ccd0ab53f24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    
     
    
     <div class=\"container-fluid\">
      <div class=\"row\">
        ";
        // line 9
        $this->loadTemplate("sidebar_clients_v2.html.twig", "clients_edit.html.twig", 9)->display($context);
        // line 10
        echo "    
    
    
    ";
        // line 13
        $this->loadTemplate("clients_breadcrumb.html.twig", "clients_edit.html.twig", 13)->display(array_merge($context, array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()))));
        echo "  
    <div class=\"col-md-9 main\">
        
        
        <h1 class=\"page-header\">";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Edit"), "html", null, true);
        echo "</h1>

          
          <form  action=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_edit", array("client_id" => $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "id", array()))), "html", null, true);
        echo "\" method=\"POST\" enctype=\"multipart/form-data\">
            
            <div class=\"form-group\">
              <label for=\"name\">";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "</label>
              <input value=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "name", array()), "html", null, true);
        echo "\" name=\"name\" data-validation=\"required\" type=\"text\" class=\"form-control\" id=\"name\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "\"  data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">
            </div>
            
            
          ";
        // line 33
        echo "            
            
             <div class=\"control-group\">
                  <label class=\"control-label\" for=\"inputDocument\">";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Logo"), "html", null, true);
        echo "</label>
                  <div class=\"controls\">
                    <img id=\"logo_img\" class=\"img-thumbnail\" style=\"max-width:150px;\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("upload/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getLogoUrl", array(), "method"), "html", null, true);
        echo "\" alt=\"logo\"/>  
                    <span class=\"btn btn-file\">
                        ";
        // line 41
        echo "                       
                        <input id=\"\" onchange=\"hideLogoImg();\" name=\"logo\" type=\"file\"   id='inputDocument' accept=\"image/*\" />
                    </span>
                    
                  </div>

              </div>
              <br/>
            
           
            <div class=\"form-group\">
              <label for=\"server_ip\">";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Server IP"), "html", null, true);
        echo "</label>
              <input value=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getServerIp", array(), "method"), "html", null, true);
        echo "\" name=\"server_ip\" data-validation=\"required\" type=\"text\" class=\"form-control\" id=\"server_ip\" placeholder=\"127.0.0.1\"  data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">
            </div>
            
            
           
            <div class=\"form-group\">
              <label for=\"server_id\">";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Server ID"), "html", null, true);
        echo "</label>
              <input value=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "getServerId", array(), "method"), "html", null, true);
        echo "\" name=\"server_id\" data-validation=\"required\" type=\"text\" class=\"form-control\" id=\"server_id\" placeholder=\"\"  data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">
            </div>
            
            
            <div class=\"form-group\">
              <input value=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "address", array()), "html", null, true);
        echo "\" id=\"address\" name=\"address\" type=\"hidden\" value=\"\" data-validation=\"required\" data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">  
              <label for=\"address_item\">";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Address(es)"), "html", null, true);
        echo "</label>
              <a href=\"javascript:void(0);\" onclick=\"addAddress()\">
                    <span class=\" glyphicon glyphicon-plus\"></span>
                </a>
              
              
              <input name=\"address_item\" type=\"text\" class=\"form-control\" id=\"address_item\" placeholder=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("NUMBER, STREET, PROVINCE, ZIP"), "html", null, true);
        echo "\"  >
              
              <br/>
            
              <div id=\"address_list\" class=\"list-group\">
                ";
        // line 80
        echo "              </div>
            
            
            </div>
              
            
              
              
              
              <div class=\"form-group\">
              <input value=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["client"]) ? $context["client"] : $this->getContext($context, "client")), "contact", array()), "html", null, true);
        echo "\" id=\"contact\" name=\"contact\" type=\"hidden\" value=\"\" data-validation=\"required\" data-validation-error-msg=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("You have not answered all required fields"), "html", null, true);
        echo "\">  
              <label for=\"address_item\">";
        // line 91
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contact(s)"), "html", null, true);
        echo "</label>
              <a href=\"javascript:void(0);\" onclick=\"addContact()\">
                    <span class=\" glyphicon glyphicon-plus\"></span>
                </a>
              
              
              <input name=\"contact_item\" type=\"text\" class=\"form-control\" id=\"contact_item\" placeholder=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("TITLE, FULLNAME, PHONE NUMBER EXT, EMAIL"), "html", null, true);
        echo "\"  >
              
              <br/>
            
              <div id=\"contact_list\" class=\"list-group\">
                ";
        // line 105
        echo "              </div>
            
            
            </div>
              
            ";
        // line 191
        echo "              
         
            
             <h3>";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Entries"), "html", null, true);
        echo " <!-- <span data-toggle=\"tooltip\" data-placement=\"top\" title=\"\" class=\"glyphicon glyphicon-info-sign\"></span>--></h3>
              <div class=\"well well-lg form-inline\">
                  
                  <h4>";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("First Entry"), "html", null, true);
        echo "</h4>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry1_name\">";
        // line 199
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 200
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry1"]) ? $context["entry1"] : $this->getContext($context, "entry1")), "'name'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['first']['name']\" id=\"entry1_name\"   type=\"text\" class=\"form-control\" id=\"entry1_name\"  placeholder=\"Entry1\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry1_kw\">";
        // line 204
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power"), "html", null, true);
        echo " (Kw)</label>
                    <input value=\"";
        // line 205
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry1"]) ? $context["entry1"] : $this->getContext($context, "entry1")), "'kw'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['first']['kw']\" id=\"entry1_kw\"   type=\"text\" class=\"form-control\" id=\"entry1_kw\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry1_priority\">";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Priority"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 210
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry1"]) ? $context["entry1"] : $this->getContext($context, "entry1")), "'priority'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['first']['priority']\" id=\"entry1_kw\"   type=\"text\" class=\"form-control\" id=\"entry1_priority\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry1_percent\">";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percentage (%)"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 215
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry1"]) ? $context["entry1"] : $this->getContext($context, "entry1")), "'percent'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['first']['percent']\" id=\"entry1_percent\"   type=\"text\" class=\"form-control\" id=\"entry1_percent\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry1_temperature\">";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Heat alarm (C)"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 220
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry1"]) ? $context["entry1"] : $this->getContext($context, "entry1")), "'temperature'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['first']['temperature']\" id=\"entry1_temperature\"   type=\"text\" class=\"form-control\" id=\"entry1_temperature\"  placeholder=\"25\">
                  </div>
                  <p></p>
                                    
                  <h4>";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Second Entry"), "html", null, true);
        echo "</h4>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry2_name\">";
        // line 226
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 227
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry2"]) ? $context["entry2"] : $this->getContext($context, "entry2")), "'name'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['second']['name']\" id=\"entry2_name\"   type=\"text\" class=\"form-control\" id=\"entry1_name\"  placeholder=\"Entry2\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry2_kw\">";
        // line 231
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power"), "html", null, true);
        echo " (Kw)</label>
                    <input value=\"";
        // line 232
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry2"]) ? $context["entry2"] : $this->getContext($context, "entry2")), "'kw'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['second']['kw']\" id=\"entry2_kw\"   type=\"text\" class=\"form-control\" id=\"entry1_kw\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry2_priority\">";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Priority"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 237
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry2"]) ? $context["entry2"] : $this->getContext($context, "entry2")), "'priority'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['second']['priority']\"  type=\"text\" class=\"form-control\" id=\"entry2_priority\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry2_percent\">";
        // line 241
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percentage (%)"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 242
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry2"]) ? $context["entry2"] : $this->getContext($context, "entry2")), "'percent'", array(), "array"), "html", null, true);
        echo "\"  name=\"entries['second']['percent']\" id=\"entry2_percent\"   type=\"text\" class=\"form-control\" id=\"entry1_percent\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry2_temperature\">";
        // line 246
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Heat alarm (C)"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 247
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry2"]) ? $context["entry2"] : $this->getContext($context, "entry2")), "'temperature'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['second']['temperature']\" id=\"entry2_temperature\"   type=\"text\" class=\"form-control\" id=\"entry1_temperature\"  placeholder=\"25\">
                  </div>
                  <p></p>
                  
                  <h4>";
        // line 251
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Third Entry"), "html", null, true);
        echo "</h4>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry3_name\">";
        // line 253
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Name"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 254
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry3"]) ? $context["entry3"] : $this->getContext($context, "entry3")), "'name'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['third']['name']\" id=\"entry3_name\"   type=\"text\" class=\"form-control\" id=\"entry1_name\"  placeholder=\"Entry3\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry3_kw\">";
        // line 258
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Power"), "html", null, true);
        echo " (Kw)</label>
                    <input value=\"";
        // line 259
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry3"]) ? $context["entry3"] : $this->getContext($context, "entry3")), "'kw'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['third']['kw']\" id=\"entry3_kw\"   type=\"text\" class=\"form-control\" id=\"entry1_kw\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry3_priority\">";
        // line 263
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Priority"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 264
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry3"]) ? $context["entry3"] : $this->getContext($context, "entry3")), "'priority'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['third']['priority']\"  type=\"text\" class=\"form-control\" id=\"entry3_priority\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry3_percent\">";
        // line 268
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Percentage (%)"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 269
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry3"]) ? $context["entry3"] : $this->getContext($context, "entry3")), "'percent'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['third']['percent']\" id=\"entry3_percent\"   type=\"text\" class=\"form-control\" id=\"entry1_percent\"  placeholder=\"\">
                  </div>
                  <p></p>
                  <div class=\"form-group\">
                    <label style=\"min-width:200px;\" for=\"entry3_temperature\">";
        // line 273
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Heat alarm (C)"), "html", null, true);
        echo "</label>
                    <input value=\"";
        // line 274
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["entry3"]) ? $context["entry3"] : $this->getContext($context, "entry3")), "'temperature'", array(), "array"), "html", null, true);
        echo "\" name=\"entries['third']['temperature']\" id=\"entry3_temperature\"   type=\"text\" class=\"form-control\" id=\"entry1_temperature\"  placeholder=\"25\">
                  </div>
                  <p></p>
                
                  
              </div>
         
              
              
            <input type=\"submit\" class=\"btn btn-primary btn-lg pull-right\" value=\"";
        // line 283
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Submit"), "html", null, true);
        echo "\"/>
          </form>
          
          

          
        </div>
          
          
     </div>
    </div>      
          
          
";
        
        $__internal_b0730c44acebddeecb62f1b1fcbd2db995744e67ac93d339767b1ccd0ab53f24->leave($__internal_b0730c44acebddeecb62f1b1fcbd2db995744e67ac93d339767b1ccd0ab53f24_prof);

    }

    // line 298
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_88f32a15b311cd9f0a6fa93d49be6af184d44592079cfc56ffd88dbe92fdefc1 = $this->env->getExtension("native_profiler");
        $__internal_88f32a15b311cd9f0a6fa93d49be6af184d44592079cfc56ffd88dbe92fdefc1->enter($__internal_88f32a15b311cd9f0a6fa93d49be6af184d44592079cfc56ffd88dbe92fdefc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 299
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    
";
        
        $__internal_88f32a15b311cd9f0a6fa93d49be6af184d44592079cfc56ffd88dbe92fdefc1->leave($__internal_88f32a15b311cd9f0a6fa93d49be6af184d44592079cfc56ffd88dbe92fdefc1_prof);

    }

    // line 306
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_1d5a9aa03070eb3ba6194ecb11862f055ba2dfce6388e3a9b48e4ee41b48decd = $this->env->getExtension("native_profiler");
        $__internal_1d5a9aa03070eb3ba6194ecb11862f055ba2dfce6388e3a9b48e4ee41b48decd->enter($__internal_1d5a9aa03070eb3ba6194ecb11862f055ba2dfce6388e3a9b48e4ee41b48decd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 307
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script src=\"//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js\"></script>
<script> \$.validate(); </script>

<script>
 \$(document).ready(function() {
                  rebuildAddress();
                  rebuildContact();
            });
            
 function hideLogoImg(){
     \$(\"#logo_img\").hide();
 }           
        
</script>    


<script>
function addAddress(){
        var address_item    = \$(\"#address_item\").val();
        var address         = new Array();
        var address_list    = \$(\"#address\").val();
        
        if (address_item.length == 0){
            return 0;
        }
        
        if (address_list.length > 0){
            address = eval(address_list);
        }
        
        //ADD ADDRESS TO THE LIST
        address.push(address_item);
        
        //SAVE LIST IN  THE INPUT
        \$(\"#address\").val(JSON.stringify(address));
        
        //REBUILD THE LIST
        rebuildAddress();
        
        //REMOVE ADDRESS TEXT
        \$(\"#address_item\").val(\"\");
        
    }
    
    function deleteAddress(id){
        
        var address_list    = \$(\"#address\").val();
        var address = eval(address_list);
        
        //SUPPRIME THE ITEM FROM THE LIST
        address.splice(id, 1);
        
        //SAVE THE LIST WITHOUT THE ITEM
        \$(\"#address\").val(JSON.stringify(address));
        
        //REBUILD THE LIST
        rebuildAddress();
        
    }
    
    function rebuildAddress(){
         var address            = eval(\$(\"#address\").val());
         var id_item            = 0;
     
         
        if (typeof address === 'undefined'){
            return 0;
        }
        
         \$(\"#address_list\").html(\"\");
         address.forEach(function(item) {
             \$(\"#address_list\").append('<div class=\"list-group-item\">'+item+'<a onclick=\"deleteAddress('+id_item+');\" href=\"javascript:void(0);\" ><span class=\" pull-right glyphicon glyphicon-minus\"></span></a>');
             id_item++;
        //console.log(item);
        });
    }
        
    
</script>    



<script>
    function addContact(){
        var contact_item    = \$(\"#contact_item\").val();
        var contact         = new Array();
        var contact_list    = \$(\"#contact\").val();
        
        if (contact_item.length == 0){
            return 0;
        }
        
        if (contact_list.length > 0){
            contact = eval(contact_list);
        }
        
        //ADD CONTACT TO THE LIST
        contact.push(contact_item);
        
        //SAVE LIST IN  THE INPUT
        \$(\"#contact\").val(JSON.stringify(contact));
        
        //REBUILD THE LIST
        rebuildContact();
        
        //REMOVE CONTACT TEXT
        \$(\"#contact_item\").val(\"\");
        
    }
    
    function deleteContact(id){
        
        var contact_list    = \$(\"#contact\").val();
        var contacts = eval(contact_list);
        
        //SUPPRIME THE ITEM FROM THE LIST
        contacts.splice(id, 1);
        
        //SAVE THE LIST WITHOUT THE ITEM
        \$(\"#contact\").val(JSON.stringify(contacts));
        
        //REBUILD THE LIST
        rebuildContact();
        
    }
    
    function rebuildContact(){
         var contact            = eval(\$(\"#contact\").val());
         var id_item            = 0;
      
         
        if (typeof contact === 'undefined'){
            return 0;
        }
        
         \$(\"#contact_list\").html(\"\");
         contact.forEach(function(item) {
             \$(\"#contact_list\").append('<div class=\"list-group-item\">'+item+'<a onclick=\"deleteContact('+id_item+');\" href=\"javascript:void(0);\"><span class=\" pull-right glyphicon glyphicon-minus\"></span></a>');
             id_item++;
        //console.log(item);
        });
    }
    
    
    
</script>    

";
        
        $__internal_1d5a9aa03070eb3ba6194ecb11862f055ba2dfce6388e3a9b48e4ee41b48decd->leave($__internal_1d5a9aa03070eb3ba6194ecb11862f055ba2dfce6388e3a9b48e4ee41b48decd_prof);

    }

    public function getTemplateName()
    {
        return "clients_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  452 => 307,  446 => 306,  436 => 299,  430 => 298,  409 => 283,  397 => 274,  393 => 273,  386 => 269,  382 => 268,  375 => 264,  371 => 263,  364 => 259,  360 => 258,  353 => 254,  349 => 253,  344 => 251,  337 => 247,  333 => 246,  326 => 242,  322 => 241,  315 => 237,  311 => 236,  304 => 232,  300 => 231,  293 => 227,  289 => 226,  284 => 224,  277 => 220,  273 => 219,  266 => 215,  262 => 214,  255 => 210,  251 => 209,  244 => 205,  240 => 204,  233 => 200,  229 => 199,  224 => 197,  218 => 194,  213 => 191,  206 => 105,  198 => 97,  189 => 91,  183 => 90,  171 => 80,  163 => 72,  154 => 66,  148 => 65,  138 => 60,  134 => 59,  123 => 53,  119 => 52,  106 => 41,  100 => 38,  95 => 36,  90 => 33,  79 => 24,  75 => 23,  69 => 20,  63 => 17,  56 => 13,  51 => 10,  49 => 9,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'imdc.html.twig' %}*/
/* */
/* {% block content %}*/
/*     */
/*      */
/*     */
/*      <div class="container-fluid">*/
/*       <div class="row">*/
/*         {% include 'sidebar_clients_v2.html.twig' %}*/
/*     */
/*     */
/*     */
/*     {% include 'clients_breadcrumb.html.twig' with {'client_id': client.id}%}  */
/*     <div class="col-md-9 main">*/
/*         */
/*         */
/*         <h1 class="page-header">{{"Edit"|trans}}</h1>*/
/* */
/*           */
/*           <form  action="{{path('_clients_edit',{ 'client_id': client.id})}}" method="POST" enctype="multipart/form-data">*/
/*             */
/*             <div class="form-group">*/
/*               <label for="name">{{"Name"|trans}}</label>*/
/*               <input value="{{client.name}}" name="name" data-validation="required" type="text" class="form-control" id="name" placeholder="{{"Name"|trans}}"  data-validation-error-msg="{{"You have not answered all required fields"|trans}}">*/
/*             </div>*/
/*             */
/*             */
/*           {#  <div class="form-group">*/
/*                 <label for="logo_url">{{"Logo"|trans}}</label>*/
/*                 <input type="file" id="logo_url">*/
/*                 <p class="help-block">Example block-level help text here.</p>*/
/*             </div>#}*/
/*             */
/*             */
/*              <div class="control-group">*/
/*                   <label class="control-label" for="inputDocument">{{"Logo"|trans}}</label>*/
/*                   <div class="controls">*/
/*                     <img id="logo_img" class="img-thumbnail" style="max-width:150px;" src="{{asset('upload/')}}{{client.getLogoUrl()}}" alt="logo"/>  */
/*                     <span class="btn btn-file">*/
/*                         {#<input type="hidden" name="MAX_FILE_SIZE" value="90000" />#}*/
/*                        */
/*                         <input id="" onchange="hideLogoImg();" name="logo" type="file"   id='inputDocument' accept="image/*" />*/
/*                     </span>*/
/*                     */
/*                   </div>*/
/* */
/*               </div>*/
/*               <br/>*/
/*             */
/*            */
/*             <div class="form-group">*/
/*               <label for="server_ip">{{"Server IP"|trans}}</label>*/
/*               <input value="{{client.getServerIp()}}" name="server_ip" data-validation="required" type="text" class="form-control" id="server_ip" placeholder="127.0.0.1"  data-validation-error-msg="{{"You have not answered all required fields"|trans}}">*/
/*             </div>*/
/*             */
/*             */
/*            */
/*             <div class="form-group">*/
/*               <label for="server_id">{{"Server ID"|trans}}</label>*/
/*               <input value="{{client.getServerId()}}" name="server_id" data-validation="required" type="text" class="form-control" id="server_id" placeholder=""  data-validation-error-msg="{{"You have not answered all required fields"|trans}}">*/
/*             </div>*/
/*             */
/*             */
/*             <div class="form-group">*/
/*               <input value="{{client.address}}" id="address" name="address" type="hidden" value="" data-validation="required" data-validation-error-msg="{{"You have not answered all required fields"|trans}}">  */
/*               <label for="address_item">{{"Address(es)"|trans}}</label>*/
/*               <a href="javascript:void(0);" onclick="addAddress()">*/
/*                     <span class=" glyphicon glyphicon-plus"></span>*/
/*                 </a>*/
/*               */
/*               */
/*               <input name="address_item" type="text" class="form-control" id="address_item" placeholder="{{"NUMBER, STREET, PROVINCE, ZIP"|trans}}"  >*/
/*               */
/*               <br/>*/
/*             */
/*               <div id="address_list" class="list-group">*/
/*                 {#<a href="#" class="list-group-item">Dapibus ac facilisis in*/
/*                     <span class=" pull-right glyphicon glyphicon-minus"></span>*/
/*                 </a>#}*/
/*               </div>*/
/*             */
/*             */
/*             </div>*/
/*               */
/*             */
/*               */
/*               */
/*               */
/*               <div class="form-group">*/
/*               <input value="{{client.contact}}" id="contact" name="contact" type="hidden" value="" data-validation="required" data-validation-error-msg="{{"You have not answered all required fields"|trans}}">  */
/*               <label for="address_item">{{"Contact(s)"|trans}}</label>*/
/*               <a href="javascript:void(0);" onclick="addContact()">*/
/*                     <span class=" glyphicon glyphicon-plus"></span>*/
/*                 </a>*/
/*               */
/*               */
/*               <input name="contact_item" type="text" class="form-control" id="contact_item" placeholder="{{"TITLE, FULLNAME, PHONE NUMBER EXT, EMAIL"|trans}}"  >*/
/*               */
/*               <br/>*/
/*             */
/*               <div id="contact_list" class="list-group">*/
/*                 {#<a href="#" class="list-group-item">Dapibus ac facilisis in*/
/*                     <span class=" pull-right glyphicon glyphicon-minus"></span>*/
/*                 </a>#}*/
/*               </div>*/
/*             */
/*             */
/*             </div>*/
/*               */
/*             {#<h3>{{"Previous bills"|trans}}  <span data-toggle="tooltip" data-placement="top" title="{{"All data must be introduced using just numerical values."|trans}}" class="glyphicon glyphicon-info-sign"></span></h3>*/
/*               <div class="well well-lg form-inline">*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_year">{{"Year"|trans}}</label>*/
/*                     <input value="{{ history_year }}" type="text" class="form-control" id="history_year" name="history['year']" placeholder="2015">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_january">{{"January"|trans}}</label>*/
/*                     <input value="{{history_01["'c'"]}}" type="text" name="history['jan']['c']" class="form-control" id="history_consumption_january" placeholder="KW">*/
/*                     <input value="{{history_01["'p'"]}}" type="text" name="history['jan']['p']" class="form-control" id="history_payment_january" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_february">{{"February"|trans}}</label>*/
/*                     <input value="{{history_02["'c'"]}}" type="text" name="history['feb']['c']" class="form-control" id="history_consumption_february" placeholder="KW">*/
/*                     <input value="{{history_02["'p'"]}}" type="text" name="history['feb']['p']" class="form-control" id="history_payment_february" placeholder="$">*/
/*                   </div>  */
/*                    <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_mars">{{"Mars"|trans}}</label>*/
/*                     <input value="{{history_03["'c'"]}}" type="text" name="history['mar']['c']" class="form-control" id="history_consumption_mars" placeholder="KW">*/
/*                     <input value="{{history_03["'p'"]}}" type="text" name="history['mar']['p']" class="form-control" id="history_payment_mars" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_avril">{{"Avril"|trans}}</label>*/
/*                     <input value="{{history_04["'c'"]}}" type="text" name="history['avr']['c']" class="form-control" id="history_consumption_avril" placeholder="KW">*/
/*                     <input value="{{history_04["'p'"]}}" type="text" name="history['avr']['p']" class="form-control" id="history_payment_avril" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_may">{{"May"|trans}}</label>*/
/*                     <input value="{{history_05["'c'"]}}" type="text" name="history['mai']['c']" class="form-control" id="history_consumption_may" placeholder="KW">*/
/*                     <input value="{{history_05["'p'"]}}" type="text" name="history['mai']['p']" class="form-control" id="history_payment_may" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_june">{{"June"|trans}}</label>*/
/*                     <input value="{{history_06["'c'"]}}" type="text" name="history['jun']['c']" class="form-control" id="history_consumption_june" placeholder="KW">*/
/*                     <input value="{{history_06["'p'"]}}" type="text" name="history['jun']['p']" class="form-control" id="history_payment_june" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_jully">{{"July"|trans}}</label>*/
/*                     <input value="{{history_07["'c'"]}}" type="text" name="history['jul']['c']" class="form-control" id="history_consumption_jully" placeholder="KW">*/
/*                     <input value="{{history_07["'p'"]}}" type="text" name="history['jul']['p']" class="form-control" id="history_payment_jully" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_august">{{"August"|trans}}</label>*/
/*                     <input value="{{history_08["'c'"]}}" type="text" name="history['aug']['c']" class="form-control" id="history_consumption_august" placeholder="KW">*/
/*                     <input value="{{history_08["'p'"]}}" type="text" name="history['aug']['p']" class="form-control" id="history_payment_august" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_september">{{"September"|trans}}</label>*/
/*                     <input value="{{history_09["'c'"]}}" type="text" name="history['sep']['c']" class="form-control" id="history_consumption_september" placeholder="KW">*/
/*                     <input value="{{history_09["'p'"]}}" type="text" name="history['sep']['p']" class="form-control" id="history_payment_september" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_october">{{"October"|trans}}</label>*/
/*                     <input value="{{history_10["'c'"]}}" type="text" name="history['oct']['c']" class="form-control" id="history_consumption_october" placeholder="KW">*/
/*                     <input value="{{history_10["'p'"]}}" type="text" name="history['oct']['p']" class="form-control" id="history_payment_october" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_november">{{"November"|trans}}</label>*/
/*                     <input value="{{history_11["'c'"]}}" type="text" name="history['nov']['c']" class="form-control" id="history_consumption_november" placeholder="KW">*/
/*                     <input value="{{history_11["'p'"]}}" type="text" name="history['nov']['p']" class="form-control" id="history_payment_november" placeholder="$">*/
/*                   </div>*/
/*                   <p></p>  */
/*                   <div class="form-group">*/
/*                     <label style="min-width:100px;" for="history_december">{{"December"|trans}}</label>*/
/*                     <input value="{{history_12["'c'"]}}" type="text" name="history['dec']['c']" class="form-control" id="history_consumption_december" placeholder="KW">*/
/*                     <input value="{{history_12["'p'"]}}" type="text" name="history['dec']['p']" class="form-control" id="history_payment_december" placeholder="$">*/
/*                   </div>    */
/*                   */
/*               </div>*/
/*               #}*/
/*               */
/*          */
/*             */
/*              <h3>{{"Entries"|trans}} <!-- <span data-toggle="tooltip" data-placement="top" title="" class="glyphicon glyphicon-info-sign"></span>--></h3>*/
/*               <div class="well well-lg form-inline">*/
/*                   */
/*                   <h4>{{"First Entry"|trans}}</h4>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry1_name">{{"Name"|trans}}</label>*/
/*                     <input value="{{entry1["'name'"]}}" name="entries['first']['name']" id="entry1_name"   type="text" class="form-control" id="entry1_name"  placeholder="Entry1">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry1_kw">{{"Power"|trans}} (Kw)</label>*/
/*                     <input value="{{entry1["'kw'"]}}" name="entries['first']['kw']" id="entry1_kw"   type="text" class="form-control" id="entry1_kw"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry1_priority">{{"Priority"|trans}}</label>*/
/*                     <input value="{{entry1["'priority'"]}}" name="entries['first']['priority']" id="entry1_kw"   type="text" class="form-control" id="entry1_priority"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry1_percent">{{"Percentage (%)"|trans}}</label>*/
/*                     <input value="{{entry1["'percent'"]}}" name="entries['first']['percent']" id="entry1_percent"   type="text" class="form-control" id="entry1_percent"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry1_temperature">{{"Heat alarm (C)"|trans}}</label>*/
/*                     <input value="{{entry1["'temperature'"]}}" name="entries['first']['temperature']" id="entry1_temperature"   type="text" class="form-control" id="entry1_temperature"  placeholder="25">*/
/*                   </div>*/
/*                   <p></p>*/
/*                                     */
/*                   <h4>{{"Second Entry"|trans}}</h4>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry2_name">{{"Name"|trans}}</label>*/
/*                     <input value="{{entry2["'name'"]}}" name="entries['second']['name']" id="entry2_name"   type="text" class="form-control" id="entry1_name"  placeholder="Entry2">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry2_kw">{{"Power"|trans}} (Kw)</label>*/
/*                     <input value="{{entry2["'kw'"]}}" name="entries['second']['kw']" id="entry2_kw"   type="text" class="form-control" id="entry1_kw"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry2_priority">{{"Priority"|trans}}</label>*/
/*                     <input value="{{entry2["'priority'"]}}" name="entries['second']['priority']"  type="text" class="form-control" id="entry2_priority"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry2_percent">{{"Percentage (%)"|trans}}</label>*/
/*                     <input value="{{entry2["'percent'"]}}"  name="entries['second']['percent']" id="entry2_percent"   type="text" class="form-control" id="entry1_percent"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry2_temperature">{{"Heat alarm (C)"|trans}}</label>*/
/*                     <input value="{{entry2["'temperature'"]}}" name="entries['second']['temperature']" id="entry2_temperature"   type="text" class="form-control" id="entry1_temperature"  placeholder="25">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   */
/*                   <h4>{{"Third Entry"|trans}}</h4>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry3_name">{{"Name"|trans}}</label>*/
/*                     <input value="{{entry3["'name'"]}}" name="entries['third']['name']" id="entry3_name"   type="text" class="form-control" id="entry1_name"  placeholder="Entry3">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry3_kw">{{"Power"|trans}} (Kw)</label>*/
/*                     <input value="{{entry3["'kw'"]}}" name="entries['third']['kw']" id="entry3_kw"   type="text" class="form-control" id="entry1_kw"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry3_priority">{{"Priority"|trans}}</label>*/
/*                     <input value="{{entry3["'priority'"]}}" name="entries['third']['priority']"  type="text" class="form-control" id="entry3_priority"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry3_percent">{{"Percentage (%)"|trans}}</label>*/
/*                     <input value="{{entry3["'percent'"]}}" name="entries['third']['percent']" id="entry3_percent"   type="text" class="form-control" id="entry1_percent"  placeholder="">*/
/*                   </div>*/
/*                   <p></p>*/
/*                   <div class="form-group">*/
/*                     <label style="min-width:200px;" for="entry3_temperature">{{"Heat alarm (C)"|trans}}</label>*/
/*                     <input value="{{entry3["'temperature'"]}}" name="entries['third']['temperature']" id="entry3_temperature"   type="text" class="form-control" id="entry1_temperature"  placeholder="25">*/
/*                   </div>*/
/*                   <p></p>*/
/*                 */
/*                   */
/*               </div>*/
/*          */
/*               */
/*               */
/*             <input type="submit" class="btn btn-primary btn-lg pull-right" value="{{"Submit"|trans}}"/>*/
/*           </form>*/
/*           */
/*           */
/* */
/*           */
/*         </div>*/
/*           */
/*           */
/*      </div>*/
/*     </div>      */
/*           */
/*           */
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/* {{parent()}}*/
/*     */
/* {% endblock %}*/
/* */
/* */
/* */
/* */
/* {% block javascripts %}*/
/* {{parent()}}*/
/* <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>*/
/* <script> $.validate(); </script>*/
/* */
/* <script>*/
/*  $(document).ready(function() {*/
/*                   rebuildAddress();*/
/*                   rebuildContact();*/
/*             });*/
/*             */
/*  function hideLogoImg(){*/
/*      $("#logo_img").hide();*/
/*  }           */
/*         */
/* </script>    */
/* */
/* */
/* <script>*/
/* function addAddress(){*/
/*         var address_item    = $("#address_item").val();*/
/*         var address         = new Array();*/
/*         var address_list    = $("#address").val();*/
/*         */
/*         if (address_item.length == 0){*/
/*             return 0;*/
/*         }*/
/*         */
/*         if (address_list.length > 0){*/
/*             address = eval(address_list);*/
/*         }*/
/*         */
/*         //ADD ADDRESS TO THE LIST*/
/*         address.push(address_item);*/
/*         */
/*         //SAVE LIST IN  THE INPUT*/
/*         $("#address").val(JSON.stringify(address));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildAddress();*/
/*         */
/*         //REMOVE ADDRESS TEXT*/
/*         $("#address_item").val("");*/
/*         */
/*     }*/
/*     */
/*     function deleteAddress(id){*/
/*         */
/*         var address_list    = $("#address").val();*/
/*         var address = eval(address_list);*/
/*         */
/*         //SUPPRIME THE ITEM FROM THE LIST*/
/*         address.splice(id, 1);*/
/*         */
/*         //SAVE THE LIST WITHOUT THE ITEM*/
/*         $("#address").val(JSON.stringify(address));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildAddress();*/
/*         */
/*     }*/
/*     */
/*     function rebuildAddress(){*/
/*          var address            = eval($("#address").val());*/
/*          var id_item            = 0;*/
/*      */
/*          */
/*         if (typeof address === 'undefined'){*/
/*             return 0;*/
/*         }*/
/*         */
/*          $("#address_list").html("");*/
/*          address.forEach(function(item) {*/
/*              $("#address_list").append('<div class="list-group-item">'+item+'<a onclick="deleteAddress('+id_item+');" href="javascript:void(0);" ><span class=" pull-right glyphicon glyphicon-minus"></span></a>');*/
/*              id_item++;*/
/*         //console.log(item);*/
/*         });*/
/*     }*/
/*         */
/*     */
/* </script>    */
/* */
/* */
/* */
/* <script>*/
/*     function addContact(){*/
/*         var contact_item    = $("#contact_item").val();*/
/*         var contact         = new Array();*/
/*         var contact_list    = $("#contact").val();*/
/*         */
/*         if (contact_item.length == 0){*/
/*             return 0;*/
/*         }*/
/*         */
/*         if (contact_list.length > 0){*/
/*             contact = eval(contact_list);*/
/*         }*/
/*         */
/*         //ADD CONTACT TO THE LIST*/
/*         contact.push(contact_item);*/
/*         */
/*         //SAVE LIST IN  THE INPUT*/
/*         $("#contact").val(JSON.stringify(contact));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildContact();*/
/*         */
/*         //REMOVE CONTACT TEXT*/
/*         $("#contact_item").val("");*/
/*         */
/*     }*/
/*     */
/*     function deleteContact(id){*/
/*         */
/*         var contact_list    = $("#contact").val();*/
/*         var contacts = eval(contact_list);*/
/*         */
/*         //SUPPRIME THE ITEM FROM THE LIST*/
/*         contacts.splice(id, 1);*/
/*         */
/*         //SAVE THE LIST WITHOUT THE ITEM*/
/*         $("#contact").val(JSON.stringify(contacts));*/
/*         */
/*         //REBUILD THE LIST*/
/*         rebuildContact();*/
/*         */
/*     }*/
/*     */
/*     function rebuildContact(){*/
/*          var contact            = eval($("#contact").val());*/
/*          var id_item            = 0;*/
/*       */
/*          */
/*         if (typeof contact === 'undefined'){*/
/*             return 0;*/
/*         }*/
/*         */
/*          $("#contact_list").html("");*/
/*          contact.forEach(function(item) {*/
/*              $("#contact_list").append('<div class="list-group-item">'+item+'<a onclick="deleteContact('+id_item+');" href="javascript:void(0);"><span class=" pull-right glyphicon glyphicon-minus"></span></a>');*/
/*              id_item++;*/
/*         //console.log(item);*/
/*         });*/
/*     }*/
/*     */
/*     */
/*     */
/* </script>    */
/* */
/* {% endblock %}*/
/* */
/* */
/* */
