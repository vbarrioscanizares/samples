<?php

/* clients_breadcrumb.html.twig */
class __TwigTemplate_ffa4916ff2d10a614fcad3d4d942b279760ba33785396e1ee59bdff614d2b6c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_364c8f06323277f255bc0f3c0fe237ff62fa63c7bd5d6053f88ebfc2c2198e19 = $this->env->getExtension("native_profiler");
        $__internal_364c8f06323277f255bc0f3c0fe237ff62fa63c7bd5d6053f88ebfc2c2198e19->enter($__internal_364c8f06323277f255bc0f3c0fe237ff62fa63c7bd5d6053f88ebfc2c2198e19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clients_breadcrumb.html.twig"));

        // line 1
        $context["route"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "_route"), "method");
        // line 2
        echo "

<ol class=\"breadcrumb\">
  ";
        // line 6
        echo "  
  
  
  ";
        // line 9
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "role"), "method") == "ROLE_ADMIN")) {
            // line 10
            echo "  
        ";
            // line 11
            if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_users")) {
                // line 12
                echo "            <li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Users"), "html", null, true);
                echo "</li>
        ";
            } else {
                // line 14
                echo "            <li><a href=\"";
                echo $this->env->getExtension('routing')->getPath("_users");
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Users"), "html", null, true);
                echo "</a></li>
        ";
            }
            // line 16
            echo "   
   ";
        }
        // line 18
        echo "        
   
   ";
        // line 20
        if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_clients")) {
            // line 21
            echo "            <li>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clients"), "html", null, true);
            echo "</li>
        ";
        } else {
            // line 23
            echo "            <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("_clients");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Clients"), "html", null, true);
            echo "</a></li>
        ";
        }
        // line 25
        echo "  
  
  ";
        // line 27
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "role"), "method") == "ROLE_ADMIN")) {
            // line 28
            echo "  
        ";
            // line 29
            if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_clients_edit")) {
                // line 30
                echo "            <li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Edit"), "html", null, true);
                echo "</li>
        ";
            } else {
                // line 32
                echo "            <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_edit", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Edit"), "html", null, true);
                echo "</a></li>
        ";
            }
            // line 34
            echo "   
   ";
        }
        // line 36
        echo "   
   ";
        // line 37
        if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_clients_board")) {
            // line 38
            echo "       <li>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Board"), "html", null, true);
            echo "</li>
   ";
        } else {
            // line 40
            echo "       <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_board", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Board"), "html", null, true);
            echo "</a></li>
   ";
        }
        // line 42
        echo "
  
   
   ";
        // line 45
        if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_clients_chart")) {
            // line 46
            echo "       <li>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Chart"), "html", null, true);
            echo "</li>
   ";
        } else {
            // line 48
            echo "       <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_chart", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Chart"), "html", null, true);
            echo "</a></li>
   ";
        }
        // line 50
        echo "   
   
   
   ";
        // line 53
        if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_clients_alarms")) {
            // line 54
            echo "        <li>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Alarms"), "html", null, true);
            echo "</li>
    ";
        } else {
            // line 56
            echo "        <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_alarms", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Alarms"), "html", null, true);
            echo "</a></li>
    ";
        }
        // line 58
        echo "   
   
   ";
        // line 60
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "get", array(0 => "role"), "method") == "ROLE_ADMIN")) {
            // line 61
            echo "   

            ";
            // line 63
            if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_clients_presets")) {
                // line 64
                echo "                <li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Presets"), "html", null, true);
                echo "</li>
            ";
            } else {
                // line 66
                echo "                <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_presets", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Presets"), "html", null, true);
                echo "</a></li>
            ";
            }
            // line 68
            echo "


            ";
            // line 71
            if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_clients_bills")) {
                // line 72
                echo "                <li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Bills"), "html", null, true);
                echo "</li>
            ";
            } else {
                // line 74
                echo "                <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_clients_bills", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Bills"), "html", null, true);
                echo "</a></li>
            ";
            }
            // line 76
            echo "
            ";
            // line 77
            if (((isset($context["route"]) ? $context["route"] : $this->getContext($context, "route")) == "_active_power_presets")) {
                // line 78
                echo "                <li>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Power presets"), "html", null, true);
                echo "</li>
            ";
            } else {
                // line 80
                echo "                <li><a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_active_power_presets", array("client_id" => (isset($context["client_id"]) ? $context["client_id"] : $this->getContext($context, "client_id")))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total Power presets"), "html", null, true);
                echo "</a></li>
            ";
            }
            // line 82
            echo "   
  
   ";
        }
        // line 85
        echo "   
</ol>";
        
        $__internal_364c8f06323277f255bc0f3c0fe237ff62fa63c7bd5d6053f88ebfc2c2198e19->leave($__internal_364c8f06323277f255bc0f3c0fe237ff62fa63c7bd5d6053f88ebfc2c2198e19_prof);

    }

    public function getTemplateName()
    {
        return "clients_breadcrumb.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  240 => 85,  235 => 82,  227 => 80,  221 => 78,  219 => 77,  216 => 76,  208 => 74,  202 => 72,  200 => 71,  195 => 68,  187 => 66,  181 => 64,  179 => 63,  175 => 61,  173 => 60,  169 => 58,  161 => 56,  155 => 54,  153 => 53,  148 => 50,  140 => 48,  134 => 46,  132 => 45,  127 => 42,  119 => 40,  113 => 38,  111 => 37,  108 => 36,  104 => 34,  96 => 32,  90 => 30,  88 => 29,  85 => 28,  83 => 27,  79 => 25,  71 => 23,  65 => 21,  63 => 20,  59 => 18,  55 => 16,  47 => 14,  41 => 12,  39 => 11,  36 => 10,  34 => 9,  29 => 6,  24 => 2,  22 => 1,);
    }
}
/* {%set route = app.request.get('_route') %}*/
/* */
/* */
/* <ol class="breadcrumb">*/
/*   {#<li><a href="{{path('_clients')}}">{{"List"|trans}}</a></li>#}*/
/*   */
/*   */
/*   */
/*   {%if app.session.get('role') == "ROLE_ADMIN"%}*/
/*   */
/*         {% if route=="_users" %}*/
/*             <li>{{"Users"|trans}}</li>*/
/*         {% else %}*/
/*             <li><a href="{{path("_users")}}">{{"Users"|trans}}</a></li>*/
/*         {% endif %}*/
/*    */
/*    {%endif%}*/
/*         */
/*    */
/*    {% if route=="_clients" %}*/
/*             <li>{{"Clients"|trans}}</li>*/
/*         {% else %}*/
/*             <li><a href="{{path("_clients")}}">{{"Clients"|trans}}</a></li>*/
/*         {% endif %}*/
/*   */
/*   */
/*   {%if app.session.get('role') == "ROLE_ADMIN"%}*/
/*   */
/*         {% if route=="_clients_edit" %}*/
/*             <li>{{"Edit"|trans}}</li>*/
/*         {% else %}*/
/*             <li><a href="{{path("_clients_edit", { 'client_id': client_id })}}">{{"Edit"|trans}}</a></li>*/
/*         {% endif %}*/
/*    */
/*    {%endif%}*/
/*    */
/*    {% if route=="_clients_board" %}*/
/*        <li>{{"Board"|trans}}</li>*/
/*    {% else %}*/
/*        <li><a href="{{path("_clients_board", { 'client_id': client_id })}}">{{"Board"|trans}}</a></li>*/
/*    {% endif %}*/
/* */
/*   */
/*    */
/*    {% if route=="_clients_chart" %}*/
/*        <li>{{"Chart"|trans}}</li>*/
/*    {% else %}*/
/*        <li><a href="{{path("_clients_chart", { 'client_id': client_id })}}">{{"Chart"|trans}}</a></li>*/
/*    {% endif %}*/
/*    */
/*    */
/*    */
/*    {% if route=="_clients_alarms" %}*/
/*         <li>{{"Alarms"|trans}}</li>*/
/*     {% else %}*/
/*         <li><a href="{{path("_clients_alarms", { 'client_id': client_id })}}">{{"Alarms"|trans}}</a></li>*/
/*     {% endif %}*/
/*    */
/*    */
/*    {%if app.session.get('role') == "ROLE_ADMIN"%}*/
/*    */
/* */
/*             {% if route=="_clients_presets" %}*/
/*                 <li>{{"Presets"|trans}}</li>*/
/*             {% else %}*/
/*                 <li><a href="{{path("_clients_presets", { 'client_id': client_id })}}">{{"Presets"|trans}}</a></li>*/
/*             {% endif %}*/
/* */
/* */
/* */
/*             {% if route=="_clients_bills" %}*/
/*                 <li>{{"Bills"|trans}}</li>*/
/*             {% else %}*/
/*                 <li><a href="{{path("_clients_bills", { 'client_id': client_id })}}">{{"Bills"|trans}}</a></li>*/
/*             {% endif %}*/
/* */
/*             {% if route=="_active_power_presets" %}*/
/*                 <li>{{"Total Power presets"|trans}}</li>*/
/*             {% else %}*/
/*                 <li><a href="{{path("_active_power_presets", { 'client_id': client_id })}}">{{"Total Power presets"|trans}}</a></li>*/
/*             {% endif %}*/
/*    */
/*   */
/*    {%endif%}*/
/*    */
/* </ol>*/
