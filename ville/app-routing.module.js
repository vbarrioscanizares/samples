"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var notfound_component_1 = require('./notfound/notfound.component');
var dossier_detail_component_1 = require('./dossier/dossier-detail.component');
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [
                router_1.RouterModule.forRoot([
                    /*{ path: 'dossier/:id', component: DossierComponent,data: {
                        title: 'Dossier'
                      }
                    },*/
                    { path: 'dossier', redirectTo: '/dossier/1', pathMatch: 'full' },
                    { path: 'dossier/:id', component: dossier_detail_component_1.DossierdetailComponent },
                    //{ path: '', component: WelcomeComponent },
                    { path: '', redirectTo: '/dossier/1', pathMatch: 'full' },
                    { path: '**', component: notfound_component_1.NotfoundComponent }
                ])
            ],
            exports: [
                router_1.RouterModule
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map