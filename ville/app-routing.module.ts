import { NgModule }     from '@angular/core';
import { RouterModule } from '@angular/router';

import { WelcomeComponent } from './welcome/welcome.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { DossierdetailComponent } from './dossier/dossier-detail.component';



@NgModule({
  imports: [
    RouterModule.forRoot([
      /*{ path: 'dossier/:id', component: DossierComponent,data: {
          title: 'Dossier'
        }
      },*/

      { path: 'dossier', redirectTo: '/dossier/1',pathMatch: 'full'},
      { path: 'dossier/:id', component: DossierdetailComponent },
      //{ path: '', component: WelcomeComponent },
      { path: '', redirectTo: '/dossier/1',pathMatch: 'full' },
      { path: '**', component: NotfoundComponent }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}