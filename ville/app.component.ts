import { Component } from '@angular/core';

@Component({
    selector: '[home]',
    moduleId: module.id,
    templateUrl: 'home.html'
})
export class AppComponent {
    public position:string="home";

 }
