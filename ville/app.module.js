"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var app_routing_module_1 = require('./app-routing.module');
var app_component_1 = require('./app.component');
var topbar_component_1 = require('./common/topbar/topbar.component');
var navbar_component_1 = require('./common/navbar/navbar.component');
var foot_component_1 = require('./common/footer/foot.component');
var welcome_component_1 = require('./welcome/welcome.component');
var notfound_component_1 = require('./notfound/notfound.component');
var dossier_detail_component_1 = require('./dossier/dossier-detail.component');
var dossier_modify_component_1 = require('./dossier/dossier-modify.component');
var dossier_empty_component_1 = require('./dossier/dossier-empty.component');
var http_1 = require('@angular/http');
var forms_1 = require('@angular/forms');
var dossier_service_1 = require('./dossier/dossier.service');
var interactive_service_1 = require('./common/interactive.service');
var core_2 = require('@angular/core');
require('./common/configuration');
var ng2_auto_complete_1 = require('ng2-auto-complete');
if (ENV != "local")
    core_2.enableProdMode();
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, app_routing_module_1.AppRoutingModule, http_1.HttpModule, http_1.JsonpModule, forms_1.FormsModule, ng2_auto_complete_1.Ng2AutoCompleteModule],
            declarations: [app_component_1.AppComponent, topbar_component_1.TopbarComponent, navbar_component_1.NavbarComponent, foot_component_1.FootComponent, welcome_component_1.WelcomeComponent, notfound_component_1.NotfoundComponent, dossier_detail_component_1.DossierdetailComponent, dossier_modify_component_1.DossiermodifyComponent, dossier_empty_component_1.DossieremptyComponent],
            providers: [dossier_service_1.DossierService, interactive_service_1.InteractiveService],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map