import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent }  from './app.component';
import { TopbarComponent } from './common/topbar/topbar.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { FootComponent } from './common/footer/foot.component';

import { WelcomeComponent } from './welcome/welcome.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { DossierdetailComponent } from './dossier/dossier-detail.component';
import {DossiermodifyComponent} from './dossier/dossier-modify.component';
import {DossieremptyComponent} from './dossier/dossier-empty.component';

import { HttpModule, JsonpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';



import { DossierService }          from './dossier/dossier.service';
import { InteractiveService }          from './common/interactive.service';
import {enableProdMode} from '@angular/core';
import './common/configuration';

import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

if (ENV != "local") enableProdMode();

@NgModule({
  imports:      [ BrowserModule, AppRoutingModule, HttpModule, JsonpModule,FormsModule,Ng2AutoCompleteModule],
  declarations: [ AppComponent, TopbarComponent, NavbarComponent, FootComponent,WelcomeComponent,NotfoundComponent,DossierdetailComponent,DossiermodifyComponent, DossieremptyComponent],
  providers: [ DossierService, InteractiveService ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
