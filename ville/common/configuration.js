var ENV = 'local';
var CONFIG_SERVER_URL = "";
var CONFIG_CLIENT_URL = "";
var host = window.location.hostname;
var BIG_NUMBER = 7777777;
//console.log(host);
switch (host) {
    case 'localhost':
        ENV = 'local';
        CONFIG_SERVER_URL = "http://localhost:7047";
        CONFIG_CLIENT_URL = "http://localhost:3000";
        break;
    case 'dvlldk01b':
        ENV = 'dev';
        CONFIG_SERVER_URL = "http://dvlldk01b:3066";
        CONFIG_CLIENT_URL = "http://dvlldk01b:3075";
        break;
    case 'accldk01b':
        ENV = 'acc';
        CONFIG_SERVER_URL = "http://accldk01b:3066";
        CONFIG_CLIENT_URL = "http://accldk01b:3075";
        break;
    default:
        ENV = 'dev';
        CONFIG_SERVER_URL = "http://dvlldk01b:3066";
        CONFIG_CLIENT_URL = "http://dvlldk01b:3075";
        break;
}
//# sourceMappingURL=configuration.js.map