"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var interactive_service_1 = require('../interactive.service');
var FootComponent = (function () {
    function FootComponent(interactive) {
        var _this = this;
        this.interactive = interactive;
        this.displaying = "general";
        setInterval(function () {
            _this.displaying = interactive.position;
        }, 100);
    }
    /* updateDisplayingView(displaying:string){this.displaying  = displaying;}*/
    FootComponent.prototype.callDossierModify = function () { this.interactive.callDossierModify(); };
    FootComponent.prototype.callDossierCreate = function () { this.interactive.callDossierCreate(); };
    FootComponent.prototype.callDossierView = function () { this.interactive.callDossierView(); };
    FootComponent.prototype.callDossierSaveCreate = function () { this.interactive.callDossierSaveCreate(); };
    FootComponent.prototype.callDossierSaveModify = function () { this.interactive.callDossierSaveModify(); };
    FootComponent = __decorate([
        core_1.Component({
            selector: '[foot]',
            moduleId: module.id,
            templateUrl: 'footer.html'
        }), 
        __metadata('design:paramtypes', [interactive_service_1.InteractiveService])
    ], FootComponent);
    return FootComponent;
}());
exports.FootComponent = FootComponent;
//# sourceMappingURL=foot.component.js.map