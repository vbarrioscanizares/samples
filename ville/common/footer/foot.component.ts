import { Component, Input } from '@angular/core';
import { Observable }       from 'rxjs/Observable';
import {Location} from '@angular/common';
import { InteractiveService }          from '../interactive.service';


@Component({
  selector: '[foot]',
  moduleId: module.id,
  templateUrl: 'footer.html'
})
export class FootComponent {
  displaying:string = "general";


   constructor(private interactive: InteractiveService) {
      setInterval(() => {
          this.displaying = interactive.position;
      },100);
   
  }


 /* updateDisplayingView(displaying:string){this.displaying  = displaying;}*/

  callDossierModify(){this.interactive.callDossierModify();}

  callDossierCreate(){this.interactive.callDossierCreate();}

  callDossierView(){this.interactive.callDossierView();}

  callDossierSaveCreate(){this.interactive.callDossierSaveCreate();}

  callDossierSaveModify(){this.interactive.callDossierSaveModify();}

  }
 
