"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var InteractiveService = (function () {
    function InteractiveService() {
        this.position = "general";
    }
    InteractiveService.prototype.callDossierModify = function () { this.dossier_modify.ModifyDossier(); };
    //callDossierCreate(){this.dossier_create.CreateDossier();}
    InteractiveService.prototype.callDossierCreate = function () { this.dossier_modify.CreateDossier(); };
    InteractiveService.prototype.callDossierView = function () {
        this.dossier_detail.showDossierView();
        return true;
        /*setTimeout(() => {
                        this.dossier_detail.showDossierView();
                       return true;
                     }, 10000);*/
    };
    InteractiveService.prototype.callDossierSaveModify = function () { this.dossier_modify.DossierSaveModify(); };
    InteractiveService.prototype.callDossierSaveCreate = function () { this.dossier_modify.DossierSaveCreate(); };
    InteractiveService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], InteractiveService);
    return InteractiveService;
}());
exports.InteractiveService = InteractiveService;
//# sourceMappingURL=interactive.service.js.map