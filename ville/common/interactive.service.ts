  
import { Injectable }    from '@angular/core';
import {DossiermodifyComponent} from '../dossier/dossier-modify.component';
//import {DossiercreateComponent} from '../dossier/dossier-create.component';
import {DossierdetailComponent} from '../dossier/dossier-detail.component';


@Injectable()
export class InteractiveService {
  constructor() { }

 position:string = "general";
 dossier_modify:DossiermodifyComponent;
 //dossier_create:DossiercreateComponent;
 dossier_detail:DossierdetailComponent;

callDossierModify(){this.dossier_modify.ModifyDossier();}
//callDossierCreate(){this.dossier_create.CreateDossier();}
callDossierCreate(){this.dossier_modify.CreateDossier();}
callDossierView(){this.dossier_detail.showDossierView();return true;
 /*setTimeout(() => {
                 this.dossier_detail.showDossierView();
                return true;
              }, 10000);*/

 
}

callDossierSaveModify(){this.dossier_modify.DossierSaveModify();}
callDossierSaveCreate(){this.dossier_modify.DossierSaveCreate();}
 
}


