"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
require('../configuration');
var router_1 = require('@angular/router');
var interactive_service_1 = require('../interactive.service');
var TopbarComponent = (function () {
    function TopbarComponent(router, interactive) {
        this.router = router;
        this.interactive = interactive;
        this._login = 'Utilisateur';
    }
    Object.defineProperty(TopbarComponent.prototype, "login", {
        get: function () { return this._login; },
        set: function (login) {
            this._login = (login && login.trim()) || 'Utilisateur';
        },
        enumerable: true,
        configurable: true
    });
    TopbarComponent.prototype.findDossier = function () {
        if (this.searchbyID === undefined || this.searchbyID === null) {
        }
        else
            window.location.href = CONFIG_CLIENT_URL + '/dossier/' + this.searchbyID;
    };
    TopbarComponent.prototype.validate = function (event) {
        var key = parseInt(event.target.value);
        isNaN(key) ? this.searchbyID = this.searchbyID.slice(0, -1) : this.searchbyID = key.toString();
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String), 
        __metadata('design:paramtypes', [String])
    ], TopbarComponent.prototype, "login", null);
    TopbarComponent = __decorate([
        core_1.Component({
            selector: '[topbar]',
            moduleId: module.id,
            templateUrl: 'topbar.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, interactive_service_1.InteractiveService])
    ], TopbarComponent);
    return TopbarComponent;
}());
exports.TopbarComponent = TopbarComponent;
//# sourceMappingURL=topbar.component.js.map