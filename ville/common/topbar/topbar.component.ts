import { Component, Input } from '@angular/core';
import '../configuration';
import {Router} from '@angular/router';
import { InteractiveService }          from '../interactive.service';

@Component({
  selector: '[topbar]',
  moduleId: module.id,
  templateUrl: 'topbar.html'
})
export class TopbarComponent {
  _login: string = 'Utilisateur';
  searchbyID:string;


    constructor(private router: Router,private interactive: InteractiveService){}

  @Input()
  set login(login: string) {
    this._login = (login && login.trim()) || 'Utilisateur';
  }
  get login() { return this._login; }

  findDossier(){
       if (this.searchbyID === undefined || this.searchbyID === null) {             
       }else window.location.href = CONFIG_CLIENT_URL+'/dossier/'+this.searchbyID;
     
  }

  validate(event: KeyboardEvent)  {

    let key = parseInt((<HTMLInputElement>event.target).value);

    isNaN(key) ? this.searchbyID = this.searchbyID.slice(0, -1) : this.searchbyID = key.toString();
  }

}