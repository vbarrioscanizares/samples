"use strict";
var Address = (function () {
    function Address(data) {
        this.ADR_PRINC = 0;
        this.DESC_ADR_PRINC = "Non";
        this.DESC_ADR = "";
        this.DESC_ADR_B = false;
        if (data === undefined || data === null) {
        }
        else {
            this.populate(data);
        }
    }
    Address.prototype.toJson = function () { return JSON.stringify(this); };
    Address.prototype.populate = function (data) {
        this.ADR_ID = data.ADR_ID;
        this.IMM_IDE = data.IMM_IDE;
        this.NODOSSIERE = data.NODOSSIERE;
        this.NOOUVRAGE = data.NOOUVRAGE;
        this.NOCIVIQUE = data.NOCIVIQUE;
        this.RUE = data.RUE;
        this.ANCIENNE_RUE = data.RUE;
        this.VILLE = data.VILLE;
        this.BAT_REM = data.BAT_REM;
        this.ADR_PRINC = data.ADR_PRINC;
        this.NO_VOIE = data.NO_VOIE;
        this.ORIENT_NO_CIVIQ = data.ORIENT_NO_CIVIQ;
        this.CODE_VILLE = data.CODE_VILLE;
        this.NOM_VOIE_LONG = data.NOM_VOIE_LONG;
        if (this.ADR_PRINC == -1)
            this.DESC_ADR_PRINC = "Oui";
        //NO_VOIE-NOCIVIQUE:ORIENT_NO_CIVIQ RUE VILLE  ADR_PRINC
        this.DESC_ADR = this.NO_VOIE + "-" + this.NOCIVIQUE + " " + this.ORIENT_NO_CIVIQ + " " + this.RUE;
        this.DESC_ADR = this.DESC_ADR.replace(/null-|null|- | -|    |   |  /gi, " ");
        this.DESC_ADR_B = true;
        /*this.DESC_ADR = this.DESC_ADR.replace("null", " ");
        this.DESC_ADR = this.DESC_ADR.replace("- ", " ");
        this.DESC_ADR = this.DESC_ADR.replace("    ", " ");
        this.DESC_ADR = this.DESC_ADR.replace("   ", " ");
        this.DESC_ADR = this.DESC_ADR.replace("  ", " ");*/
    }; //populate
    return Address;
}());
exports.Address = Address; //class
//# sourceMappingURL=address.js.map