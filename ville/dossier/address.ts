export class Address {

    public ADR_ID:number;
    public IMM_IDE:number;
    public NODOSSIERE:string;
    public NOOUVRAGE:string;
    public NOCIVIQUE:string;
    public RUE:string;
    public VILLE:string;

    public CODE_VILLE:string;
    public NOM_VOIE_LONG:string;
    
    public BAT_REM:string;
    public ADR_PRINC:number = 0;
    public NO_VOIE:string;
    public ORIENT_NO_CIVIQ:string;
    public ANCIENNE_RUE:string;

    public DESC_ADR_PRINC:string    = "Non";
    public DESC_ADR:string          = ""; 
    public DESC_ADR_B:Boolean          = false;


  constructor(); 
  constructor(data:any);     
  constructor(data?:any) {
   
        if (data === undefined || data === null) {
        }else{
            this.populate(data);
        }
        
  }


toJson():string{return JSON.stringify(this);}

populate(data : any){

        this.ADR_ID             = data.ADR_ID;
        this.IMM_IDE            = data.IMM_IDE;
        this.NODOSSIERE         = data.NODOSSIERE;
        this.NOOUVRAGE          = data.NOOUVRAGE;
        this.NOCIVIQUE          = data.NOCIVIQUE;
        this.RUE                = data.RUE;
        this.ANCIENNE_RUE       = data.RUE;
        this.VILLE              = data.VILLE;
        this.BAT_REM            = data.BAT_REM;
        this.ADR_PRINC          = data.ADR_PRINC;
        this.NO_VOIE            = data.NO_VOIE;
        this.ORIENT_NO_CIVIQ    = data.ORIENT_NO_CIVIQ;
        this.CODE_VILLE         = data.CODE_VILLE;
        this.NOM_VOIE_LONG      = data.NOM_VOIE_LONG;

        if (this.ADR_PRINC == -1)                 this.DESC_ADR_PRINC = "Oui";

        //NO_VOIE-NOCIVIQUE:ORIENT_NO_CIVIQ RUE VILLE  ADR_PRINC
        this.DESC_ADR = `${this.NO_VOIE }-${this.NOCIVIQUE} ${this.ORIENT_NO_CIVIQ} ${this.RUE}`;
        this.DESC_ADR = this.DESC_ADR.replace(/null-|null|- | -|    |   |  /gi, " ");

        this.DESC_ADR_B = true;
        /*this.DESC_ADR = this.DESC_ADR.replace("null", " ");
        this.DESC_ADR = this.DESC_ADR.replace("- ", " ");
        this.DESC_ADR = this.DESC_ADR.replace("    ", " ");
        this.DESC_ADR = this.DESC_ADR.replace("   ", " ");
        this.DESC_ADR = this.DESC_ADR.replace("  ", " ");*/

}//populate
}//class