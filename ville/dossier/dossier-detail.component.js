"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var dossier_service_1 = require('./dossier.service');
var interactive_service_1 = require('../common/interactive.service');
require('../common/rxjs-operators');
var http_1 = require('@angular/http');
var dossier_1 = require('./dossier');
var qualificationterrain_1 = require('./qualificationterrain');
var fichequalificative_1 = require('./fichequalificative');
var evaluation_fonciere_1 = require('./evaluation-fonciere');
var address_1 = require('./address');
var localisation_1 = require('./localisation');
var rire_1 = require('./rire');
var list_1 = require('./list');
var DossierdetailComponent = (function () {
    function DossierdetailComponent(route, router, service, http, interactive) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.service = service;
        this.http = http;
        this.interactive = interactive;
        this.dossier = new dossier_1.Dossier();
        this.qualificationTerrain = new qualificationterrain_1.QualificationTerrain();
        this.fichequalificative = new fichequalificative_1.FicheQualificative();
        this.evaluationfonciere = new evaluation_fonciere_1.EvaluationFonciere();
        this.addresses = [];
        this.localisations = [];
        this.addresses2delete = [];
        this.localisations2delete = [];
        this.listCadastre = new list_1.List();
        this.listArrondissement = new list_1.List();
        this.listCategorie = new list_1.List();
        this.listType = new list_1.List();
        this.listEcoterritoires = new list_1.List();
        this.listCompetence = new list_1.List();
        this.listOLogements = new list_1.List();
        this.listePlanAction = new list_1.List();
        this.listeDevelop = new list_1.List();
        this.listeTopographie = new list_1.List();
        this.listeDescSecteur = new list_1.List();
        this.listVilles = new list_1.List();
        this.listRire = [];
        this.availableRire = [];
        this.listTitresProprieties = new list_1.List();
        this.listServitudes = new list_1.List();
        this.listSpecificationSol = new list_1.List();
        this.listBatiment = new list_1.List();
        this.autocompleteAddressChanged = false;
        this.ignoreChange = false;
        this.showView = true;
        this.showModify = false;
        this.showCreate = false;
        this.showEmpty = false;
        this.searching = false;
        this.saving = false;
        this.addressesOpen = true;
        this.address_principal = 0;
        this.route.params.forEach(function (params) { _this.selectedId = +params['id']; });
        interactive.dossier_detail = this;
    }
    DossierdetailComponent.prototype.showDossierEmpty = function () { this.showEmpty = true; this.showView = false; this.showModify = false; this.showCreate = false; this.interactive.position = "general"; this.recalculateNavbarHeight(); };
    DossierdetailComponent.prototype.showDossierModify = function () { this.showView = false; this.showModify = true; this.showCreate = false; this.interactive.position = "dossier_modify"; this.recalculateNavbarHeight(); };
    DossierdetailComponent.prototype.showDossierCreate = function () { this.showView = false; this.showModify = true; this.showCreate = false; this.interactive.position = "dossier_create"; this.recalculateNavbarHeight(); };
    DossierdetailComponent.prototype.showDossierView = function () { this.updateData(); this.showView = true; this.showModify = false; this.showCreate = false; this.interactive.position = "dossier_view"; this.recalculateNavbarHeight(); };
    DossierdetailComponent.prototype.ngAfterViewInit = function () {
        jQuery("#dossier_code.m").mask("31H99-005-9999-99", { placeholder: "31HDD-005-DDDD-DD" });
        jQuery("#dossier_code.c").mask("31H99-005-9999-99", { placeholder: "31HDD-005-DDDD-DD" });
        this.recalculateNavbarHeight();
    };
    DossierdetailComponent.prototype.allowAutocomplete2Delete = function () {
        //SHOW LIST OF OPTIONS
        this.ignoreChange = true;
    };
    DossierdetailComponent.prototype.rireAutocompleteAddressChange = function (event, item) {
        //AUTOCOMPLETE CHANGE EVENT
        if (event === undefined || event === null || event.id === undefined || event.id === null) {
            if (this.ignoreChange) {
                item.ORIENT_NO_CIVIQ = "";
                item.VILLE = "";
                item.NOM_VOIE_LONG = "";
            }
        }
        else {
            //SELECTED ITEM
            this.autocompleteAddressChanged = true;
            this.ignoreChange = true;
            item.NO_VOIE = event.id;
            item.NOM_VOIE_LONG = event.value;
            //DATA FROM RIRE
            item.ORIENT_NO_CIVIQ = this.listRire[event.id + " " + event.value].ORIENT_NO_CIVIQ;
            item.VILLE = this.listVilles.getValue(this.listRire[event.id + " " + event.value].CODE_VILLE);
        }
    };
    DossierdetailComponent.prototype.rireAutocompleteLocalisationChange = function (event, item) {
        //AUTOCOMPLETE CHANGE EVENT
        if (event === undefined || event === null || event.id === undefined || event.id === null) {
            if (this.ignoreChange) {
                item.ORIENT_NO_CIVIQ = "";
                item.CODE_VILLE = "";
                item.VILLE = "";
                item.NOM_VOIE_LONG = "";
            }
        }
        else {
            //SELECTED ITEM
            this.autocompleteAddressChanged = true;
            this.ignoreChange = true;
            item.NO_VOIE = event.id;
            item.NOM_VOIE_LONG = event.value;
            //DATA FROM RIRE
            item.ORIENT_NO_CIVIQ = this.listRire[event.id + " " + event.value].ORIENT_NO_CIVIQ;
            item.CODE_VILLE = this.listRire[event.id + " " + event.value].CODE_VILLE;
            item.VILLE = this.listVilles.getValue(item.CODE_VILLE);
        }
    };
    DossierdetailComponent.prototype.recalculateNavbarHeight = function () {
        //Adjust sidebar height
        var body = document.body, html = document.documentElement;
        var height = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight) + 40;
        //console.log(height);
        jQuery(".navbar.sidebar").height(height);
    };
    DossierdetailComponent.prototype.isSearching = function () {
        if (this.searching == true) {
            return true;
        }
        else
            return false;
    };
    DossierdetailComponent.prototype.updateData = function () {
        var _this = this;
        jQuery.blockUI({ message: '<p> Communication avec la base de données en cours...</p>' });
        this.searching = true;
        this.autocompleteAddressChanged = false;
        this.ignoreChange = false;
        setTimeout(function () {
            if (_this.searching == true) {
                jQuery.unblockUI();
                window.location.reload();
            }
        }, 5000);
        this.route.params.forEach(function (params) { _this.selectedId = +params['id']; });
        this.service.getDossier(this.selectedId)
            .then(function (data) {
            try {
                if (data[0] === undefined || data[0] === null) {
                    jQuery.unblockUI();
                    _this.searching = false;
                    _this.showDossierEmpty();
                }
                else {
                    _this.dossier.populate(data[0]);
                    //Obtain Information related to qualification-terrain
                    _this.getQualificationTerrainInformation();
                }
            }
            catch (error) {
                console.log(error);
                _this.showDossierEmpty();
            }
            finally {
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    DossierdetailComponent.prototype.getLists = function () {
        var _this = this;
        this.getRire();
        this.service.getListeArrondissements()
            .then(function (data) { return _this.listArrondissement.populate(data); }, function (error) { return _this.errorMessage = error; });
        this.service.getListeCadastres()
            .then(function (data) { return _this.listCadastre.populate(data); }, function (error) { return _this.errorMessage = error; });
        this.service.getListeCategories()
            .then(function (data) { return _this.listCategorie.populate(data); }, function (error) { return _this.errorMessage = error; });
        this.service.getListeTypes()
            .then(function (data) { return _this.listType.populate(data); }, function (error) { return _this.errorMessage = error; });
        this.service.getListeEcotterritoires()
            .then(function (data) { return _this.listEcoterritoires.populate(data); }, function (error) { return _this.errorMessage = error; });
        this.service.getListeCompetence()
            .then(function (data) {
            _this.listCompetence.populate(data);
            _this.dossier.DESC_COMPETENCE = _this.listCompetence.getValue(_this.dossier.COMPETENCE);
        }, function (error) { return _this.errorMessage = error; });
        this.service.getListeOperationLogement()
            .then(function (data) {
            _this.listOLogements.populate(data);
            _this.dossier.DESC_OPERATION_LOGEMENT = _this.listOLogements.getValue(_this.dossier.PROJET_1500_LOGEMENTS);
        }, function (error) { return _this.errorMessage = error; });
        this.service.getListePlanAction()
            .then(function (data) { return _this.listePlanAction.populate(data); }, function (error) { return _this.errorMessage = error; });
        this.service.getListeDevelop()
            .then(function (data) { return _this.listeDevelop.populate(data); }, function (error) { return _this.errorMessage = error; });
        this.service.getListeTopographie()
            .then(function (data) { return _this.listeTopographie.populate(data); }, function (error) { return _this.errorMessage = error; });
        this.service.getListeDescSecteur()
            .then(function (data) { return _this.listeDescSecteur.populate(data); }, function (error) { return _this.errorMessage = error; });
        this.service.getListeVilles()
            .then(function (data) { return _this.listVilles.populate(data); }, function (error) { return _this.errorMessage = error; });
        this.listTitresProprieties.populate([{ "ID": "Conforme", "VALUE": "Conforme" }, { "ID": "À clarifier", "VALUE": "À clarifier" }]);
        this.listServitudes.populate([{ "ID": "Aucune", "VALUE": "Aucune" }, { "ID": "N/D", "VALUE": "N/D" }, { "ID": "Oui", "VALUE": "Oui" }, { "ID": "Sans mention", "VALUE": "Sans mention" }]);
        this.listSpecificationSol.populate([{ "ID": "Contamination", "VALUE": "Contamination" }, { "ID": "N/D", "VALUE": "N/D" }]);
        this.listBatiment.populate([{ "ID": "Contamination", "VALUE": "Contamination" }, { "ID": "N/D", "VALUE": "N/D" }]);
    };
    DossierdetailComponent.prototype.getQualificationTerrainInformation = function () {
        var _this = this;
        this.service.getQualificationTerrain(this.selectedId)
            .then(function (data) {
            try {
                //console.log(data);
                //this.qualificationTerrain.populate(data);
                if (data[0] === undefined || data[0] === null) {
                }
                else {
                    //console.log(data);
                    _this.qualificationTerrain.populate(data[0]);
                    _this.qualificationTerrain.DESC_TOPOGRAPHIE = _this.listeTopographie.getValue(_this.qualificationTerrain.TOPOGRAPHIE);
                    _this.qualificationTerrain.DESC_SECTEUR = _this.listeDescSecteur.getValue(_this.qualificationTerrain.DESCDESECTEUR);
                    _this.qualificationTerrain.DESC_PLANDACTION = _this.listePlanAction.getValue(_this.qualificationTerrain.PLANDACTION);
                    _this.qualificationTerrain.DESC_DEVELOPPABLE = _this.listeDevelop.getValue(_this.qualificationTerrain.DEVELOPPABLE);
                    _this.getAddresses();
                }
            }
            catch (error) {
            }
            finally {
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    DossierdetailComponent.prototype.getAddresses = function () {
        var _this = this;
        this.addresses = [];
        this.addresses2delete = [];
        this.service.getAddresses(this.selectedId)
            .then(function (data) {
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var address = data_1[_i];
                var new_item = new address_1.Address(address);
                new_item.VILLE = _this.listVilles.getValue(new_item.CODE_VILLE);
                _this.addresses.push(new_item);
                if (address.ADR_PRINC == -1)
                    _this.address_principal = address.ADR_ID;
            }
            ;
            _this.getLocalisations();
        }, function (error) { return console.log(error); });
    };
    DossierdetailComponent.prototype.getLocalisations = function () {
        var _this = this;
        this.localisations = [];
        this.localisations2delete = [];
        this.service.getLocalisations(this.selectedId)
            .then(function (data) {
            for (var _i = 0, data_2 = data; _i < data_2.length; _i++) {
                var localisation = data_2[_i];
                var new_item = new localisation_1.Localisation(localisation);
                new_item.VILLE = _this.listVilles.getValue(new_item.CODE_VILLE);
                _this.localisations.push(new_item);
            }
            ;
            _this.getFicheQualificative();
        }, function (error) { return console.log(error); });
    };
    DossierdetailComponent.prototype.getEvaluationFonciere = function () {
        var _this = this;
        this.service.getEvaluationFonciere(this.selectedId)
            .then(function (data) {
            try {
                if (data[0] === undefined || data[0] === null) {
                }
                else {
                    _this.evaluationfonciere.populate(data[0]);
                    jQuery.unblockUI();
                    _this.searching = false;
                    _this.recalculateNavbarHeight();
                }
            }
            catch (error) {
            }
            finally {
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    DossierdetailComponent.prototype.getFicheQualificative = function () {
        var _this = this;
        this.service.getFicheQualificative(this.selectedId)
            .then(function (data) {
            try {
                //console.log(data);
                //this.qualificationTerrain.populate(data);
                if (data[0] === undefined || data[0] === null) {
                }
                else {
                    //console.log(data);
                    _this.fichequalificative.populate(data[0]);
                    _this.getEvaluationFonciere();
                }
            }
            catch (error) {
            }
            finally {
            }
        }, function (error) { return _this.errorMessage = error; });
    };
    DossierdetailComponent.prototype.getRire = function () {
        var _this = this;
        this.service.getRire()
            .then(function (data) {
            for (var _i = 0, data_3 = data; _i < data_3.length; _i++) {
                var info = data_3[_i];
                var rire_item = new rire_1.Rire(info);
                var available_item = {};
                available_item.id = rire_item.NO_VOIE;
                available_item.value = rire_item.NOM_VOIE_LONG;
                _this.listRire[rire_item.NO_VOIE + " " + rire_item.NOM_VOIE_LONG] = rire_item;
                _this.availableRire.push(available_item);
            }
            ;
            //console.log(this.availableRire);
        }, function (error) { return console.log(error); });
    };
    DossierdetailComponent.prototype.validateCode = function (mode, value) {
        if (mode === void 0) { mode = "m"; }
        if (mode == "m") {
            if (value.indexOf("D") > -1) {
                this.dossier.CODE = "";
            }
            else
                this.dossier.CODE = value;
        }
        else {
            //mode = "c"
            if (value.indexOf("D") > -1) {
                this.dossier.CODE = "";
            }
            else
                this.dossier.CODE = value;
        }
    };
    DossierdetailComponent.prototype.syncOnglets = function (id, papa) {
        jQuery("div.tab-pane").removeClass("active");
        jQuery("div.tab-pane").removeClass("in");
        jQuery("#" + id + "").addClass("active");
        jQuery("#" + id + "").addClass("in");
        jQuery("#" + id + "_c").addClass("active");
        jQuery("#" + id + "_c").addClass("in");
        jQuery("#" + id + "_m").addClass("active");
        jQuery("#" + id + "_m").addClass("in");
        this.syncPOnglet(papa);
    };
    DossierdetailComponent.prototype.syncPOnglet = function (id) {
        jQuery("li.dropdown").removeClass("active");
        jQuery("li.dropdown." + id).addClass("active");
    };
    DossierdetailComponent.prototype.syncAddresses = function () {
        if (this.addressesOpen) {
            this.addressesOpen = false;
            jQuery(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
            //jQuery(".panel-collapse").addClass("in");
            jQuery(".panel-collapse").slideUp("slow", function () { });
        }
        else {
            this.addressesOpen = true;
            //jQuery(".panel-collapse").removeClass("in");
            jQuery(".panel-collapse").slideDown("slow", function () { });
            jQuery(".glyphicon-chevron-up").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
        }
    };
    DossierdetailComponent.prototype.addAddress = function () {
        var count = this.addresses.length;
        var new_ind = BIG_NUMBER + count;
        var new_address = new address_1.Address();
        new_address.ADR_ID = new_ind;
        new_address.IMM_IDE = this.selectedId;
        this.addresses.push(new_address);
    };
    DossierdetailComponent.prototype.delAddress = function (ind, adr_id) {
        if (adr_id === void 0) { adr_id = 0; }
        this.addresses2delete.push(adr_id);
        this.addresses.splice(ind, 1);
    };
    DossierdetailComponent.prototype.addLocalisation = function () {
        var count = this.localisations.length;
        var new_ind = BIG_NUMBER + count;
        var new_localisation = new localisation_1.Localisation();
        new_localisation.LOCALISATION_ID = new_ind;
        new_localisation.IMM_IDE = this.selectedId;
        this.localisations.push(new_localisation);
    };
    DossierdetailComponent.prototype.delLocalisation = function (ind, adr_id) {
        if (adr_id === void 0) { adr_id = 0; }
        this.localisations2delete.push(adr_id);
        this.localisations.splice(ind, 1);
    };
    DossierdetailComponent.prototype.updatePrincipalAddress = function (new_principal) {
        var _this = this;
        this.address_principal = new_principal;
        this.addresses.forEach(function (address, index) {
            if (address.ADR_ID != _this.address_principal) {
                address.ADR_PRINC = 0;
            }
            else
                address.ADR_PRINC = -1;
        });
    };
    DossierdetailComponent.prototype.ngOnInit = function () {
        this.updateData();
        this.getLists();
        this.interactive.position = "dossier_view";
    };
    DossierdetailComponent = __decorate([
        core_1.Component({
            selector: 'dossier-detail',
            moduleId: module.id,
            templateUrl: 'dossier-detail.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, dossier_service_1.DossierService, http_1.Http, interactive_service_1.InteractiveService])
    ], DossierdetailComponent);
    return DossierdetailComponent;
}());
exports.DossierdetailComponent = DossierdetailComponent;
//# sourceMappingURL=dossier-detail.component.js.map