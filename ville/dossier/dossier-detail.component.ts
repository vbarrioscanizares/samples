import { Component, Input, ViewChild } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DossierService } from './dossier.service';
import { InteractiveService } from '../common/interactive.service';
import { Observable } from 'rxjs/Observable';
import '../common/rxjs-operators';
import { Http, Response } from '@angular/http';

import { DossiermodifyComponent } from './dossier-modify.component';
//import { DossiercreateComponent } from './dossier-create.component';
import { DossieremptyComponent } from './dossier-empty.component';
import { Dossier } from './dossier';
import { QualificationTerrain } from './qualificationterrain';
import { FicheQualificative } from './fichequalificative';
import { EvaluationFonciere } from './evaluation-fonciere';
import { Address } from './address';
import { Localisation } from './localisation';
import { Rire } from './rire';
import { List } from './list';

declare var jQuery: any;



@Component({
  selector: 'dossier-detail',
  moduleId: module.id,
  templateUrl: 'dossier-detail.html'

})


export class DossierdetailComponent {
  selectedId: number;
  errorMessage: string;
  dossier: Dossier = new Dossier();
  qualificationTerrain = new QualificationTerrain();
  fichequalificative = new FicheQualificative();
  evaluationfonciere = new EvaluationFonciere();
  addresses: Address[] = [];
  localisations: Localisation[] = [];
  addresses2delete: number[] = [];
  localisations2delete: number[] = [];

  listCadastre: List = new List();
  listArrondissement: List = new List();
  listCategorie: List = new List();
  listType: List = new List();
  listEcoterritoires: List = new List();

  listCompetence: List = new List();
  listOLogements: List = new List();

  listePlanAction: List = new List();
  listeDevelop: List = new List();
  listeTopographie: List = new List();
  listeDescSecteur: List = new List();

  listVilles: List = new List();
  listRire: Rire[] = [];
  availableRire: any[] = [];

  listTitresProprieties: List = new List();
  listServitudes: List = new List();
  listSpecificationSol: List = new List();
  listBatiment: List = new List();

  autocompleteAddressChanged: Boolean = false;
  ignoreChange: Boolean = false;


  showView: Boolean = true;
  showModify: Boolean = false;
  showCreate: Boolean = false;
  showEmpty: Boolean = false;

  public searching: Boolean = false;
  public saving: Boolean = false;

  addressesOpen: Boolean = true;
  address_principal: number = 0;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: DossierService,
    private http: Http,
    private interactive: InteractiveService
  ) {

    this.route.params.forEach((params: Params) => { this.selectedId = +params['id']; });

    interactive.dossier_detail = this;
  }

  showDossierEmpty() { this.showEmpty = true; this.showView = false; this.showModify = false; this.showCreate = false; this.interactive.position = "general"; this.recalculateNavbarHeight(); }
  showDossierModify() { this.showView = false; this.showModify = true; this.showCreate = false; this.interactive.position = "dossier_modify"; this.recalculateNavbarHeight(); }
  showDossierCreate() { this.showView = false; this.showModify = true; this.showCreate = false; this.interactive.position = "dossier_create"; this.recalculateNavbarHeight(); }
  showDossierView() { this.updateData(); this.showView = true; this.showModify = false; this.showCreate = false; this.interactive.position = "dossier_view"; this.recalculateNavbarHeight(); }
  ngAfterViewInit() {


    jQuery("#dossier_code.m").mask("31H99-005-9999-99", { placeholder: "31HDD-005-DDDD-DD" });
    jQuery("#dossier_code.c").mask("31H99-005-9999-99", { placeholder: "31HDD-005-DDDD-DD" });

    this.recalculateNavbarHeight();



  }

  allowAutocomplete2Delete() {
    //SHOW LIST OF OPTIONS
    this.ignoreChange = true;
  }

  rireAutocompleteAddressChange(event: any, item: Address) {
    //AUTOCOMPLETE CHANGE EVENT

    if (event === undefined || event === null || event.id === undefined || event.id === null) {

      if (this.ignoreChange) {
        item.ORIENT_NO_CIVIQ = "";
        item.VILLE = "";
        item.NOM_VOIE_LONG = "";
      }
    } else {
      //SELECTED ITEM

      this.autocompleteAddressChanged = true;
      this.ignoreChange = true;
      item.NO_VOIE = event.id;
      item.NOM_VOIE_LONG = event.value;

      //DATA FROM RIRE
      item.ORIENT_NO_CIVIQ = this.listRire[event.id + " " + event.value].ORIENT_NO_CIVIQ;
      item.VILLE = this.listVilles.getValue(this.listRire[event.id + " " + event.value].CODE_VILLE);
    }
  }


  rireAutocompleteLocalisationChange(event: any, item: Localisation) {
    //AUTOCOMPLETE CHANGE EVENT

    if (event === undefined || event === null || event.id === undefined || event.id === null) {

      if (this.ignoreChange) {
        item.ORIENT_NO_CIVIQ = "";
        item.CODE_VILLE = "";
        item.VILLE = "";
        item.NOM_VOIE_LONG = "";
      }
    } else {
      //SELECTED ITEM

      this.autocompleteAddressChanged = true;
      this.ignoreChange = true;
      item.NO_VOIE = event.id;
      item.NOM_VOIE_LONG = event.value;

      //DATA FROM RIRE
      item.ORIENT_NO_CIVIQ = this.listRire[event.id + " " + event.value].ORIENT_NO_CIVIQ;
      item.CODE_VILLE = this.listRire[event.id + " " + event.value].CODE_VILLE;
      item.VILLE = this.listVilles.getValue(item.CODE_VILLE);
    }



  }

  recalculateNavbarHeight() {
    //Adjust sidebar height
    var body = document.body,
      html = document.documentElement;
    var height = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight) + 40;
    //console.log(height);
    jQuery(".navbar.sidebar").height(height);
  }

  isSearching() {
    if (this.searching == true) {
      return true;
    } else return false;
  }
  updateData() {
    jQuery.blockUI({ message: '<p> Communication avec la base de données en cours...</p>' });
    this.searching = true;
    this.autocompleteAddressChanged = false;
    this.ignoreChange = false;

    setTimeout(() => {
      if (this.searching == true) {
        jQuery.unblockUI();
        window.location.reload();
      }
    }, 5000);


    this.route.params.forEach((params: Params) => { this.selectedId = +params['id']; });

    this.service.getDossier(this.selectedId)
      .then(
      data => {
        try {

          if (data[0] === undefined || data[0] === null) {
            jQuery.unblockUI();
            this.searching = false;
            this.showDossierEmpty();
          } else {
            this.dossier.populate(data[0]);

            //Obtain Information related to qualification-terrain
            this.getQualificationTerrainInformation();

          }

        } catch (error) {
          console.log(error);
          this.showDossierEmpty();
        } finally {

        }
      },
      error => this.errorMessage = <any>error);


  }

  getLists() {

    this.getRire();


    this.service.getListeArrondissements()
      .then(
      data => this.listArrondissement.populate(data),
      error => this.errorMessage = <any>error);


    this.service.getListeCadastres()
      .then(
      data => this.listCadastre.populate(data),
      error => this.errorMessage = <any>error);


    this.service.getListeCategories()
      .then(
      data => this.listCategorie.populate(data),
      error => this.errorMessage = <any>error);

    this.service.getListeTypes()
      .then(
      data => this.listType.populate(data),
      error => this.errorMessage = <any>error);

    this.service.getListeEcotterritoires()
      .then(
      data => this.listEcoterritoires.populate(data),
      error => this.errorMessage = <any>error);

    this.service.getListeCompetence()
      .then(
      data => {
        this.listCompetence.populate(data);

        this.dossier.DESC_COMPETENCE = this.listCompetence.getValue(this.dossier.COMPETENCE);
      },
      error => this.errorMessage = <any>error);

    this.service.getListeOperationLogement()
      .then(
      data => {
        this.listOLogements.populate(data);
        this.dossier.DESC_OPERATION_LOGEMENT = this.listOLogements.getValue(this.dossier.PROJET_1500_LOGEMENTS);
      },
      error => this.errorMessage = <any>error);

    this.service.getListePlanAction()
      .then(
      data => this.listePlanAction.populate(data),
      error => this.errorMessage = <any>error);

    this.service.getListeDevelop()
      .then(
      data => this.listeDevelop.populate(data),
      error => this.errorMessage = <any>error);
    this.service.getListeTopographie()
      .then(
      data => this.listeTopographie.populate(data),
      error => this.errorMessage = <any>error);

    this.service.getListeDescSecteur()
      .then(
      data => this.listeDescSecteur.populate(data),
      error => this.errorMessage = <any>error);

    this.service.getListeVilles()
      .then(
      data => this.listVilles.populate(data),
      error => this.errorMessage = <any>error);



    this.listTitresProprieties.populate([{"ID":"Conforme","VALUE":"Conforme"},{"ID":"À clarifier","VALUE":"À clarifier"}]);
    this.listServitudes.populate([{"ID":"Aucune","VALUE":"Aucune"},{"ID":"N/D","VALUE":"N/D"},{"ID":"Oui","VALUE":"Oui"},{"ID":"Sans mention","VALUE":"Sans mention"}]);
    this.listSpecificationSol.populate([{"ID":"Contamination","VALUE":"Contamination"},{"ID":"N/D","VALUE":"N/D"}]);
    this.listBatiment.populate([{"ID":"Contamination","VALUE":"Contamination"},{"ID":"N/D","VALUE":"N/D"}]);

  }




  getQualificationTerrainInformation() {


    this.service.getQualificationTerrain(this.selectedId)
      .then(
      data => {
        try {
          //console.log(data);
          //this.qualificationTerrain.populate(data);
          if (data[0] === undefined || data[0] === null) {
            //console.log(data);
          } else {
            //console.log(data);
            this.qualificationTerrain.populate(data[0]);
            this.qualificationTerrain.DESC_TOPOGRAPHIE = this.listeTopographie.getValue(this.qualificationTerrain.TOPOGRAPHIE);
            this.qualificationTerrain.DESC_SECTEUR = this.listeDescSecteur.getValue(this.qualificationTerrain.DESCDESECTEUR);
            this.qualificationTerrain.DESC_PLANDACTION = this.listePlanAction.getValue(this.qualificationTerrain.PLANDACTION);
            this.qualificationTerrain.DESC_DEVELOPPABLE = this.listeDevelop.getValue(this.qualificationTerrain.DEVELOPPABLE);


            this.getAddresses();
          }
        } catch (error) {
          //console.log(error);
        } finally {

        }
      },
      error => this.errorMessage = <any>error);


  }






  getAddresses() {
    this.addresses = [];
    this.addresses2delete = [];
    this.service.getAddresses(this.selectedId)
      .then(
      data => {

        for (let address of data) {
          let new_item = new Address(address);
          new_item.VILLE = this.listVilles.getValue(new_item.CODE_VILLE);
          this.addresses.push(new_item);
          if (address.ADR_PRINC == -1) this.address_principal = address.ADR_ID;
        };

        this.getLocalisations();

      },
      error => console.log(error));

  }


  getLocalisations() {
    this.localisations = [];
    this.localisations2delete = [];
    this.service.getLocalisations(this.selectedId)
      .then(
      data => {

        for (let localisation of data) {
          let new_item = new Localisation(localisation);
          new_item.VILLE = this.listVilles.getValue(new_item.CODE_VILLE);
          this.localisations.push(new_item);
        };

        this.getFicheQualificative();


      },
      error => console.log(error));

  }


  getEvaluationFonciere(){
 this.service.getEvaluationFonciere(this.selectedId)
      .then(
      data => {
        try {
          if (data[0] === undefined || data[0] === null) {
          } else {
            this.evaluationfonciere.populate(data[0]);

            jQuery.unblockUI();
            this.searching = false;
            this.recalculateNavbarHeight();
          }
        } catch (error) {
        } finally {
        }
      },
      error => this.errorMessage = <any>error);

  }
  getFicheQualificative() {


    this.service.getFicheQualificative(this.selectedId)
      .then(
      data => {
        try {
          //console.log(data);
          //this.qualificationTerrain.populate(data);
          if (data[0] === undefined || data[0] === null) {
            //console.log(data);
          } else {
            //console.log(data);
            this.fichequalificative.populate(data[0]);
            this.getEvaluationFonciere();
          }
        } catch (error) {
          //console.log(error);
        } finally {

        }
      },
      error => this.errorMessage = <any>error);


  }

  getRire() {
    this.service.getRire()
      .then(
      data => {

        for (let info of data) {
          let rire_item: Rire = new Rire(info);
          let available_item: any = {};
          available_item.id = rire_item.NO_VOIE;
          available_item.value = rire_item.NOM_VOIE_LONG;



          this.listRire[rire_item.NO_VOIE + " " + rire_item.NOM_VOIE_LONG] = rire_item;
          this.availableRire.push(available_item);


        };


        //console.log(this.availableRire);

      },
      error => console.log(error));

  }

  validateCode(mode: string = "m", value: string) {

    if (mode == "m") {
      if (value.indexOf("D") > -1) {
        this.dossier.CODE = "";
      } else this.dossier.CODE = value;
    } else {
      //mode = "c"
      if (value.indexOf("D") > -1) {
        this.dossier.CODE = "";
      } else this.dossier.CODE = value;
    }

  }



  syncOnglets(id: string, papa: string) {
    jQuery("div.tab-pane").removeClass("active");
    jQuery("div.tab-pane").removeClass("in");

    jQuery("#" + id + "").addClass("active");
    jQuery("#" + id + "").addClass("in");

    jQuery("#" + id + "_c").addClass("active");
    jQuery("#" + id + "_c").addClass("in");

    jQuery("#" + id + "_m").addClass("active");
    jQuery("#" + id + "_m").addClass("in");

    this.syncPOnglet(papa);
  }

  syncPOnglet(id: string) {
    jQuery("li.dropdown").removeClass("active");
    jQuery("li.dropdown." + id).addClass("active");
  }



  syncAddresses() {
    if (this.addressesOpen) {
      this.addressesOpen = false;
      jQuery(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
      //jQuery(".panel-collapse").addClass("in");
      jQuery(".panel-collapse").slideUp("slow", function () { });

    } else {
      this.addressesOpen = true;

      //jQuery(".panel-collapse").removeClass("in");
      jQuery(".panel-collapse").slideDown("slow", function () { });
      jQuery(".glyphicon-chevron-up").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
    }




  }


  addAddress() {
    var count = this.addresses.length;
    var new_ind = BIG_NUMBER + count;

    var new_address = new Address();
    new_address.ADR_ID = new_ind;
    new_address.IMM_IDE = this.selectedId;

    this.addresses.push(new_address);


  }

  delAddress(ind: number, adr_id: number = 0) {
    this.addresses2delete.push(adr_id);
    this.addresses.splice(ind, 1);

  }


  addLocalisation() {
    var count = this.localisations.length;
    var new_ind = BIG_NUMBER + count;

    var new_localisation = new Localisation();
    new_localisation.LOCALISATION_ID = new_ind;
    new_localisation.IMM_IDE = this.selectedId;

    this.localisations.push(new_localisation);


  }

  delLocalisation(ind: number, adr_id: number = 0) {
    this.localisations2delete.push(adr_id);
    this.localisations.splice(ind, 1);

  }



  updatePrincipalAddress(new_principal: number) {
    this.address_principal = new_principal;

    this.addresses.forEach((address, index) => {
      if (address.ADR_ID != this.address_principal) {
        address.ADR_PRINC = 0;
      } else address.ADR_PRINC = -1;
    });


  }


  ngOnInit() {


    this.updateData();
    this.getLists();
    this.interactive.position = "dossier_view";

  }




}
