"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var dossier_service_1 = require('./dossier.service');
var interactive_service_1 = require('../common/interactive.service');
require('../common/rxjs-operators');
var http_1 = require('@angular/http');
var dossier_detail_component_1 = require('./dossier-detail.component');
var dossier_1 = require('./dossier');
var qualificationterrain_1 = require('./qualificationterrain');
var fichequalificative_1 = require('./fichequalificative');
var evaluation_fonciere_1 = require('./evaluation-fonciere');
var DossiermodifyComponent = (function () {
    function DossiermodifyComponent(route, router, service, http, _parent, interactive) {
        this.route = route;
        this.router = router;
        this.service = service;
        this.http = http;
        this._parent = _parent;
        this.interactive = interactive;
        this.suffix = "_m";
        interactive.dossier_modify = this;
    }
    DossiermodifyComponent.prototype.ModifyDossier = function () { this._parent.showDossierModify(); };
    DossiermodifyComponent.prototype.CreateDossier = function () { this.clean(); this._parent.showDossierCreate(); };
    DossiermodifyComponent.prototype.showDossierView = function () { this._parent.showDossierView(); };
    DossiermodifyComponent.prototype.DossierSaveModify = function () {
        var _this = this;
        if ((this.dossier_code.errors === undefined || this.dossier_code.errors === null) &&
            (this.dossier_arrondissement.errors === undefined || this.dossier_arrondissement.errors === null) &&
            (this.dossier_categorie.errors === undefined || this.dossier_categorie.errors === null) &&
            (this.dossier_numero.errors === undefined || this.dossier_numero.errors === null)) {
            jQuery.blockUI({ message: '<p> Communication avec la base de données en cours...</p>' });
            var current_dossier_service = this.service;
            this.service.modifyDossier(this._parent.dossier)
                .then(function (data) {
                //ADDRESSES
                _this.service.saveAddresses(_this._parent.addresses, _this._parent.addresses2delete);
                //LOCALISATIONS
                _this.service.saveLocalisations(_this._parent.localisations, _this._parent.localisations2delete);
                //QUALIFICATION TERRAIN 
                _this.service.modifyQualificationTerrain(_this._parent.qualificationTerrain)
                    .then(function (data) {
                    _this.service.modifyFicheQualificative(_this._parent.fichequalificative)
                        .then(function (data) {
                        _this.showDossierView();
                    }, //qualificationTerrain
                    function (//qualificationTerrain
                        error) { return _this.errorMessage = error; });
                }, //qualificationTerrain
                function (//qualificationTerrain
                    error) { return _this.errorMessage = error; });
            }, //dossier
            function (//dossier
                error) { return _this.errorMessage = error; });
        }
        else
            return false;
    };
    DossiermodifyComponent.prototype.DossierSaveCreate = function () {
        var _this = this;
        if ((this.dossier_code.errors === undefined || this.dossier_code.errors === null) &&
            (this.dossier_arrondissement.errors === undefined || this.dossier_arrondissement.errors === null) &&
            (this.dossier_categorie.errors === undefined || this.dossier_categorie.errors === null) &&
            (this.dossier_numero.errors === undefined || this.dossier_numero.errors === null)) {
            jQuery.blockUI({ message: '<p> Communication avec la base de données en cours...</p>' });
            this.service.createDossier(this._parent.dossier)
                .then(function (data) {
                var created_dossier_id = data[0];
                _this._parent.qualificationTerrain.IMM_IDE = created_dossier_id;
                _this._parent.fichequalificative.IMM_ID = created_dossier_id;
                _this._parent.addresses.forEach(function (addr) { addr.IMM_IDE = created_dossier_id; });
                _this._parent.localisations.forEach(function (addr) { addr.IMM_IDE = created_dossier_id; });
                //ADDRESSES
                _this.service.saveAddresses(_this._parent.addresses, []);
                //LOCALISATIONS
                _this.service.saveLocalisations(_this._parent.localisations, []);
                _this.service.createQualificationTerrain(_this._parent.qualificationTerrain)
                    .then(function (data) {
                    _this.service.createFicheQualificative(_this._parent.fichequalificative)
                        .then(function (data) { return window.location.href = CONFIG_CLIENT_URL + '/dossier/' + created_dossier_id; }, function (error) { return _this.errorMessage = error; });
                }, function (error) { return _this.errorMessage = error; });
            }, function (error) { return _this.errorMessage = error; });
            return true;
        }
        else
            return false;
    };
    DossiermodifyComponent.prototype.clean = function () {
        this._parent.dossier = new dossier_1.Dossier();
        this._parent.qualificationTerrain = new qualificationterrain_1.QualificationTerrain();
        this._parent.fichequalificative = new fichequalificative_1.FicheQualificative();
        this._parent.evaluationfonciere = new evaluation_fonciere_1.EvaluationFonciere();
        //Addreses & Localisation
        this._parent.addresses = [];
        this._parent.addresses2delete = [];
        this._parent.localisations = [];
        this._parent.localisations2delete = [];
        this._parent.ignoreChange = false;
        this._parent.autocompleteAddressChanged = false;
    };
    DossiermodifyComponent.prototype.ngAfterViewInit = function () { };
    DossiermodifyComponent.prototype.ngOnInit = function () { };
    __decorate([
        core_1.ViewChild('dossier_code'), 
        __metadata('design:type', Object)
    ], DossiermodifyComponent.prototype, "dossier_code", void 0);
    __decorate([
        core_1.ViewChild('dossier_arrondissement'), 
        __metadata('design:type', Object)
    ], DossiermodifyComponent.prototype, "dossier_arrondissement", void 0);
    __decorate([
        core_1.ViewChild('dossier_numero'), 
        __metadata('design:type', Object)
    ], DossiermodifyComponent.prototype, "dossier_numero", void 0);
    __decorate([
        core_1.ViewChild('dossier_competence'), 
        __metadata('design:type', Object)
    ], DossiermodifyComponent.prototype, "dossier_competence", void 0);
    __decorate([
        core_1.ViewChild('dossier_projet'), 
        __metadata('design:type', Object)
    ], DossiermodifyComponent.prototype, "dossier_projet", void 0);
    __decorate([
        core_1.ViewChild('dossier_categorie'), 
        __metadata('design:type', Object)
    ], DossiermodifyComponent.prototype, "dossier_categorie", void 0);
    DossiermodifyComponent = __decorate([
        core_1.Component({
            selector: 'dossier-modify',
            moduleId: module.id,
            templateUrl: 'dossier-modify.html'
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, dossier_service_1.DossierService, http_1.Http, dossier_detail_component_1.DossierdetailComponent, interactive_service_1.InteractiveService])
    ], DossiermodifyComponent);
    return DossiermodifyComponent;
}());
exports.DossiermodifyComponent = DossiermodifyComponent;
//# sourceMappingURL=dossier-modify.component.js.map