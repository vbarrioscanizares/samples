import { Component, Input, ViewChild } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DossierService } from './dossier.service';
import { InteractiveService } from '../common/interactive.service';
import { Observable } from 'rxjs/Observable';
import '../common/rxjs-operators';
import { Http, Response } from '@angular/http';
import { DossierdetailComponent } from './dossier-detail.component';
import { Dossier } from './dossier';
import { Address } from './address';
import { Localisation } from './localisation';
import { Rire } from './rire';
import { QualificationTerrain } from './qualificationterrain';
import { FicheQualificative } from './fichequalificative';
import { EvaluationFonciere } from './evaluation-fonciere';
declare var jQuery: any;

@Component({
  selector: 'dossier-modify',
  moduleId: module.id,
  templateUrl: 'dossier-modify.html'
})


export class DossiermodifyComponent {
  errorMessage: string;
  //dossier:  Dossier;
  data: string;
  suffix: string = "_m";

  @ViewChild('dossier_code') dossier_code: any;
  @ViewChild('dossier_arrondissement') dossier_arrondissement: any;
  @ViewChild('dossier_numero') dossier_numero: any;
  @ViewChild('dossier_competence') dossier_competence: any;
  @ViewChild('dossier_projet') dossier_projet: any;
  @ViewChild('dossier_categorie') dossier_categorie: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: DossierService,
    private http: Http,
    private _parent: DossierdetailComponent,
    private interactive: InteractiveService
  ) {

    interactive.dossier_modify = this;
  }

  ModifyDossier() { this._parent.showDossierModify(); }
  CreateDossier() { this.clean(); this._parent.showDossierCreate(); }
  showDossierView() { this._parent.showDossierView(); }

  DossierSaveModify(): Boolean {



    if ((this.dossier_code.errors === undefined || this.dossier_code.errors === null) &&
      (this.dossier_arrondissement.errors === undefined || this.dossier_arrondissement.errors === null) &&
      (this.dossier_categorie.errors === undefined || this.dossier_categorie.errors === null) &&
      (this.dossier_numero.errors === undefined || this.dossier_numero.errors === null)
    ) {


      jQuery.blockUI({ message: '<p> Communication avec la base de données en cours...</p>' });
      let current_dossier_service = this.service;
      this.service.modifyDossier(this._parent.dossier)
        .then(
        data => {

          //ADDRESSES
          this.service.saveAddresses(this._parent.addresses, this._parent.addresses2delete)

          //LOCALISATIONS
          this.service.saveLocalisations(this._parent.localisations, this._parent.localisations2delete)

          //QUALIFICATION TERRAIN 
          this.service.modifyQualificationTerrain(this._parent.qualificationTerrain)
            .then(
            data => {

              this.service.modifyFicheQualificative(this._parent.fichequalificative)
                .then(
                data => {
                  this.showDossierView();
                },//qualificationTerrain
                error => this.errorMessage = <any>error);
            },//qualificationTerrain
            error => this.errorMessage = <any>error);




        },//dossier
        error => this.errorMessage = <any>error);


    } else return false;




  }

  DossierSaveCreate(): Boolean {


    if ((this.dossier_code.errors === undefined || this.dossier_code.errors === null) &&
      (this.dossier_arrondissement.errors === undefined || this.dossier_arrondissement.errors === null) &&
      (this.dossier_categorie.errors === undefined || this.dossier_categorie.errors === null) &&
      (this.dossier_numero.errors === undefined || this.dossier_numero.errors === null)
    ) {
      jQuery.blockUI({ message: '<p> Communication avec la base de données en cours...</p>' });
      this.service.createDossier(this._parent.dossier)
        .then(
        data => {
          var created_dossier_id = data[0];

          this._parent.qualificationTerrain.IMM_IDE = created_dossier_id;
          this._parent.fichequalificative.IMM_ID = created_dossier_id;
          this._parent.addresses.forEach(function (addr: Address) { addr.IMM_IDE = created_dossier_id; });
          this._parent.localisations.forEach(function (addr: Localisation) { addr.IMM_IDE = created_dossier_id; });
          //ADDRESSES
          this.service.saveAddresses(this._parent.addresses, [])

          //LOCALISATIONS
          this.service.saveLocalisations(this._parent.localisations, [])

          this.service.createQualificationTerrain(this._parent.qualificationTerrain)
            .then(
            data => {
              this.service.createFicheQualificative(this._parent.fichequalificative)
                .then(
                data => window.location.href = CONFIG_CLIENT_URL + '/dossier/' + created_dossier_id
                ,
                error => this.errorMessage = <any>error);
            }
            ,
            error => this.errorMessage = <any>error);
        },
        error => this.errorMessage = <any>error);

      return true;
    } else return false;




  }

  clean() {
    this._parent.dossier = new Dossier();
    this._parent.qualificationTerrain = new QualificationTerrain();
    this._parent.fichequalificative = new FicheQualificative();
    this._parent.evaluationfonciere = new EvaluationFonciere();

    //Addreses & Localisation
    this._parent.addresses = [];
    this._parent.addresses2delete = [];
    this._parent.localisations = [];
    this._parent.localisations2delete = [];
    this._parent.ignoreChange = false;
    this._parent.autocompleteAddressChanged = false;

  }

  ngAfterViewInit() { }
  ngOnInit() { }
}
