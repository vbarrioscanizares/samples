"use strict";
var Dossier = (function () {
    function Dossier() {
        this.COMPTABILISE = 0;
        this.MANDAT_ACTIF = 0;
        this.ECO_TERRITOIRE = 0;
        this.DONE = 0;
        this.DESC_COMPETENCE = "";
        this.DESC_OPERATION_LOGEMENT = "";
    }
    Dossier.prototype.toJson = function () { return JSON.stringify(this); };
    Dossier.prototype.populate = function (data) {
        this.ID = data.ID;
        this.NUMERO = data.NUMERO;
        this.CODE = data.CODE;
        this.COMPTABILISE = data.COMPTABILISE;
        this.NOM = data.NOM;
        this.LOCALISATION = data.LOCALISATION;
        this.MANDAT_ACTIF = data.MANDAT_ACTIF;
        this.CAD_ID = data.CAD_ID;
        this.CAT_ID = data.CAT_ID;
        this.CATEGORIE = data.CATEGORIE;
        this.ARR_ID = data.ARR_ID;
        this.ARRONDISSEMENT = data.ARRONDISSEMENT;
        this.NO_REGLEMENT = data.NO_REGLEMENT;
        this.ECO_TERRITOIRE = data.ECO_TERRITOIRE;
        this.DESCRIPTION_ECO_TERRITOIRE = data.DESCRIPTION_ECO_TERRITOIRE;
        this.PROJET_1500_LOGEMENTS = data.PROJET_1500_LOGEMENTS;
        this.DONE = data.DONE;
        this.COMPETENCE = data.COMPETENCE;
        this.TYPE_ID = data.TYPE_ID;
        this.TYPE = data.TYPE;
        this.FACADE = data.FACADE;
        this.FACADEM = data.FACADEM;
        this.PROFONDEUR = data.PROFONDEUR;
        this.PROFONDEURM = data.PROFONDEURM;
        this.SUPERFICIE = data.SUPERFICIE;
        this.SUPERFICIEM = data.SUPERFICIEM;
        this.VOCATION = data.VOCATION;
        this.REMARQUE = data.REMARQUE;
        this.VISITENOTE = data.VISITENOTE;
        this.VISITEDATE = data.VISITEDATE;
        this.VISITEEMP_ID = data.VISITEEMP_ID;
        this.EMP_NOM = data.EMP_NOM;
        this.DATE_MISE_A_JOUR = data.DATE_MISE_A_JOUR;
        this.DESC_MANDAT_ACTIF = data.DESC_MANDAT_ACTIF;
        this.DESC_COMPTABILISE = data.DESC_COMPTABILISE;
        this.DESC_ECO_TERR = data.DESC_ECO_TERR;
        this.MISE_A_JOUR = data.MISE_A_JOUR;
        this.CADASTRE = data.CADASTRE;
        this.DESC_ECO_TERRITOIRE = data.DESC_ECO_TERRITOIRE;
    };
    return Dossier;
}());
exports.Dossier = Dossier;
//# sourceMappingURL=dossier.js.map