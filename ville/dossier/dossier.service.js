"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/operator/toPromise');
var http_2 = require('@angular/http');
require('../common/rxjs-operators');
require('../common/configuration');
var DossierService = (function () {
    function DossierService(http, jsonp) {
        this.http = http;
        this.jsonp = jsonp;
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
    }
    DossierService.prototype.extractData = function (res) {
        var body = res.json();
        return body;
    };
    DossierService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        //console.error(errMsg);
        return Observable_1.Observable.throw(errMsg);
    };
    DossierService.prototype.getDossier = function (id) {
        return this.http.get(CONFIG_SERVER_URL + '/dossier/' + id)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.createDossier = function (dossier) {
        /*let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let body = JSON.stringify(dossier);*/
        // return this.http.put(CONFIG_SERVER_URL+'/dossier', body, headers)
        return this.http.put(CONFIG_SERVER_URL + '/dossier', dossier)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.modifyDossier = function (dossier) {
        // console.log(dossier);
        return this.http.post(CONFIG_SERVER_URL + '/dossier/' + dossier.ID, dossier)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.deleteDossier = function (id) {
        return this.http.delete(CONFIG_SERVER_URL + '/dossier/' + id)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeArrondissements = function () {
        return this.http.get(CONFIG_SERVER_URL + '/arrondissements')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getQualificationTerrain = function (id) {
        return this.http.get(CONFIG_SERVER_URL + '/qualification-terrain/' + id)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.createQualificationTerrain = function (qualificationTerrain) {
        return this.http.put(CONFIG_SERVER_URL + '/qualification-terrain', qualificationTerrain)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.modifyQualificationTerrain = function (item) {
        return this.http.post(CONFIG_SERVER_URL + '/qualification-terrain/' + item.QT_ID, item)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getFicheQualificative = function (id) {
        return this.http.get(CONFIG_SERVER_URL + '/fiche-qualificative/' + id)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.createFicheQualificative = function (item) {
        return this.http.put(CONFIG_SERVER_URL + '/fiche-qualificative', item)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.modifyFicheQualificative = function (item) {
        return this.http.post(CONFIG_SERVER_URL + '/fiche-qualificative/' + item.IMM_ID, item)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getAddresses = function (id) {
        return this.http.get(CONFIG_SERVER_URL + '/addresses-dossier/' + id)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.createAddress = function (address) {
        return this.http.put(CONFIG_SERVER_URL + '/addresses-dossier', address)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.modifyAddress = function (address) {
        return this.http.post(CONFIG_SERVER_URL + '/addresses-dossier/' + address.ADR_ID, address)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.deleteAddress = function (addr_id) {
        return this.http.delete(CONFIG_SERVER_URL + '/addresses-dossier/' + addr_id)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getLocalisations = function (id) {
        return this.http.get(CONFIG_SERVER_URL + '/localisations-dossier/' + id)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.createLocalisation = function (localisations) {
        return this.http.put(CONFIG_SERVER_URL + '/localisations-dossier', localisations)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.modifyLocalisation = function (localisations) {
        return this.http.post(CONFIG_SERVER_URL + '/localisations-dossier/' + localisations.LOCALISATION_ID, localisations)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.deleteLocalisation = function (_id) {
        return this.http.delete(CONFIG_SERVER_URL + '/localisations-dossier/' + _id)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeCadastres = function () {
        return this.http.get(CONFIG_SERVER_URL + '/cadastres')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeCategories = function () {
        return this.http.get(CONFIG_SERVER_URL + '/categories')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeTypes = function () {
        return this.http.get(CONFIG_SERVER_URL + '/listeTypeDossier')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeEcotterritoires = function () {
        return this.http.get(CONFIG_SERVER_URL + '/eco-territoires')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeDevelop = function () {
        return this.http.get(CONFIG_SERVER_URL + '/listeDevelop')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListePlanAction = function () {
        return this.http.get(CONFIG_SERVER_URL + '/listePlanAction')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeTopographie = function () {
        return this.http.get(CONFIG_SERVER_URL + '/listeTopographie')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeDescSecteur = function () {
        return this.http.get(CONFIG_SERVER_URL + '/listeDescSecteur')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeCompetence = function () {
        return this.http.get(CONFIG_SERVER_URL + '/listeCompetence')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeOperationLogement = function () {
        return this.http.get(CONFIG_SERVER_URL + '/listeOperationLogement')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getListeVilles = function () {
        return this.http.get(CONFIG_SERVER_URL + '/listeVilles')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getRire = function () {
        return this.http.get(CONFIG_SERVER_URL + '/rire')
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.getEvaluationFonciere = function (_id) {
        return this.http.get(CONFIG_SERVER_URL + '/evaluation-dossier/' + _id)
            .toPromise()
            .then(this.extractData)
            .catch(this.handleError);
    };
    DossierService.prototype.saveAddresses = function (addresses, addresses2delete) {
        var current_service = this;
        //console.log("SaveAddresses", addresses);
        addresses2delete.forEach(function (addr_ind) {
            //DELETE
            current_service.deleteAddress(addr_ind);
        });
        addresses.forEach(function (addr) {
            //console.log(addr);
            if (addr.ADR_ID >= BIG_NUMBER) {
                //CREATE
                current_service.createAddress(addr);
            }
            else {
                //MODIFY
                current_service.modifyAddress(addr);
            }
        });
    };
    DossierService.prototype.saveLocalisations = function (localisations, localisations2delete) {
        //console.log("SaveLocalisations", localisations);
        var current_service = this;
        localisations2delete.forEach(function (_ind) {
            //DELETE
            current_service.deleteLocalisation(_ind);
        });
        localisations.forEach(function (loc) {
            //console.log(addr);
            if (loc.LOCALISATION_ID >= BIG_NUMBER) {
                //CREATE
                current_service.createLocalisation(loc);
            }
            else {
                //MODIFY
                current_service.modifyLocalisation(loc);
            }
        });
    };
    DossierService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, http_2.Jsonp])
    ], DossierService);
    return DossierService;
}());
exports.DossierService = DossierService;
//# sourceMappingURL=dossier.service.js.map