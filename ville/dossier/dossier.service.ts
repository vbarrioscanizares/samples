
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { Jsonp } from '@angular/http';
import { Dossier } from './dossier';
import { QualificationTerrain } from './qualificationterrain';
import { FicheQualificative } from './fichequalificative';
import { EvaluationFonciere } from './evaluation-fonciere';

import { List } from './list';
import { Address } from './address';
import { Localisation } from './localisation';
import { Rire } from './rire';
import '../common/rxjs-operators';
import '../common/configuration';



@Injectable()
export class DossierService {
  private headers = new Headers({ 'Content-Type': 'application/json' });
  constructor(private http: Http, private jsonp: Jsonp) { }


  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    //console.error(errMsg);
    return Observable.throw(errMsg);
  }



  getDossier(id: number): Promise<Dossier> {
    return this.http.get(CONFIG_SERVER_URL + '/dossier/' + id)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }








  createDossier(dossier: Dossier) {
    /*let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(dossier);*/

    // return this.http.put(CONFIG_SERVER_URL+'/dossier', body, headers)
    return this.http.put(CONFIG_SERVER_URL + '/dossier', dossier)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }


  modifyDossier(dossier: Dossier) {
    // console.log(dossier);

    return this.http.post(CONFIG_SERVER_URL + '/dossier/' + dossier.ID, dossier)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }


  deleteDossier(id: number) {
    return this.http.delete(CONFIG_SERVER_URL + '/dossier/' + id)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }


  getListeArrondissements(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/arrondissements')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }



  getQualificationTerrain(id: number): Promise<QualificationTerrain> {
    return this.http.get(CONFIG_SERVER_URL + '/qualification-terrain/' + id)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  createQualificationTerrain(qualificationTerrain: QualificationTerrain) {
    return this.http.put(CONFIG_SERVER_URL + '/qualification-terrain', qualificationTerrain)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }


  modifyQualificationTerrain(item: QualificationTerrain) {
    return this.http.post(CONFIG_SERVER_URL + '/qualification-terrain/' + item.QT_ID, item)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }


  getFicheQualificative(id: number): Promise<FicheQualificative> {
    return this.http.get(CONFIG_SERVER_URL + '/fiche-qualificative/' + id)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  createFicheQualificative(item: FicheQualificative) {
    return this.http.put(CONFIG_SERVER_URL + '/fiche-qualificative', item)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }


  modifyFicheQualificative(item: FicheQualificative) {
    return this.http.post(CONFIG_SERVER_URL + '/fiche-qualificative/' + item.IMM_ID, item)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }


  getAddresses(id: number): Promise<Address[]> {
    return this.http.get(CONFIG_SERVER_URL + '/addresses-dossier/' + id)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  createAddress(address: Address) {
    return this.http.put(CONFIG_SERVER_URL + '/addresses-dossier', address)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }


  modifyAddress(address: Address) {
    return this.http.post(CONFIG_SERVER_URL + '/addresses-dossier/' + address.ADR_ID, address)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }

  deleteAddress(addr_id: number) {
    return this.http.delete(CONFIG_SERVER_URL + '/addresses-dossier/' + addr_id)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }






  getLocalisations(id: number): Promise<Localisation[]> {
    return this.http.get(CONFIG_SERVER_URL + '/localisations-dossier/' + id)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  createLocalisation(localisations: Localisation) {
    return this.http.put(CONFIG_SERVER_URL + '/localisations-dossier', localisations)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }


  modifyLocalisation(localisations: Localisation) {
    return this.http.post(CONFIG_SERVER_URL + '/localisations-dossier/' + localisations.LOCALISATION_ID, localisations)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }

  deleteLocalisation(_id: number) {
    return this.http.delete(CONFIG_SERVER_URL + '/localisations-dossier/' + _id)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);

  }










  getListeCadastres(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/cadastres')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  getListeCategories(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/categories')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  getListeTypes(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/listeTypeDossier')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  getListeEcotterritoires(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/eco-territoires')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }


  getListeDevelop(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/listeDevelop')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }


  getListePlanAction(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/listePlanAction')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }


  getListeTopographie(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/listeTopographie')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  getListeDescSecteur(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/listeDescSecteur')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }


  getListeCompetence(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/listeCompetence')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }


  getListeOperationLogement(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/listeOperationLogement')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }




  getListeVilles(): Promise<List> {
    return this.http.get(CONFIG_SERVER_URL + '/listeVilles')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }


  getRire(): Promise<Rire[]> {
    return this.http.get(CONFIG_SERVER_URL + '/rire')
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }

  getEvaluationFonciere(_id: number): Promise<EvaluationFonciere> {
    return this.http.get(CONFIG_SERVER_URL + '/evaluation-dossier/'+_id)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleError);
  }



  saveAddresses(addresses: Address[], addresses2delete: number[]) {
    var current_service = this;
    //console.log("SaveAddresses", addresses);
    addresses2delete.forEach(function (addr_ind: number) {
      //DELETE
      current_service.deleteAddress(addr_ind);
    });
    addresses.forEach(function (addr: Address) {

      //console.log(addr);
      if (addr.ADR_ID >= BIG_NUMBER) {
        //CREATE
        current_service.createAddress(addr);
      } else {
        //MODIFY
        current_service.modifyAddress(addr);
      }
    });
  }

  saveLocalisations(localisations: Localisation[], localisations2delete: number[]) {
    //console.log("SaveLocalisations", localisations);
    var current_service = this;
    localisations2delete.forEach(function (_ind: number) {
      //DELETE
      current_service.deleteLocalisation(_ind);
    });
    localisations.forEach(function (loc: Localisation) {

      //console.log(addr);
      if (loc.LOCALISATION_ID >= BIG_NUMBER) {
        //CREATE
        current_service.createLocalisation(loc);
      } else {
        //MODIFY
        current_service.modifyLocalisation(loc);
      }
    });
  }





}