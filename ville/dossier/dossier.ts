export class Dossier {
        public ID: number;
        public NUMERO:string;
        public CODE:string;
        public COMPTABILISE:number = 0;
        public NOM:string;
        public LOCALISATION:string;
        public MANDAT_ACTIF:number = 0;
        public CAD_ID:number;
        public CAT_ID:number;
        public CATEGORIE:string;
        public ARR_ID:number;
        public ARRONDISSEMENT:string;
        public NO_REGLEMENT:string;
        public ECO_TERRITOIRE:number = 0;
        public DESCRIPTION_ECO_TERRITOIRE:string;
        public DESC_ECO_TERRITOIRE:string;
        public PROJET_1500_LOGEMENTS:string;
        public DONE:number = 0;
        public COMPETENCE:string;
        public TYPE_ID:number;
        public TYPE:string;
        public FACADE:number;
        public FACADEM:number;
        public PROFONDEUR:number;
        public PROFONDEURM:number;
        public SUPERFICIE:number;
        public SUPERFICIEM:number;
        public VOCATION:string;
        public REMARQUE:string;
        public VISITENOTE:string;
        public VISITEDATE:string;
        public VISITEEMP_ID:number;
        public EMP_NOM:string;
        public DATE_MISE_A_JOUR:string;
        public DESC_MANDAT_ACTIF:string;
        public DESC_COMPTABILISE:string;
        public DESC_ECO_TERR:string;
        public MISE_A_JOUR:string;
        public CADASTRE:string;

        public DESC_COMPETENCE:string = "";
        public DESC_OPERATION_LOGEMENT:string = "";

  constructor() {}

  toJson():string{return JSON.stringify(this);}

   populate(data : any){
        this.ID = data.ID;
        this.NUMERO = data.NUMERO;
        this.CODE = data.CODE;
        this.COMPTABILISE= data.COMPTABILISE;
        this.NOM = data.NOM;
        this.LOCALISATION = data.LOCALISATION;
        this.MANDAT_ACTIF = data.MANDAT_ACTIF;
        this.CAD_ID = data.CAD_ID;
        this.CAT_ID = data.CAT_ID;
        this.CATEGORIE = data.CATEGORIE;
        this.ARR_ID = data.ARR_ID;
        this.ARRONDISSEMENT = data.ARRONDISSEMENT;
        this.NO_REGLEMENT = data.NO_REGLEMENT;
        this.ECO_TERRITOIRE = data.ECO_TERRITOIRE;
        this.DESCRIPTION_ECO_TERRITOIRE = data.DESCRIPTION_ECO_TERRITOIRE;
        this.PROJET_1500_LOGEMENTS = data.PROJET_1500_LOGEMENTS;
        this.DONE = data.DONE;
        this.COMPETENCE = data.COMPETENCE;
        this.TYPE_ID = data.TYPE_ID;
        this.TYPE = data.TYPE;
        this.FACADE = data.FACADE;
        this.FACADEM = data.FACADEM;
        this.PROFONDEUR = data.PROFONDEUR;
        this.PROFONDEURM = data.PROFONDEURM;
        this.SUPERFICIE = data.SUPERFICIE;
        this.SUPERFICIEM = data.SUPERFICIEM;
        this.VOCATION = data.VOCATION;
        this.REMARQUE = data.REMARQUE;
        this.VISITENOTE = data.VISITENOTE;
        this.VISITEDATE = data.VISITEDATE;
        this.VISITEEMP_ID = data.VISITEEMP_ID;
        this.EMP_NOM = data.EMP_NOM;
        this.DATE_MISE_A_JOUR = data.DATE_MISE_A_JOUR;
        this.DESC_MANDAT_ACTIF = data.DESC_MANDAT_ACTIF;
        this.DESC_COMPTABILISE = data.DESC_COMPTABILISE;
        this.DESC_ECO_TERR = data.DESC_ECO_TERR;
        this.MISE_A_JOUR = data.MISE_A_JOUR;
        this.CADASTRE = data.CADASTRE;
        this.DESC_ECO_TERRITOIRE = data.DESC_ECO_TERRITOIRE;

        
      
   }
}