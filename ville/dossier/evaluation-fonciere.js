"use strict";
var EvaluationFonciere = (function () {
    function EvaluationFonciere() {
    }
    EvaluationFonciere.prototype.toJson = function () { return JSON.stringify(this); };
    EvaluationFonciere.prototype.populate = function (data) {
        this.MAXANNEEROLE = data.MAXANNEEROLE;
        this.SUMEVALUATIONTERRAIN = data.SUMEVALUATIONTERRAIN;
        this.SUMEVALUATIONBATISSE = data.SUMEVALUATIONBATISSE;
        this.SUMEVALUATIONTOTALE = data.SUMEVALUATIONTOTALE;
        this.SUMTOTALSURFACEM = data.SUMTOTALSURFACEM;
    };
    return EvaluationFonciere;
}());
exports.EvaluationFonciere = EvaluationFonciere;
//# sourceMappingURL=evaluation-fonciere.js.map