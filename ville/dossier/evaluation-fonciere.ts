export class EvaluationFonciere {



    public MAXANNEEROLE: string;
    public SUMEVALUATIONTERRAIN: string;
    public SUMEVALUATIONBATISSE: string;
    public SUMEVALUATIONTOTALE: string;
    public SUMTOTALSURFACEM: string;



    constructor() { }

    toJson(): string { return JSON.stringify(this); }

    populate(data: any) {
        this.MAXANNEEROLE = data.MAXANNEEROLE;
        this.SUMEVALUATIONTERRAIN = data.SUMEVALUATIONTERRAIN;
        this.SUMEVALUATIONBATISSE = data.SUMEVALUATIONBATISSE;
        this.SUMEVALUATIONTOTALE = data.SUMEVALUATIONTOTALE;
        this.SUMTOTALSURFACEM = data.SUMTOTALSURFACEM;



    }
}