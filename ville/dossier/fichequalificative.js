"use strict";
var FicheQualificative = (function () {
    function FicheQualificative() {
    }
    FicheQualificative.prototype.toJson = function () { return JSON.stringify(this); };
    FicheQualificative.prototype.populate = function (data) {
        this.IMM_ID = data.IMM_ID;
        this.TITRE_PROPRIETE = data.TITRE_PROPRIETE;
        this.SERVITUDES = data.SERVITUDES;
        this.SPECIF_SOL = data.SPECIF_SOL;
        this.BATIM_SUR_IMM = data.BATIM_SUR_IMM;
        this.REFERENCE = data.REFERENCE;
        this.CAT_USAGE = data.CAT_USAGE;
        this.DENSITE = data.DENSITE;
        this.MARGES = data.MARGES;
        this.HAUTEUR_METRES = data.HAUTEUR_METRES;
        this.HAUTEUR_ETAGES = data.HAUTEUR_ETAGES;
        this.AQUEDUC = data.AQUEDUC;
        this.EGOUTS = data.EGOUTS;
        this.ELECTRICITE = data.ELECTRICITE;
        this.NATURE_VOIES = data.NATURE_VOIES;
        this.CAPACITE_RESEAU = data.CAPACITE_RESEAU;
        this.ADAPTABILITE = data.ADAPTABILITE;
        this.TRANSP_COMMUN = data.TRANSP_COMMUN;
        this.UTILISATION_HOMOG = data.UTILISATION_HOMOG;
        this.ELEM_POSITIF = data.ELEM_POSITIF;
        this.ELEM_NEGATIF = data.ELEM_NEGATIF;
        this.CARAC_SPECIF_NOTES = data.CARAC_SPECIF_NOTES;
        this.NORMES_REGLEM_NOTES = data.NORMES_REGLEM_NOTES;
        this.ACCES_NOTES = data.ACCES_NOTES;
        this.MILIEU_BATIM_NOTES = data.MILIEU_BATIM_NOTES;
        this.CONCLUSION = data.CONCLUSION;
    };
    return FicheQualificative;
}());
exports.FicheQualificative = FicheQualificative;
//# sourceMappingURL=fichequalificative.js.map