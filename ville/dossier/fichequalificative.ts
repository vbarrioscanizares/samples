export class FicheQualificative {


    public IMM_ID: string;
    public TITRE_PROPRIETE: string;
    public SERVITUDES: string;
    public SPECIF_SOL: string;
    public BATIM_SUR_IMM: string;
    public REFERENCE: string;
    public CAT_USAGE: string;
    public DENSITE: string;
    public MARGES: string;
    public HAUTEUR_METRES: string;
    public HAUTEUR_ETAGES: string;
    public AQUEDUC: string;
    public EGOUTS: string;
    public ELECTRICITE: string;
    public NATURE_VOIES: string;
    public CAPACITE_RESEAU: string;
    public ADAPTABILITE: string;
    public TRANSP_COMMUN: string;
    public UTILISATION_HOMOG: string;
    public ELEM_POSITIF: string;
    public ELEM_NEGATIF: string;
    public CARAC_SPECIF_NOTES: string;
    public NORMES_REGLEM_NOTES: string;
    public ACCES_NOTES: string;
    public MILIEU_BATIM_NOTES: string;
    public CONCLUSION: string;




    constructor() { }

    toJson(): string { return JSON.stringify(this); }

    populate(data: any) {


        this.IMM_ID = data.IMM_ID;
        this.TITRE_PROPRIETE = data.TITRE_PROPRIETE;
        this.SERVITUDES = data.SERVITUDES;
        this.SPECIF_SOL = data.SPECIF_SOL;
        this.BATIM_SUR_IMM = data.BATIM_SUR_IMM;
        this.REFERENCE = data.REFERENCE;
        this.CAT_USAGE = data.CAT_USAGE;
        this.DENSITE = data.DENSITE;
        this.MARGES = data.MARGES;
        this.HAUTEUR_METRES = data.HAUTEUR_METRES;
        this.HAUTEUR_ETAGES = data.HAUTEUR_ETAGES;
        this.AQUEDUC = data.AQUEDUC;
        this.EGOUTS = data.EGOUTS;
        this.ELECTRICITE = data.ELECTRICITE;
        this.NATURE_VOIES = data.NATURE_VOIES;
        this.CAPACITE_RESEAU = data.CAPACITE_RESEAU;
        this.ADAPTABILITE = data.ADAPTABILITE;
        this.TRANSP_COMMUN = data.TRANSP_COMMUN;
        this.UTILISATION_HOMOG = data.UTILISATION_HOMOG;
        this.ELEM_POSITIF = data.ELEM_POSITIF;
        this.ELEM_NEGATIF = data.ELEM_NEGATIF;
        this.CARAC_SPECIF_NOTES = data.CARAC_SPECIF_NOTES;
        this.NORMES_REGLEM_NOTES = data.NORMES_REGLEM_NOTES;
        this.ACCES_NOTES = data.ACCES_NOTES;
        this.MILIEU_BATIM_NOTES = data.MILIEU_BATIM_NOTES;
        this.CONCLUSION = data.CONCLUSION;


    }
}