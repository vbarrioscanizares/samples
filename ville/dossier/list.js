"use strict";
var List = (function () {
    function List() {
    }
    List.prototype.toJson = function () { return JSON.stringify(this); };
    List.prototype.populate = function (data) { this.items = data; };
    List.prototype.getValue = function (id) {
        var result = "";
        for (var _i = 0, _a = this.items; _i < _a.length; _i++) {
            var item = _a[_i];
            if (item.ID == id)
                result = item.VALUE;
        }
        return result;
    };
    return List;
}());
exports.List = List;
//# sourceMappingURL=list.js.map