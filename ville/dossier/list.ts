export class List {
        items: any;

  constructor() {}

  toJson():string{return JSON.stringify(this);}

   populate(data : any){this.items = data;}

   getValue(id:any):string {
            var result = "";

         for (let item of this.items) {
            if (item.ID == id)   result = item.VALUE; 
            }

         return result;   
   }   
        
      
   
}