"use strict";
var Localisation = (function () {
    function Localisation(data) {
        if (data === undefined || data === null) {
        }
        else {
            this.populate(data);
        }
    }
    Localisation.prototype.toJson = function () { return JSON.stringify(this); };
    Localisation.prototype.populate = function (data) {
        this.LOCALISATION_ID = data.LOCALISATION_ID;
        this.IMM_IDE = data.IMM_IDE;
        this.NO_VOIE = data.NO_VOIE;
        this.CODE_VILLE = data.CODE_VILLE;
        this.NO_ALPHA_NOM = data.NO_ALPHA_NOM;
        this.STATUT = data.STATUT;
        this.NO_NOM_VOIE = data.NO_NOM_VOIE;
        this.NOM_VOIE_LONG = data.NOM_VOIE_LONG;
        this.NOM_MAJUS_SPECIF_COMPRIME = data.NOM_MAJUS_SPECIF_COMPRIME;
        this.NO_GEN = data.NO_GEN;
        this.ORIENT_NO_CIVIQ = data.ORIENT_NO_CIVIQ;
        this.NOM_GEN = data.NOM_GEN;
    }; //populate
    return Localisation;
}());
exports.Localisation = Localisation; //class
//# sourceMappingURL=localisation.js.map