"use strict";
var QualificationTerrain = (function () {
    function QualificationTerrain() {
        this.EGOUT = 0;
        this.AQUEDUC = 0;
        this.GAZ = 0;
        this.ELECTRICITEAE = 0;
        this.ELECTRICITEST = 0;
        this.RUEASPHALTEE = 0;
        this.TROTTOIR = 0;
        this.SERVITUDE = 0;
        this.PROXSERVICES = 0;
        this.CONTAMINATION = 0;
        this.DESC_EGOUT = "Non";
        this.DESC_AQUEDUC = "Non";
        this.DESC_GAZ = "Non";
        this.DESC_ELECTRICITEAE = "Non";
        this.DESC_ELECTRICITEST = "Non";
        this.DESC_RUEASPHALTEE = "Non";
        this.DESC_TROTTOIR = "Non";
        this.DESC_SERVITUDE = "Non";
        this.DESC_PROXSERVICES = "Non";
        this.DESC_CONTAMINATION = "Non";
        this.DESC_TOPOGRAPHIE = "";
        this.DESC_SECTEUR = "";
        this.DESC_PLANDACTION = "";
        this.DESC_DEVELOPPABLE = "";
    }
    QualificationTerrain.prototype.toJson = function () { return JSON.stringify(this); };
    QualificationTerrain.prototype.populate = function (data) {
        this.QT_ID = data.QT_ID;
        this.IMM_IDE = data.IMM_IDE;
        this.EGOUT = data.EGOUT;
        this.AQUEDUC = data.AQUEDUC;
        this.GAZ = data.GAZ;
        this.ELECTRICITEAE = data.ELECTRICITEAE;
        this.ELECTRICITEST = data.ELECTRICITEST;
        this.RUEASPHALTEE = data.RUEASPHALTEE;
        this.TROTTOIR = data.TROTTOIR;
        this.SERVITUDE = data.SERVITUDE;
        this.PROXSERVICES = data.PROXSERVICES;
        this.CONTAMINATION = data.CONTAMINATION;
        this.TOPOGRAPHIE = data.TOPOGRAPHIE;
        this.QTVOCATION = data.QTVOCATION;
        this.DESCDESECTEURTEXT = data.DESCDESECTEURTEXT;
        this.DESCDESECTEUR = data.DESCDESECTEUR;
        this.COMMENTAIRE = data.COMMENTAIRE;
        this.PLANDACTION = data.PLANDACTION;
        this.COMMENT_CONT = data.COMMENT_CONT;
        this.PROGRAM_SUBV = data.PROGRAM_SUBV;
        this.COUT_REHABIL = data.COUT_REHABIL;
        this.COUT_CARACT = data.COUT_CARACT;
        this.NIVEAU_CONT = data.NIVEAU_CONT;
        this.CONTAMIN_POT = data.CONTAMIN_POT;
        this.DEVELOPPABLE = data.DEVELOPPABLE;
        if (this.EGOUT == -1)
            this.DESC_EGOUT = "Oui";
        if (this.AQUEDUC == -1)
            this.DESC_AQUEDUC = "Oui";
        if (this.GAZ == -1)
            this.DESC_GAZ = "Oui";
        if (this.ELECTRICITEAE == -1)
            this.DESC_ELECTRICITEAE = "Oui";
        if (this.ELECTRICITEST == -1)
            this.DESC_ELECTRICITEST = "Oui";
        if (this.RUEASPHALTEE == -1)
            this.DESC_RUEASPHALTEE = "Oui";
        if (this.TROTTOIR == -1)
            this.DESC_TROTTOIR = "Oui";
        if (this.SERVITUDE == -1)
            this.DESC_SERVITUDE = "Oui";
        if (this.PROXSERVICES == -1)
            this.DESC_PROXSERVICES = "Oui";
        if (this.CONTAMINATION == -1)
            this.DESC_CONTAMINATION = "Oui";
    };
    return QualificationTerrain;
}());
exports.QualificationTerrain = QualificationTerrain;
//# sourceMappingURL=qualificationterrain.js.map