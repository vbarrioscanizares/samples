export class QualificationTerrain {
       
        public QT_ID:string;
        public IMM_IDE:string;
        public EGOUT:number = 0;
        public AQUEDUC:number = 0;
        public GAZ:number = 0;
        public ELECTRICITEAE:number = 0;
        public ELECTRICITEST:number = 0;
        public RUEASPHALTEE:number = 0;
        public TROTTOIR:number = 0;
        public SERVITUDE:number = 0;
        public PROXSERVICES:number = 0;
        public CONTAMINATION:number = 0;
        public TOPOGRAPHIE:number;
        public QTVOCATION:string;
        public DESCDESECTEURTEXT:string;
        public DESCDESECTEUR:number;
        public COMMENTAIRE:string;
        public PLANDACTION:number;
        public COMMENT_CONT:string;
        public PROGRAM_SUBV:string;
        public COUT_REHABIL:string;
        public COUT_CARACT:string;
        public NIVEAU_CONT:string;
        public CONTAMIN_POT:string;
        public DEVELOPPABLE:number;


        public DESC_EGOUT:string          = "Non";
        public DESC_AQUEDUC:string        = "Non";
        public DESC_GAZ:string            = "Non";
        public DESC_ELECTRICITEAE:string  = "Non";
        public DESC_ELECTRICITEST:string  = "Non";
        public DESC_RUEASPHALTEE:string   = "Non";
        public DESC_TROTTOIR:string       = "Non";
        public DESC_SERVITUDE:string      = "Non";
        public DESC_PROXSERVICES:string   = "Non";
        public DESC_CONTAMINATION:string  = "Non";

        public DESC_TOPOGRAPHIE:string    = "";
        public DESC_SECTEUR:string        = "";
        public DESC_PLANDACTION:string    = "";
        public DESC_DEVELOPPABLE:string   = "";



  constructor() {}

  toJson():string{return JSON.stringify(this);}

   populate(data : any){

        this.QT_ID               = data.QT_ID;
        this.IMM_IDE             = data.IMM_IDE;
        this.EGOUT               = data.EGOUT;
        this.AQUEDUC             = data.AQUEDUC;
        this.GAZ                 = data.GAZ;
        this.ELECTRICITEAE       = data.ELECTRICITEAE;
        this.ELECTRICITEST       = data.ELECTRICITEST;
        this.RUEASPHALTEE        = data.RUEASPHALTEE;
        this.TROTTOIR            = data.TROTTOIR;
        this.SERVITUDE           = data.SERVITUDE;
        this.PROXSERVICES        = data.PROXSERVICES;
        this.CONTAMINATION       = data.CONTAMINATION;
        this.TOPOGRAPHIE         = data.TOPOGRAPHIE;
        this.QTVOCATION          = data.QTVOCATION;
        this.DESCDESECTEURTEXT   = data.DESCDESECTEURTEXT;
        this.DESCDESECTEUR       = data.DESCDESECTEUR;
        this.COMMENTAIRE         = data.COMMENTAIRE;
        this.PLANDACTION         = data.PLANDACTION;
        this.COMMENT_CONT        = data.COMMENT_CONT;
        this.PROGRAM_SUBV        = data.PROGRAM_SUBV;
        this.COUT_REHABIL        = data.COUT_REHABIL;
        this.COUT_CARACT         = data.COUT_CARACT;
        this.NIVEAU_CONT         = data.NIVEAU_CONT;
        this.CONTAMIN_POT        = data.CONTAMIN_POT;
        this.DEVELOPPABLE        = data.DEVELOPPABLE;

        if (this.EGOUT == -1)                 this.DESC_EGOUT = "Oui";
        if (this.AQUEDUC == -1)               this.DESC_AQUEDUC = "Oui";
        if (this.GAZ == -1)                   this.DESC_GAZ = "Oui";
        if (this.ELECTRICITEAE == -1)         this.DESC_ELECTRICITEAE = "Oui";
        if (this.ELECTRICITEST == -1)         this.DESC_ELECTRICITEST = "Oui";
        if (this.RUEASPHALTEE == -1)          this.DESC_RUEASPHALTEE = "Oui";
        if (this.TROTTOIR == -1)              this.DESC_TROTTOIR = "Oui";
        if (this.SERVITUDE == -1)             this.DESC_SERVITUDE = "Oui";
        if (this.PROXSERVICES == -1)          this.DESC_PROXSERVICES = "Oui";
        if (this.CONTAMINATION == -1)         this.DESC_CONTAMINATION = "Oui";



        
      
   }
}