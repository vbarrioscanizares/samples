"use strict";
var Rire = (function () {
    function Rire(data) {
        if (data === undefined || data === null) {
        }
        else {
            this.populate(data);
        }
    }
    Rire.prototype.toJson = function () { return JSON.stringify(this); };
    Rire.prototype.populate = function (data) {
        this.CODE_VILLE = data.CODE_VILLE;
        this.NO_ALPHA_NOM = data.NO_ALPHA_NOM;
        this.STATUT = data.STATUT;
        this.NO_NOM_VOIE = data.NO_NOM_VOIE;
        this.NOM_VOIE_LONG = data.NOM_VOIE_LONG;
        this.NOM_MAJUS_SPECIF_COMPRIME = data.NOM_MAJUS_SPECIF_COMPRIME;
        this.NO_GEN = data.NO_GEN;
        this.ORIENT_NO_CIVIQ = data.ORIENT_NO_CIVIQ;
        this.NOM_GEN = data.NOM_GEN;
        this.NO_VOIE = data.NO_VOIE;
    }; //populate
    return Rire;
}());
exports.Rire = Rire; //class
//# sourceMappingURL=rire.js.map