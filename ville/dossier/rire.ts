export class Rire {

    public CODE_VILLE:string;
    public NO_ALPHA_NOM:string;
    public STATUT:string;
    public NO_NOM_VOIE:string;
    public NOM_VOIE_LONG:string;
    public NOM_MAJUS_SPECIF_COMPRIME:string;
    public NO_GEN:string;
    public ORIENT_NO_CIVIQ:string;
    public NOM_GEN:string ;
    public NO_VOIE:string;


  constructor(); 
  constructor(data:any);     
  constructor(data?:any) {
   
        if (data === undefined || data === null) {
        }else{
            this.populate(data);
        }
        
  }


toJson():string{return JSON.stringify(this);}

populate(data : any){

    this.CODE_VILLE             = data.CODE_VILLE;
    this.NO_ALPHA_NOM           = data.NO_ALPHA_NOM;
    this.STATUT                 = data.STATUT;
    this.NO_NOM_VOIE            = data.NO_NOM_VOIE;
    this.NOM_VOIE_LONG          = data.NOM_VOIE_LONG;
    this.NOM_MAJUS_SPECIF_COMPRIME  = data.NOM_MAJUS_SPECIF_COMPRIME;
    this.NO_GEN                     = data.NO_GEN;
    this.ORIENT_NO_CIVIQ            = data.ORIENT_NO_CIVIQ;
    this.NOM_GEN                    = data.NOM_GEN;
    this.NO_VOIE                    = data.NO_VOIE;


}//populate
}//class