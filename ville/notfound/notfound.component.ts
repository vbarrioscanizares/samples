import { Component } from '@angular/core';

@Component({
    selector: '[notfound]',
    moduleId: module.id,
    templateUrl: 'notfound.html'
})
export class NotfoundComponent { }
