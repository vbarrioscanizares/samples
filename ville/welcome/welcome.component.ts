import { Component } from '@angular/core';

@Component({
    selector: '[welcome]',
    moduleId: module.id,
    templateUrl: 'welcome.html'
})
export class WelcomeComponent { }
